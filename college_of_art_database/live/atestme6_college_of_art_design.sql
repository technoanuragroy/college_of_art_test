-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 03, 2020 at 08:23 AM
-- Server version: 5.6.32-78.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atestme6_college_of_art_design`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admins`
--

CREATE TABLE `tbl_admins` (
  `id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1 = Super Admin, 2 = Admin',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_admins`
--

INSERT INTO `tbl_admins` (`id`, `type`, `name`, `username`, `password`, `email`, `phone`, `date_created`, `status`) VALUES
(1, 1, 'Surajit Das', 'developer1', '$2y$10$vSiwL0wZRfRsnGOorW/AR.4leFD9BivEbPrKFZDSZPnT2tphOERn2', 'surajit@ablion.in', '9851661169', '2017-05-05 00:00:00', 1),
(4, 1, 'Test Admin', 'developer2', '$2y$10$R1vpsfYExmuUHL9vnsjoK.MBhwMWfYmXtB7GXd1tz4ZYe7uchV5Me', 'rafi@ablion.in', '9851661168', '2019-12-16 17:51:13', 1),
(5, 2, 'Rafi', 'rafitest', '$2y$10$doX2KNdV92MIje79tWf3E.b/.SKeR78.sKObsF5uEBdc0p1/J13Be', 'rafi@ablion.in', '8653694046', '2019-12-20 16:07:11', 1),
(6, 1, 'Upasona', 'upasona', '$2y$10$nlomqFRMxuAfa1WI3xAFqeuWUXtIjFVAc1LzirX1Uj.wcTW4phPRa', 'upasona@ablion.in', '8653694046', '2019-12-20 16:07:58', 1),
(7, 2, 'Soumyadeep', 'soumya', '$2y$10$mJ/HppKSXiEhJQLObNAhL.gf8m/VztN/acbwUB9os6IXE/BdTiW7i', 'rafi@ablion.in', '8653694046', '2019-12-20 16:59:35', 1),
(8, 2, 'Biswajit', 'biswajit.ablion', '$2y$10$wNzX60hi3bNotZb1tykMseq2weZTGzxWTf6dOH8DXPKI0jTdH.FD2', 'biswajit@ablion.in', '7908170051', '2020-05-28 01:17:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `date_month` date NOT NULL,
  `present` decimal(10,1) NOT NULL,
  `absent` decimal(10,1) NOT NULL,
  `pl` decimal(10,1) NOT NULL,
  `cl` decimal(10,1) NOT NULL,
  `sl` decimal(10,1) NOT NULL,
  `opl` decimal(10,1) NOT NULL,
  `working_days` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_attendance`
--

INSERT INTO `tbl_attendance` (`id`, `emp_id`, `date_month`, `present`, `absent`, `pl`, `cl`, `sl`, `opl`, `working_days`, `note`, `created_at`, `updated_at`, `created_by`) VALUES
(7, 21, '2015-06-01', 22.0, 2.0, 0.0, 2.0, 1.0, 2.0, 30, '', '2015-11-04 13:23:16', '2015-11-04 13:23:16', 'Pratik Dutta'),
(2, 13, '2015-06-01', 24.0, 1.0, 1.0, 1.0, 0.0, 1.0, 30, '', '2015-11-04 13:02:43', '2015-11-04 13:02:43', 'Pratik Dutta'),
(3, 13, '2015-07-01', 20.0, 3.0, 2.0, 1.0, 1.0, 1.0, 30, '', '2015-07-23 07:30:39', '2015-07-14 06:39:19', 'Pratik Dutta'),
(4, 2, '2015-07-01', 18.0, 2.0, 2.0, 2.5, 2.0, 1.5, 30, '', '2015-08-06 07:01:52', '2015-08-06 07:01:52', 'Pratik Dutta'),
(10, 23, '2015-06-01', 20.0, 0.0, 0.0, 0.5, 0.0, 0.5, 30, '', '2015-08-12 05:13:28', '2015-08-12 05:13:28', 'Pratik Dutta'),
(11, 2, '2015-05-01', 22.0, 4.0, 0.0, 2.0, 0.0, 2.0, 30, '', '2015-07-23 07:30:55', '0000-00-00 00:00:00', 'Pratik Dutta'),
(12, 15, '2015-06-01', 23.0, 2.0, 1.0, 1.0, 0.0, 0.0, 30, 'dfgdf', '2015-07-23 06:24:29', '0000-00-00 00:00:00', 'Pratik Dutta'),
(13, 24, '2015-06-01', 23.0, 4.0, 0.0, 1.0, 0.0, 0.0, 30, '', '2015-07-23 07:31:00', '0000-00-00 00:00:00', 'Pratik Dutta'),
(14, 3, '2015-06-01', 18.0, 4.0, 2.0, 2.0, 0.0, 1.0, 30, '', '2015-07-23 07:31:04', '0000-00-00 00:00:00', 'Pratik Dutta'),
(15, 23, '2015-05-01', 12.0, 18.0, 0.0, 2.0, 2.0, 14.0, 30, '', '2015-07-23 06:56:39', '0000-00-00 00:00:00', 'Pratik Dutta'),
(16, 13, '2015-05-01', 12.0, 1.0, 1.0, 1.5, 1.5, 1.0, 30, 'fd', '2015-08-05 04:57:15', '2015-08-05 04:57:15', 'Pratik Dutta'),
(17, 13, '2015-08-01', 28.5, 2.5, 1.0, 1.5, 1.5, 1.5, 30, 'f', '2015-09-07 04:56:02', '0000-00-00 00:00:00', 'Pratik Dutta'),
(18, 17, '2016-11-01', 20.0, 10.0, 0.0, 5.0, 2.0, 0.0, 30, '', '2017-01-06 10:58:33', '0000-00-00 00:00:00', 'Pratik Dutta'),
(19, 2, '2017-02-01', 24.0, 2.0, 0.0, 9.0, 1.0, 0.0, 30, '', '2017-05-08 05:24:41', '2017-05-08 05:24:41', 'Pratik Dutta'),
(20, 26, '2017-09-01', 29.0, 1.0, 0.0, 1.0, 0.0, 0.0, 30, '', '2018-01-10 07:42:40', '0000-00-00 00:00:00', 'Pratik Dutta'),
(21, 26, '2017-10-01', 29.0, 1.0, 0.0, 1.0, 0.0, 0.0, 30, '', '2018-01-10 07:44:25', '0000-00-00 00:00:00', 'Pratik Dutta'),
(22, 26, '2018-01-01', 30.0, 0.0, 0.0, 0.0, 0.0, 0.0, 30, '', '2018-01-10 10:59:06', '0000-00-00 00:00:00', 'Pratik Dutta'),
(23, 13, '2017-12-01', 20.0, 3.0, 0.0, 3.0, 0.0, 0.0, 30, '', '2018-01-10 13:20:41', '0000-00-00 00:00:00', 'Pratik Dutta'),
(24, 3, '2018-03-01', 30.0, 0.0, 0.0, 0.0, 0.0, 0.0, 30, '', '2019-04-25 12:41:56', '0000-00-00 00:00:00', 'Pratik Dutta'),
(25, 3, '2019-05-01', 30.0, 0.0, 0.0, 0.0, 0.0, 0.0, 30, '', '2019-05-03 05:31:13', '0000-00-00 00:00:00', 'Surajit Das'),
(28, 1, '2020-03-01', 20.0, 0.0, 0.0, 0.0, 0.0, 0.0, 30, 'Present', '2020-03-18 07:46:28', '0000-00-00 00:00:00', NULL),
(27, 0, '2020-03-01', 25.0, 0.0, 0.0, 0.0, 0.0, 0.0, 30, 'Present', '2020-03-17 09:40:26', '0000-00-00 00:00:00', NULL),
(29, 1, '2020-02-01', 22.0, 2.0, 0.0, 1.0, 1.0, 0.0, 30, '', '2020-03-24 10:49:12', '0000-00-00 00:00:00', NULL),
(30, 1, '2020-06-01', 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 30, '', '2020-06-16 05:51:39', '0000-00-00 00:00:00', NULL),
(31, 5, '2020-06-01', 20.0, 0.0, 0.0, 0.0, 0.0, 0.0, 30, 'Note', '2020-06-17 05:17:59', '0000-00-00 00:00:00', NULL),
(32, 1, '2020-05-01', 23.0, 2.0, 0.0, 1.0, 1.0, 0.0, 25, '', '2020-06-17 13:05:45', '2020-06-17 13:05:45', 'Surajit Das');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_pages`
--

CREATE TABLE `tbl_cms_pages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `url_alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `page_heading` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `page_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_cms_pages`
--

INSERT INTO `tbl_cms_pages` (`id`, `name`, `url_alias`, `page_heading`, `page_content`, `page_title`, `meta_keywords`, `meta_description`) VALUES
(2, 'About Us Page', 'about-us-page', 'About Us', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'About Us', 'Web Development Company, Best Web Development Company', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>'),
(3, 'Contact Up Page', 'contact-up-page', 'Contact Us', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 'Contact us', 'Contact, Address', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_college_payment_settings`
--

CREATE TABLE `tbl_college_payment_settings` (
  `id` int(11) NOT NULL,
  `payment_enabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Enabled, 0 = DIsabled',
  `online_payment_enabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Enabled, 0 = DIsabled',
  `payment_gateway_environment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Live, 0 = Test'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_college_payment_settings`
--

INSERT INTO `tbl_college_payment_settings` (`id`, `payment_enabled`, `online_payment_enabled`, `payment_gateway_environment`) VALUES
(1, 1, 1, 0),
(2, 1, 0, 0),
(3, 1, 1, 0),
(4, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_common_login`
--

CREATE TABLE `tbl_common_login` (
  `id` int(11) NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = User',
  `last_login_date` datetime NOT NULL,
  `last_login_ip` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `register_ip` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Active, 0 = Inactive',
  `email_verification` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Verified, 0 = Not verified',
  `otp_verification` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Verified, 0 = Not verified'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_common_login`
--

INSERT INTO `tbl_common_login` (`id`, `username`, `password`, `type`, `last_login_date`, `last_login_ip`, `register_ip`, `status`, `email_verification`, `otp_verification`) VALUES
(1, '9474728606', '$2y$10$99FQDOh3mz5JaQKjOhkimegsj7c5wB8wQ7sPj72jtLhesvzlgCCIK', 1, '2019-12-17 10:55:59', '192.168.1.111', '192.168.1.111', 1, 0, 0),
(3, '9851661168', '$2y$10$/FcfNxSUd8UbAC7AdhHrmO9v.W8oUEpFp3gc6QMzIVoLqzB0eV7FC', 1, '2019-12-20 17:43:29', '192.168.1.107', '192.168.1.111', 1, 1, 1),
(4, '8653694046', '$2y$10$w8nNYFilxOG70ZqPZQaDXuxbY400XRSINW/YPtFHPg.OjjfDiRALm', 1, '2019-12-20 16:20:28', '192.168.1.133', '192.168.1.133', 1, 1, 0),
(5, '7384634012', '$2y$10$0Q3nB2O8IP4bWkc66oRVD.p7SH36OJ7QzPUWkRC2rDBN/Ye9UeQUy', 1, '2019-12-20 16:24:32', '192.168.1.133', '192.168.1.133', 1, 0, 1),
(6, 'upasona@ablion.in', '$2y$10$R1ph2/0OJ8qfG/XQgJLWP.zwQNAcaLu83UrAF5SQzJErcDB8uAE4u', 1, '2019-12-20 16:40:40', '192.168.1.133', '192.168.1.133', 1, 1, 0),
(7, 'rafi@ablion.in', '$2y$10$RxVm0f9/Hfr85tN229/Ff.RmDP4pY5JPliizls02bnRKGidBSZzKG', 1, '2019-12-20 16:42:55', '192.168.1.133', '192.168.1.133', 1, 1, 0),
(8, 'soumyadeep@ablion.in', '$2y$10$t8OUedKJrTV..XBZiqy7fOP7YMHOyZZ4cQhQvLFXLllnxSC1XJwam', 1, '2019-12-20 16:47:48', '192.168.1.133', '192.168.1.133', 1, 0, 1),
(11, 'demo@gmail.com', '$2y$10$ILFKIfk11WRhPVYoD8JfG.4RLrGR2WBScTmJM8ByQAvp3FinSl6iK', 1, '2020-06-30 07:14:38', '157.43.235.18', '47.11.96.85', 1, 0, 0),
(12, 'biswajit@ablion.in', '$2y$10$ZN0YUErkDX3oiQh1DVwW6eDsetjXWkR7VaJ399LQjd1VKbtSivaFm', 1, '2020-07-03 06:32:56', '47.11.181.182', '47.11.64.30', 1, 0, 0),
(16, 'Samantak', '$2y$10$mkGjNazrRwSA5s38lORlg.F4dVq9XZScylywEVwMYYw9Mh1v3I8mi', 1, '2020-07-03 07:17:31', '47.11.143.146', '47.11.228.56', 1, 0, 0),
(13, 'anurag@ablion.in', '$2y$10$QWiBQi.U27DseXPJZsRh.uP/9l2lDs8W74WEdmsr1hqYvno7tDuCe', 1, '2020-06-25 02:24:59', '157.43.151.173', '157.43.202.143', 1, 0, 0),
(14, 'rafi@ablion.in', '$2y$10$FXm2B78PzRykyBBORsxcT.7bpNjIqFydmdebP96E7kVjIpFyORX.6', 1, '0000-00-00 00:00:00', '', '202.142.99.67', 1, 0, 0),
(15, 'kanan', '$2y$10$q0ifJrZn8XUOhfbl9jKvVu2ZnfDHOsQANRfXFAUQejXVV35FN/QcW', 1, '0000-00-00 00:00:00', '', '157.43.178.45', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_courses`
--

CREATE TABLE `tbl_courses` (
  `id` int(11) NOT NULL,
  `course_name` varchar(155) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active,0=inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_courses`
--

INSERT INTO `tbl_courses` (`id`, `course_name`, `status`) VALUES
(2, 'Bachelor of Fine Art (BFA) [4 Years / 8 SEM]', 1),
(3, 'Master of Fine Art (MFA) [2 Years / 4 SEM]', 1),
(4, 'Diploma In Visual Arts (Craft & Design) [2 Years]', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_edu_qualification`
--

CREATE TABLE `tbl_edu_qualification` (
  `id` int(11) NOT NULL,
  `student_from_id` int(11) NOT NULL,
  `exam_passed` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `year_of_pass` varchar(255) NOT NULL,
  `board_coun_uni` varchar(255) NOT NULL,
  `div_obt` varchar(255) NOT NULL,
  `document_url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_edu_qualification`
--

INSERT INTO `tbl_edu_qualification` (`id`, `student_from_id`, `exam_passed`, `subject`, `year_of_pass`, `board_coun_uni`, `div_obt`, `document_url`) VALUES
(1, 1, 'HS', 'Englisg', '2011', 'BU', '1st', ''),
(2, 1, 'madhyamik', 'bengali, english, mathematics, life science, physical science, history, geography', '2006', 'wbbse', 'i', ''),
(3, 1, 'higher secondary', 'bengali, english, mathematics, physics, chemistry, biology', '2008', 'wbchse', 'i', ''),
(8, 5, 'HS', 'Englisg', '2011', 'BU', '1st', ''),
(9, 5, 'MadhyaMik', 'Bengali', '2009', 'BU', '1st', ''),
(10, 6, 'madhyamik', 'Bengali, English, mathematics, life science, physical science, history, geography', '2016', 'wbbse', 'i', ''),
(11, 6, 'hs', 'ben, eng. math', '2018', 'wbchse', 'i', ''),
(12, 7, '10th', 'jkhhkh, uiyuiy, tytt,', '2012', 'icse', 'i', ''),
(13, 7, '12th', 'Wyuyiuy, iuiuoiu, oiuiou. ters', '2014', 'cbse', 'ii', ''),
(14, 8, 'madhyamik', 'ben, eng, geo', '2017', 'wbbse', 'I', '14_architecture-balcony-building-534182-768x432.jpg'),
(15, 9, 'Hs', 'English', '2011', 'BU', '1st', '15_sunflower-flowers-bright-yellow-46216.jpeg'),
(16, 10, 'madhyamik', 'beng, eng, math, hist', '2015', 'wbbse', 'i', '16_Newsletter_Aug14.pdf'),
(17, 11, 'Hs', 'English', '2011', 'BU', '1st', '17_unnamed.jpg'),
(18, 12, 'Hs', 'English', '2011', 'BU', '1st', '18_unnamed.jpg'),
(19, 13, 'Hs', 'English', '2011', 'BU', '1st', '19_dummy.pdf'),
(20, 14, 'Hs', 'English', '2011', 'BU', '1st', '20_dummy.pdf'),
(21, 15, 'madhyamik', 'beng, eng, math', '2016', 'wbbse', 'i', '21_unnamed.jpg'),
(22, 15, 'hs', 'jhhklb. uyguiyuy', '2018', 'wbchse', '1', '22_dummy.pdf'),
(23, 16, 'Hs', 'English', '2011', 'BU', '1st', ''),
(24, 19, 'lkjikj', 'kljkj', '2019a', 'iuhihh', 'i', '24_Top_10_Mutual_Funds_for_2018_-_Rediff.pdf'),
(25, 19, 'hs', 'kjiiopi, iuiu', 'oiuiouoiu', '287686', '1', '25_Order_Form.pdf'),
(26, 20, 'Madhyamik', 'Beng, Eng, Math, Hist, Geo, Lsc, Psc', '2016', 'WBBSE', 'I', '26_Scanned_doc_jpeg.jpg'),
(27, 20, 'HS', 'Beng, Eng, Math, Phy, Chem', '2018', 'WBCHSE', 'II', '27_Scanned_doc.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_emp_payslip`
--

CREATE TABLE `tbl_emp_payslip` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `payslip_date` date NOT NULL,
  `attendance_id` int(11) NOT NULL,
  `basic_pay` decimal(10,2) NOT NULL,
  `grade_pay` decimal(10,2) NOT NULL,
  `other_allow` decimal(10,2) NOT NULL,
  `other_allow_title` varchar(100) DEFAULT NULL,
  `medical_allow` decimal(10,2) NOT NULL,
  `emp_contribute_pf` decimal(10,2) NOT NULL,
  `salary_total` decimal(10,2) NOT NULL,
  `income_tax` decimal(10,2) DEFAULT NULL,
  `pf_deduction` decimal(10,2) NOT NULL,
  `fest_advance` decimal(10,2) NOT NULL,
  `other_deduction` double(10,2) DEFAULT NULL,
  `other_deduction_title` varchar(100) DEFAULT NULL,
  `total_deduction` decimal(10,2) NOT NULL,
  `ptax` decimal(10,2) NOT NULL,
  `net_pay` decimal(10,2) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_details` text,
  `note` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_emp_payslip`
--

INSERT INTO `tbl_emp_payslip` (`id`, `emp_id`, `payslip_date`, `attendance_id`, `basic_pay`, `grade_pay`, `other_allow`, `other_allow_title`, `medical_allow`, `emp_contribute_pf`, `salary_total`, `income_tax`, `pf_deduction`, `fest_advance`, `other_deduction`, `other_deduction_title`, `total_deduction`, `ptax`, `net_pay`, `payment_date`, `payment_details`, `note`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 1, '2020-05-01', 0, 22160.00, 3000.00, 100.00, 'FEST', 2200.00, 3016.00, 30476.00, 0.00, 5675.00, 500.00, 100.00, 'FEST', 6425.00, 150.00, 24051.00, '2020-06-23', 'Note', 'Note', '2020-06-23 12:47:38', NULL, 'Surajit Das');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam`
--

CREATE TABLE `tbl_exam` (
  `id` int(11) NOT NULL,
  `term_id` int(11) NOT NULL,
  `exam_name` varchar(255) NOT NULL,
  `exam_display_name` varchar(255) NOT NULL,
  `exam_start_date` date NOT NULL,
  `exam_end_date` date NOT NULL,
  `publish_date` int(11) NOT NULL,
  `publish_status` tinyint(4) NOT NULL COMMENT '0=not published,1=published'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_exam`
--

INSERT INTO `tbl_exam` (`id`, `term_id`, `exam_name`, `exam_display_name`, `exam_start_date`, `exam_end_date`, `publish_date`, `publish_status`) VALUES
(1, 1, 'Unit 2', 'Unit Test 2', '2020-06-01', '2020-06-10', 1592058600, 0),
(2, 1, 'Unit 3', 'Unit Test 3', '2020-06-01', '2020-06-10', 1592058600, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam_scores`
--

CREATE TABLE `tbl_exam_scores` (
  `id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `full_marks` int(11) NOT NULL,
  `pass_marks` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_exam_scores`
--

INSERT INTO `tbl_exam_scores` (`id`, `exam_id`, `subject_id`, `full_marks`, `pass_marks`, `sort_order`) VALUES
(1, 1, 2, 100, 45, 0),
(2, 1, 3, 100, 40, 0),
(3, 2, 2, 100, 45, 0),
(4, 2, 3, 100, 40, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fees_payment`
--

CREATE TABLE `tbl_fees_payment` (
  `id` int(11) NOT NULL,
  `fees_id` int(11) NOT NULL,
  `fees_breakup_id` int(11) NOT NULL,
  `manual_fees_breakup_id` int(11) NOT NULL,
  `fees` longtext NOT NULL,
  `class_info` text NOT NULL,
  `student_id` int(11) NOT NULL,
  `fees_amount` float(20,2) NOT NULL,
  `late_fine` float(20,2) NOT NULL,
  `service_charges` float(20,2) NOT NULL,
  `total_amount` float(20,2) NOT NULL,
  `paid_amount` float(20,2) NOT NULL,
  `payment_datetime` datetime NOT NULL,
  `payment_status` tinyint(1) NOT NULL COMMENT '0 = Pending, 1 = Paid',
  `payment_ref_id` varchar(100) NOT NULL,
  `payment_gateway_id` int(11) NOT NULL,
  `created_by` tinyint(1) NOT NULL COMMENT '1 = School, 3 = Parent'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fees_structure`
--

CREATE TABLE `tbl_fees_structure` (
  `id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `cycle_type` int(11) NOT NULL COMMENT '1 = Monthly, 2 = Quarterly, 3 = Half Yearly, 4 = Yearly',
  `general_comment` longtext NOT NULL,
  `late_payment_setting` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = Disabled, 1 = Enabled',
  `late_payment_day_limit` int(11) NOT NULL,
  `late_payment_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Fixed, 2 = Percentage',
  `late_payment_amount` float(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_fees_structure`
--

INSERT INTO `tbl_fees_structure` (`id`, `semester_id`, `cycle_type`, `general_comment`, `late_payment_setting`, `late_payment_day_limit`, `late_payment_type`, `late_payment_amount`) VALUES
(3, 2, 2, 'Test', 0, 0, 1, 0.00),
(8, 3, 2, '2020-21 Session', 0, 0, 1, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fees_structure_breakups`
--

CREATE TABLE `tbl_fees_structure_breakups` (
  `id` int(11) NOT NULL,
  `fees_id` int(11) NOT NULL,
  `month` date NOT NULL,
  `breakup_label` varchar(255) NOT NULL,
  `fees` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_fees_structure_breakups`
--

INSERT INTO `tbl_fees_structure_breakups` (`id`, `fees_id`, `month`, `breakup_label`, `fees`) VALUES
(1, 8, '2020-04-01', 'Fees For April, 2020 -> June, 2020', '[{\"label\":\"Transport Fees\",\"amount\":\"100\",\"option\":\"2\"}]'),
(2, 8, '2020-07-01', 'Fees For July, 2020 -> September, 2020', '[{\"label\":\"Transport Fees\",\"amount\":\"100\",\"option\":\"2\"}]'),
(3, 8, '2020-10-01', 'Fees For October, 2020 -> December, 2020', '[{\"label\":\"Transport Fees\",\"amount\":\"100\",\"option\":\"2\"}]'),
(4, 8, '2021-01-01', 'Fees For January, 2021 -> March, 2021', '[{\"label\":\"Transport Fees\",\"amount\":\"100\",\"option\":\"2\"}]'),
(5, 3, '2020-04-01', 'Fees For April, 2020 -> June, 2020', '[{\"label\":\"Transport Fees\",\"amount\":\"3000\",\"option\":\"1\"},{\"label\":\"Tuition Fees\",\"amount\":\"9000\",\"option\":\"2\"},{\"label\":\"Admission Fees\",\"amount\":\"25000\",\"option\":\"2\"},{\"label\":\"Development Fees\",\"amount\":\"100000\",\"option\":\"2\"}]'),
(6, 3, '2020-07-01', 'Fees For July, 2020 -> September, 2020', '[{\"label\":\"Transport Fees\",\"amount\":\"3000\",\"option\":\"1\"},{\"label\":\"Tuition Fees\",\"amount\":\"9000\",\"option\":\"2\"}]'),
(7, 3, '2020-10-01', 'Fees For October, 2020 -> December, 2020', '[{\"label\":\"Transport Fees\",\"amount\":\"3000\",\"option\":\"1\"},{\"label\":\"Tuition Fees\",\"amount\":\"9000\",\"option\":\"2\"}]'),
(8, 3, '2021-01-01', 'Fees For January, 2021 -> March, 2021', '[{\"label\":\"Transport Fees\",\"amount\":\"3000\",\"option\":\"1\"},{\"label\":\"Tuition Fees\",\"amount\":\"9000\",\"option\":\"2\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fees_structure_breakups_manual`
--

CREATE TABLE `tbl_fees_structure_breakups_manual` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `breakup_id` int(11) NOT NULL,
  `fees` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery_albums`
--

CREATE TABLE `tbl_gallery_albums` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_album` int(11) NOT NULL,
  `cover_image_url` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_gallery_albums`
--

INSERT INTO `tbl_gallery_albums` (`id`, `name`, `details`, `parent_album`, `cover_image_url`) VALUES
(1, 'Album One', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 0, '1_lord-shiva-bholenath-hd-wallpaper.jpg'),
(2, 'Album Two', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, '2_screen-2.jpg'),
(3, 'Album Three', 'Test Album', 2, '3_rooster-Rhode-Island-Red-roosters-chicken-domestication.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery_files`
--

CREATE TABLE `tbl_gallery_files` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `album_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Image, 2 = Video(Youtube Url), 3 = Video File',
  `file_url` text COLLATE utf8_unicode_ci NOT NULL,
  `preview_image_url` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'For Video Files'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_gallery_files`
--

INSERT INTO `tbl_gallery_files` (`id`, `title`, `details`, `album_id`, `type`, `file_url`, `preview_image_url`) VALUES
(1, 'Video File', 'Video File', 1, 3, '1_1581594644_SampleVideo_1280x720_5mb.mp4', '1_preview_1538565554953.jpg'),
(2, 'Video File', 'File Details', 1, 3, '2_1581594754_SampleVideo_1280x720_5mb.mp4', '2_preview_1538565554953.jpg'),
(3, 'adawd', 'awdawd', 0, 3, '3_1581594978_SampleVideo_1280x720_5mb.mp4', '3_preview_red-tshirt.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gateway_settings`
--

CREATE TABLE `tbl_gateway_settings` (
  `id` int(11) NOT NULL,
  `gateway_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gateway_label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gateway_config` longtext COLLATE utf8_unicode_ci NOT NULL,
  `gateway_settings` tinyint(1) NOT NULL COMMENT '1 = Enabled, 0 = DIsabled'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_gateway_settings`
--

INSERT INTO `tbl_gateway_settings` (`id`, `gateway_key`, `gateway_label`, `gateway_config`, `gateway_settings`) VALUES
(1, 'email', 'AWS Email Config', '{\"host\":\"ssl:\\/\\/email-smtp.eu-west-1.amazonaws.com\",\"port\":\"465\",\"username\":\"AKIAJHHLXXTUJTXXQVIA\",\"password\":\"AgqADeE+U89sZ1o42mv7j\\/aRZYnR6+5pE6Y4cN78lqNf\"}', 1),
(2, 'sms', 'My SMS Shop', '{\"sms_url\":\"http:\\/\\/mysmsshop.in\\/V2\\/http-api.php?\",\"apikey\":\"ow3PwR2dtGdWheYi\",\"senderid\":\"BIDALY\",\"format\":\"json\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notice`
--

CREATE TABLE `tbl_notice` (
  `id` int(11) NOT NULL,
  `semester_id` varchar(50) NOT NULL COMMENT 'only if type = 3',
  `notice_heading` varchar(512) NOT NULL,
  `issue_date` date NOT NULL,
  `notice_text` longtext NOT NULL,
  `publish_date` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active,2=inactive',
  `document_url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notice`
--

INSERT INTO `tbl_notice` (`id`, `semester_id`, `notice_heading`, `issue_date`, `notice_text`, `publish_date`, `status`, `document_url`) VALUES
(1, '', 'Notification for Admission 2020', '2020-06-04', '<p>Test notification for admission in the 2020 session<br />Please visit the website for details.</p>', 0, 1, '1_Application_Form.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_paymentgateway`
--

CREATE TABLE `tbl_paymentgateway` (
  `id` int(11) NOT NULL,
  `payment_gateway_name` varchar(255) NOT NULL,
  `label_for_ui` varchar(255) NOT NULL,
  `default_gateway` tinyint(1) NOT NULL COMMENT '1 = This is the default gateway',
  `display_tip` text CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = Active, 0 = Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_paymentgateway`
--

INSERT INTO `tbl_paymentgateway` (`id`, `payment_gateway_name`, `label_for_ui`, `default_gateway`, `display_tip`, `status`) VALUES
(1, 'Razorpay', 'Online', 1, '', 1),
(2, 'Offline', 'Offline', 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_principal_record`
--

CREATE TABLE `tbl_principal_record` (
  `id` int(11) NOT NULL,
  `principal_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_principal_record`
--

INSERT INTO `tbl_principal_record` (`id`, `principal_name`, `status`) VALUES
(1, 'Test', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_score`
--

CREATE TABLE `tbl_score` (
  `id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `score_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `score` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_section`
--

CREATE TABLE `tbl_section` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `section_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_section`
--

INSERT INTO `tbl_section` (`id`, `class_id`, `section_name`, `status`) VALUES
(3, 2, 'A', 1),
(4, 2, 'B', 1),
(5, 4, 'A', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_semester`
--

CREATE TABLE `tbl_semester` (
  `id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `semester_name` varchar(255) NOT NULL,
  `combined_term_results` tinyint(1) NOT NULL COMMENT '0=no,1=yes',
  `combined_result_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_semester`
--

INSERT INTO `tbl_semester` (`id`, `session_id`, `course_id`, `semester_name`, `combined_term_results`, `combined_result_name`) VALUES
(2, 2, 2, 'Semester  One', 1, 'Test2'),
(3, 2, 3, 'Semester  Two', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_session`
--

CREATE TABLE `tbl_session` (
  `id` int(11) NOT NULL,
  `session_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_session`
--

INSERT INTO `tbl_session` (`id`, `session_name`, `from_date`, `to_date`) VALUES
(2, '2020 - 21', '2020-04-01', '2021-03-31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff`
--

CREATE TABLE `tbl_staff` (
  `id` int(11) NOT NULL,
  `staff_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `designation` tinyint(4) NOT NULL COMMENT '1=Principal,2=Teaching Staff,3=Non Teaching Staff',
  `pf_ac_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gross_salary` double(16,2) NOT NULL,
  `grade_pay` double(16,2) DEFAULT NULL,
  `med_allow` double(16,2) DEFAULT NULL,
  `phone_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=active,0=inactive'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_staff`
--

INSERT INTO `tbl_staff` (`id`, `staff_name`, `designation`, `pf_ac_no`, `gross_salary`, `grade_pay`, `med_allow`, `phone_number`, `email`, `user_name`, `password`, `status`) VALUES
(1, 'Kuntal Mondal', 1, '123456', 22160.00, 3000.00, 2200.00, '7896578965', 'kuntal@ablion.in', 'kuntal@ablion.in', '', 1),
(3, 'Guest Teacher1', 2, '', 10000.00, 0.00, NULL, '7802547854', 'jkhakjhfksahf@akjklj.com', 'gteacher1', '$2y$10$dNH4iqijH7S8owvVtXTgheA.YsbtYjGT8cRWz8ZyzZy1sHbksYCr6', 1),
(4, 'Bikas karmakar', 2, '', 10000.00, 0.00, NULL, '7894569874', 'test@ablion.in', 'test@ablion.in', '$2y$10$6Z9EwlGuN/3UBgRnmJqWDe4mfK7E2vfoOzRTulXU1EyQIYCVJA4.6', 1),
(5, 'Bikas Pal', 3, '', 10000.00, 0.00, NULL, '7894596789', 'test2@ablion.in', 'test2@ablion.in', '$2y$10$btHw3Rnvgihzb8DKlcYBNOkH1a0itCurltE5EyUXG18WlYLt4Vmci', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `join_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`id`, `name`, `email`, `phone`, `join_date`) VALUES
(11, 'demo demo', 'demo@gmail.com', '7898965478', '2020-05-29 08:53:10'),
(12, 'biswajit', 'bdas.tom@gmail.com', '7908170051', '2020-06-01 00:32:26'),
(13, 'Anurag', 'anurag@ablion.in', '7894561235', '2020-06-11 07:36:15'),
(14, 'Rafikur', 'rafi@ablion.in', '08653694046', '2020-06-19 06:28:22'),
(15, 'kanan', 'xyz@xyz.com', '9932027480', '2020-06-23 02:42:30'),
(16, 'Samantak Karmakar', 'biswajitdas.burdwan@gmail.com', '7908170051', '2020-06-29 05:34:27');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_enrollment`
--

CREATE TABLE `tbl_student_enrollment` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `semester_id` int(11) DEFAULT NULL,
  `roll_no` varchar(50) DEFAULT NULL,
  `enrollment_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_enrollment`
--

INSERT INTO `tbl_student_enrollment` (`id`, `student_id`, `class_id`, `semester_id`, `roll_no`, `enrollment_time`) VALUES
(3, 1, 3, 3, '1237', 1591683460),
(4, 5, 2, 2, '1279', 1591683698),
(5, 2, 2, 2, '123', 1591683460),
(6, 6, 2, 2, '124', 1591683460),
(7, 10, 2, 2, '123456', 1591683460),
(9, 19, 2, 0, '', 1591877879),
(10, 20, 2, 0, '', 1593500459);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_from`
--

CREATE TABLE `tbl_student_from` (
  `id` int(11) NOT NULL,
  `from_id` varchar(200) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `father_or_mother_name` varchar(255) DEFAULT NULL,
  `guardian_name` varchar(255) DEFAULT NULL,
  `perm_address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_no` varchar(100) DEFAULT NULL,
  `corres_address` varchar(255) DEFAULT NULL,
  `aadhar_no` varchar(100) DEFAULT NULL,
  `reg_mobile_no` varchar(100) DEFAULT NULL,
  `gud_annual_income` varchar(100) NOT NULL,
  `dob` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL COMMENT '1=male,2=female,3=others',
  `marital_status` tinyint(4) DEFAULT NULL COMMENT '1=Married,2=Single,3=Divorced',
  `nationality` varchar(100) DEFAULT NULL,
  `caste` tinyint(4) DEFAULT NULL COMMENT '1=General,2=SC,3=ST,4=OBC,5=Physically Handicapped',
  `local_address` varchar(255) DEFAULT NULL,
  `local_gu_name_adds` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `rules_condition_first` tinyint(4) DEFAULT NULL COMMENT '1=Yes,0=No',
  `rules_condition_one` tinyint(4) DEFAULT NULL COMMENT '1=Yes,0=No',
  `rules_condition_two` tinyint(4) DEFAULT NULL COMMENT '1=Yes,0=No',
  `admit_card_type` tinyint(4) DEFAULT NULL COMMENT '0=No Admit Issue,1=Admission test,2=Direct admission on',
  `admit_card_issue_date` date DEFAULT NULL,
  `is_deleted` int(11) DEFAULT NULL COMMENT '0=not deleted,1=deleted ',
  `manual_fees_setting` tinyint(11) DEFAULT NULL COMMENT '0 = Disabled, 1 = Enabled',
  `status` tinyint(4) NOT NULL COMMENT '1=Need Review,2=Review Pending,3=Admit (Test) Issued,4=Admit (Admission) Issued,5=Payment Link Activated,6=Admission Fees received,7=Enrolled	'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_from`
--

INSERT INTO `tbl_student_from` (`id`, `from_id`, `student_id`, `course_id`, `session_id`, `student_name`, `father_or_mother_name`, `guardian_name`, `perm_address`, `email`, `phone_no`, `corres_address`, `aadhar_no`, `reg_mobile_no`, `gud_annual_income`, `dob`, `age`, `gender`, `marital_status`, `nationality`, `caste`, `local_address`, `local_gu_name_adds`, `date`, `rules_condition_first`, `rules_condition_one`, `rules_condition_two`, `admit_card_type`, `admit_card_issue_date`, `is_deleted`, `manual_fees_setting`, `status`) VALUES
(8, 'CAD2020748860', 12, 2, 2, 'SHILAJIT DAS', 'father das', 'father das', 'dvc more, kanainatshal, burdwan-3', 'biswajit@ablion.in', '9851236616', 'dvc more, burdwan', '412367453214', '7908170051', '50000', '2000-06-20', 20, 1, 2, 'indian', 4, 'bs road burdwan', 'gagan das, burdwan', '2020-06-05', 1, 1, 1, 0, NULL, 0, 0, 1),
(9, 'CAD2020644523', 15, 2, 2, 'ANURAG ROY', 'Anurag', 'Anurag', 'Bardhanab', 'admin@mail.com', '7894567896', 'Anurag', '123456778945', '7894567896', '', '2004-06-01', 16, 1, 2, 'Indian', 4, 'Anurag', 'Anurag', '2020-06-05', 1, 1, 1, 0, NULL, 0, 0, 1),
(10, 'CAD2020503082', 11, 2, 2, 'JEET DAS', 'father kr das', 'father kr das', 'kachari road, burdwan, PIN - 713101', 'biswajit@ablion.in', '7908170051', 'Curzon Gate, Burdwan', '123478905678', '6296236427', '', '1998-06-10', 22, 1, 2, 'indian', 4, 'burdwan', 'guardian das, bwn', '2020-06-09', 1, 1, 1, 0, NULL, 0, 0, 1),
(11, 'CAD2020635800', NULL, 2, 2, 'ANURAG ROY', 'Anurag', 'ANURAG ROY', 'Bardhanab', 'anurag@ablion.in', '7894567894', 'ANURAG ROY', '789456789456', '7894567894', '', '1995-06-01', 25, 1, 2, 'Indian', 4, 'Anurag', 'Anurag', '2020-06-10', 1, 1, 1, 0, NULL, 0, 0, 7),
(14, 'CAD2020549825', 13, 2, 2, 'ANURAG ROY', 'Anurag', 'Anurag', 'Bardhanab', 'admin@mail.com', '7894567894', 'Anurag', '789456789412', '7894567894', '', '1976-06-01', 44, 1, 2, 'Indian', 4, 'Anurag', 'Anurag', '2020-06-10', 1, 1, 1, 0, NULL, 0, 0, 1),
(15, 'CAD2020719131', 12, 2, 2, 'TRINA DEY', 'triloknath dey', 'jiban dutta', 'hatgobindopur, burdwan', 'xyz@abc.com', '5678901234', 'bajepratappur, burdwan - 713101', '789012345678', '9651236616', '', '2000-05-10', 20, 2, 2, 'indian', 4, 'burdwan', 'jiban dutta, bwn', '2020-06-10', 1, 1, 1, 0, NULL, 0, 1, 7),
(19, 'CAD2020719139', 14, 2, 2, 'RADHA KHAN', 'fat khan', 'guardian', 'bs road, burdwan - 713101', 'biswa@xyz.com', '7812356432', 'bc road, burdwan', '786745343214', '7654623456', '', '2002-06-20', 18, 2, 3, 'nepali', 4, 'bwn', 'guardian, bwn', '2020-06-11', 1, 1, 1, 0, NULL, NULL, 1, 7),
(20, 'CAD2020555425', 16, 2, 2, 'SAMANTAK KARMAKAR', 'Sujan Karmakar', 'Shilpa Karmakar', 'B. C. Road, Burdwan - 713101', 'biswajitdas.burdwan@gmail.com', '7908170051', '5, B. C. Road, Burdwan - 713101', '123456789012', '7908170051', '', '2000-06-15', 20, 1, 2, 'Indian', 4, 'Burdwan', 'Jahar Karmakar, Burdwan', '2020-06-30', 1, 1, 1, 1, '2020-08-01', NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_from_document`
--

CREATE TABLE `tbl_student_from_document` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `age_photo` varchar(255) DEFAULT NULL,
  `caste_photo` varchar(255) DEFAULT NULL,
  `guardian_sign` varchar(255) DEFAULT NULL,
  `student_sign` varchar(255) DEFAULT NULL,
  `student_sign_full` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_from_document`
--

INSERT INTO `tbl_student_from_document` (`id`, `student_id`, `passport`, `age_photo`, `caste_photo`, `guardian_sign`, `student_sign`, `student_sign_full`) VALUES
(1, 8, '14_unnamed.jpg', '14_dummy.pdf', '14_dummy.pdf', '14_20141111-14372582-7606-capture-2.png', '14_20141111-14372582-7606-capture-2.png', '14_20141111-14372582-7606-capture-2.png'),
(2, 15, '', '15_dummy.pdf', '15.jpg', '15_20141111-14372582-7606-capture-2.png', '15_20141111-14372582-7606-capture-2.png', '15_20141111-14372582-7606-capture-2.png'),
(3, 16, '', '', '', '', '', ''),
(4, 19, '19_7.png', '19_ORDER_CLOSING_FORM.pdf', '19_Order_Form.pdf', '19_signature.jpg', '19_1.png', '19_signature.jpg'),
(5, 20, '20_IMG_test.jpg', '20_Scanned_doc_jpeg.jpg', '20_Scanned_doc.pdf', '20_signature.jpg', '20_signature1.jpg', '20_signature1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_payment_status`
--

CREATE TABLE `tbl_student_payment_status` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `admission_from_payment` tinyint(11) NOT NULL COMMENT '1=Received,0=Pending',
  `admission_from_payment_ammount` float(20,2) NOT NULL,
  `admission_payment` tinyint(11) NOT NULL COMMENT '1=Received,0=Pending',
  `admission_payment_ammount` float(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_payment_status`
--

INSERT INTO `tbl_student_payment_status` (`id`, `student_id`, `admission_from_payment`, `admission_from_payment_ammount`, `admission_payment`, `admission_payment_ammount`) VALUES
(1, 11, 0, 0.00, 0, 0.00),
(2, 12, 0, 0.00, 0, 0.00),
(3, 13, 0, 0.00, 0, 0.00),
(4, 14, 0, 0.00, 0, 0.00),
(5, 15, 0, 0.00, 0, 0.00),
(6, 16, 1, 400.00, 0, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subjects`
--

CREATE TABLE `tbl_subjects` (
  `id` int(11) NOT NULL,
  `subject_name` varchar(128) NOT NULL,
  `subject_code` varchar(50) DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subjects`
--

INSERT INTO `tbl_subjects` (`id`, `subject_name`, `subject_code`, `status`) VALUES
(2, 'History1', NULL, 1),
(3, 'Gegraphy', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_terms`
--

CREATE TABLE `tbl_terms` (
  `id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `term_name` varchar(255) NOT NULL,
  `combined_exam_results` tinyint(1) NOT NULL COMMENT '0=no,1=yes',
  `combined_result_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_terms`
--

INSERT INTO `tbl_terms` (`id`, `semester_id`, `term_name`, `combined_exam_results`, `combined_result_name`) VALUES
(1, 2, 'Term One', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `join_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `email`, `phone`, `join_date`) VALUES
(1, 'Surajit Das', 'surajit@ablion.in', '9474728606', '2019-12-13 14:55:23'),
(3, 'Surajit Das', 'surajit@ablion.in', '9851661168', '2019-12-17 11:07:34'),
(4, 'Biswajit Dutta', 'biswajit@ablion.in', '8653694046', '2019-12-20 16:19:33'),
(5, 'Tapas Das', 'rafi@ablion.in', '8653694046', '2019-12-20 16:23:34'),
(6, 'Upasona Banerjee', 'upasona@ablion.in', '8768673981', '2019-12-20 16:29:00'),
(7, 'Rafi', 'rafi@ablion.in', '8653694046', '2019-12-20 16:41:50'),
(8, 'Soumyadeep Sadhu', 'soumyadeep@ablion.in', '8653694046', '2019-12-20 16:47:34'),
(9, 'anurag', 'anurag@ablion.in', '7501904571', '2020-05-29 02:03:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_otp`
--

CREATE TABLE `tbl_user_otp` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `otp` int(11) NOT NULL,
  `otp_sending_time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_user_otp`
--

INSERT INTO `tbl_user_otp` (`id`, `user_id`, `otp`, `otp_sending_time`) VALUES
(1, 10, 3154, 2020),
(2, 11, 1412, 2020),
(3, 12, 9817, 2020),
(4, 13, 7438, 2020),
(5, 14, 2664, 2020),
(6, 15, 6835, 2020),
(7, 16, 54, 2020);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_settings`
--

CREATE TABLE `tbl_user_settings` (
  `id` int(11) NOT NULL,
  `email_verification` tinyint(1) NOT NULL COMMENT '1 = Yes, 0 = No',
  `otp_verification` tinyint(1) NOT NULL COMMENT '1 = Yes, 0 = No',
  `username` tinyint(1) NOT NULL COMMENT '1 = Email, 2 = Mobile, 3 = General'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_user_settings`
--

INSERT INTO `tbl_user_settings` (`id`, `email_verification`, `otp_verification`, `username`) VALUES
(1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_verification_tokens`
--

CREATE TABLE `tbl_verification_tokens` (
  `id` int(11) NOT NULL,
  `user_type` tinyint(1) NOT NULL COMMENT '1 = Admin, 2 = User',
  `user_id` int(11) NOT NULL,
  `token_type` tinyint(1) NOT NULL COMMENT '1 = Email Verification, 2 = Forgot Password',
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_time` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_verification_tokens`
--

INSERT INTO `tbl_verification_tokens` (`id`, `user_type`, `user_id`, `token_type`, `token`, `expiry_time`) VALUES
(1, 1, 5, 2, 'c7a5b4ec133b3fb7a567a7a395d807a1123636d1', '2019-12-21 16:10:04'),
(6, 1, 1, 2, 'a4bce3e3a8d593a7db9af796e9913153cb0d2044', '2019-12-21 16:51:41'),
(7, 1, 7, 2, 'f121f20b4b10ae76ed3b6b3f79affc0b571b96a6', '2019-12-21 17:00:07'),
(9, 2, 10, 3, '3154', '2020-05-30 08:40:14'),
(10, 2, 11, 3, '1412', '2020-05-30 08:53:10'),
(11, 2, 12, 3, '9817', '2020-06-02 00:32:26'),
(12, 2, 13, 3, '7438', '2020-06-12 07:36:15'),
(13, 2, 14, 3, '2664', '2020-06-20 06:28:22'),
(14, 2, 15, 3, '6835', '2020-06-24 02:42:30'),
(18, 2, 16, 3, '0054', '2020-06-30 05:34:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cms_pages`
--
ALTER TABLE `tbl_cms_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `url_alise` (`url_alias`);

--
-- Indexes for table `tbl_college_payment_settings`
--
ALTER TABLE `tbl_college_payment_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_common_login`
--
ALTER TABLE `tbl_common_login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`),
  ADD KEY `type` (`type`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `tbl_courses`
--
ALTER TABLE `tbl_courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_name` (`course_name`);

--
-- Indexes for table `tbl_edu_qualification`
--
ALTER TABLE `tbl_edu_qualification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_emp_payslip`
--
ALTER TABLE `tbl_emp_payslip`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emp_id` (`emp_id`,`attendance_id`);

--
-- Indexes for table `tbl_exam`
--
ALTER TABLE `tbl_exam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `exam_start_date` (`exam_start_date`),
  ADD KEY `exam_end_date` (`exam_end_date`),
  ADD KEY `publish_date` (`publish_date`);

--
-- Indexes for table `tbl_exam_scores`
--
ALTER TABLE `tbl_exam_scores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exam_id` (`exam_id`),
  ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `tbl_fees_payment`
--
ALTER TABLE `tbl_fees_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fees_breakup_id` (`fees_breakup_id`);

--
-- Indexes for table `tbl_fees_structure`
--
ALTER TABLE `tbl_fees_structure`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_id` (`semester_id`);

--
-- Indexes for table `tbl_fees_structure_breakups`
--
ALTER TABLE `tbl_fees_structure_breakups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`fees_id`);

--
-- Indexes for table `tbl_fees_structure_breakups_manual`
--
ALTER TABLE `tbl_fees_structure_breakups_manual`
  ADD PRIMARY KEY (`id`),
  ADD KEY `child_id` (`student_id`),
  ADD KEY `class_id` (`semester_id`),
  ADD KEY `breakup_id` (`breakup_id`);

--
-- Indexes for table `tbl_gallery_albums`
--
ALTER TABLE `tbl_gallery_albums`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_album` (`parent_album`);

--
-- Indexes for table `tbl_gallery_files`
--
ALTER TABLE `tbl_gallery_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `album_id` (`album_id`);

--
-- Indexes for table `tbl_gateway_settings`
--
ALTER TABLE `tbl_gateway_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notice`
--
ALTER TABLE `tbl_notice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_id` (`semester_id`),
  ADD KEY `publish_date` (`publish_date`);

--
-- Indexes for table `tbl_paymentgateway`
--
ALTER TABLE `tbl_paymentgateway`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_principal_record`
--
ALTER TABLE `tbl_principal_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_score`
--
ALTER TABLE `tbl_score`
  ADD PRIMARY KEY (`id`),
  ADD KEY `score_id` (`score_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `exam_id` (`exam_id`);

--
-- Indexes for table `tbl_section`
--
ALTER TABLE `tbl_section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `tbl_semester`
--
ALTER TABLE `tbl_semester`
  ADD PRIMARY KEY (`id`),
  ADD KEY `session_id` (`session_id`),
  ADD KEY `class_id` (`course_id`);

--
-- Indexes for table `tbl_session`
--
ALTER TABLE `tbl_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_dsate` (`from_date`),
  ADD KEY `to_date` (`to_date`);

--
-- Indexes for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student_enrollment`
--
ALTER TABLE `tbl_student_enrollment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `semester_id` (`semester_id`),
  ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `tbl_student_from`
--
ALTER TABLE `tbl_student_from`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `from_id_3` (`from_id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `from_id_2` (`from_id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `tbl_student_from_document`
--
ALTER TABLE `tbl_student_from_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student_payment_status`
--
ALTER TABLE `tbl_student_payment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_terms`
--
ALTER TABLE `tbl_terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `semester_id` (`semester_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_otp`
--
ALTER TABLE `tbl_user_otp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_user_settings`
--
ALTER TABLE `tbl_user_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `otp_verification` (`otp_verification`);

--
-- Indexes for table `tbl_verification_tokens`
--
ALTER TABLE `tbl_verification_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `expiry_time` (`expiry_time`),
  ADD KEY `token` (`token`),
  ADD KEY `user_type` (`user_type`),
  ADD KEY `token_type` (`token_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admins`
--
ALTER TABLE `tbl_admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tbl_cms_pages`
--
ALTER TABLE `tbl_cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_college_payment_settings`
--
ALTER TABLE `tbl_college_payment_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_common_login`
--
ALTER TABLE `tbl_common_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_courses`
--
ALTER TABLE `tbl_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_edu_qualification`
--
ALTER TABLE `tbl_edu_qualification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_emp_payslip`
--
ALTER TABLE `tbl_emp_payslip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_exam`
--
ALTER TABLE `tbl_exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_exam_scores`
--
ALTER TABLE `tbl_exam_scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_fees_payment`
--
ALTER TABLE `tbl_fees_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_fees_structure`
--
ALTER TABLE `tbl_fees_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_fees_structure_breakups`
--
ALTER TABLE `tbl_fees_structure_breakups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_fees_structure_breakups_manual`
--
ALTER TABLE `tbl_fees_structure_breakups_manual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_gallery_albums`
--
ALTER TABLE `tbl_gallery_albums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_gallery_files`
--
ALTER TABLE `tbl_gallery_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_gateway_settings`
--
ALTER TABLE `tbl_gateway_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_notice`
--
ALTER TABLE `tbl_notice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_paymentgateway`
--
ALTER TABLE `tbl_paymentgateway`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_principal_record`
--
ALTER TABLE `tbl_principal_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_score`
--
ALTER TABLE `tbl_score`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_section`
--
ALTER TABLE `tbl_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_semester`
--
ALTER TABLE `tbl_semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_session`
--
ALTER TABLE `tbl_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_staff`
--
ALTER TABLE `tbl_staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_student_enrollment`
--
ALTER TABLE `tbl_student_enrollment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_student_from`
--
ALTER TABLE `tbl_student_from`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_student_from_document`
--
ALTER TABLE `tbl_student_from_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_student_payment_status`
--
ALTER TABLE `tbl_student_payment_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_terms`
--
ALTER TABLE `tbl_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_user_otp`
--
ALTER TABLE `tbl_user_otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_user_settings`
--
ALTER TABLE `tbl_user_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_verification_tokens`
--
ALTER TABLE `tbl_verification_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
