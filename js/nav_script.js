$(document).ready(function(){	
	$(".main_nav li:has('ul')").children("a").append("<span class='drop_down_icon fa fa-caret-down'></span>");			
	if ($(window).width() < 900) {
		$(".main_nav li").not(".sub-menu li a").click(function(){
			$(this).find(".drop_down_icon").toggleClass('rotate_icon');
			$(this).find(".sub-menu").slideToggle(400);
		});
		
		$(".toggle_icon").click(function(){
			$(".main_nav").toggleClass('slideOpenMenu');
			$(this).find('i').toggleClass('fa-bars fa-times')
		});	
		
    }
	else{
		$(".main_nav li").not(".sub-menu li").hover(function(){
			$(this).find(".drop_down_icon").toggleClass('rotate_icon');
			$(this).find(".sub-menu").fadeToggle(400);
		});	
	}
});