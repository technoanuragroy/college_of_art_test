<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class College extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->checkAdminSecurity();
        $this->load->model("admin/College_model");
    }

    /*
    | --------------------------------------------------------------------------
    | Manage CMS
    | --------------------------------------------------------------------------
    */
    function index() {

        // Get the data
        //echo "<pre>";print_r($query);die;
        $data['cms_pages'] = $this->Cms_model->get_all_cms_pages();

        foreach ($data['cms_pages'] as $key => $page) {

            $data['cms_pages'][$key]['encrypted_id'] = $this->ablfunctions->ablEncrypt($page['id']);
        }

        $this->load->view("admin/cms/manage_cms", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Staff
    | --------------------------------------------------------------------------
    */
    function manage_staff(){

        // Get the data
        $data['all_staff'] = $this->my_custom_functions->get_multiple_data("tbl_staff",'');

        $this->load->view("admin/college/manage_staff", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add new Staff
    | --------------------------------------------------------------------------
    */
    function addStaff() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {


            $this->form_validation->set_rules("staff_name", "Staff Name", "trim|required");
            $this->form_validation->set_rules("designation", "Designation", "trim|required");
            $this->form_validation->set_rules("scale", "Scale", "trim|required");
            $this->form_validation->set_rules("phone_number", "Phone Number", "trim|required");
            $this->form_validation->set_rules("pf_ac_no", "PF a/c No", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            $this->form_validation->set_rules("user_name", "User Name", "trim|required|is_unique[tbl_staff.user_name]");
            $this->form_validation->set_rules("password", "Password", "trim|required|min_length[6]|max_length[32]");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/manage_staff");
            } else {

              // Add new admin data
                $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);
                $insert_data = array(
                    "staff_name" => $this->input->post("staff_name"),
                    "designation" => $this->input->post("designation"),
                    "gross_salary" => $this->input->post("scale"),
                    "grade_pay" => $this->input->post("grade_pay"),
                    "med_allow" => $this->input->post("med_allow"),
                    "pf_ac_no" => $this->input->post("pf_ac_no"),
                    "phone_number" => $this->input->post("phone_number"),
                    "email" => $this->input->post("email"),
                    "user_name" => $this->input->post("user_name"),
                    "password" => $password,
                    "status" => $this->input->post("status")
                );

                $page_id = $this->my_custom_functions->insert_data($insert_data,"tbl_staff");///////  insert to user table

                if ($page_id) {
                    // Upload Passport Photo
                    if (isset($_FILES['staff_photo'])) {

                        if ($_FILES['staff_photo']['name'] != "") {

                            $file_name_old = $page_id.'.'.'jpg';
                            $file_name = str_replace(' ', '_', $file_name_old);
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STAFF_PHOTO;
                            $config['allowed_types']        = ALLOWED_PHOTO_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_PHOTO_IMAGE_SIZE;
                            // $config['max_width']  = PASSPORT_WIDTH;
                            // $config['max_height']  = PASSPORT_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('staff_photo')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                //Upload thumb image
                                $source = UPLOAD_DIR.STAFF_PHOTO.$file_name;
                                $destination = UPLOAD_DIR.STAFF_PHOTO_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                    if (isset($_FILES['staff_sign'])) {

                        if ($_FILES['staff_sign']['name'] != "") {

                            $file_name_old = $page_id.'.'.'jpg';
                            $file_name = str_replace(' ', '_', $file_name_old);
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STAFF_SIGN;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            // $config['max_width']  = SIGN_WIDTH;
                            // $config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('staff_sign')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                //Upload thumb image
                                $source = UPLOAD_DIR.STAFF_SIGN.$file_name;
                                $destination = UPLOAD_DIR.STAFF_SIGN_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }

                    $this->session->set_flashdata("s_message", "You have successfully added a Staff.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the Staff! Please try again.");
                }

                redirect("admin/college/manage_staff");
            }
        } else {

            $this->load->view("admin/college/add_staff");
        }
    }


    /*
    | --------------------------------------------------------------------------
    | Edit page
    | --------------------------------------------------------------------------
    */
    function editStaff() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('staff_id');
            $staff_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("staff_name", "Staff Name", "trim|required");
            $this->form_validation->set_rules("designation", "Designation", "trim|required");
            $this->form_validation->set_rules("scale", "Scale", "trim|required");
            $this->form_validation->set_rules("phone_number", "Phone Number", "trim|required");
            $this->form_validation->set_rules("pf_ac_no", "PF a/c No", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            //$this->form_validation->set_rules("status", "Page Content", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editStaff/".$encrypted_id."");
            } else {

              // Add new page data
              $update_data = array(
                  "staff_name" => $this->input->post("staff_name"),
                  "designation" => $this->input->post("designation"),
                  "gross_salary" => $this->input->post("scale"),
                  "grade_pay" => $this->input->post("grade_pay"),
                  "med_allow" => $this->input->post("med_allow"),
                  "pf_ac_no" => $this->input->post("pf_ac_no"),
                  "phone_number" => $this->input->post("phone_number"),
                  "email" => $this->input->post("email"),
                  "status" => $this->input->post("status")
              );
              $where = array(
                "id" => $staff_id,
              );
              // Update password if password provided only
              if($this->input->post("password") AND $this->input->post("password") != "") {

                  $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);
                  $update_data["password"] = $password;
              }

                $update = $this->my_custom_functions->update_data($update_data,"tbl_staff",$where);

                if ($update) {

                    // Upload Passport Photo
                    if (isset($_FILES['staff_photo'])) {

                        if ($_FILES['staff_photo']['name'] != "") {

                            $file_name_old = $staff_id.'.'.'jpg';
                            $file_name = str_replace(' ', '_', $file_name_old);
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STAFF_PHOTO;
                            $config['allowed_types']        = ALLOWED_PHOTO_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_PHOTO_IMAGE_SIZE;
                            // $config['max_width']  = PASSPORT_WIDTH;
                            // $config['max_height']  = PASSPORT_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('staff_photo')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                //Upload thumb image
                                $source = UPLOAD_DIR.STAFF_PHOTO.$file_name;
                                $destination = UPLOAD_DIR.STAFF_PHOTO_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                    if (isset($_FILES['staff_sign'])) {

                        if ($_FILES['staff_sign']['name'] != "") {

                            $file_name_old = $staff_id.'.'.'jpg';
                            $file_name = str_replace(' ', '_', $file_name_old);
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STAFF_SIGN;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            // $config['max_width']  = SIGN_WIDTH;
                            // $config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('staff_sign')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                //Upload thumb image
                                $source = UPLOAD_DIR.STAFF_SIGN.$file_name;
                                $destination = UPLOAD_DIR.STAFF_SIGN_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }

                    $this->session->set_flashdata("s_message", "Staff record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Staff record.");
                }

                redirect("admin/college/manage_staff");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $staff_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            $data['staff_details'] = $this->ablfunctions->getDetailsFromId($staff_id, "tbl_staff");

            $this->load->view("admin/college/edit_staff", $data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Delete Staff
    | --------------------------------------------------------------------------
    */
    function deleteStaff() {

        $encrypted_id = $this->uri->segment(4);
        $staff_id = $this->ablfunctions->ablDecrypt($encrypted_id);

        // Delete page data
    $query = $this->ablfunctions->deleteData("tbl_staff", array("id" => $staff_id));
    if ($query) {
        $pathp=UPLOAD_DIR.STAFF_PHOTO.$staff_id.".jpg";
        $pathp_thu=UPLOAD_DIR.STAFF_PHOTO_THUMB.$staff_id.".jpg";
        @unlink(''.$pathp.'');
        @unlink(''.$pathp_thu.'');
        $paths=UPLOAD_DIR.STAFF_SIGN.$staff_id.".jpg";
        $paths_thu=UPLOAD_DIR.STAFF_SIGN_THUMB.$staff_id.".jpg";
        @unlink(''.$paths.'');
        @unlink(''.$paths_thu.'');
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manage_staff");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manage_staff");
    }
    }

    /*
    | --------------------------------------------------------------------------
    | Get URL alias auto populated through ajax call from page name text
    | --------------------------------------------------------------------------
    */
    public function getUrlAlias() {

        $name = $this->input->post('name');

        $url_alias = $this->ablfunctions->CreateAlise($name);

        echo $url_alias;
    }

    /*
    | --------------------------------------------------------------------------
    | Validate URL alias through ajax call from add page page
    | --------------------------------------------------------------------------
    */
    public function validateUrlAlias() {

        $url_alias = $this->input->post('url_alias');

        if (isset($url_alias) && $url_alias != "") {

            if($this->input->post("page")) {

                $encrypted_id = $this->input->post("page");
                $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
                $url_alias_count = $this->ablfunctions->getParticularCount("tbl_cms_pages", " and url_alias='" . $url_alias . "' and id!='" . $page_id . "'");
            } else {

                $url_alias_count = $this->ablfunctions->getParticularCount("tbl_cms_pages", " and url_alias='" . $url_alias . "'");
            }

            if ($url_alias_count > 0) {

                echo "URL alias already exists on system";
            } else {

                echo "";
            }
        } else {

            echo "Invalid URL alias";
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Delete page
    | --------------------------------------------------------------------------
    */
    function deletePage() {

        $encrypted_id = $this->uri->segment(4);
        $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);

        // Delete page data
        $this->ablfunctions->deleteData("tbl_cms_pages", array("id" => $page_id));

        $this->session->set_flashdata('s_message', "Deleted successfully.");
        redirect("admin/cms");
    }

    /*
    | --------------------------------------------------------------------------
    | Manage Staff
    | --------------------------------------------------------------------------
    */
    function manage_attendance() {
        if ($this->input->post('submit') AND ($this->input->post('submit') == "Submit")) {
            $month = $this->input->post('month');
            $this->session->set_userdata('sess_month', $month);
            $year = $this->input->post('year');
        } else { //if loaded directly
            if ($this->session->userdata('sess_month')) {
                $month = $this->session->userdata('sess_month');
            } else {
                $month = date("m") - 1;
            }

            $year = date("Y");
        }

        $all_years = $this->College_model->get_attendence_year_list();
        foreach ($all_years as $row) {
            $years[$row->att_years] = $row->att_years;
        }

        $query = $this->College_model->manage_attendance($month, $year);
        $data = array(
          "attend" => $query,
          'years' => $years,
          'month' => $month,
          'year' => $year,
        );
        $this->load->view("admin/college/manage_attendance", $data);
    }
    function addAttendance() {
      if ($this->input->post('submit') AND ($this->input->post('submit') == "Submit")) {

          $emp_id = $this->input->post('emp_id');
          $date_month = $this->input->post('year') . "-" . $this->input->post('month') . "-" . "01";
          $result = $this->College_model->get_attendance_details($emp_id, $date_month);
          if ($result) {

              $this->session->set_flashdata('e_message', 'Attendance already added.');
              redirect('admin/college/addAttendance');
          } else {

              $data = array(
                'emp_id' => $this->input->post('emp_id'),
                'date_month' => $date_month,
                'present' => $this->input->post('present'),
                'absent' => $this->input->post('absent'),
                'pl' => $this->input->post('pl'),
                'cl' => $this->input->post('cl'),
                'sl' => $this->input->post('sl'),
                'opl' => $this->input->post('opl'),
                'working_days' => $this->input->post('working_days'),
                'note' => $this->input->post('note'),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->input->post('admin_name'),
              );
              $employee = $this->my_custom_functions->insert_data($data,"tbl_attendance");///////  insert to user table
              if (isset($employee)) {
                  $this->session->set_flashdata('s_message', 'Successfully Added Attdance');
                  redirect("admin/college/manage_attendance");
              }
          }
      } else {

          $employee = $this->my_custom_functions->get_details_from_id("", "tbl_staff", array("id" => $this->uri->segment(3)));
          //$all_emp = $this->my_custom_functions->get_multiple_data("tbl_staff",'');
          //echo '<pre>';print_r($all_emp);die;
          $all_emp = $this->College_model->get_all_employee_details();
          $cl = $this->College_model->get_cl($this->uri->segment(3));

          $data = array(
            'employee' => $employee,
            'all_emp' => $all_emp,
            'cl' => $cl,
          );
          $this->load->view('admin/college/add_attendance', $data);
      }
        //$this->load->view("admin/college/add_attendance");
    }
    /*
    | --------------------------------------------------------------------------
    | Attendance Report
    | --------------------------------------------------------------------------
    */
    function attendance_report() {

    if ($this->input->post('submit') AND ($this->input->post('submit') == "Search")) {
        //$emp_id = $this->College_model->get_emp_id($this->input->post('emp_name'));
        $emp_id = $this->my_custom_functions->get_particular_field_value("tbl_staff","id", 'and staff_name="'.$this->input->post('emp_name').'"');
        $attend = $this->College_model->get_attendance_report($emp_id);
        //echo '<pre>';print_r($attend);die;
        $data = array(
          'attend' => $attend,
          'emp_name' => $this->input->post('emp_name'),
          'from_date' => $this->input->post('from_date'),
          'to_date' => $this->input->post('to_date'),
        );



        $this->load->view('admin/college/attendance_report.php', $data);
    } else {
        $this->load->view('admin/college/attendance_report.php');
    }
    }
    ///////////////////////////////////////////////////////////////
    function ajax_get_emps()
    {
    $return_arr = array();
    $term = $this->input->get('term');
    $fetch = $this->College_model->get_ajax_emps($term);

    foreach ($fetch->result_array() as $row)
    {
      $row_array['value'] = $row['staff_name'];
      array_push($return_arr,$row_array);
    }

    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Attendance
    | --------------------------------------------------------------------------
    */
    function edit_attendance() {
    if ($this->input->post('submit') AND ($this->input->post('submit') == "Update")) {
        $update_data = array(
          'date_month' => $this->input->post('year') . "-" . $this->input->post('month') . "-" . "01",
          'present' => $this->input->post('present'),
          'absent' => $this->input->post('absent'),
          'pl' => $this->input->post('pl'),
          'cl' => $this->input->post('cl'),
          'sl' => $this->input->post('sl'),
          'opl' => $this->input->post('opl'),
          'note' => $this->input->post('note'),
          'updated_at' => date('Y-m-d H:i:s'),
          'created_by' => $this->input->post('admin_name'),
        );
        $where = array(
          "id" => $this->input->post('id'),
          'emp_id' => $this->input->post('emp_id')
        );

        $employee = $this->my_custom_functions->update_data($update_data,"tbl_attendance",$where);
        //$employee = $this->emp_model->update_attendance($data);
        if (isset($employee)) {
            $this->session->set_flashdata('s_message', 'Successfully Updated Attdence');
            //redirect('admin/college/manage_attendance');
            redirect('admin/college/edit_attendance/' . $this->input->post('emp_id') . '/' .$this->input->post('id'));
        }
    } else {

        $emp_id = $this->uri->segment(4);
        $attendance_id = $this->uri->segment(5);

        $employee = $this->my_custom_functions->get_details_from_id("", "tbl_staff", array("id" => $emp_id));
        //$query = $this->my_custom_functions->get_details_from_id("", "tbl_attendance", array("id" => $attendance_id));
        $query = $this->College_model->get_attendance($attendance_id);

        $dtime = new DateTime($query->date_month);
        $year = $dtime->format("Y");
        $cl = $this->College_model->get_cl_for_selected_year($year, $emp_id);

        $data = array(
          "employee" => $employee,
          "attend_id" => $attendance_id,
          "attend" => $query,
          'cl' => $cl
        );

        $this->load->view('admin/college/edit_attendance', $data);
    }
    }
    /*
    | --------------------------------------------------------------------------
    | Delete Attendance
    | --------------------------------------------------------------------------
    */
    function delete_attendance() {
      $id = $this->uri->segment(4);
      $query = $this->ablfunctions->deleteData("tbl_attendance", array("id" => $id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect('admin/college/add_attendance');
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect('admin/college/add_attendance');
    }
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Staff
    | --------------------------------------------------------------------------
    */
    function manageSession(){

        // Get the data
        $data['all_session'] = $this->my_custom_functions->get_multiple_data("tbl_session",'');
        $this->load->view("admin/college/manage_session", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add new Session
    | --------------------------------------------------------------------------
    */
    function addSession() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            //echo "<pre>";print_r ($_POST);die;
            $this->form_validation->set_rules("session_name", "Session Name", "trim|required");
            $this->form_validation->set_rules("from_date", "From Date", "trim|required");
            $this->form_validation->set_rules("to_date", "To Date", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/addSession");
            } else {

                $insert_data = array(
                    "session_name" => $this->input->post("session_name"),
                    "from_date" => $this->input->post("from_date"),
                    "to_date" => $this->input->post("to_date")
                );

                $page_id = $this->my_custom_functions->insert_data($insert_data,"tbl_session");///////  insert to user table

                if ($page_id) {

                    $this->session->set_flashdata("s_message", "You have successfully added a Session.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the Session! Please try again.");
                }

                redirect("admin/college/manageSession");
            }
        } else {

            $this->load->view("admin/college/add_session");
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Session
    | --------------------------------------------------------------------------
    */
    function editSession() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('session_id');
            $session_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("session_name", "Session Name", "trim|required");
            $this->form_validation->set_rules("from_date", "From Date", "trim|required");
            $this->form_validation->set_rules("to_date", "To Date", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editSession/".$encrypted_id."");
            } else {

              $update_data = array(
                  "session_name" => $this->input->post("session_name"),
                  "from_date" => $this->input->post("from_date"),
                  "to_date" => $this->input->post("to_date")
              );
              $where = array(
                "id" => $session_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_session",$where);

                if ($update) {

                    $this->session->set_flashdata("s_message", "Session record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Session record.");
                }

                redirect("admin/college/manageSession");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $session_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            $data['session_details'] = $this->ablfunctions->getDetailsFromId($session_id, "tbl_session");

            $this->load->view("admin/college/edit_session", $data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Delete Session
    | --------------------------------------------------------------------------
    */
    function deleteSession() {
      $encrypted_id = $this->uri->segment(4);
      $session_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_session", array("id" => $session_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect('admin/college/manageSession');
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect('admin/college/manageSession');
    }
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Staff
    | --------------------------------------------------------------------------
    */
    function manageClasses(){

        // Get the data
        $data['all_classes'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
        $this->load->view("admin/college/manage_classes", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add new Classes
    | --------------------------------------------------------------------------
    */
    function addClasses() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->form_validation->set_rules("course_name", "Course Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/addClasses");
            } else {

                $insert_data = array(
                    "course_name" => $this->input->post("course_name"),
                    "status" => $this->input->post("status")
                );

                $page_id = $this->my_custom_functions->insert_data($insert_data,"tbl_courses");///////  insert to user table

                if ($page_id) {

                    $this->session->set_flashdata("s_message", "You have successfully added a Classes.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the Classes! Please try again.");
                }

                redirect("admin/college/manageClasses");
            }
        } else {

            $this->load->view("admin/college/add_classes");
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Classes
    | --------------------------------------------------------------------------
    */
    function editClasses() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('class_id');
            $class_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("course_name", "Course Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editClasses/".$encrypted_id."");
            } else {

              $update_data = array(
                  "course_name" => $this->input->post("course_name"),
                  "status" => $this->input->post("status")
              );
              $where = array(
                "id" => $class_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_courses",$where);

                if ($update) {

                    $this->session->set_flashdata("s_message", "Classes record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Classes record.");
                }

                redirect("admin/college/manageClasses");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $class_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            $data['class_details'] = $this->ablfunctions->getDetailsFromId($class_id, "tbl_courses");

            $this->load->view("admin/college/edit_classes", $data);
        }
    }
        /*
    | --------------------------------------------------------------------------
    | Delete Classes
    | --------------------------------------------------------------------------
    */
    function deleteClasses() {
      $encrypted_id = $this->uri->segment(4);
      $class_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_courses", array("id" => $class_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manageClasses");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manageClasses");
    }
    }
        /*
    | --------------------------------------------------------------------------
    | Manage Sections
    | --------------------------------------------------------------------------
    */

        function manageSections() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //$data['post_data'] = array('class_id' => $this->input->post('class'));
            $data['page_title'] = 'Section Search Details';
            $data['all_section'] = $this->my_custom_functions->get_multiple_data('tbl_section',' and class_id = "' . $this->input->post('class'). '" and status = 1');
        } else {
             $data['page_title'] = 'Manage section';
             $data['all_section'] = $this->my_custom_functions->get_multiple_data("tbl_section",'');
            //$data['classes'] = $this->School_user_model->get_section_data();
        }
            // Get the data
            $data['class_list'] = $this->my_custom_functions->get_multiple_data('tbl_courses', 'and status = 1');

        $this->load->view("admin/college/manage_sections", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add new Sections
    | --------------------------------------------------------------------------
    */
    function addSections() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->form_validation->set_rules("class_id", "Classes Name", "trim|required");
            $this->form_validation->set_rules("section_name", "Section Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/addSections");
            } else {

                $insert_data = array(
                    "class_id" => $this->input->post("class_id"),
                    "section_name" => $this->input->post("section_name"),
                    "status" => $this->input->post("status")
                );

                $page_id = $this->my_custom_functions->insert_data($insert_data,"tbl_section");///////  insert to user table

                if ($page_id) {

                    $this->session->set_flashdata("s_message", "You have successfully added a Section.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the Section! Please try again.");
                }

                redirect("admin/college/manageSections");
            }
        } else {

            $data['class_list'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'and status="1"');

            $this->load->view("admin/college/add_sections",$data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Sections
    | --------------------------------------------------------------------------
    */
    function editSections() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('section_id');
            $section_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("class_id", "Classes Name", "trim|required");
            $this->form_validation->set_rules("section_name", "Section Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editSections/".$encrypted_id."");
            } else {

              $update_data = array(
                    "class_id" => $this->input->post("class_id"),
                    "section_name" => $this->input->post("section_name"),
                    "status" => $this->input->post("status")
                );
              $where = array(
                "id" => $section_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_section",$where);

                if ($update) {

                    $this->session->set_flashdata("s_message", "Section record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Section record.");
                }

                redirect("admin/college/manageSections");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $section_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            $data['section_details'] = $this->ablfunctions->getDetailsFromId($section_id, "tbl_section");
            $data['class_list'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'and status="1"');

            $this->load->view("admin/college/edit_sections",$data);
        }
    }
        /*
    | --------------------------------------------------------------------------
    | Delete Sections
    | --------------------------------------------------------------------------
    */
    function deleteSections() {
      $encrypted_id = $this->uri->segment(4);
      $section_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_section", array("id" => $section_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manageSections");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manageSections");
    }
    }
        /*
    | --------------------------------------------------------------------------
    | Manage Subjects
    | --------------------------------------------------------------------------
    */
    function manageSubjects(){


        // Get the data
        $data['all_subject'] = $this->my_custom_functions->get_multiple_data("tbl_subjects",'');
        $this->load->view("admin/college/manage_subject", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add new Subjects
    | --------------------------------------------------------------------------
    */
    function addSubjects() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->form_validation->set_rules("subject_name", "Subject Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/addClasses");
            } else {

                $insert_data = array(
                    "subject_name" => $this->input->post("subject_name"),
                    "status" => $this->input->post("status")
                );

                $page_id = $this->my_custom_functions->insert_data($insert_data,"tbl_subjects");///////  insert to user table

                if ($page_id) {

                    $this->session->set_flashdata("s_message", "You have successfully added a Subjects.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the Subjects! Please try again.");
                }

                redirect("admin/college/manageSubjects");
            }
        } else {

            $this->load->view("admin/college/add_subject");
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Subjects
    | --------------------------------------------------------------------------
    */
    function editSubjects() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('subject_id');
            $subject_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("subject_name", "Subject Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editSubjects/".$encrypted_id."");
            } else {

              $update_data = array(
                    "subject_name" => $this->input->post("subject_name"),
                    "status" => $this->input->post("status")
                );

              $where = array(
                "id" => $subject_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_subjects",$where);

                if ($update) {

                    $this->session->set_flashdata("s_message", "Classes record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Classes record.");
                }

                redirect("admin/college/manageSubjects");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $subject_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            $data['details'] = $this->ablfunctions->getDetailsFromId($subject_id, "tbl_subjects");

            $this->load->view("admin/college/edit_subject", $data);
        }
    }
        /*
    | --------------------------------------------------------------------------
    | Delete Subjects
    | --------------------------------------------------------------------------
    */
    function deleteSubjects() {
      $encrypted_id = $this->uri->segment(4);
      $subject_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_subjects", array("id" => $subject_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manageSubjects");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manageSubjects");
    }
    }
     /*
    | --------------------------------------------------------------------------
    | Manage Semester
    | --------------------------------------------------------------------------
    */
    function manageSemester(){

        // Get the data
        $data['all_semester'] = $this->my_custom_functions->get_multiple_data("tbl_semester",'');
        $this->load->view("admin/college/manage_semester", $data);
    }
        /*
    | --------------------------------------------------------------------------
    | Add semester
    | --------------------------------------------------------------------------
    */

    public function addSemester() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules("course_id", "Course Name", "trim|required");
            //$this->form_validation->set_rules("session", "Session Name", "trim|required");
            $this->form_validation->set_rules("semester_name", "Semester Name", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/addSemester");
            } else {

                $chek_unique = $this->my_custom_functions->get_perticular_count('tbl_semester', 'and session_id="' .$this->input->post('session_id') . '" and course_id="' . $this->input->post('course_id') . '" and semester_name="' . $this->input->post('semester_name') . '"');
                if ($chek_unique > 0) {
                    $this->session->set_flashdata("e_message", 'Semester already exists with selected session and class');
                    redirect("admin/college/addSemester");
                }

                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }

                $insert_data = array(
                    'session_id' => $this->input->post('session_id'),
                    'course_id' => $this->input->post('course_id'),
                    'semester_name' => $this->input->post('semester_name'),
                    'combined_term_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('term_label')
                );

                $add_session_data = $this->my_custom_functions->insert_data($insert_data,"tbl_semester");///////  insert

                if ($add_session_data) {

                    $this->session->set_flashdata("s_message", 'Semester added successfully.');
                    redirect("admin/college/manageSemester");
                } else {

                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/college/addSemester");
                }
            }
        } else {

            $data['session_details']= $this->my_custom_functions->get_multiple_data("tbl_session",'');
            //$data['session_details']= $this->College_model->get_current_session();
            $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');

            $this->load->view('admin/college/add_semester', $data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Semester
    | --------------------------------------------------------------------------
    */
    public function editSemester() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $semester_id = $this->input->post('semester_id');

            $this->form_validation->set_rules("course_id", "Course Name", "trim|required");
            //$this->form_validation->set_rules("session", "Session Name", "trim|required");
            $this->form_validation->set_rules("semester_name", "Semester Name", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/editSemester" . $semester_id);
            } else {

                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }
                $semester_data = array(
                    'session_id' => $this->input->post('session_id'),
                    'course_id' => $this->input->post('course_id'),
                    'semester_name' => $this->input->post('semester_name'),
                    'combined_term_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('term_label')
                );
                $condition = array(
                    'id' => $this->ablfunctions->ablDecrypt($semester_id)
                );
                $edit_class_data = $this->my_custom_functions->update_data($semester_data,"tbl_semester", $condition);

                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Semester edited successfully.');
                    redirect("admin/college/manageSemester");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/college/editSemester/". $semester_id);
                }
            }
        } else {


            $semester_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
            $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
            $data['details'] = $this->ablfunctions->getDetailsFromId($semester_id, "tbl_semester");

            $this->load->view('admin/college/edit_semester', $data);
        }
    }
            /*
    | --------------------------------------------------------------------------
    | Delete Semester
    | --------------------------------------------------------------------------
    */
    function deleteSemester() {
      $encrypted_id = $this->uri->segment(4);
      $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_semester", array("id" => $page_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manageSemester");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manageSemester");
    }
    }

         /*
    | --------------------------------------------------------------------------
    | Manage Terms
    | --------------------------------------------------------------------------
    */
    function manageTerms(){

        // Get the data
        $data['all_terms'] = $this->my_custom_functions->get_multiple_data("tbl_terms",'');
        $this->load->view("admin/college/manage_terms", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add semester
    | --------------------------------------------------------------------------
    */

    public function addTerms() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $this->form_validation->set_rules("semester_id", "Semester Name", "trim|required");
            //$this->form_validation->set_rules("session", "Session Name", "trim|required");
            $this->form_validation->set_rules("term_name", "Term Name", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/addTerms");
            } else {


                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }

                $insert_data = array(
                    'semester_id' => $this->input->post('semester_id'),
                    'term_name' => $this->input->post('term_name'),
                    'combined_exam_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('term_label')
                );

                $add_session_data = $this->my_custom_functions->insert_data($insert_data,"tbl_terms");///////  insert

                if ($add_session_data) {

                    $this->session->set_flashdata("s_message", 'Term added successfully.');
                    redirect("admin/college/manageTerms");
                } else {

                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/college/addTerms");
                }
            }
        } else {
            $data['all_semester'] = $this->my_custom_functions->get_multiple_data("tbl_semester",'');

            $this->load->view('admin/college/add_term',$data);
        }
    }
        /*
    | --------------------------------------------------------------------------
    | Edit Terms
    | --------------------------------------------------------------------------
    */
    public function editTerm() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $page_id = $this->input->post('page_id');

            $this->form_validation->set_rules("semester_id", "Semester Name", "trim|required");
            //$this->form_validation->set_rules("session", "Session Name", "trim|required");
            $this->form_validation->set_rules("term_name", "Term Name", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/editTerm/". $page_id);
            } else {

                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }
                $page_data = array(
                    'semester_id' => $this->input->post('semester_id'),
                    'term_name' => $this->input->post('term_name'),
                    'combined_exam_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('term_label')
                );
                $condition = array(
                    'id' => $this->ablfunctions->ablDecrypt($page_id)
                );
                $edit_class_data = $this->my_custom_functions->update_data($page_data,"tbl_terms", $condition);

                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Semester edited successfully.');
                    redirect("admin/college/manageTerms");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/college/editTerm/". $page_id);
                }
            }
        } else {


            $term_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
            $data['all_semester'] = $this->my_custom_functions->get_multiple_data("tbl_semester",'');
            $data['details'] = $this->ablfunctions->getDetailsFromId($term_id, "tbl_terms");

            $this->load->view('admin/college/edit_term', $data);
        }
    }
                /*
    | --------------------------------------------------------------------------
    | Delete Semester
    | --------------------------------------------------------------------------
    */
    function deleteTerm() {
      $encrypted_id = $this->uri->segment(4);
      $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_terms", array("id" => $page_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manageTerms");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manageTerms");
    }
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Exam
    | --------------------------------------------------------------------------
    */
    function manageExam() {
        // Get the data
        $data['details'] = $this->my_custom_functions->get_multiple_data("tbl_exam",'');
        $this->load->view('admin/college/manage_exam', $data);
    }
        /*
    | --------------------------------------------------------------------------
    | Add Exam
    | --------------------------------------------------------------------------
    */
    function addExam() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {

          /// tbl_admins contents
            $this->form_validation->set_rules("term", "Term", "trim|required");
            $this->form_validation->set_rules("exam_name", "Exam name", "trim|required");
            $this->form_validation->set_rules("exam_disp_name", "Exam display name", "trim|required");
            $this->form_validation->set_rules("exam_start_date", "Exam start date", "trim|required");
            $this->form_validation->set_rules("exam_end_date", "Exam end date", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/addExam");
            } else {

                $publish_date = $this->my_custom_functions->database_date($this->input->post('publish_date'));
                $publish_hour_post = $this->input->post('start_hour');
                $publish_minute_post = $this->input->post('start_minute');
                $publish_meridian = $this->input->post('start_meridian');
                if ($publish_hour_post < 10) {
                    $publish_hour = '0' . $publish_hour_post;
                } else {
                    $publish_hour = $publish_hour_post;
                }
                if ($publish_minute_post < 10) {
                    $publish_minute = '0' . $publish_minute_post;
                } else {
                    $publish_minute = $publish_minute_post;
                }

                $publish_date_time = strtotime($publish_date . " " . $publish_hour . ":" . $publish_minute . $publish_meridian);


                $exam_data = array(
                    'term_id' => $this->input->post('term'),
                    'exam_name' => $this->input->post('exam_name'),
                    'exam_display_name' => $this->input->post('exam_disp_name'),
                    'exam_start_date' => $this->my_custom_functions->database_date($this->input->post('exam_start_date')),
                    'exam_end_date' => $this->my_custom_functions->database_date($this->input->post('exam_end_date')),
                    'publish_date' => $publish_date_time,
                );
                $exam_id = $this->my_custom_functions->insert_data_last_id($exam_data,'tbl_exam');
                //$exam_id = 2;

                $subject_list = $this->input->post('subject');
                $fullmarks = $this->input->post('total_marks');
                $passmarks = $this->input->post('pass_marks');

                foreach ($subject_list as $key => $subject_id) {
                    $marks_data = array(
                        'exam_id' => $exam_id,
                        'subject_id' => $subject_id,
                        'full_marks' => $fullmarks[$key],
                        'pass_marks' => $passmarks[$key]
                    );
                    //echo "<pre>";print_r($marks_data);
                    $marks = $this->my_custom_functions->insert_data($marks_data,'tbl_exam_scores');
                }
                $this->session->set_flashdata("s_message", 'Exam record added successfully.');
                redirect("admin/college/manageExam");
            }
        } else {

            $data['page_title'] = 'Add Exam';
            $data['term_list'] = $this->my_custom_functions->get_multiple_data("tbl_terms",'');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data("tbl_subjects",'');
            $this->load->view('admin/college/add_exam', $data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Exam
    | --------------------------------------------------------------------------
    */
    function editExam() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $exam_id = $this->ablfunctions->ablDecrypt($this->input->post('exam_id'));

            /// tbl_admins contents
            $this->form_validation->set_rules("term", "Term", "trim|required");
            $this->form_validation->set_rules("exam_name", "Exam name", "trim|required");
            $this->form_validation->set_rules("exam_disp_name", "Exam display name", "trim|required");
            $this->form_validation->set_rules("exam_start_date", "Exam start date", "trim|required");
            $this->form_validation->set_rules("exam_end_date", "Exam end date", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/editExam/" . $this->input->post('exam_id'));
            } else {
                $publish_date = $this->my_custom_functions->database_date($this->input->post('publish_date'));
                $publish_hour_post = $this->input->post('start_hour');
                $publish_minute_post = $this->input->post('start_minute');
                $publish_meridian = $this->input->post('start_meridian');
                if ($publish_hour_post < 10) {
                    $publish_hour = '0' . $publish_hour_post;
                } else {
                    $publish_hour = $publish_hour_post;
                }
                if ($publish_minute_post < 10) {
                    $publish_minute = '0' . $publish_minute_post;
                } else {
                    $publish_minute = $publish_minute_post;
                }

                $publish_date_time = strtotime($publish_date . " " . $publish_hour . ":" . $publish_minute . $publish_meridian);


                $exam_data = array(
                    'term_id' => $this->input->post('term'),
                    'exam_name' => $this->input->post('exam_name'),
                    'exam_display_name' => $this->input->post('exam_disp_name'),
                    'exam_start_date' => $this->my_custom_functions->database_date($this->input->post('exam_start_date')),
                    'exam_end_date' => $this->my_custom_functions->database_date($this->input->post('exam_end_date')),
                    'publish_date' => $publish_date_time,
                );
                $condition = array('id' => $exam_id);

                $update_exam_data = $this->my_custom_functions->update_data($exam_data,'tbl_exam', $condition);


                $update_score_id = $this->input->post('update_id');
                $update_total_marks = $this->input->post('update_total_marks');
                $update_pass_marks = $this->input->post('update_pass_marks');
                if (!empty($update_score_id)) {
                    foreach ($update_score_id as $key => $score_id) {
                        $update_score = array(
                            'full_marks' => $update_total_marks[$key],
                            'pass_marks' => $update_pass_marks[$key]
                        );
                        $condition_score = array('id' => $score_id);
                        $update_scores = $this->my_custom_functions->update_data($update_score, 'tbl_exam_scores', $condition_score);
                    }
                }
                $subject_list = $this->input->post('subject');
                $fullmarks = $this->input->post('total_marks');
                $passmarks = $this->input->post('pass_marks');
                if (!empty($subject_list)) {
                    foreach ($subject_list as $key => $subject_id) {
                        $marks_data = array(
                            'exam_id' => $exam_id,
                            'subject_id' => $subject_id,
                            'full_marks' => $fullmarks[$key],
                            'pass_marks' => $passmarks[$key]
                        );

                        $marks = $this->my_custom_functions->insert_data($marks_data, 'tbl_exam_scores');
                    }
                }
                $this->session->set_flashdata("s_message", 'Exam record updated successfully.');
                redirect("admin/college/manageExam");
            }
        } else {
            $exam_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
            $data['page_title'] = 'Edit Exam';
            $data['exam_data'] = $this->College_model->get_exam_data_list($exam_id);
           $data['term_list'] = $this->my_custom_functions->get_multiple_data("tbl_terms",'');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data("tbl_subjects",'');
            $this->load->view('admin/college/edit_exam', $data);
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Copy Exam
    | --------------------------------------------------------------------------
    */
    function copyExam() {
        $exam_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
            $data['page_title'] = 'Edit Exam';
            $data['exam_data'] = $this->College_model->get_exam_data_list($exam_id);
           $data['term_list'] = $this->my_custom_functions->get_multiple_data("tbl_terms",'');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data("tbl_subjects",'');
            $this->load->view('admin/college/copy_exam', $data);
    }
        /*
    | --------------------------------------------------------------------------
    | Delete Exam subject
    | --------------------------------------------------------------------------
    */
    function delete_exam_subject() {
        $ecam_score_id = $this->ablfunctions->ablDecrypt($this->input->post('exam_score_id'));
        $delete_score_condition_one = array('score_id' => $ecam_score_id);
        $delete_score = $this->my_custom_functions->delete_data('tbl_score', $delete_score_condition_one);
        $delete_score_condition_two = array('id' => $ecam_score_id);
        $delete_score = $this->my_custom_functions->delete_data('tbl_exam_scores', $delete_score_condition_two);
        echo "success";
    }
                    /*
    | --------------------------------------------------------------------------
    | Delete Semester
    | --------------------------------------------------------------------------
    */
    function deleteExam() {
      $encrypted_id = $this->uri->segment(4);
      $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query_exam = $this->ablfunctions->deleteData("tbl_exam", array("id" => $page_id));
      $query_score = $this->ablfunctions->deleteData("tbl_exam_scores", array("exam_id" => $page_id));
    if ($query_exam AND $query_score) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manageExam");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manageExam");
    }
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Student Register
    | --------------------------------------------------------------------------
    */
    function manageStudentRegister(){


        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $data['post_data'] = array('session_id' => $this->input->post('session'));
            $data['select_id'] = $this->input->post('session');
            $data['secarch_name'] = "session_id";
            $data['session_active'] = '1';
            $data['all_student'] = $this->my_custom_functions->get_multiple_data('tbl_student_from',' and session_id = "' . $this->input->post('session'). '"');
        }
        else if ($this->input->post('search') && $this->input->post('search') != '') {
            $data['post_data'] = array('status' => $this->input->post('status'));
            $data['secarch_name'] = "status";
            $data['select_id'] = $this->input->post('status');
            $data['all_student'] = $this->my_custom_functions->get_multiple_data('tbl_student_from',' and status = "' . $this->input->post('status'). '" and session_id = "' . $this->input->post('current_session_id'). '"');
            $data['session_active'] = '1';
        } else {
             //$data['page_title'] = 'Manage section';
            //$data['all_student'] = $this->my_custom_functions->get_multiple_data("tbl_student_from",'');
            $data['all_student'] = '';
        }
        // Get the data
        $data['all_session'] = $this->my_custom_functions->get_multiple_data("tbl_session",'');
        $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
        $data['all_status'] = $this->my_custom_functions->get_multiple_data("tbl_student_from",'');
        $this->load->view("admin/college/manage_student_register", $data);
    }
        /*
    | --------------------------------------------------------------------------
    | Edit Student Register
    | --------------------------------------------------------------------------
    */
    function editStudentRegister() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('student_id');
            $student_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("course_id", "Select Course", "required");
            //$this->form_validation->set_rules("session_id", "Select session", "required");
            $this->form_validation->set_rules("student_name", "Name of the Candidate", "trim|required");
            $this->form_validation->set_rules("father_or_mother_name", "Father’s /Mother’s Name", "trim|required");
            $this->form_validation->set_rules("guardian_name", "Name of the Guardian", "trim|required");
            $this->form_validation->set_rules("perm_address", "Permanent Address", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            //$this->form_validation->set_rules("phone_number", "Phone Number", "trim|required|is_unique[tbl_student_from.phone_number]");
            $this->form_validation->set_rules("phone_no", "Phone Number", "trim|required|regex_match[/^[0-9]{10}$/]");
            $this->form_validation->set_rules("corres_address", "Address for correspondence", "trim|required");
            $this->form_validation->set_rules("aadhar_no", "Aadhar No", "trim|required|min_length[12]|max_length[12]");
            $this->form_validation->set_rules("reg_mobile_no", "Registered Mobile No", "trim|required|regex_match[/^[0-9]{10}$/]");
            $this->form_validation->set_rules("dob", "D.O.B", "trim|required");
            $this->form_validation->set_rules("gud_annual_income", "Annual income of the Guardian", "required");
            //$this->form_validation->set_rules("student_passport_photo", "Passport Photo", "required");
            $this->form_validation->set_rules("age", "Age", "trim|required");
            $this->form_validation->set_rules("gender", "Gender", "required");
            $this->form_validation->set_rules("marital_status", "Marital Status", "required");
            $this->form_validation->set_rules("nationality", "nationality", "required");
            $this->form_validation->set_rules("caste", "Caste", "required");
            $this->form_validation->set_rules("local_address", "Local address of the candidate", "trim|required");
            $this->form_validation->set_rules("local_gu_name_adds", "Name & Address of the local guardian", "trim|required");
            //$this->form_validation->set_rules("rules_condition_one", "Condition", "required");
            //$this->form_validation->set_rules("rules_condition_two", "Condition", "required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editStudentRegister/".$encrypted_id."");
            } else {

              $update_data = array(
                    "course_id" => $this->input->post("course_id"),
                    //"session_id" => $this->input->post("session_id"),
                    "student_name" => $this->input->post("student_name"),
                    "father_or_mother_name" => $this->input->post("father_or_mother_name"),
                    "guardian_name" => $this->input->post("guardian_name"),
                    "perm_address" => $this->input->post("perm_address"),
                    "email" => $this->input->post("email"),
                    "phone_no" => $this->input->post("phone_no"),
                    "corres_address" => $this->input->post("corres_address"),
                    "aadhar_no" => $this->input->post("aadhar_no"),
                    "reg_mobile_no" => $this->input->post("reg_mobile_no"),
                    "gud_annual_income" => $this->input->post("gud_annual_income"),
                    "dob" => $this->input->post("dob"),
                    "age" => $this->input->post("age"),
                    "gender" => $this->input->post("gender"),
                    "marital_status" => $this->input->post("marital_status"),
                    "nationality" => $this->input->post("nationality"),
                    "caste" => $this->input->post("caste"),
                    "local_address" => $this->input->post("local_address"),
                    "local_gu_name_adds" => $this->input->post("local_gu_name_adds"),
                    "status" => $this->input->post("status")
                );

              $where = array(
                "id" => $student_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_student_from",$where);
                //$update =1;

                if ($update) {
                    $education_details= $this->College_model->get_educational_details($student_id);

                    foreach ($education_details as $row) {

                    if ($this->input->post('exam_pass_'.$row['id'].'')!='' AND $this->input->post('subject_'.$row['id'].'')!='' AND $this->input->post('year_pass_'.$row['id'].'')!='' AND $this->input->post('board_'.$row['id'].'')!='' AND $this->input->post('div_obt_'.$row['id'].'')!='') {

                    $exam_data = array(
                    "exam_passed" => $this->input->post("exam_pass_".$row['id'].""),
                    "subject" => $this->input->post("subject_".$row['id'].""),
                    "year_of_pass" => $this->input->post("year_pass_".$row['id'].""),
                    "board_coun_uni" => $this->input->post("board_".$row['id'].""),
                    "div_obt" => $this->input->post("div_obt_".$row['id']."")
                    );


                    $where = array(
                    "id" => $row['id'],
                    );
                    $edu_table_update = $this->my_custom_functions->update_data($exam_data,"tbl_edu_qualification",$where);
                        if ($edu_table_update) {
                         if (isset($_FILES["document_".$row['id'].""])) {

                        if ($_FILES["document_".$row['id'].""]['name'] != "") {

                            //$file_name = $student_from_id.'.'.'jpg';
                            $file_name_old = $row['id'].'_'.basename($_FILES["document_".$row['id'].""]['name']);
                            $file_name = str_replace(' ', '_', $file_name_old);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('document_'.$row['id'].'')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                // Upload thumb image
                                // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                                // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                                // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                //Update database with the cover image url
                                $data = array(
                                    "document_url" => $file_name
                                );

                                $table = "tbl_edu_qualification";

                                $where = array(
                                    "id" => $row['id']
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                    }

                    }
                    }
                    ////////////////File Upload///////////////
                    // Upload Passport Photo
                    $img_upload_message = '';
                    if (isset($_FILES['student_passport_photo'])) {

                        if ($_FILES['student_passport_photo']['name'] != "") {

                            //$file_name = $student_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name_old = $student_id.'_'.basename($_FILES['cover_image_url']['name']);
                            $file_name = str_replace(' ', '_', $file_name_old);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO;
                            $config['allowed_types']        = ALLOWED_PHOTO_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_PHOTO_IMAGE_SIZE;
                            // $config['max_width']  = PASSPORT_WIDTH;
                            // $config['max_height']  = PASSPORT_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('student_passport_photo')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                //Upload thumb image
                                $source = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                // Update database with the cover image url
                                $data = array(
                                    "passport" => $file_name
                                );

                                $table = "tbl_student_from_document";

                                $where = array(
                                    "student_id" => $student_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }

                     // Upload Original copies of mark sheets
                    // if (isset($_FILES['mark_sheets_xerox'])) {

                    //     if ($_FILES['mark_sheets_xerox']['name'] != "") {

                    //         $file_name = $student_from_id.'.'.'jpg';
                    //         //$file_name = $student_from_id.'_'.'jpg';
                    //         //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                    //         $config = array();
                    //         $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                    //         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                    //         $config['file_name']            = $file_name;
                    //         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                    //         $this->load->library('upload');
                    //         $this->upload->initialize($config);

                    //         if (!$this->upload->do_upload('mark_sheets_xerox')) {

                    //             $img_upload_message = $this->upload->display_errors();

                    //         } else {

                    //             // Upload thumb image
                    //             $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                    //             $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                    //             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                    //             // Update database with the cover image url
                    //             // $data = array(
                    //             //     "cover_image_url" => $file_name
                    //             // );

                    //             // $table = "tbl_gallery_albums";

                    //             // $where = array(
                    //             //     "id" => $album_id
                    //             // );
                    //             // $this->ablfunctions->updateData($data, $table, $where);

                    //             // $img_upload_message = 'Cover image uploaded successfully.';
                    //         }
                    //     }
                    // }
                     // Upload Original copies of age verification
                    if (isset($_FILES['age_verification_xerox'])) {

                        if ($_FILES['age_verification_xerox']['name'] != "") {

                            //$file_name = $student_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name_old = $student_id.'_'.basename($_FILES['age_verification_xerox']['name']);
                            $file_name = str_replace(' ', '_', $file_name_old);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_AGE_PROOF;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('age_verification_xerox')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                // Upload thumb image
                                // $source = UPLOAD_DIR.STUDENT_AGE_PROOF.$file_name;
                                // $destination = UPLOAD_DIR.STUDENT_AGE_PROOF_THUMB.$file_name;
                                //
                                // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                // Update database with the cover image url
                                $data = array(
                                    "age_photo" => $file_name
                                );

                                $table = "tbl_student_from_document";

                                $where = array(
                                    "student_id" => $student_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                     // Upload Original copies of SC/ST/OBC/Handicapped Certificate
                    if (isset($_FILES['caste_certificate_xerox'])) {

                        if ($_FILES['caste_certificate_xerox']['name'] != "") {

                            $file_name = $student_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);
                            //$file_name = str_replace(' ', '_', $file_name_old);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_CASTE;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('caste_certificate_xerox')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                // Upload thumb image
                                // $source = UPLOAD_DIR.STUDENT_CASTE.$file_name;
                                // $destination = UPLOAD_DIR.STUDENT_CASTE_THUMB.$file_name;
                                //
                                // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                // Update database with the cover image url
                                $data = array(
                                    "caste_photo" => $file_name
                                );

                                $table = "tbl_student_from_document";

                                $where = array(
                                    "student_id" => $student_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                     // Upload Countersignature of the Guardian
                    if (isset($_FILES['guardian_signature'])) {

                        if ($_FILES['guardian_signature']['name'] != "") {

                            //$file_name = $student_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name_old = $student_id.'_'.basename($_FILES['guardian_signature']['name']);
                            $file_name = str_replace(' ', '_', $file_name_old);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_GURDAIN_SIGN;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            // $config['max_width']  = SIGN_WIDTH;
                            // $config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('guardian_signature')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                // Upload thumb image
                                $source = UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                // Update database with the cover image url
                                $data = array(
                                    "guardian_sign" => $file_name
                                );

                                $table = "tbl_student_from_document";

                                $where = array(
                                    "student_id" => $student_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                     // Upload Signature of Candidate
                    if (isset($_FILES['candidate_signature'])) {

                        if ($_FILES['candidate_signature']['name'] != "") {

                            //$file_name = $student_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name_old = $student_id.'_'.basename($_FILES['candidate_signature']['name']);
                            $file_name = str_replace(' ', '_', $file_name_old);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            // $config['max_width']  = SIGN_WIDTH;
                            // $config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('candidate_signature')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                //Upload thumb image
                                $source = UPLOAD_DIR.STUDENT_SIGN.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_SIGN_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                // Update database with the cover image url
                                $data = array(
                                    "student_sign" => $file_name
                                );

                                $table = "tbl_student_from_document";

                                $where = array(
                                    "student_id" => $student_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                    // Upload Signature of Candidate in full
                    if (isset($_FILES['candidate_signature_full'])) {

                        if ($_FILES['candidate_signature_full']['name'] != "") {

                            //$file_name = $student_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name_old = $student_id.'_'.basename($_FILES['candidate_signature_full']['name']);
                            $file_name = str_replace(' ', '_', $file_name_old);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN_FULL;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            // $config['max_width']  = SIGN_WIDTH;
                            // $config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('candidate_signature_full')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                // Upload thumb image
                                $source = UPLOAD_DIR.STUDENT_SIGN_FULL.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                // Update database with the cover image url
                                $data = array(
                                    "student_sign_full" => $file_name
                                );

                                $table = "tbl_student_from_document";

                                $where = array(
                                    "student_id" => $student_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }

                    }

                    $this->session->set_flashdata("s_message", "Student Admissions record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Student Admissions record.");
                }

                redirect("admin/college/editStudentRegister/".$encrypted_id."");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
            $student_id = $this->ablfunctions->ablDecrypt($encrypted_id);

             $data['education_details']= $this->College_model->get_educational_details($student_id);
             //echo "<pre>";print_r ($data['education_details']);die;
            $data['details'] = $this->ablfunctions->getDetailsFromId($student_id, "tbl_student_from");

            $this->load->view("admin/college/edit_student_register", $data);
        }
    }
         /*
    | --------------------------------------------------------------------------
    | Issue Admit card
    | --------------------------------------------------------------------------
    */
    function issueAdmitCard(){

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('page_id');
            $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("course_id", "Course", "trim|required");
            $this->form_validation->set_rules("admit_card_type", "Admission Type", "trim|required");
            $this->form_validation->set_rules("admit_card_issue_date", "Date", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/issueAdmitCard/".$encrypted_id."");
            } else {

            if ($this->input->post("admit_card_type")==1) {
                $status=3;
            }
            else if ($this->input->post("admit_card_type")==2) {
                $status=4;
            }
            $publish_date = $this->my_custom_functions->database_date($this->input->post('admit_card_issue_date'));
            $publish_hour_post = $this->input->post('start_hour');
            $publish_minute_post = $this->input->post('start_minute');
            $publish_meridian = $this->input->post('start_meridian');
            if ($publish_hour_post < 10) {
                $publish_hour = '0' . $publish_hour_post;
            } else {
                $publish_hour = $publish_hour_post;
            }
            if ($publish_minute_post < 10) {
                $publish_minute = '0' . $publish_minute_post;
            } else {
                $publish_minute = $publish_minute_post;
            }

            $publish_date_time = strtotime($publish_date . " " . $publish_hour . ":" . $publish_minute . $publish_meridian);

              $update_data = array(
                  "course_id" => $this->input->post("course_id"),
                  "admit_card_type" => $this->input->post("admit_card_type"),
                  "admit_card_issue_date" => $publish_date_time,
                  "status" => $status
              );
              $where = array(
                "id" => $page_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_student_from",$where);

                if ($update) {

                    $this->session->set_flashdata("s_message", "successfully Issue Admit card.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to Issue Admit card.");
                }

                redirect("admin/college/editStudentRegister/".$encrypted_id."");

            }
        } else {

        $encrypted_id = $this->uri->segment(4);
        $from_id = $this->ablfunctions->ablDecrypt($encrypted_id);
        $data['card_details'] = $this->ablfunctions->getDetailsFromId($from_id, "tbl_student_from");
        $data['principal'] = $this->my_custom_functions->get_multiple_data("tbl_staff",'and designation="1" LIMIT 1');
        $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');

        $this->load->view("admin/college/admit_card_view", $data);
        }
    }

             /*
    | --------------------------------------------------------------------------
    | delete Image
    | --------------------------------------------------------------------------
    */
    function deletePassportImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
        $path=UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$document_details['passport'];
        $path_thu=UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$document_details['passport'];
        //echo $path;die;
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "passport" =>'',
              );

              $table = "tbl_student_from_document";

              $where = array(
                    "student_id" =>$student_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
             redirect("admin/college/editStudentRegister/".$cript_id."");


    }
    function deleteAgeproofImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
        $path=UPLOAD_DIR.STUDENT_AGE_PROOF.$document_details['age_photo'];
        $path_thu=UPLOAD_DIR.STUDENT_AGE_PROOF_THUMB.$document_details['age_photo'];
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "age_photo" =>'',
              );

              $table = "tbl_student_from_document";

              $where = array(
                    "student_id" =>$student_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
             redirect("admin/college/editStudentRegister/".$cript_id."");


    }
    function deleteCasteImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
        $path=UPLOAD_DIR.STUDENT_CASTE.$document_details['caste_photo'];
        $path_thu=UPLOAD_DIR.STUDENT_CASTE_THUMB.$document_details['caste_photo'];
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "caste_photo" =>'',
              );

              $table = "tbl_student_from_document";

              $where = array(
                    "student_id" =>$student_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStudentRegister/".$cript_id."");


    }
    function deleteGdsignImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
        $path=UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$document_details['guardian_sign'];
        $path_thu=UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$document_details['guardian_sign'];
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "guardian_sign" =>'',
              );

              $table = "tbl_student_from_document";

              $where = array(
                    "student_id" =>$student_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStudentRegister/".$cript_id."");


    }
    function deleteStsignImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
        $path=UPLOAD_DIR.STUDENT_SIGN.$document_details['student_sign'];
        $path_thu=UPLOAD_DIR.STUDENT_SIGN_THUMB.$document_details['student_sign'];
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "student_sign" =>'',
              );

              $table = "tbl_student_from_document";

              $where = array(
                    "student_id" =>$student_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStudentRegister/".$cript_id."");


    }
    function deleteStfsignImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
        $path=UPLOAD_DIR.STUDENT_SIGN_FULL.$document_details['student_sign_full'];
        $path_thu=UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$document_details['student_sign_full'];
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "student_sign_full" =>'',
              );

              $table = "tbl_student_from_document";

              $where = array(
                    "student_id" =>$student_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStudentRegister/".$cript_id."");

    }
    function deleteStaffPhoto($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.STAFF_PHOTO.$student_id.".jpg";
        $path_thu=UPLOAD_DIR.STAFF_PHOTO_THUMB.$student_id.".jpg";
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStaff/".$cript_id."");

    }
    function deleteStaffSign($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.STAFF_SIGN.$student_id.".jpg";
        $path_thu=UPLOAD_DIR.STAFF_SIGN_THUMB.$student_id.".jpg";
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStaff/".$cript_id."");

    }
    function deleteMarkSheetImage($document_url,$student_id,$edu_table_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.STUDENT_MARKSHEET.$document_url;
        $path_thu=UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$document_url;
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "document_url" =>'',
              );

              $table = "tbl_edu_qualification";

              $where = array(
                    "id" => $edu_table_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        if ($delete) {
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStudentRegister/".$cript_id."");
        }



    }

    function deletePrincipalImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.PRINCIPAL_SIGN.$student_id.".jpg";
        $path_thu=UPLOAD_DIR.PRINCIPAL_SIGN_THUMB.$student_id.".jpg";
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editPrincipal/".$cript_id."");


    }



        function manageNotice() {
        $data['page_title'] = 'Manage notice';
        $data['details'] = $this->my_custom_functions->get_multiple_data('tbl_notice', 'ORDER BY issue_date DESC');
        $this->load->view('admin/college/manage_notice', $data);
    }
        /*
    | --------------------------------------------------------------------------
    | Edit page
    | --------------------------------------------------------------------------
    */
    function issueNotice() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $this->form_validation->set_rules("notice_heading", "Notice Heading", "trim|required");
            $this->form_validation->set_rules("notice_text", "Notice Text", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/issueNotice");

            }else{
            $data = array(
                    'notice_heading' => $this->input->post('notice_heading'),
                    'issue_date' => date('Y-m-d'),
                    'notice_text' => $this->input->post('notice_text'),
                    'status' => 1
                );
            $notice_id = $this->my_custom_functions->insert_data_last_id($data, TBL_NOTICE);

                     if ($notice_id) {
                         if (isset($_FILES['doc_upload'])) {

                        if ($_FILES['doc_upload']['name'] != "") {

                            //$file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name = $notice_id.'_'.basename($_FILES['doc_upload']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.NOTICE_DOCUMENT;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            //$config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('doc_upload')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {
                                $data = array(
                                    "document_url" => $file_name
                                );

                                $table = "tbl_notice";

                                $where = array(
                                    "id" => $notice_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                    $this->session->set_flashdata("s_message", "Notice  successfully Register.");
                    redirect("admin/college/manageNotice");
                    }
                    }
    } else {
            $data['page_title'] = 'Add notice';
            $this->load->view('admin/college/issue_notice', $data);
        }
    }
    function editNotice() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $encrypted_id = $this->input->post('notice_id');
            $notice_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("notice_heading", "Notice Heading", "trim|required");
            $this->form_validation->set_rules("notice_text", "Notice Text", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editNotice/".$encrypted_id."");

            }else{

             $data = array(
                    'notice_heading' => $this->input->post('notice_heading'),
                    'notice_text' => $this->input->post('notice_text'),
                    'status' => $this->input->post('status')
                );
             $condition = array(
                "id" => $notice_id
                );
            $update_id = $this->my_custom_functions->update_data($data,TBL_NOTICE, $condition);


                         if (isset($_FILES['doc_upload'])) {
                        if ($update_id) {

                        if ($_FILES['doc_upload']['name'] != "") {

                            //$file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name_old = $notice_id.'_'.basename($_FILES['doc_upload']['name']);
                            $file_name = str_replace(' ', '_', $file_name_old);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.NOTICE_DOCUMENT;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            //$config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('doc_upload')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {
                                $data = array(
                                    "document_url" => $file_name
                                );

                                $table = "tbl_notice";

                                $where = array(
                                    "id" => $notice_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                    $this->session->set_flashdata("s_message", "Notice  successfully Updated.");
                    redirect("admin/college/manageNotice");
                    }
                }
            } else {
            $data['page_title'] = 'Edit notice';
            $notice_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));

            $data['notice_detail'] = $this->my_custom_functions->get_details_from_id($notice_id, TBL_NOTICE);
            $this->load->view('admin/college/edit_notice', $data);
        }
    }
    function deleteNoticeFile($file,$file_id){

        $data = array(
                "document_url" =>'',
                );
        $where = array(
                "id" => $file_id
                );
        $this->ablfunctions->updateData($data,"tbl_notice", $where);
        $cript_id = $this->ablfunctions->ablDecrypt($file_id);
        $path=UPLOAD_DIR.NOTICE_DOCUMENT.$file;
        @unlink(''.$path.'');
        $this->session->set_flashdata("s_message", "File successfully Deleted.");
        redirect("admin/college/editNotice/".$cript_id."");


    }
    /*
    | --------------------------------------------------------------------------
    | Delete Attendance
    | --------------------------------------------------------------------------
    */
    function deleteNotice($file,$file_id){
      $query = $this->ablfunctions->deleteData("tbl_notice", array("id" => $file_id));
      $path=UPLOAD_DIR.NOTICE_DOCUMENT.$file;
      @unlink(''.$path.'');
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect('admin/college/manageNotice');
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect('admin/college/manageNotice');
    }
    }
    function manageStudent() {
    $data['page_title'] = 'Manage Student';
    $data['details'] = $this->my_custom_functions->get_multiple_data('tbl_student', 'ORDER BY id DESC');
    $this->load->view('admin/college/manage_student', $data);
}
/*
| --------------------------------------------------------------------------
| Edit Classes
| --------------------------------------------------------------------------
*/
function editStudent() {

    if ($this->input->post("submit") && $this->input->post("submit") != "") {

        $encrypted_id = $this->input->post('class_id');
        $class_id = $this->ablfunctions->ablDecrypt($encrypted_id);

        $this->form_validation->set_rules("name", "Name", "trim|required");
        $this->form_validation->set_rules("email", "Email", "trim|required");

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata("e_message", validation_errors());
            redirect("admin/college/editStudent/".$encrypted_id."");
        } else {

          $update_data = array(
              "name" => $this->input->post("name"),
              "email" => $this->input->post("email")
          );
          $where = array(
            "id" => $class_id,
          );

            $update = $this->my_custom_functions->update_data($update_data,"tbl_student",$where);

            if ($update) {

                $this->session->set_flashdata("s_message", "Record updated successfully.");
            } else {

                $this->session->set_flashdata('e_message', "Failed to update the Record.");
            }

            redirect("admin/college/manageStudent");
        }
    } else {

        $encrypted_id = $this->uri->segment(4);
        $class_id = $this->ablfunctions->ablDecrypt($encrypted_id);
        $data['details'] = $this->ablfunctions->getDetailsFromId($class_id, "tbl_student");

        $this->load->view("admin/college/edit_student", $data);
    }
}
        ////////////////////////////////////////////////////////////////////////////
    // Manage main fees structure records
    ////////////////////////////////////////////////////////////////////////////
    function manageFeesStructure() {

        $data['page_title'] = 'Manage fees structure';

        $data['details'] = $this->my_custom_functions->get_multiple_data('tbl_fees_structure', 'ORDER BY id DESC');

        $this->load->view('admin/college/manage_fees_structure', $data);
    }
        ////////////////////////////////////////////////////////////////////////////
    // Add main fees structure record for a semester
    ////////////////////////////////////////////////////////////////////////////
    function addFeesStructure() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $semester_id = $this->input->post('semester_id');
            $cycle_id = $this->input->post('cycle_id');
            $general_comment = $this->input->post('comment');

            $existing_fees_for_this_semester = $this->my_custom_functions->get_perticular_count('tbl_fees_structure', 'and semester_id="' . $semester_id . '"');

            if ($existing_fees_for_this_semester > 0) {

                $this->session->set_flashdata("e_message", 'There already exists a fees structure for this semester.');
                redirect("admin/college/manageFeesStructure");
            } else {

                $add_data = array(
                    'semester_id' => $semester_id,
                    'cycle_type' => $cycle_id,
                    'general_comment' => $general_comment
                );
                $fees_structure_id = $this->my_custom_functions->insert_data($add_data, 'tbl_fees_structure');

                redirect('admin/college/feesStructureBreakup/' . $this->ablfunctions->ablEncrypt($fees_structure_id));
            }
        } else {

            $data['page_title'] = 'Add fees structure';

            $data['semesters'] = $this->my_custom_functions->get_multiple_data("tbl_semester",'');

            $this->load->view('admin/college/add_fees_structure', $data);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    // Edit main fees structure record for a class
    ////////////////////////////////////////////////////////////////////////////
    function editFeesStructure() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $update = $this->College_model->update_fees_structure();

            if ($update) {

                $this->session->set_flashdata("s_message", 'Fees structure successfully updated.');
                redirect("admin/college/manageFeesStructure");
            } else {

                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("admin/college/manageFeesStructure");
            }
        } else {

            $data['page_title'] = 'Edit fees structure';

            $fees_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
            $data['fees_structure_detail'] = $this->my_custom_functions->get_details_from_id($fees_id, TBL_FEES_STRUCTURE);

            $this->load->view('admin/college/edit_fees_structure', $data);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    // Delete main fees structure record as well as the breakups
    ////////////////////////////////////////////////////////////////////////////
    function deleteFeesStructure() {

        $fees_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));

        $delete = $this->College_model->delete_fees_structure($fees_id);

        if ($delete) {
            $this->session->set_flashdata("s_message", 'Fees Structure successfully Deleted.');
            redirect("admin/college/manageFeesStructure");
        } else {
            $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
            redirect("admin/college/manageFeesStructure");
        }
    }
        ////////////////////////////////////////////////////////////////////////////
    // Add break ups to a main fees structure
    ////////////////////////////////////////////////////////////////////////////
    function feesStructureBreakup() {

        $data['page_title'] = 'Fees structure break up';

        if ($this->input->post('submit')) {

            $this->College_model->insert_fees_breakup();

            $this->session->set_flashdata("s_message", "Successfully added fees structure breakups.");
            redirect('admin/college/editFeesStructureBreakup/' . $this->ablfunctions->ablEncrypt($this->input->post('fees_id')));
        } else {

            $fees_structure_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
            $data['fees_structure'] = $this->my_custom_functions->get_details_from_id($fees_structure_id, 'tbl_fees_structure');

            $this->load->view('admin/college/add_fees_structure_breakup', $data);
        }
    }
        ////////////////////////////////////////////////////////////////////////////
    // Edit break ups of a main fees structure
    ////////////////////////////////////////////////////////////////////////////
    function editFeesStructureBreakup() {

        $data['page_title'] = 'Fees structure break up';

        if ($this->input->post('submit')) {

            $this->College_model->update_fees_breakup();

            $this->session->set_flashdata("s_message", "Successfully updated fees structure breakups.");
            redirect('admin/college/editFeesStructureBreakup/' . $this->ablfunctions->ablEncrypt($this->input->post('fees_id')));
        } else {

            $fees_structure_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
            $data['fees_structure'] = $this->my_custom_functions->get_details_from_id($fees_structure_id, TBL_FEES_STRUCTURE);
            $data['breakups'] = $this->my_custom_functions->get_multiple_data(TBL_FEES_STRUCTURE_BREAKUPS, ' and fees_id="' . $fees_structure_id . '"');

            $this->load->view('admin/college/edit_fees_structure_breakup', $data);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    // Edit break ups of a main fees structure
    ////////////////////////////////////////////////////////////////////////////
    function manualFeesBreakups() {

        $data['page_title'] = 'Set Fees Manually';

        // Update the fees
        if ($this->input->post('submit')) {

            $this->College_model->manuallly_update_fees_breakup();

            $this->session->set_flashdata("s_message", "Fees structure breakup updated manually.");
            redirect('admin/college/manualFeesBreakups/' . $this->ablfunctions->ablEncrypt($this->input->post('student_id')));
        }
        // Update the manual fees setting
        else if ($this->input->post('setting_submit')) {

            $this->College_model->update_manual_fees_setting();

            $this->session->set_flashdata("s_message", "Manual fees setting has been updated successfully.");
            redirect('admin/college/manualFeesBreakups/' . $this->ablfunctions->ablEncrypt($this->input->post('setting_student_id')));
        } else {

            if ($this->uri->segment(4)) {

                $student_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
                $data['student_details'] = $this->my_custom_functions->get_details_from_id($student_id,"tbl_student_from");
                $data['student_semesters'] = $this->my_custom_functions->get_semesters_of_a_student($student_id, $this->session->userdata('session_id'));

                if($this->input->post('manual_fees_semester')) {

                    $data['fees_structure'] = $this->my_custom_functions->get_details_from_id("", TBL_FEES_STRUCTURE, array("semester_id" => $this->input->post('manual_fees_semester')));

                    if (!empty($data['fees_structure'])) {
                        $data['breakups'] = $this->College_model->get_fees_breakups_for_manual_fees($data['fees_structure']['id'], $student_id);
                        //echo "<pre>";print_r($data['breakups']);die;
                    }
                }

                $this->load->view('admin/college/manually_set_fees_breakups', $data);
            } else {
                redirect('admin/college/manageStudent');
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Edit main fees structure record for a class
    ////////////////////////////////////////////////////////////////////////////
/*
| --------------------------------------------------------------------------
| Add Student From Register
| --------------------------------------------------------------------------
*/
function addStudentManual() {

if ($this->input->post("submit") && $this->input->post("submit") != "") {

    //echo "<pre>";print_r ($_POST);die;


    $this->form_validation->set_rules("course_id", "Select Course", "required");
    //$this->form_validation->set_rules("session_id", "Select session", "required");
    // $this->form_validation->set_rules("student_name", "Name of the Candidate", "trim|required");
    // $this->form_validation->set_rules("father_or_mother_name", "Father’s /Mother’s Name", "trim|required");
    // $this->form_validation->set_rules("guardian_name", "Name of the Guardian", "trim|required");
    // $this->form_validation->set_rules("perm_address", "Permanent Address", "trim|required");
    // $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
    // //$this->form_validation->set_rules("phone_number", "Phone Number", "trim|required|is_unique[tbl_student_from.phone_number]");
    // $this->form_validation->set_rules("phone_no", "Phone Number", "trim|required|regex_match[/^[0-9]{10}$/]");
    // $this->form_validation->set_rules("corres_address", "Address for correspondence", "trim|required");
    // $this->form_validation->set_rules("aadhar_no", "Aadhar No", "trim|required|min_length[12]|max_length[12]");
    // $this->form_validation->set_rules("reg_mobile_no", "Registered Mobile No", "trim|required|regex_match[/^[0-9]{10}$/]");
    // $this->form_validation->set_rules("dob", "D.O.B", "trim|required");
    //
    // //$this->form_validation->set_rules("student_passport_photo", "Passport Photo", "required");
    // $this->form_validation->set_rules("age", "Age", "trim|required");
    // $this->form_validation->set_rules("gender", "Gender", "required");
    // $this->form_validation->set_rules("marital_status", "Marital Status", "required");
    // $this->form_validation->set_rules("nationality", "nationality", "required");
    // $this->form_validation->set_rules("caste", "Caste", "required");
    // $this->form_validation->set_rules("local_address", "Local address of the candidate", "trim|required");
    // $this->form_validation->set_rules("local_gu_name_adds", "Name & Address of the local guardian", "trim|required");
    // $this->form_validation->set_rules("rules_condition_one", "Condition", "required");
    // $this->form_validation->set_rules("rules_condition_two", "Condition", "required");
    // $this->form_validation->set_rules("rules_condition_first", "Condition", "required");

    if ($this->form_validation->run() == false) {

        $this->session->set_flashdata("e_message", validation_errors());
    // $data['session_details']= $this->Main_model->get_current_session();
    // $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_classes",'');
    // $this->load->view("student_from", $data);
    redirect("admin/college/addStudentManual");
  } else {

      if ($this->input->post('rules_condition_first')!='') {
           $rules_condition_first=1;
        }else{ $rules_condition_one=0; }
      if ($this->input->post('rules_condition_one')!='') {
           $rules_condition_one=1;
        }else{ $rules_condition_one=0; }
      if ($this->input->post('rules_condition_two')!='') {
           $rules_condition_two=1;
        }else{ $rules_condition_two=0; }

      $insert_data = array(
          "student_id" => $this->session->userdata('user_id'),
          "course_id" => $this->input->post("course_id"),
          "session_id" => $this->input->post("session_id"),
          "student_name" => $this->input->post("student_name"),
          "father_or_mother_name" => $this->input->post("father_or_mother_name"),
          "guardian_name" => $this->input->post("guardian_name"),
          "perm_address" => $this->input->post("perm_address"),
          "email" => $this->input->post("email"),
          "phone_no" => $this->input->post("phone_no"),
          "corres_address" => $this->input->post("corres_address"),
          "aadhar_no" => $this->input->post("aadhar_no"),
          "reg_mobile_no" => $this->input->post("reg_mobile_no"),
          "dob" => $this->input->post("dob"),
          "age" => $this->input->post("age"),
          "gender" => $this->input->post("gender"),
          "marital_status" => $this->input->post("marital_status"),
          "nationality" => $this->input->post("nationality"),
          "caste" => $this->input->post("caste"),
          "local_address" => $this->input->post("local_address"),
          "local_gu_name_adds" => $this->input->post("local_gu_name_adds"),
          "date" => date(DATE_FORMAT),
          "rules_condition_first" => $rules_condition_first,
          "rules_condition_one" => $rules_condition_one,
          "rules_condition_two" => $rules_condition_two,
          "admit_card_type" =>0,
          "status" => 1
      );

      $student_from_id = $this->my_custom_functions->insert_data($insert_data,"tbl_student_from");///////  insert to user table

      $student_semester_link_data = array(
          'student_id' => $student_from_id,
          'class_id' => $this->input->post("course_id"),
          'semester_id' => $this->input->post("semester_id"),
          'roll_no' => "",
          'enrollment_time' => time()
      );
      $this->my_custom_functions->insert_data($student_semester_link_data, TBL_STUDENT_ENROLLMENT);


      if ($student_from_id) {

          if ($this->input->post('exam_pass_one')!='' AND $this->input->post('subject_one')!='' AND $this->input->post('year_pass_one')!='' AND $this->input->post('board_one')!='' AND$this->input->post('div_obt_one')!='') {

              $exam_data = array(
          "student_from_id" => $student_from_id,
          "exam_passed" => $this->input->post("exam_pass_one"),
          "subject" => $this->input->post("subject_one"),
          "year_of_pass" => $this->input->post("year_pass_one"),
          "board_coun_uni" => $this->input->post("board_one"),
          "div_obt" => $this->input->post("div_obt_one")
          );

          $edu_table_id =$this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
              if ($edu_table_id) {
               if (isset($_FILES['student_document_one'])) {

              if ($_FILES['student_document_one']['name'] != "") {

                  //$file_name = $student_from_id.'.'.'jpg';
                  //$file_name = $student_from_id.'_'.'jpg';
                  $file_name_old = $edu_table_id.'_'.basename($_FILES['student_document_one']['name']);
                  $file_name = str_replace(' ', '_', $file_name_old);

                  $config = array();
                  $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                  $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                  $config['file_name']            = $file_name;
                  $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                  $this->load->library('upload');
                  $this->upload->initialize($config);

                  if (!$this->upload->do_upload('student_document_one')) {

                      $img_upload_message = $this->upload->display_errors();

                  } else {

                      // Upload thumb image
                      // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                      // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                      // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                      //Update database with the cover image url
                      $data = array(
                          "document_url" => $file_name
                      );

                      $table = "tbl_edu_qualification";

                      $where = array(
                          "id" => $edu_table_id
                      );
                      $this->ablfunctions->updateData($data, $table, $where);

                      // $img_upload_message = 'Cover image uploaded successfully.';
                  }
              }
          }
          }
          }

          if ($this->input->post('exam_pass_two')!='' AND $this->input->post('subject_two')!='' AND $this->input->post('year_pass_two')!='' AND $this->input->post('board_two')!='' AND$this->input->post('div_obt_two')!='') {

              $exam_data = array(
          "student_from_id" => $student_from_id,
          "exam_passed" => $this->input->post("exam_pass_two"),
          "subject" => $this->input->post("subject_two"),
          "year_of_pass" => $this->input->post("year_pass_two"),
          "board_coun_uni" => $this->input->post("board_two"),
          "div_obt" => $this->input->post("div_obt_two")
          );

          $edu_table_id = $this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
              if ($edu_table_id) {
               if (isset($_FILES['student_document_two'])) {

              if ($_FILES['student_document_two']['name'] != "") {

                  //$file_name = $student_from_id.'.'.'jpg';
                  //$file_name = $student_from_id.'_'.'jpg';
                  $file_name_old = $edu_table_id.'_'.basename($_FILES['student_document_two']['name']);
                  $file_name = str_replace(' ', '_', $file_name_old);

                  $config = array();
                  $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                  $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                  $config['file_name']            = $file_name;
                  $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                  $this->load->library('upload');
                  $this->upload->initialize($config);

                  if (!$this->upload->do_upload('student_document_two')) {

                      $img_upload_message = $this->upload->display_errors();

                  } else {

                      // Upload thumb image
                      // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                      // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                      // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                      //Update database with the cover image url
                      $data = array(
                          "document_url" => $file_name
                      );

                      $table = "tbl_edu_qualification";

                      $where = array(
                          "id" => $edu_table_id
                      );
                      $this->ablfunctions->updateData($data, $table, $where);

                      // $img_upload_message = 'Cover image uploaded successfully.';
                  }
              }
          }
          }
          }

          if ($this->input->post('exam_pass_three')!='' AND $this->input->post('subject_three')!='' AND $this->input->post('year_pass_three')!='' AND $this->input->post('board_three')!='' AND$this->input->post('div_obt_three')!='') {

              $exam_data = array(
          "student_from_id" => $student_from_id,
          "exam_passed" => $this->input->post("exam_pass_three"),
          "subject" => $this->input->post("subject_three"),
          "year_of_pass" => $this->input->post("year_pass_three"),
          "board_coun_uni" => $this->input->post("board_three"),
          "div_obt" => $this->input->post("div_obt_three")
          );

          $edu_table_id = $this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
              if ($edu_table_id) {
               if (isset($_FILES['student_document_three'])) {

              if ($_FILES['student_document_three']['name'] != "") {

                  //$file_name = $student_from_id.'.'.'jpg';
                  //$file_name = $student_from_id.'_'.'jpg';
                  $file_name_old = $edu_table_id.'_'.basename($_FILES['student_document_three']['name']);
                  $file_name = str_replace(' ', '_', $file_name_old);

                  $config = array();
                  $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                  $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                  $config['file_name']            = $file_name;
                  $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                  $this->load->library('upload');
                  $this->upload->initialize($config);

                  if (!$this->upload->do_upload('student_document_three')) {

                      $img_upload_message = $this->upload->display_errors();

                  } else {

                      // Upload thumb image
                      // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                      // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                      // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                      //Update database with the cover image url
                      $data = array(
                          "document_url" => $file_name
                      );

                      $table = "tbl_edu_qualification";

                      $where = array(
                          "id" => $edu_table_id
                      );
                      $this->ablfunctions->updateData($data, $table, $where);

                      // $img_upload_message = 'Cover image uploaded successfully.';
                  }
              }
          }
          }
          }

          if ($this->input->post('exam_pass_four')!='' AND $this->input->post('subject_four')!='' AND $this->input->post('year_pass_four')!='' AND $this->input->post('board_four')!='' AND$this->input->post('div_obt_four')!='') {

              $exam_data = array(
          "student_from_id" => $student_from_id,
          "exam_passed" => $this->input->post("exam_pass_four"),
          "subject" => $this->input->post("subject_four"),
          "year_of_pass" => $this->input->post("year_pass_four"),
          "board_coun_uni" => $this->input->post("board_four"),
          "div_obt" => $this->input->post("div_obt_four")
          );

          $edu_table_id = $this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
              if ($edu_table_id) {
               if (isset($_FILES['student_document_four'])) {

              if ($_FILES['student_document_four']['name'] != "") {

                  //$file_name = $student_from_id.'.'.'jpg';
                  //$file_name = $student_from_id.'_'.'jpg';
                  $file_name_old = $edu_table_id.'_'.basename($_FILES['student_document_four']['name']);
                  $file_name = str_replace(' ', '_', $file_name_old);

                  $config = array();
                  $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                  $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                  $config['file_name']            = $file_name;
                  $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                  $this->load->library('upload');
                  $this->upload->initialize($config);

                  if (!$this->upload->do_upload('student_document_four')) {

                      $img_upload_message = $this->upload->display_errors();

                  } else {

                      // Upload thumb image
                      // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                      // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                      // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                      //Update database with the cover image url
                      $data = array(
                          "document_url" => $file_name
                      );

                      $table = "tbl_edu_qualification";

                      $where = array(
                          "id" => $edu_table_id
                      );
                      $this->ablfunctions->updateData($data, $table, $where);

                      // $img_upload_message = 'Cover image uploaded successfully.';
                  }
              }
          }
          }
          }


          // Upload Passport Photo
          $img_upload_message = '';
          if (isset($_FILES['student_passport'])) {

              if ($_FILES['student_passport']['name'] != "") {

                  //$file_name = $student_from_id.'.'.'jpg';
                  //$file_name = $student_from_id.'_'.'jpg';
                  $file_name_old = $student_from_id.'_'.basename($_FILES['student_passport']['name']);
                  $file_name = str_replace(' ', '_', $file_name_old);

                  $config = array();
                  $config['upload_path']          = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO;
                  $config['allowed_types']        = ALLOWED_PHOTO_IMAGE_TYPES;
                  $config['file_name']            = $file_name;
                  $config['max_size']             = ALLOWED_PHOTO_IMAGE_SIZE;
                  // $config['max_width']  = PASSPORT_WIDTH;
                  // $config['max_height']  = PASSPORT_HEIGHT;

                  $this->load->library('upload');
                  $this->upload->initialize($config);

                  if (!$this->upload->do_upload('student_passport')) {

                      $img_upload_message = $this->upload->display_errors();

                  } else {

                      //Upload thumb image
                      $source = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$file_name;
                      $destination = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$file_name;

                      $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                      //Update database with the cover image url
                      $file_data = array(
                          "student_id" => $student_from_id,
                          "passport" => $file_name,
                          "age_photo" => '',
                          "caste_photo" => '',
                          "guardian_sign" => '',
                          "student_sign" => '',
                          "student_sign_full" => ''
                      );
                      //echo "<pre>";print_r($file_data);die;

                      $this->my_custom_functions->insert_data($file_data,"tbl_student_from_document");

                      // $img_upload_message = 'Cover image uploaded successfully.';
                  }
              }
          }

           // Upload Original copies of mark sheets
          // if (isset($_FILES['mark_sheets_xerox'])) {

          //     if ($_FILES['mark_sheets_xerox']['name'] != "") {

          //         $file_name = $student_from_id.'.'.'jpg';
          //         //$file_name = $student_from_id.'_'.'jpg';
          //         //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

          //         $config = array();
          //         $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
          //         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
          //         $config['file_name']            = $file_name;
          //         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

          //         $this->load->library('upload');
          //         $this->upload->initialize($config);

          //         if (!$this->upload->do_upload('mark_sheets_xerox')) {

          //             $img_upload_message = $this->upload->display_errors();

          //         } else {

          //             // Upload thumb image
          //             $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
          //             $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

          //             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

          //             // Update database with the cover image url
          //             // $data = array(
          //             //     "cover_image_url" => $file_name
          //             // );

          //             // $table = "tbl_gallery_albums";

          //             // $where = array(
          //             //     "id" => $album_id
          //             // );
          //             // $this->ablfunctions->updateData($data, $table, $where);

          //             // $img_upload_message = 'Cover image uploaded successfully.';
          //         }
          //     }
          // }
           // Upload Original copies of age verification
          if (isset($_FILES['student_age_verification'])) {

              if ($_FILES['student_age_verification']['name'] != "") {

                  //$file_name = $student_from_id.'.'.'jpg';
                  //$file_name = $student_from_id.'_'.'jpg';
                  $file_name_old = $student_from_id.'_'.basename($_FILES['student_age_verification']['name']);
                  $file_name = str_replace(' ', '_', $file_name_old);

                  $config = array();
                  $config['upload_path']          = UPLOAD_DIR.STUDENT_AGE_PROOF;
                  $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                  $config['file_name']            = $file_name;
                  $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                  $this->load->library('upload');
                  $this->upload->initialize($config);

                  if (!$this->upload->do_upload('student_age_verification')) {

                      $img_upload_message = $this->upload->display_errors();

                  }
                  else {

                      //Upload thumb image
                      // $source = UPLOAD_DIR.STUDENT_AGE_PROOF.$file_name;
                      // $destination = UPLOAD_DIR.STUDENT_AGE_PROOF_THUMB.$file_name;
                      //
                      // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                      //Update database with the cover image url
                      $data = array(
                          "age_photo" => $file_name
                      );

                      $table = "tbl_student_from_document";

                      $where = array(
                          "student_id" => $student_from_id
                      );
                      $this->ablfunctions->updateData($data, $table, $where);

                  }
              }
          }
           // Upload Original copies of SC/ST/OBC/Handicapped Certificate
          if (isset($_FILES['student_caste_cf'])) {

              if ($_FILES['student_caste_cf']['name'] != "") {

                  //$file_name = $student_from_id.'.'.'jpg';
                  //$file_name = $student_from_id.'_'.'jpg';
                  $file_name_old = $student_from_id.'_'.basename($_FILES['student_caste_cf']['name']);
                  $file_name = str_replace(' ', '_', $file_name_old);

                  $config = array();
                  $config['upload_path']          = UPLOAD_DIR.STUDENT_CASTE;
                  $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                  $config['file_name']            = $file_name;
                  $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                  $this->load->library('upload');
                  $this->upload->initialize($config);

                  if (!$this->upload->do_upload('student_caste_cf')) {

                      $img_upload_message = $this->upload->display_errors();

                  }
                  else {

                      // Upload thumb image
                      // $source = UPLOAD_DIR.STUDENT_CASTE.$file_name;
                      // $destination = UPLOAD_DIR.STUDENT_CASTE_THUMB.$file_name;
                      //
                      // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                      //Update database with the cover image url
                      $data = array(
                          "caste_photo" => $file_name
                      );

                      $table = "tbl_student_from_document";

                      $where = array(
                          "student_id" => $student_from_id
                      );
                      $this->ablfunctions->updateData($data, $table, $where);


                  }
              }
          }
           // Upload Countersignature of the Guardian
          if (isset($_FILES['student_guardian_sign'])) {

              if ($_FILES['student_guardian_sign']['name'] != "") {

                  //$file_name = $student_from_id.'.'.'jpg';
                  //$file_name = $student_from_id.'_'.'jpg';
                  $file_name_old = $student_from_id.'_'.basename($_FILES['student_guardian_sign']['name']);
                  $file_name = str_replace(' ', '_', $file_name_old);

                  $config = array();
                  $config['upload_path']          = UPLOAD_DIR.STUDENT_GURDAIN_SIGN;
                  $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                  $config['file_name']            = $file_name;
                  $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                  // $config['max_width']  = SIGN_WIDTH;
                  // $config['max_height']  = SIGN_HEIGHT;

                  $this->load->library('upload');
                  $this->upload->initialize($config);

                  if (!$this->upload->do_upload('student_guardian_sign')) {

                      $img_upload_message = $this->upload->display_errors();

                  } else {

                      // Upload thumb image
                      $source = UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$file_name;
                      $destination = UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$file_name;

                      $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                      //Update database with the cover image url
                      $data = array(
                          "guardian_sign" => $file_name
                      );

                      $table = "tbl_student_from_document";

                      $where = array(
                          "student_id" => $student_from_id
                      );
                      $this->ablfunctions->updateData($data, $table, $where);


                  }
              }
          }
           // Upload Signature of Candidate
          if (isset($_FILES['student_candidate_sign'])) {

              if ($_FILES['student_candidate_sign']['name'] != "") {

                  //$file_name = $student_from_id.'.'.'jpg';
                  //$file_name = $student_from_id.'_'.'jpg';
                  $file_name_old = $student_from_id.'_'.basename($_FILES['student_candidate_sign']['name']);
                  $file_name = str_replace(' ', '_', $file_name_old);

                  $config = array();
                  $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN;
                  $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                  $config['file_name']            = $file_name;
                  $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                  //$config['max_width']  = SIGN_WIDTH;
                  //$config['max_height']  = SIGN_HEIGHT;

                  $this->load->library('upload');
                  $this->upload->initialize($config);

                  if (!$this->upload->do_upload('student_candidate_sign')) {

                      $img_upload_message = $this->upload->display_errors();

                  } else {

                      //Upload thumb image
                      $source = UPLOAD_DIR.STUDENT_SIGN.$file_name;
                      $destination = UPLOAD_DIR.STUDENT_SIGN_THUMB.$file_name;

                      $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                      //Update database with the cover image url
                      $data = array(
                          "student_sign" => $file_name
                      );

                      $table = "tbl_student_from_document";

                      $where = array(
                          "student_id" => $student_from_id
                      );
                      $this->ablfunctions->updateData($data, $table, $where);


                  }
              }
          }
          // Upload Signature of Candidate in full
          if (isset($_FILES['student_candidate_sign_full'])) {

              if ($_FILES['student_candidate_sign_full']['name'] != "") {

                  //$file_name = $student_from_id.'.'.'jpg';
                  //$file_name = $student_from_id.'_'.'jpg';
                  $file_name_old = $student_from_id.'_'.basename($_FILES['student_candidate_sign_full']['name']);
                  $file_name = str_replace(' ', '_', $file_name_old);

                  $config = array();
                  $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN_FULL;
                  $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                  $config['file_name']            = $file_name;
                  $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                  //$config['max_width']  = SIGN_WIDTH;
                  //$config['max_height']  = SIGN_HEIGHT;

                  $this->load->library('upload');
                  $this->upload->initialize($config);

                  if (!$this->upload->do_upload('student_candidate_sign_full')) {

                      $img_upload_message = $this->upload->display_errors();

                  } else {

                      // Upload thumb image
                      $source = UPLOAD_DIR.STUDENT_SIGN_FULL.$file_name;
                      $destination = UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$file_name;

                      $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                      //Update database with the cover image url
                      $data = array(
                          "student_sign_full" => $file_name
                      );

                      $table = "tbl_student_from_document";

                      $where = array(
                          "student_id" => $student_from_id
                      );
                      $this->ablfunctions->updateData($data, $table, $where);


                  }
              }

          }

            $this->session->set_flashdata("s_message", "You have successfully Register.".$img_upload_message);

            //$this->session->set_flashdata("s_message", "You have successfully Register.");
        } else {

            $this->session->set_flashdata("e_message", "Failed to Register! Please try again.");
        }

        //redirect("main/studentFromRegister",$img_upload_message);
        redirect("admin/college/manageStudentRegister",$img_upload_message);
    }
} else {

    $data['session_details']= $this->my_custom_functions->get_multiple_data("tbl_session",'');
    //echo "<pre>";print_r ($data['session_details']);die;
    //$data['session_details'] = $this->my_custom_functions->get_multiple_data("tbl_session",'');
    $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
    $data['semester'] = $this->my_custom_functions->get_multiple_data("tbl_semester",'');
    $this->load->view("admin/college/add_student_manual", $data);
}
}

////////////////////////////////////////////////////////////////////////////

function get_semester_dropdown_from_session() {

    $session_id = $this->input->post('session_id');
    $semester_list = $this->my_custom_functions->get_all_semesters_of_a_session($session_id);

    echo '<option value="">Select Section</option>';
    foreach ($semester_list as $semester) {
        echo '<option value="' . $semester['id'] . '">' . $semester['semester_name'] . '</option>';
    }
}
////////////////////////////////////////////////////////////////////////////

function get_section_dropdown_from_semester() {

    $semester_id = $this->input->post('semester_id');
    $section_list = $this->my_custom_functions->get_section_list("", $semester_id);

    echo '<option value="">Select Section</option>';
    foreach ($section_list as $section) {
        echo '<option value="' . $section['id'] . '">' . $section['section_name'] . '</option>';
    }
}

/*
| --------------------------------------------------------------------------
| Manage Student Register
| --------------------------------------------------------------------------
*/
function manageEnrollStudent(){


    if ($this->input->post('submit') && $this->input->post('submit') != '') {
        $data['post_data'] = array('session_id' => $this->input->post('session'));
        $data['session_active'] = '1';
        $data['all_student'] = $this->my_custom_functions->get_multiple_data('tbl_student_from',' and session_id = "' . $this->input->post('session'). '"  and status = "7"');
    }
    // else if ($this->input->post('search') && $this->input->post('search') != '') {
    //     $data['post_data'] = array('status' => $this->input->post('status'));
    //     $data['all_student'] = $this->my_custom_functions->get_multiple_data('tbl_student_from',' and status = "' . $this->input->post('status'). '" and session_id = "' . $this->input->post('current_session_id'). '"');
    //     $data['session_active'] = '1';
    // }
     else {
         //$data['page_title'] = 'Manage section';
        //$data['all_student'] = $this->my_custom_functions->get_multiple_data("tbl_student_from",'');
        $data['all_student'] = '';
    }
    // Get the data
    $data['all_session'] = $this->my_custom_functions->get_multiple_data("tbl_session",'');
    $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
    $data['all_status'] = $this->my_custom_functions->get_multiple_data("tbl_student_from",'');
    $this->load->view("admin/college/enroll_student_register", $data);
}
/*
| --------------------------------------------------------------------------
| Edit Student Register
| --------------------------------------------------------------------------
*/
function editStudentEnroll() {

if ($this->input->post("submit") && $this->input->post("submit") != "") {

    $encrypted_id = $this->input->post('student_id');
    $student_id = $this->ablfunctions->ablDecrypt($encrypted_id);

    //$this->form_validation->set_rules("course_id", "Select Course", "required");
    //$this->form_validation->set_rules("session_id", "Select session", "required");
    $this->form_validation->set_rules("student_name", "Name of the Candidate", "trim|required");
    $this->form_validation->set_rules("father_or_mother_name", "Father’s /Mother’s Name", "trim|required");
    $this->form_validation->set_rules("guardian_name", "Name of the Guardian", "trim|required");
    $this->form_validation->set_rules("perm_address", "Permanent Address", "trim|required");
    $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
    //$this->form_validation->set_rules("phone_number", "Phone Number", "trim|required|is_unique[tbl_student_from.phone_number]");
    $this->form_validation->set_rules("phone_no", "Phone Number", "trim|required|regex_match[/^[0-9]{10}$/]");
    $this->form_validation->set_rules("corres_address", "Address for correspondence", "trim|required");
    $this->form_validation->set_rules("aadhar_no", "Aadhar No", "trim|required|min_length[12]|max_length[12]");
    $this->form_validation->set_rules("reg_mobile_no", "Registered Mobile No", "trim|required|regex_match[/^[0-9]{10}$/]");
    $this->form_validation->set_rules("dob", "D.O.B", "trim|required");

    //$this->form_validation->set_rules("student_passport_photo", "Passport Photo", "required");
    $this->form_validation->set_rules("age", "Age", "trim|required");
    $this->form_validation->set_rules("gender", "Gender", "required");
    $this->form_validation->set_rules("marital_status", "Marital Status", "required");
    $this->form_validation->set_rules("nationality", "nationality", "required");
    $this->form_validation->set_rules("caste", "Caste", "required");
    $this->form_validation->set_rules("local_address", "Local address of the candidate", "trim|required");
    $this->form_validation->set_rules("local_gu_name_adds", "Name & Address of the local guardian", "trim|required");
    //$this->form_validation->set_rules("rules_condition_one", "Condition", "required");
    //$this->form_validation->set_rules("rules_condition_two", "Condition", "required");

    if ($this->form_validation->run() == false) {

        $this->session->set_flashdata("e_message", validation_errors());
        redirect("admin/college/editStudentEnroll/".$encrypted_id."");
    } else {

      $update_data = array(
            //"course_id" => $this->input->post("course_id"),
            //"session_id" => $this->input->post("session_id"),
            "student_name" => $this->input->post("student_name"),
            "father_or_mother_name" => $this->input->post("father_or_mother_name"),
            "guardian_name" => $this->input->post("guardian_name"),
            "perm_address" => $this->input->post("perm_address"),
            "email" => $this->input->post("email"),
            "phone_no" => $this->input->post("phone_no"),
            "corres_address" => $this->input->post("corres_address"),
            "aadhar_no" => $this->input->post("aadhar_no"),
            "reg_mobile_no" => $this->input->post("reg_mobile_no"),
            "dob" => $this->input->post("dob"),
            "age" => $this->input->post("age"),
            "gender" => $this->input->post("gender"),
            "marital_status" => $this->input->post("marital_status"),
            "nationality" => $this->input->post("nationality"),
            "caste" => $this->input->post("caste"),
            "local_address" => $this->input->post("local_address"),
            "local_gu_name_adds" => $this->input->post("local_gu_name_adds")
            //"status" => $this->input->post("status")
        );

      $where = array(
        "id" => $student_id,
      );

        $update = $this->my_custom_functions->update_data($update_data,"tbl_student_from",$where);
        //$update =1;

        if ($update) {
            $education_details= $this->College_model->get_educational_details($student_id);

            foreach ($education_details as $row) {

            if ($this->input->post('exam_pass_'.$row['id'].'')!='' AND $this->input->post('subject_'.$row['id'].'')!='' AND $this->input->post('year_pass_'.$row['id'].'')!='' AND $this->input->post('board_'.$row['id'].'')!='' AND $this->input->post('div_obt_'.$row['id'].'')!='') {

            $exam_data = array(
            "exam_passed" => $this->input->post("exam_pass_".$row['id'].""),
            "subject" => $this->input->post("subject_".$row['id'].""),
            "year_of_pass" => $this->input->post("year_pass_".$row['id'].""),
            "board_coun_uni" => $this->input->post("board_".$row['id'].""),
            "div_obt" => $this->input->post("div_obt_".$row['id']."")
            );


            $where = array(
            "id" => $row['id'],
            );
            $edu_table_update = $this->my_custom_functions->update_data($exam_data,"tbl_edu_qualification",$where);
                if ($edu_table_update) {
                 if (isset($_FILES["document_".$row['id'].""])) {

                if ($_FILES["document_".$row['id'].""]['name'] != "") {

                    //$file_name = $student_from_id.'.'.'jpg';
                    $file_name_old = $row['id'].'_'.basename($_FILES["document_".$row['id'].""]['name']);
                    $file_name = str_replace(' ', '_', $file_name_old);

                    $config = array();
                    $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                    $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                    $config['file_name']            = $file_name;
                    $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                    $this->load->library('upload');
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('document_'.$row['id'].'')) {

                        $img_upload_message = $this->upload->display_errors();

                    } else {

                        // Upload thumb image
                        // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                        // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                        // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                        //Update database with the cover image url
                        $data = array(
                            "document_url" => $file_name
                        );

                        $table = "tbl_edu_qualification";

                        $where = array(
                            "id" => $row['id']
                        );
                        $this->ablfunctions->updateData($data, $table, $where);

                        // $img_upload_message = 'Cover image uploaded successfully.';
                    }
                }
            }
            }

            }
            }
            ////////////////File Upload///////////////
            // Upload Passport Photo
            $img_upload_message = '';
            if (isset($_FILES['student_passport_photo'])) {

                if ($_FILES['student_passport_photo']['name'] != "") {

                    //$file_name = $student_id.'.'.'jpg';
                    //$file_name = $student_from_id.'_'.'jpg';
                    $file_name_old = $student_id.'_'.basename($_FILES['cover_image_url']['name']);
                    $file_name = str_replace(' ', '_', $file_name_old);

                    $config = array();
                    $config['upload_path']          = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO;
                    $config['allowed_types']        = ALLOWED_PHOTO_IMAGE_TYPES;
                    $config['file_name']            = $file_name;
                    $config['max_size']             = ALLOWED_PHOTO_IMAGE_SIZE;
                    // $config['max_width']  = PASSPORT_WIDTH;
                    // $config['max_height']  = PASSPORT_HEIGHT;

                    $this->load->library('upload');
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('student_passport_photo')) {

                        $img_upload_message = $this->upload->display_errors();

                    } else {

                        //Upload thumb image
                        $source = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$file_name;
                        $destination = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$file_name;

                        $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                        // Update database with the cover image url
                        $data = array(
                            "passport" => $file_name
                        );

                        $table = "tbl_student_from_document";

                        $where = array(
                            "student_id" => $student_id
                        );
                        $this->ablfunctions->updateData($data, $table, $where);

                        // $img_upload_message = 'Cover image uploaded successfully.';
                    }
                }
            }


             // Upload Original copies of age verification
            if (isset($_FILES['age_verification_xerox'])) {

                if ($_FILES['age_verification_xerox']['name'] != "") {

                    //$file_name = $student_id.'.'.'jpg';
                    //$file_name = $student_from_id.'_'.'jpg';
                    $file_name_old = $student_id.'_'.basename($_FILES['age_verification_xerox']['name']);
                    $file_name = str_replace(' ', '_', $file_name_old);

                    $config = array();
                    $config['upload_path']          = UPLOAD_DIR.STUDENT_AGE_PROOF;
                    $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                    $config['file_name']            = $file_name;
                    $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                    $this->load->library('upload');
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('age_verification_xerox')) {

                        $img_upload_message = $this->upload->display_errors();

                    } else {

                        // Upload thumb image
                        // $source = UPLOAD_DIR.STUDENT_AGE_PROOF.$file_name;
                        // $destination = UPLOAD_DIR.STUDENT_AGE_PROOF_THUMB.$file_name;
                        //
                        // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                        // Update database with the cover image url
                        $data = array(
                            "age_photo" => $file_name
                        );

                        $table = "tbl_student_from_document";

                        $where = array(
                            "student_id" => $student_id
                        );
                        $this->ablfunctions->updateData($data, $table, $where);

                        // $img_upload_message = 'Cover image uploaded successfully.';
                    }
                }
            }
             // Upload Original copies of SC/ST/OBC/Handicapped Certificate
            if (isset($_FILES['caste_certificate_xerox'])) {

                if ($_FILES['caste_certificate_xerox']['name'] != "") {

                    $file_name = $student_id.'.'.'jpg';
                    //$file_name = $student_from_id.'_'.'jpg';
                    //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);
                    //$file_name = str_replace(' ', '_', $file_name_old);

                    $config = array();
                    $config['upload_path']          = UPLOAD_DIR.STUDENT_CASTE;
                    $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                    $config['file_name']            = $file_name;
                    $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                    $this->load->library('upload');
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('caste_certificate_xerox')) {

                        $img_upload_message = $this->upload->display_errors();

                    } else {

                        // Upload thumb image
                        // $source = UPLOAD_DIR.STUDENT_CASTE.$file_name;
                        // $destination = UPLOAD_DIR.STUDENT_CASTE_THUMB.$file_name;
                        //
                        // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                        // Update database with the cover image url
                        $data = array(
                            "caste_photo" => $file_name
                        );

                        $table = "tbl_student_from_document";

                        $where = array(
                            "student_id" => $student_id
                        );
                        $this->ablfunctions->updateData($data, $table, $where);

                        // $img_upload_message = 'Cover image uploaded successfully.';
                    }
                }
            }
             // Upload Countersignature of the Guardian
            if (isset($_FILES['guardian_signature'])) {

                if ($_FILES['guardian_signature']['name'] != "") {

                    //$file_name = $student_id.'.'.'jpg';
                    //$file_name = $student_from_id.'_'.'jpg';
                    $file_name_old = $student_id.'_'.basename($_FILES['guardian_signature']['name']);
                    $file_name = str_replace(' ', '_', $file_name_old);

                    $config = array();
                    $config['upload_path']          = UPLOAD_DIR.STUDENT_GURDAIN_SIGN;
                    $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                    $config['file_name']            = $file_name;
                    $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                    // $config['max_width']  = SIGN_WIDTH;
                    // $config['max_height']  = SIGN_HEIGHT;

                    $this->load->library('upload');
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('guardian_signature')) {

                        $img_upload_message = $this->upload->display_errors();

                    } else {

                        // Upload thumb image
                        $source = UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$file_name;
                        $destination = UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$file_name;

                        $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                        // Update database with the cover image url
                        $data = array(
                            "guardian_sign" => $file_name
                        );

                        $table = "tbl_student_from_document";

                        $where = array(
                            "student_id" => $student_id
                        );
                        $this->ablfunctions->updateData($data, $table, $where);

                        // $img_upload_message = 'Cover image uploaded successfully.';
                    }
                }
            }
             // Upload Signature of Candidate
            if (isset($_FILES['candidate_signature'])) {

                if ($_FILES['candidate_signature']['name'] != "") {

                    //$file_name = $student_id.'.'.'jpg';
                    //$file_name = $student_from_id.'_'.'jpg';
                    $file_name_old = $student_id.'_'.basename($_FILES['candidate_signature']['name']);
                    $file_name = str_replace(' ', '_', $file_name_old);

                    $config = array();
                    $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN;
                    $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                    $config['file_name']            = $file_name;
                    $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                    // $config['max_width']  = SIGN_WIDTH;
                    // $config['max_height']  = SIGN_HEIGHT;

                    $this->load->library('upload');
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('candidate_signature')) {

                        $img_upload_message = $this->upload->display_errors();

                    } else {

                        //Upload thumb image
                        $source = UPLOAD_DIR.STUDENT_SIGN.$file_name;
                        $destination = UPLOAD_DIR.STUDENT_SIGN_THUMB.$file_name;

                        $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                        // Update database with the cover image url
                        $data = array(
                            "student_sign" => $file_name
                        );

                        $table = "tbl_student_from_document";

                        $where = array(
                            "student_id" => $student_id
                        );
                        $this->ablfunctions->updateData($data, $table, $where);

                        // $img_upload_message = 'Cover image uploaded successfully.';
                    }
                }
            }
            // Upload Signature of Candidate in full
            if (isset($_FILES['candidate_signature_full'])) {

                if ($_FILES['candidate_signature_full']['name'] != "") {

                    //$file_name = $student_id.'.'.'jpg';
                    //$file_name = $student_from_id.'_'.'jpg';
                    $file_name_old = $student_id.'_'.basename($_FILES['candidate_signature_full']['name']);
                    $file_name = str_replace(' ', '_', $file_name_old);

                    $config = array();
                    $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN_FULL;
                    $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                    $config['file_name']            = $file_name;
                    $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                    // $config['max_width']  = SIGN_WIDTH;
                    // $config['max_height']  = SIGN_HEIGHT;

                    $this->load->library('upload');
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('candidate_signature_full')) {

                        $img_upload_message = $this->upload->display_errors();

                    } else {

                        // Upload thumb image
                        $source = UPLOAD_DIR.STUDENT_SIGN_FULL.$file_name;
                        $destination = UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$file_name;

                        $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                        // Update database with the cover image url
                        $data = array(
                            "student_sign_full" => $file_name
                        );

                        $table = "tbl_student_from_document";

                        $where = array(
                            "student_id" => $student_id
                        );
                        $this->ablfunctions->updateData($data, $table, $where);

                        // $img_upload_message = 'Cover image uploaded successfully.';
                    }
                }

            }

            $this->session->set_flashdata("s_message", "Student Admissions record updated successfully.");
        } else {

            $this->session->set_flashdata('e_message', "Failed to update the Student Admissions record.");
        }

        redirect("admin/college/editStudentEnroll/".$encrypted_id."");
    }
} else {

    $encrypted_id = $this->uri->segment(4);
    $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
    $student_id = $this->ablfunctions->ablDecrypt($encrypted_id);

     $data['education_details']= $this->College_model->get_educational_details($student_id);
     //echo "<pre>";print_r ($data['education_details']);die;
    $data['details'] = $this->ablfunctions->getDetailsFromId($student_id, "tbl_student_from");

    $this->load->view("admin/college/edit_student_enroll", $data);
}
}
//Manage Payslip
function pay_slip() {
    if ($this->input->post('submit') AND ($this->input->post('submit') == "Submit")) {
        $month = $this->input->post('month');
        $this->session->set_userdata('sess_month', $month);
        $year = $this->input->post('year');
    } else { //if loaded directly
        if ($this->session->userdata('sess_month')) {
            $month = $this->session->userdata('sess_month');
        } else {
            $month = date("m") - 1;
        }
        $year = date("Y");
        //$year = "2015";
    }

    $all_years = $this->College_model->get_attendence_year_list();
    foreach ($all_years as $row) {
        $years[$row->att_years] = $row->att_years;
    }

    $query = $this->College_model->manage_payslip($month, $year);

    $data = array(
      "slip" => $query,
      'year' => $year,
      'month' => $month,
      'years' => $years,
    );

    $this->load->view('admin/college/pay_slip', $data);
  }


  /*
  | --------------------------------------------------------------------------
  | Delete Session
  | --------------------------------------------------------------------------
  */
  function deleteAttendence() {
    $encrypted_id = $this->uri->segment(4);
    //$session_id = $this->ablfunctions->ablDecrypt($encrypted_id);
    $query = $this->ablfunctions->deleteData("tbl_attendance", array("id" => $encrypted_id));
  if ($query) { // if deleted
      $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
      redirect('admin/college/manage_attendance');
  } else {
      $this->session->set_flashdata('e_message', 'Record Deletion Failed');
      redirect('admin/college/manage_attendance');
  }
  }
  /*
  | --------------------------------------------------------------------------
  | Add payslip
  | --------------------------------------------------------------------------
  */
  function add_payslip() {
    if ($this->input->post('submit') AND ($this->input->post('submit') == "Submit")) {
      $emp_id = $this->input->post('emp_id');
      $date_month = $this->input->post('year') . "-" . $this->input->post('month') . "-" . "01";
      $result = $this->College_model->get_payslip_details($emp_id, $date_month);
      if ($result) {

    $this->session->set_flashdata('e_message', 'PaySlip already added.');
    redirect('admin/college/add_payslip');
   } else {
    $insert_data = array(
      'emp_id' => $emp_id,
      'payslip_date' => $date_month,
      'attendance_id' => $this->input->post('attend_id'),
      'basic_pay' => $this->input->post('basic_pay'),
      'grade_pay' => $this->input->post('grade_pay'),
      'other_allow' => $this->input->post('other_allow'),
      'other_allow_title' => $this->input->post('other_allow_title'),
      'medical_allow' => $this->input->post('medical_allow'),
      'emp_contribute_pf' => $this->input->post('emp_contribute_pf'),
      'salary_total' => $this->input->post('salary_total'),
      'income_tax' => $this->input->post('income_tax'),
      'pf_deduction' => $this->input->post('pf_deduction'),
      'fest_advance' => $this->input->post('fest_advance'),
      'other_deduction' => $this->input->post('other_deduction'),
      'other_deduction_title' => $this->input->post('other_deduction_title'),
      'total_deduction' => $this->input->post('total_deduction'),
      'ptax' => $this->input->post('ptax'),
      'net_pay' => $this->input->post('net_pay'),
      'payment_date' => $this->input->post('payment_date'),
      'payment_details' => $this->input->post('payment_details'),
      'note' => $this->input->post('note'),
      'created_at' => date('Y-m-d H:i:s'),
      'created_by' => $this->input->post('admin_name'),
    );
    $employee = $this->my_custom_functions->insert_data($insert_data,"tbl_emp_payslip");
    if (isset($employee)) {
        $this->session->set_flashdata('s_message', 'Record Added Successfully.');
        redirect('admin/college/pay_slip');
    }
    }
    }else {
      //$all_emp = $this->my_custom_functions->get_multiple_data("tbl_staff",'and status = 1 and ORDERBY staff_name ASC');
      $all_emp = $this->College_model->get_all_employee_details();
      $data = array(
        'all_emp' => $all_emp,
      );
      $this->load->view('admin/college/add_payslip', $data);
    }

}
    function get_emp_attend_details() {
    header('Content-Type: application/x-json; charset=utf-8');
    echo(json_encode($this->College_model->select_emp_attend_details()));
    }

    function get_cl_details() {
    header('Content-Type: application/x-json; charset=utf-8');
    echo(json_encode($this->College_model->get_cl_details()));
    }

    function get_emp_details() {

    header('Content-Type: application/x-json; charset=utf-8');
    echo(json_encode($this->College_model->select_emp_details()));
    }
        /*
    | --------------------------------------------------------------------------
    | Delete PaySlip
    | --------------------------------------------------------------------------
    */
    function deletePaySlip() {
      $encrypted_id = $this->uri->segment(4);
      $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_emp_payslip", array("id" => $page_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/pay_slip");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/pay_slip");
    }
    }
    function edit_payslip() {
      if ($this->input->post('submit') AND ($this->input->post('submit') == "Update")) {
      $data = array(
        'basic_pay' => $this->input->post('basic_pay'),
        'grade_pay' => $this->input->post('grade_pay'),
        'other_allow' => $this->input->post('other_allow'),
        'other_allow_title' => $this->input->post('other_allow_title'),
        'medical_allow' => $this->input->post('medical_allow'),
        'emp_contribute_pf' => $this->input->post('emp_contribute_pf'),
        'salary_total' => $this->input->post('salary_total'),
        'income_tax' => $this->input->post('income_tax'),
        'pf_deduction' => $this->input->post('pf_deduction'),
        'fest_advance' => $this->input->post('fest_advance'),
        'other_deduction' => $this->input->post('other_deduction'),
        'other_deduction_title' => $this->input->post('other_deduction_title'),
        'total_deduction' => $this->input->post('total_deduction'),
        'ptax' => $this->input->post('ptax'),
        'net_pay' => $this->input->post('net_pay'),
        'payment_date' => $this->input->post('payment_date'),
        'payment_details' => $this->input->post('payment_details'),
        'note' => $this->input->post('note'),
        'updated_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->input->post('admin_name'),
      );

      $condition = array(
          'id' =>  $this->input->post('payslip_id'),
      );

      $employee = $this->my_custom_functions->update_data($data,"tbl_emp_payslip", $condition);

      if (isset($employee)) {
          $this->session->set_flashdata('s_message', 'Record Updated Successfully.');
          redirect('admin/college/pay_slip');
      }
      }else {
      $encrypted_id = $this->uri->segment(5);
      $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
    $slip = $this->ablfunctions->getDetailsFromId($page_id, "tbl_emp_payslip");
    $attend = $this->my_custom_functions->get_details_from_id("", "tbl_attendance", array("emp_id" =>$this->uri->segment(4)));

        //echo "<pre>";print_r($attend);die;
    $data = array(
      'slip' => $slip,
      'attend' => $attend,
    );
    $this->load->view('admin/college/edit_payslip', $data);
    }
    }
    /*
    | --------------------------------------------------------------------------
    | View PaySlip
    | --------------------------------------------------------------------------
    */
    function viewPayslip() {
    $page_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
    $data['slip'] = $this->ablfunctions->getDetailsFromId($page_id, "tbl_emp_payslip");

    $this->load->view('admin/college/view_payslip', $data);
    }

    function pay_slip_report() {
    if ($this->input->post('submit') AND ($this->input->post('submit') == "Search")) {

        $emp_id = $emp_id = $this->my_custom_functions->get_particular_field_value("tbl_staff","id", 'and staff_name="'.$this->input->post('emp_name').'"');
        $payslip = $this->College_model->get_payslip_report($emp_id);

            $data = array(
              'slip' => $payslip,
              'emp_name' => $this->input->post('emp_name'),
              'from_date' => $this->input->post('from_date'),
              'to_date' => $this->input->post('to_date'),
            );

        $this->load->view('admin/college/pay_slip_report', $data);
    }
    else {
        $this->load->view('admin/college/pay_slip_report');
    }
    }
    function paymentRegister() {
      if ($this->input->post('submit') AND ($this->input->post('submit') == "Search")) {

          $email = $this->input->post('email');
          $phone = $this->input->post('phone');
          $details = $this->College_model->get_payment_details($email,$phone);
              $data = array(
                'details' => $details,
                'payment_type' => $this->input->post('payment_type'),
              );

          $this->load->view('admin/college/payment_register', $data);
      }
      else if ($this->input->post('submit') AND ($this->input->post('submit') == "Confirm")) {

          $student_id = $this->input->post('student_id');
          $payment_type = $this->input->post('payment_type');
          if ($payment_type=="admission_from_payment") {
            // if ($this->input->post('admission_from_payment')!='') {
            //   $admission_from_payment=1;
            // }else {
            //   $admission_from_payment=0;
            // }
            $data = array(
              'admission_from_payment' => 1,
              'admission_from_payment_ammount' => $this->input->post('admission_from_payment_ammount'),
            );

            $condition = array(
                'student_id' => $student_id,
            );

            $this->my_custom_functions->update_data($data,"tbl_student_payment_status", $condition);
            # code...
          }else if ($payment_type=="admission_payment") {
            // if ($this->input->post('admission_payment')!='') {
            //   $admission_payment=1;
            // }else {
            //   $admission_payment=0;
            // }
            $data = array(
              'admission_payment' => 1,
              'admission_payment_ammount' => $this->input->post('admission_payment_ammount'),
            );

            $condition = array(
                'student_id' => $student_id,
            );

            $this->my_custom_functions->update_data($data,"tbl_student_payment_status", $condition);
            # code...
          }

          $this->session->set_flashdata('s_message', 'Record Updated Successfully.');
          redirect('admin/college/paymentRecord');
      }
      else {
        $this->load->view('admin/college/payment_register');
      }

    }
    /*
    | --------------------------------------------------------------------------
    | Edit Classes
    | --------------------------------------------------------------------------
    */
    function editPaymentRegister() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('class_id');
            $class_id = $this->ablfunctions->ablDecrypt($encrypted_id);


              $update_data = array(
                  "admission_from_payment" => $this->input->post("admission_from_payment"),
                  "admission_payment" => $this->input->post("admission_payment"),
              );
              $where = array(
                "student_id" => $class_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_student_payment_status",$where);

                if ($update) {

                    $this->session->set_flashdata("s_message", "Record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Record.");
                }

                redirect("admin/college/paymentRegister");
        } else {

            $encrypted_id = $this->uri->segment(4);
            $class_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            $data['details'] = $this->ablfunctions->getDetailsFromId($class_id, "tbl_student");
            $data['payment_details'] = $this->my_custom_functions->get_details_from_id("", "tbl_student_payment_status", array("student_id" => $class_id));

            $this->load->view("admin/college/edit_payment_register", $data);
        }
    }
    function paymentRecord() {

        $this->load->view('admin/college/payment_record_view');
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Staff
    | --------------------------------------------------------------------------
    */
    function paymentReport(){

      if ($this->input->post('submit') AND ($this->input->post('submit') == "Search")) {

          $from_date_old = $this->input->post('from_date');
          $to_date_old = $this->input->post('to_date');
          $from_date = $from_date_old.' 00:00:00';
          $to_date = $to_date_old.' 24:00:00';
          //$data['details'] = $this->my_custom_functions->get_multiple_data("tbl_student_payment_status",'');
          $details = $this->College_model->payment_report($from_date,$to_date);
              $data = array(
                'details' => $details,
                'from_date' => $this->input->post('from_date'),
                'to_date' => $this->input->post('to_date'),
              );

          $this->load->view('admin/college/payment_report_view', $data);
      }


      else {

        //$data['details'] = $this->my_custom_functions->get_multiple_data("tbl_student_payment_status",'');
        $this->load->view("admin/college/payment_report_view");
      }

    }
    function bulk_record_payment_report(){

       if (($this->input->post('submit_one') !== "") AND($this->input->post('submit_one') == "Update")) {
        if($this->input->post('check')!='')
        {
          $check=$this->input->post('check');
          //echo "<pre>";print_r($this->input->post());die;
          $from_payment =$this->input->post('admission_from_payment_ammount');
          $payment=$this->input->post('admission_payment_ammount');
          $count=count($check);
          $status=$this->input->post('status');
              //for($i=0; $i < $count; $i++)
              if(count($check) > 0) {
              foreach($check as $i)
              {
                // //if (($from_payment[$i]=='') AND ($payment[$i]=='') ) {
                //   $this->session->set_flashdata("s_message", "Record updated successfully.");
                //   //redirect("admin/college/paymentReport");
                // }else{
                $update_data = array();
                if (array_key_exists($i, $from_payment)) {
                  if ($from_payment[$i]!='') {
                      $update_data["admission_from_payment"] = 1;
                      $update_data["admission_from_payment_ammount"] = $from_payment[$i];
                  }
                }

                if (array_key_exists($i, $payment)) {
                  if ($payment[$i]!='') {
                      $update_data["admission_payment"] = 1;
                      $update_data["admission_payment_ammount"] = $payment[$i];
                  }
                }

                if(!empty($update_data)) {
                    $where = array(
                      "student_id" => $check[$i],
                    );

                     $update = $this->my_custom_functions->update_data($update_data,"tbl_student_payment_status",$where);
                     //$this->College_model->bulk_edit_paymentReport($check[$i],$from_payment[$i],$payment[$i]);
                 }
              }
              }
              $this->session->set_flashdata("s_message", "Record updated successfully.");
              $from_date_old = $this->input->post('from_date');
              $to_date_old = $this->input->post('to_date');
              $from_date = $from_date_old.' 00:00:00';
              $to_date = $to_date_old.' 24:00:00';
              //$data['details'] = $this->my_custom_functions->get_multiple_data("tbl_student_payment_status",'');
              $details = $this->College_model->payment_report($from_date,$to_date);
                  $data = array(
                    'details' => $details,
                    'from_date' => $from_date_old,
                    'to_date' => $to_date_old,
                  );

              $this->load->view('admin/college/payment_report_view', $data);
              // redirect("admin/college/paymentReport");
        }
        //echo "<pre>";print_r($this->input->post());die;
      }

    }

    // download Student Records In Excel
    function downloadStudentRecords($secarch_name,$select_id) {

    $this->load->library('Classes/PHPExcel');
    $all_student = $this->my_custom_functions->get_multiple_data('tbl_student_from',' and '.$secarch_name.' = "' .$select_id. '"');


    $objPHPExcel = new PHPExcel();

    $excel_title = 'Student List';

    // Add tab label to the sheet
    $objPHPExcel->getActiveSheet()->setTitle('Student List');


    // Sheet heading in the first row
    $objPHPExcel->getActiveSheet()->mergeCells('A1:AZ1');
    $objPHPExcel->getActiveSheet()->setCellValue('A1','College of Art & Design');

    // Column headings in the forth row
    $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Student Name:');
    $objPHPExcel->getActiveSheet()->setCellValue('B3', 'Course Name:');
    $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Form No:');
    $objPHPExcel->getActiveSheet()->setCellValue('D3', 'Session:');
    $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Father’s /Mother’s Name:');
    $objPHPExcel->getActiveSheet()->setCellValue('F3', 'Name of the Guardian:');
    $objPHPExcel->getActiveSheet()->setCellValue('G3', 'Permanent Address:');
    $objPHPExcel->getActiveSheet()->setCellValue('H3', 'Student Email:');
    $objPHPExcel->getActiveSheet()->setCellValue('I3', 'Phone Number:');
    $objPHPExcel->getActiveSheet()->setCellValue('J3', 'Address for correspondence:');
    $objPHPExcel->getActiveSheet()->setCellValue('K3', 'Aadhar No:');
    $objPHPExcel->getActiveSheet()->setCellValue('L3', 'Registered Mobile No:');
    $objPHPExcel->getActiveSheet()->setCellValue('M3', 'D.O.B:');
    $objPHPExcel->getActiveSheet()->setCellValue('N3', 'Passport Photo:');
    $objPHPExcel->getActiveSheet()->setCellValue('O3', 'Age:');
    $objPHPExcel->getActiveSheet()->setCellValue('P3', 'Annual income of the Guardian:');
    $objPHPExcel->getActiveSheet()->setCellValue('Q3', 'Sex:');
    $objPHPExcel->getActiveSheet()->setCellValue('R3', 'Marital Status:');
    $objPHPExcel->getActiveSheet()->setCellValue('S3', 'Nationality:');
    $objPHPExcel->getActiveSheet()->setCellValue('T3', 'Original copies of age verification:');
    $objPHPExcel->getActiveSheet()->setCellValue('U3', 'Original copies of SC/ST/OBC/Handicapped Certificate:');
    $objPHPExcel->getActiveSheet()->setCellValue('V3', 'Countersignature of the Guardian:');
    $objPHPExcel->getActiveSheet()->setCellValue('W3', 'Signature of Candidate:');
    $objPHPExcel->getActiveSheet()->setCellValue('X3', 'Signature of Candidate in full:');
    $objPHPExcel->getActiveSheet()->setCellValue('Y3', 'Local address of the candidate:');
    $objPHPExcel->getActiveSheet()->setCellValue('Z3', 'Name & Address of the local guardian:');
    $objPHPExcel->getActiveSheet()->setCellValue('AA3', 'Date Of Apply:');
    $objPHPExcel->getActiveSheet()->setCellValue('AB3', 'Status:');
    $objPHPExcel->getActiveSheet()->mergeCells('AC3:AF3');
    $objPHPExcel->getActiveSheet()->setCellValue('AC3', 'Educational Qualification:');

    foreach($all_student as $value) {
    $educational_details = $this->my_custom_functions->get_multiple_data('tbl_edu_qualification',' and student_from_id = "' .$value['id']. '"');
    }

      $row = 4;
      foreach($all_student as $value) {

          $objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $value['student_name']);
          $objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $this->my_custom_functions->get_particular_field_value("tbl_courses","course_name", 'and id="'.$value['course_id'].'"'));
          $objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $value['from_id']);
          $objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $this->my_custom_functions->get_particular_field_value("tbl_session","session_name", 'and id="'.$value['session_id'].'"'));
          $objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $value['father_or_mother_name']);
          $objPHPExcel->getActiveSheet()->setCellValue('F' . $row, $value['guardian_name']);
          $objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $value['perm_address']);
          $objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $value['email']);
          $objPHPExcel->getActiveSheet()->setCellValue('I' . $row, $value['phone_no']);
          $objPHPExcel->getActiveSheet()->setCellValue('J' . $row, $value['corres_address']);
          $objPHPExcel->getActiveSheet()->setCellValue('K' . $row, $value['aadhar_no']);
          $objPHPExcel->getActiveSheet()->setCellValue('L' . $row, $value['reg_mobile_no']);
          $objPHPExcel->getActiveSheet()->setCellValue('M' . $row, $value['dob']);
          $passport_photo = base_url().UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$this->my_custom_functions->get_particular_field_value("tbl_student_from_document","passport", 'and student_id="'.$value['id'].'"');
          $objPHPExcel->getActiveSheet()->setCellValue('N' . $row, $passport_photo);
          $objPHPExcel->getActiveSheet()->setCellValue('O' . $row, $value['age']);
          $objPHPExcel->getActiveSheet()->setCellValue('P' . $row, $value['gud_annual_income']);
          if ($value['gender']==1) {
            $gender = "Male";
          }else if ($value['gender']==2) {
            $gender = "Female";
          }else if ($value['gender']==3) {
            $gender = "Others";
          }
          $objPHPExcel->getActiveSheet()->setCellValue('Q' . $row, $gender);
          if ($value['marital_status']==1) {
            $marital_statuser = "Married";
          }else if ($value['marital_status']==2) {
            $marital_statuser = "Single";
          }else if ($value['marital_status']==3) {
            $marital_statuser = "Divorced";
          }
          $objPHPExcel->getActiveSheet()->setCellValue('R' . $row, $marital_statuser);
          $objPHPExcel->getActiveSheet()->setCellValue('S' . $row, $value['nationality']);
          $age_photo = base_url().UPLOAD_DIR.STUDENT_AGE_PROOF.$this->my_custom_functions->get_particular_field_value("tbl_student_from_document","age_photo", 'and student_id="'.$value['id'].'"');
          $objPHPExcel->getActiveSheet()->setCellValue('T' . $row,$age_photo);
          $caste_photo = base_url().UPLOAD_DIR.STUDENT_CASTE.$this->my_custom_functions->get_particular_field_value("tbl_student_from_document","caste_photo", 'and student_id="'.$value['id'].'"');
          $objPHPExcel->getActiveSheet()->setCellValue('U' . $row,$caste_photo);
          $guardian_photo = base_url().UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$this->my_custom_functions->get_particular_field_value("tbl_student_from_document","guardian_sign", 'and student_id="'.$value['id'].'"');
          $objPHPExcel->getActiveSheet()->setCellValue('V' . $row,$guardian_photo);
          $student_sign_photo = base_url().UPLOAD_DIR.STUDENT_SIGN.$this->my_custom_functions->get_particular_field_value("tbl_student_from_document","student_sign", 'and student_id="'.$value['id'].'"');
          $objPHPExcel->getActiveSheet()->setCellValue('W' . $row,$student_sign_photo);
          $student_sign_full_photo = base_url().UPLOAD_DIR.STUDENT_SIGN.$this->my_custom_functions->get_particular_field_value("tbl_student_from_document","student_sign_full", 'and student_id="'.$value['id'].'"');
          $objPHPExcel->getActiveSheet()->setCellValue('X' . $row,$student_sign_full_photo);
          $objPHPExcel->getActiveSheet()->setCellValue('Y' . $row, $value['local_address']);
          $objPHPExcel->getActiveSheet()->setCellValue('Z' . $row, $value['local_gu_name_adds']);
          $objPHPExcel->getActiveSheet()->setCellValue('AA' . $row, $value['date']);
          if ($value['status']==1) {
            $status = "Need Review";
          }else if ($value['status']==2) {
            $status = "Review Pending";
          }else if ($value['status']==3) {
            $status = "Admit (Test) Issued";
          }else if ($value['status']==4) {
            $status = "Admit (Admission) Issued";
          }else if ($value['status']==5) {
            $status = "Payment Link Activated";
          }else if ($value['status']==6) {
            $status = "Admission Fees received";
          }else if ($value['status']==7) {
            $status = "Enrolled";
          }
          $objPHPExcel->getActiveSheet()->setCellValue('AB' . $row, $status);


          $letter = 'AC';
            $educational_details = $this->my_custom_functions->get_multiple_data('tbl_edu_qualification',' and student_from_id = "' .$value['id']. '"');
            if(count($educational_details) > 0) {
                foreach($educational_details as $edudet) {
                    $qualification = 'Exam: '.$edudet['exam_passed'].'. '
                            // . 'Subject : '.$edudet['subject'].'. '
                            // . 'Year Of Passing : '.$edudet['year_of_pass'].'. '
                            // . 'Board/Council/University : '.$edudet['board_coun_uni'].'. '
                            // . 'Division Obtained : '.$edudet['div_obt'].'. '
                            . 'Document : '.base_url().UPLOAD_DIR.STUDENT_MARKSHEET.$edudet['document_url'];
                    $objPHPExcel->getActiveSheet()->setCellValue($letter . $row, $qualification);
                    $letter++;

                }
            }
          //$objPHPExcel->getActiveSheet()->getStyle('D' . $row) ->getAlignment() ->setWrapText(true);

          $row++;
      }



      $objPHPExcel->setActiveSheetIndex(0);

      // Redirect output to a client’s web browser
      //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$excel_title.'.xlsx"');
     header('Cache-Control: max-age=0');

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');

    }

    // download Student Records In Excel
    function downloadPayslipRecordMonth($month, $year) {

    $this->load->library('Classes/PHPExcel');
    $details = $this->College_model->manage_payslip($month, $year);
    //echo "<pre>";print_r($details);die;
    $objPHPExcel = new PHPExcel();
    $excel_pay_title = 'Payment List';

    // Add tab label to the sheet
    $objPHPExcel->getActiveSheet()->setTitle('Payment List');


    // Sheet heading in the first row
    $objPHPExcel->getActiveSheet()->mergeCells('A1:AZ1');
    $objPHPExcel->getActiveSheet()->setCellValue('A1','Payslip Record');

    // Column headings in the forth row
    $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Employee:');
    $objPHPExcel->getActiveSheet()->setCellValue('B3', 'Pay:');
    $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Grade Pay:');
    $objPHPExcel->getActiveSheet()->setCellValue('D3', 'Other Allow:');
    $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Other & Med. Allow:');
    $objPHPExcel->getActiveSheet()->setCellValue('F3', 'Employer P.F. (13.61%):');
    $objPHPExcel->getActiveSheet()->setCellValue('G3', 'TOTAL:');
    $objPHPExcel->getActiveSheet()->setCellValue('H3', 'P.Tax:');
    $objPHPExcel->getActiveSheet()->setCellValue('I3', 'I. Tax:');
    $objPHPExcel->getActiveSheet()->setCellValue('J3', 'PF25.61%:');
    $objPHPExcel->getActiveSheet()->setCellValue('K3', 'Festival Adv:');
    $objPHPExcel->getActiveSheet()->setCellValue('L3', 'Other Deduction:');
    $objPHPExcel->getActiveSheet()->setCellValue('M3', 'Total Deduction:');
    $objPHPExcel->getActiveSheet()->setCellValue('N3', 'Net Pay:');
    $objPHPExcel->getActiveSheet()->setCellValue('O3', 'Payment Date:');



      $row = 4;
      foreach($details as $value) {

          $objPHPExcel->getActiveSheet()->setCellValue('A'. $row, $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$value->emp_id.'"'));
          $objPHPExcel->getActiveSheet()->setCellValue('B'. $row, $value->basic_pay);
          $basic_pay_total+=$value->basic_pay;
          $objPHPExcel->getActiveSheet()->setCellValue('C'. $row, $value->grade_pay);
          $grade_pay_total+=$value->grade_pay;
          $objPHPExcel->getActiveSheet()->setCellValue('D'. $row, $value->other_allow);
          $other_allow_total+=$value->other_allow;
          $objPHPExcel->getActiveSheet()->setCellValue('E'. $row, $value->medical_allow);
          $medical_allow_total+=$value->medical_allow;
          $objPHPExcel->getActiveSheet()->setCellValue('F'. $row, $value->emp_contribute_pf);
          $emp_contribute_pf_total+=$value->emp_contribute_pf;
          $objPHPExcel->getActiveSheet()->setCellValue('G'. $row, $value->salary_total);
          $salary_total_total+=$value->salary_total;
          $objPHPExcel->getActiveSheet()->setCellValue('H'. $row, $value->ptax);
          $ptax_total+=$value->ptax;
          $objPHPExcel->getActiveSheet()->setCellValue('I'. $row, $value->income_tax);
          $income_tax_total+=$value->income_tax;
          $objPHPExcel->getActiveSheet()->setCellValue('J'. $row, $value->pf_deduction);
          $pf_deduction_total+=$value->pf_deduction;
          $objPHPExcel->getActiveSheet()->setCellValue('K'. $row, $value->fest_advance);
          $fest_advance_total+=$value->fest_advance;
          $objPHPExcel->getActiveSheet()->setCellValue('L'. $row, $value->other_deduction);
          $other_deduction_total+=$value->other_deduction;
          $objPHPExcel->getActiveSheet()->setCellValue('M'. $row, $value->total_deduction);
          $total_deduction_total+=$value->total_deduction;
          $objPHPExcel->getActiveSheet()->setCellValue('N'. $row, $value->net_pay);
          $net_pay_total+=$value->net_pay;
          $objPHPExcel->getActiveSheet()->setCellValue('O'. $row, $value->payment_date);

            }
          //$objPHPExcel->getActiveSheet()->getStyle('D' . $row) ->getAlignment() ->setWrapText(true);

          $row++;
          $row_tl = 5+count($details);
          // Column Total Calculation
          $objPHPExcel->getActiveSheet()->setCellValue('A'. $row_tl, 'Total:');
          $objPHPExcel->getActiveSheet()->setCellValue('B'. $row_tl, $basic_pay_total);
          $objPHPExcel->getActiveSheet()->setCellValue('C'. $row_tl, $grade_pay_total);
          $objPHPExcel->getActiveSheet()->setCellValue('D'. $row_tl, $other_allow_total);
          $objPHPExcel->getActiveSheet()->setCellValue('E'. $row_tl, $medical_allow_total);
          $objPHPExcel->getActiveSheet()->setCellValue('F'. $row_tl, $emp_contribute_pf_total);
          $objPHPExcel->getActiveSheet()->setCellValue('G'. $row_tl, $salary_total_total);
          $objPHPExcel->getActiveSheet()->setCellValue('H'. $row_tl, $ptax_total);
          $objPHPExcel->getActiveSheet()->setCellValue('I'. $row_tl, $income_tax_total);
          $objPHPExcel->getActiveSheet()->setCellValue('J'. $row_tl, $pf_deduction_total);
          $objPHPExcel->getActiveSheet()->setCellValue('K'. $row_tl, $fest_advance_total);
          $objPHPExcel->getActiveSheet()->setCellValue('L'. $row_tl, $other_deduction_total);
          $objPHPExcel->getActiveSheet()->setCellValue('M'. $row_tl, $total_deduction_total);
          $objPHPExcel->getActiveSheet()->setCellValue('N'. $row_tl, $net_pay_total);
          $objPHPExcel->getActiveSheet()->setCellValue('O'. $row_tl, '');
      //}

      $objPHPExcel->setActiveSheetIndex(0);

      // Redirect output to a client’s web browser
      //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$excel_pay_title.'.xlsx"');
      header('Cache-Control: max-age=0');

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');

    }
    // download Student Records In Excel
    function downloadPayslipRecordEmployee($emp_id,$from_date,$to_date) {

    $this->load->library('Classes/PHPExcel');
    $details = $this->College_model->get_payslip_report_excel($emp_id,$from_date,$to_date);
    $objPHPExcel = new PHPExcel();
    //echo "<pre>";print_r($details);die;
    $emp_name = $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$emp_id.'"');

    $excel_title = 'Payment List';

    // Add tab label to the sheet
    $objPHPExcel->getActiveSheet()->setTitle('Payslip Record of '.$emp_name.'');


    // Sheet heading in the first row
    $objPHPExcel->getActiveSheet()->mergeCells('A1:AZ1');
    $objPHPExcel->getActiveSheet()->setCellValue('A1','Payslip Record of '.$emp_name.' From date '.$from_date.' To date '.$to_date.'');

    // Column headings in the forth row
    $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Employee:');
    $objPHPExcel->getActiveSheet()->setCellValue('B3', 'Pay:');
    $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Grade Pay:');
    $objPHPExcel->getActiveSheet()->setCellValue('D3', 'Other Allow:');
    $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Other & Med. Allow:');
    $objPHPExcel->getActiveSheet()->setCellValue('F3', 'Employer P.F. (13.61%):');
    $objPHPExcel->getActiveSheet()->setCellValue('G3', 'TOTAL:');
    $objPHPExcel->getActiveSheet()->setCellValue('H3', 'P.Tax:');
    $objPHPExcel->getActiveSheet()->setCellValue('I3', 'I. Tax:');
    $objPHPExcel->getActiveSheet()->setCellValue('J3', 'PF25.61%:');
    $objPHPExcel->getActiveSheet()->setCellValue('K3', 'Festival Adv:');
    $objPHPExcel->getActiveSheet()->setCellValue('L3', 'Other Deduction:');
    $objPHPExcel->getActiveSheet()->setCellValue('M3', 'Total Deduction:');
    $objPHPExcel->getActiveSheet()->setCellValue('N3', 'Net Pay:');
    $objPHPExcel->getActiveSheet()->setCellValue('O3', 'Payment Date:');



      $row = 4;
      foreach($details as $value) {

          $objPHPExcel->getActiveSheet()->setCellValue('A'. $row, $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$value->emp_id.'"'));
          $objPHPExcel->getActiveSheet()->setCellValue('B'. $row, $value->basic_pay);
          $basic_pay_total+=$value->basic_pay;
          $objPHPExcel->getActiveSheet()->setCellValue('C'. $row, $value->grade_pay);
          $grade_pay_total+=$value->grade_pay;
          $objPHPExcel->getActiveSheet()->setCellValue('D'. $row, $value->other_allow);
          $other_allow_total+=$value->other_allow;
          $objPHPExcel->getActiveSheet()->setCellValue('E'. $row, $value->medical_allow);
          $medical_allow_total+=$value->medical_allow;
          $objPHPExcel->getActiveSheet()->setCellValue('F'. $row, $value->emp_contribute_pf);
          $emp_contribute_pf_total+=$value->emp_contribute_pf;
          $objPHPExcel->getActiveSheet()->setCellValue('G'. $row, $value->salary_total);
          $salary_total_total+=$value->salary_total;
          $objPHPExcel->getActiveSheet()->setCellValue('H'. $row, $value->ptax);
          $ptax_total+=$value->ptax;
          $objPHPExcel->getActiveSheet()->setCellValue('I'. $row, $value->income_tax);
          $income_tax_total+=$value->income_tax;
          $objPHPExcel->getActiveSheet()->setCellValue('J'. $row, $value->pf_deduction);
          $pf_deduction_total+=$value->pf_deduction;
          $objPHPExcel->getActiveSheet()->setCellValue('K'. $row, $value->fest_advance);
          $fest_advance_total+=$value->fest_advance;
          $objPHPExcel->getActiveSheet()->setCellValue('L'. $row, $value->other_deduction);
          $other_deduction_total+=$value->other_deduction;
          $objPHPExcel->getActiveSheet()->setCellValue('M'. $row, $value->total_deduction);
          $total_deduction_total+=$value->total_deduction;
          $objPHPExcel->getActiveSheet()->setCellValue('N'. $row, $value->net_pay);
          $net_pay_total+=$value->net_pay;
          $objPHPExcel->getActiveSheet()->setCellValue('O'. $row, $value->payment_date);

            }
          //$objPHPExcel->getActiveSheet()->getStyle('D' . $row) ->getAlignment() ->setWrapText(true);

          $row++;
          $row_tl = 5+count($details);
          // Column Total Calculation
          $objPHPExcel->getActiveSheet()->setCellValue('A'. $row_tl, 'Total:');
          $objPHPExcel->getActiveSheet()->setCellValue('B'. $row_tl, $basic_pay_total);
          $objPHPExcel->getActiveSheet()->setCellValue('C'. $row_tl, $grade_pay_total);
          $objPHPExcel->getActiveSheet()->setCellValue('D'. $row_tl, $other_allow_total);
          $objPHPExcel->getActiveSheet()->setCellValue('E'. $row_tl, $medical_allow_total);
          $objPHPExcel->getActiveSheet()->setCellValue('F'. $row_tl, $emp_contribute_pf_total);
          $objPHPExcel->getActiveSheet()->setCellValue('G'. $row_tl, $salary_total_total);
          $objPHPExcel->getActiveSheet()->setCellValue('H'. $row_tl, $ptax_total);
          $objPHPExcel->getActiveSheet()->setCellValue('I'. $row_tl, $income_tax_total);
          $objPHPExcel->getActiveSheet()->setCellValue('J'. $row_tl, $pf_deduction_total);
          $objPHPExcel->getActiveSheet()->setCellValue('K'. $row_tl, $fest_advance_total);
          $objPHPExcel->getActiveSheet()->setCellValue('L'. $row_tl, $other_deduction_total);
          $objPHPExcel->getActiveSheet()->setCellValue('M'. $row_tl, $total_deduction_total);
          $objPHPExcel->getActiveSheet()->setCellValue('N'. $row_tl, $net_pay_total);
          $objPHPExcel->getActiveSheet()->setCellValue('O'. $row_tl, '');
      //}

      $objPHPExcel->setActiveSheetIndex(0);

      // Redirect output to a client’s web browser
      //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$excel_title.'.xlsx"');
      header('Cache-Control: max-age=0');

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');

    }
    /*
    | --------------------------------------------------------------------------
    | Payment Received Report
    | --------------------------------------------------------------------------
    */
    function paymentReceived(){

      if ($this->input->post('submit') AND ($this->input->post('submit') == "Search")) {

          $from_date = $this->input->post('from_date');
          $to_date = $this->input->post('to_date');
          $details = $this->College_model->payment_received_report($from_date,$to_date);

              $data = array(
                'details' => $details,
                'from_date' => $this->input->post('from_date'),
                'to_date' => $this->input->post('to_date'),
              );

          $this->load->view('admin/college/payment_received_view', $data);
      } else {
          $this->load->view("admin/college/payment_received_view");
      }

    }

    /*
| --------------------------------------------------------------------------
| Add Payment Received
| --------------------------------------------------------------------------
*/

public function addPaymentReceived() {

    if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

        $this->form_validation->set_rules("date", "Date", "required");
        $this->form_validation->set_rules("reference_id", "Reference Id", "trim|required");
        $this->form_validation->set_rules("student_name", "Student Name", "trim|required");
        $this->form_validation->set_rules("payment_type", "Payment Type", "trim|required");
        $this->form_validation->set_rules("particular", "Particular", "trim|required");

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
            redirect("admin/college/addPaymentReceived");
        } else if ((($this->input->post('credit_ammount')=='')AND ($this->input->post('debit_ammount')=='')) OR (($this->input->post('credit_ammount')!='')AND ($this->input->post('debit_ammount')!='')) ) {

          $this->session->set_flashdata("e_message",'Please Enter Credit Or Debit Amount');
          redirect("admin/college/addPaymentReceived");
        } else{

            $chek_unique = $this->my_custom_functions->get_perticular_count('tbl_payment_received', 'and reference_id="' .$this->input->post('reference_id') .'"');
            if ($chek_unique > 0) {
                $this->session->set_flashdata("e_message", 'Student Record Registered');
                redirect("admin/college/addPaymentReceived");
            }

            $insert_data = array(
                'date' => $this->input->post('date'),
                'reference_id' => $this->input->post('reference_id'),
                'student_name' => $this->input->post('student_name'),
                'payment_type' => $this->input->post('payment_type'),
                'particular' => $this->input->post('particular'),
                'credit_ammount' => $this->input->post('credit_ammount'),
                'debit_ammount' => $this->input->post('debit_ammount')
            );

            $add_data = $this->my_custom_functions->insert_data($insert_data,"tbl_payment_received");

            if ($add_data) {

                $this->session->set_flashdata("s_message", 'Payment Received added successfully.');
                redirect("admin/college/paymentReceived");
            } else {

                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("admin/college/addPaymentReceived");
            }
        }
    } else {

        $this->load->view('admin/college/add_payment_received');
    }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit PaymentReceived
    | --------------------------------------------------------------------------
    */

    public function editPaymentReceived() {

    if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {


      $encrypted_id = $this->input->post('page_id');
      $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
        $this->form_validation->set_rules("date", "Date", "required");
        $this->form_validation->set_rules("reference_id", "Reference Id", "trim|required");
        $this->form_validation->set_rules("student_name", "Student Name", "trim|required");
        $this->form_validation->set_rules("payment_type", "Payment Type", "trim|required");
        $this->form_validation->set_rules("particular", "Particular", "trim|required");

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
            redirect("admin/college/editPaymentReceived/".$page_id);
        }
        // else if ((($this->input->post('credit_ammount')=='')AND ($this->input->post('debit_ammount')=='')) OR (($this->input->post('credit_ammount')!='')AND ($this->input->post('debit_ammount')!='')) ) {
        //
        //   $this->session->set_flashdata("e_message",'Please Enter Credit Or Debit Amount');
        //   redirect("admin/college/editPaymentReceived");
        // }
         else{


            $update_data = array(
                'date' => $this->input->post('date'),
                'reference_id' => $this->input->post('reference_id'),
                'student_name' => $this->input->post('student_name'),
                'payment_type' => $this->input->post('payment_type'),
                'particular' => $this->input->post('particular'),
                'credit_ammount' => $this->input->post('credit_ammount'),
                'debit_ammount' => $this->input->post('debit_ammount')
            );

            $where = array(
              "id" => $page_id,
            );

            $update = $this->my_custom_functions->update_data($update_data,"tbl_payment_received",$where);

            if ($update) {

                $this->session->set_flashdata("s_message", 'Payment Received updated successfully.');
                redirect("admin/college/paymentReceived");
            } else {

                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("admin/college/editPaymentReceived/".$page_id);
            }
        }
    } else {

      $page_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
      $data['details'] = $this->ablfunctions->getDetailsFromId($page_id, "tbl_payment_received");
      $this->load->view('admin/college/edit_payment_received',$data);
    }
    }
        /*
    | --------------------------------------------------------------------------
    | Delete Semester
    | --------------------------------------------------------------------------
    */
    function deletePaymentReceived() {
    $encrypted_id = $this->uri->segment(4);
    $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
    $query = $this->ablfunctions->deleteData("tbl_payment_received", array("id" => $page_id));
    if ($query) { // if deleted
    $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
    redirect("admin/college/paymentReceived");
    } else {
    $this->session->set_flashdata('e_message', 'Record Deletion Failed');
    redirect("admin/college/paymentReceived");
    }
    }
    // download Student Records In Excel
    function downloadPaymentReceived($from_date,$to_date) {

    $this->load->library('Classes/PHPExcel');
    $details = $this->College_model->payment_received_report($from_date,$to_date);
    //echo "<pre>";print_r($details);die;
    $objPHPExcel = new PHPExcel();
    $excel_pay_title = 'Payment Received Report';

    // Add tab label to the sheet
    $objPHPExcel->getActiveSheet()->setTitle('Payment Received Report');


    // Sheet heading in the first row
    $objPHPExcel->getActiveSheet()->mergeCells('A1:Z1');
    $objPHPExcel->getActiveSheet()->setCellValue('A1','Payment Received Report From '.$from_date.' To '.$to_date.'');

    // Column headings in the forth row
    $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Date:');
    $objPHPExcel->getActiveSheet()->setCellValue('B3', 'Reference Id:');
    $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Student Name:');
    $objPHPExcel->getActiveSheet()->setCellValue('D3', 'Student Payment Type:');
    $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Particular:');
    $objPHPExcel->getActiveSheet()->setCellValue('F3', 'Credit Amount:');
    $objPHPExcel->getActiveSheet()->setCellValue('G3', 'Debit Amount:');


      $row = 4;
      $credit_ammount_total =0;
      $debit_ammount_total =0;
      foreach($details as $value) {

          $objPHPExcel->getActiveSheet()->setCellValue('A'. $row, $value->date);
          $objPHPExcel->getActiveSheet()->setCellValue('B'. $row, $value->reference_id);
          $objPHPExcel->getActiveSheet()->setCellValue('C'. $row, $value->student_name);
          $p_type = $this->config->item('student_payment_type');
          $p_id = $value->payment_type;
          //echo $p_type[$p_id];
          $objPHPExcel->getActiveSheet()->setCellValue('D'. $row, $p_type[$p_id]);
          $objPHPExcel->getActiveSheet()->setCellValue('E'. $row, $value->particular);
          $objPHPExcel->getActiveSheet()->setCellValue('F'. $row, $value->credit_ammount);
          $credit_ammount_total+=$value->credit_ammount;
          $objPHPExcel->getActiveSheet()->setCellValue('G'. $row, $value->debit_ammount);
          $debit_ammount_total+=$value->debit_ammount;

            }
          //$objPHPExcel->getActiveSheet()->getStyle('D' . $row) ->getAlignment() ->setWrapText(true);

          $row++;
          $row_tl = 5+count($details);
          // Column Total Calculation
          $objPHPExcel->getActiveSheet()->mergeCells('A'.$row_tl.':E'.$row_tl.'');
          $objPHPExcel->getActiveSheet()->setCellValue('A'. $row_tl, 'Total:');
          $objPHPExcel->getActiveSheet()->setCellValue('F'. $row_tl, $credit_ammount_total);
          $objPHPExcel->getActiveSheet()->setCellValue('G'. $row_tl, $debit_ammount_total);

      //}

      $objPHPExcel->setActiveSheetIndex(0);

      // Redirect output to a client’s web browser
      //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$excel_pay_title.'.xlsx"');
      header('Cache-Control: max-age=0');

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');

    }
    /*
    | --------------------------------------------------------------------------
    | Payment expense Report
    | --------------------------------------------------------------------------
    */
    function expenseReport(){

      if ($this->input->post('submit') AND ($this->input->post('submit') == "Search")) {

          $from_date = $this->input->post('from_date');
          $to_date = $this->input->post('to_date');
          $details = $this->College_model->payment_expense_report($from_date,$to_date);

              $data = array(
                'details' => $details,
                'from_date' => $this->input->post('from_date'),
                'to_date' => $this->input->post('to_date'),
              );

          $this->load->view('admin/college/expense_report', $data);
      } else {
          $this->load->view("admin/college/expense_report");
      }

    }
        /*
    | --------------------------------------------------------------------------
    | Add Expenses
    | --------------------------------------------------------------------------
    */

    public function addExpenses() {

    if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

        $this->form_validation->set_rules("date", "Date", "required");
        $this->form_validation->set_rules("reference_id", "Reference Id", "trim|required");
        $this->form_validation->set_rules("account_name", "Account Name", "trim|required");
        //$this->form_validation->set_rules("payment_type", "Payment Type", "trim|required");
        $this->form_validation->set_rules("particular", "Particular", "trim|required");

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
            redirect("admin/college/addExpenses");
        } else if ((($this->input->post('credit_ammount')=='')AND ($this->input->post('debit_ammount')=='')) OR (($this->input->post('credit_ammount')!='')AND ($this->input->post('debit_ammount')!='')) ) {

          $this->session->set_flashdata("e_message",'Please Enter Credit Or Debit Amount');
          redirect("admin/college/addExpenses");
        } else{

            $chek_unique = $this->my_custom_functions->get_perticular_count('tbl_college_expenses', 'and reference_id="' .$this->input->post('reference_id') .'"');
            if ($chek_unique > 0) {
                $this->session->set_flashdata("e_message", 'Expenses Record Registered');
                redirect("admin/college/addExpenses");
            }

            $insert_data = array(
                'date' => $this->input->post('date'),
                'reference_id' => $this->input->post('reference_id'),
                'account_name' => $this->input->post('account_name'),
                //'payment_type' => $this->input->post('payment_type'),
                'particular' => $this->input->post('particular'),
                'credit_ammount' => $this->input->post('credit_ammount'),
                'debit_ammount' => $this->input->post('debit_ammount')
            );

            $add_data = $this->my_custom_functions->insert_data($insert_data,"tbl_college_expenses");

            if ($add_data) {

                $this->session->set_flashdata("s_message", 'Expenses Record added successfully.');
                redirect("admin/college/expenseReport");
            } else {

                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("admin/college/addExpenses");
            }
        }
    } else {

        $this->load->view('admin/college/add_expenses');
    }
    }
        /*
    | --------------------------------------------------------------------------
    | Edit Expenses
    | --------------------------------------------------------------------------
    */

    public function editExpenses() {

    if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {


      $encrypted_id = $this->input->post('page_id');
      $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
        $this->form_validation->set_rules("date", "Date", "required");
        $this->form_validation->set_rules("reference_id", "Reference Id", "trim|required");
        $this->form_validation->set_rules("account_name", "Account Name", "trim|required");
        //$this->form_validation->set_rules("payment_type", "Payment Type", "trim|required");
        $this->form_validation->set_rules("particular", "Particular", "trim|required");

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
            redirect("admin/college/editExpenses/".$page_id);
        }
        // else if ((($this->input->post('credit_ammount')=='')AND ($this->input->post('debit_ammount')=='')) OR (($this->input->post('credit_ammount')!='')AND ($this->input->post('debit_ammount')!='')) ) {
        //
        //   $this->session->set_flashdata("e_message",'Please Enter Credit Or Debit Amount');
        //   redirect("admin/college/editExpenses");
        // }
         else{


            $update_data = array(
                'date' => $this->input->post('date'),
                'reference_id' => $this->input->post('reference_id'),
                'account_name' => $this->input->post('account_name'),
                //'payment_type' => $this->input->post('payment_type'),
                'particular' => $this->input->post('particular'),
                'credit_ammount' => $this->input->post('credit_ammount'),
                'debit_ammount' => $this->input->post('debit_ammount')
            );

            $where = array(
              "id" => $page_id,
            );

            $update = $this->my_custom_functions->update_data($update_data,"tbl_college_expenses",$where);

            if ($update) {

                $this->session->set_flashdata("s_message", 'Expenses Record updated successfully.');
                redirect("admin/college/expenseReport");
            } else {

                $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                redirect("admin/college/editExpenses/".$page_id);
            }
        }
    } else {

      $page_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
      $data['details'] = $this->ablfunctions->getDetailsFromId($page_id, "tbl_college_expenses");
      $this->load->view('admin/college/edit_expenses',$data);
    }
    }
        /*
    | --------------------------------------------------------------------------
    | Delete Semester
    | --------------------------------------------------------------------------
    */
    function deleteExpenses() {
    $encrypted_id = $this->uri->segment(4);
    $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
    $query = $this->ablfunctions->deleteData("tbl_college_expenses", array("id" => $page_id));
    if ($query) { // if deleted
    $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
    redirect("admin/college/expenseReport");
    } else {
    $this->session->set_flashdata('e_message', 'Record Deletion Failed');
    redirect("admin/college/expenseReport");
    }
    }
    // download Student Records In Excel
    function downloadExpenses($from_date,$to_date) {

    $this->load->library('Classes/PHPExcel');
    $details = $this->College_model->payment_expense_report($from_date,$to_date);
    //echo "<pre>";print_r($details);die;
    $objPHPExcel = new PHPExcel();
    $excel_pay_title = 'Expenses Report';

    // Add tab label to the sheet
    $objPHPExcel->getActiveSheet()->setTitle('Expenses Report');


    // Sheet heading in the first row
    $objPHPExcel->getActiveSheet()->mergeCells('A1:Z1');
    $objPHPExcel->getActiveSheet()->setCellValue('A1','Expenses Report From '.$from_date.' To '.$to_date.'');

    // Column headings in the forth row
    $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Date:');
    $objPHPExcel->getActiveSheet()->setCellValue('B3', 'Reference Id:');
    $objPHPExcel->getActiveSheet()->setCellValue('C3', 'Account Name:');
    $objPHPExcel->getActiveSheet()->setCellValue('D3', 'Particular:');
    $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Credit Amount:');
    $objPHPExcel->getActiveSheet()->setCellValue('F3', 'Debit Amount:');


      $row = 4;
      $credit_ammount_total =0;
      $debit_ammount_total =0;
      foreach($details as $value) {

          $objPHPExcel->getActiveSheet()->setCellValue('A'. $row, $value->date);
          $objPHPExcel->getActiveSheet()->setCellValue('B'. $row, $value->reference_id);
          $objPHPExcel->getActiveSheet()->setCellValue('C'. $row, $value->account_name);
          $objPHPExcel->getActiveSheet()->setCellValue('D'. $row, $value->particular);
          $objPHPExcel->getActiveSheet()->setCellValue('E'. $row, $value->credit_ammount);
          $credit_ammount_total+=$value->credit_ammount;
          $objPHPExcel->getActiveSheet()->setCellValue('F'. $row, $value->debit_ammount);
          $debit_ammount_total+=$value->debit_ammount;

            }
          //$objPHPExcel->getActiveSheet()->getStyle('D' . $row) ->getAlignment() ->setWrapText(true);

          $row++;
          $row_tl = 5+count($details);
          // Column Total Calculation
          $objPHPExcel->getActiveSheet()->mergeCells('A'.$row_tl.':D'.$row_tl.'');
          $objPHPExcel->getActiveSheet()->setCellValue('A'. $row_tl, 'Total:');
          $objPHPExcel->getActiveSheet()->setCellValue('E'. $row_tl, $credit_ammount_total);
          $objPHPExcel->getActiveSheet()->setCellValue('F'. $row_tl, $debit_ammount_total);

      //}

      $objPHPExcel->setActiveSheetIndex(0);

      // Redirect output to a client’s web browser
      //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheet\ml.sheet');
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;filename="'.$excel_pay_title.'.xlsx"');
      header('Cache-Control: max-age=0');

      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');

    }
    /*
    | --------------------------------------------------------------------------
    | Payment expense Report
    | --------------------------------------------------------------------------
    */
    function totalPaymentReport(){

      if ($this->input->post('submit') AND ($this->input->post('submit') == "Search")) {

          $from_date = $this->input->post('from_date');
          $to_date = $this->input->post('to_date');
          $payment_details = $this->College_model->payment_received_report($from_date,$to_date);
          $expense_details = $this->College_model->payment_expense_report($from_date,$to_date);

              $data = array(
                'payment_details' => $payment_details,
                'expense_details' => $expense_details,
                'from_date' => $this->input->post('from_date'),
                'to_date' => $this->input->post('to_date'),
              );

          $this->load->view('admin/college/total_payment_report', $data);
      } else {
          $this->load->view("admin/college/total_payment_report");
      }

    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////   MANAGE TIME TABLE PART
    //////////////////////////////////////////////////////////////////////

    public function manageTimeTable() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            $class_id = $this->input->post('class_id');
            $section_id = $this->input->post('section_id');
            $semester_id = $this->my_custom_functions->get_particular_field_value('tbl_semester', 'id', 'and course_id = "' . $class_id . '" ');
            $data['period'] = $this->College_model->get_time_table_data($semester_id, $section_id);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data('tbl_courses', 'and status = 1');
            //$data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1 order by period_start_time ASC');
            $data['post_data'] = array('class_id' => $class_id, 'section_id' => $section_id);
        } else {
            $data['period'] = array();
            $data['post_data'] = array('class_id' => '', 'section_id' => '');
            //$data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1 order by period_start_time ASC');
            $data['class_list'] = $this->my_custom_functions->get_multiple_data("tbl_courses", 'and status = 1');
        }
        $data['page_title'] = 'Manage timetable';
        $this->load->view('admin/college/manage_time_table', $data);
    }
    public function addTimeTable() {
        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {
            //echo "<pre>";print_r($_POST);die;
            /// tbl_admins contents
            $this->form_validation->set_rules("day_id", "Day", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            //$this->form_validation->set_rules("period_start_time", "Period Start Time", "trim|required");
            //$this->form_validation->set_rules("period_end_time", "Period End Time", "trim|required");
            //$this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("school/user/addTimeTable");
            } else { /// Update admin
                if ($this->input->post('attendance_class')) {
                    $attendance_period = 1;
                } else {
                    $attendance_period = 0;
                }
                if ($this->input->post('start_minute') < 10) {
                    $start_min = "0" . $this->input->post('start_minute');
                } else {
                    $start_min = $this->input->post('start_minute');
                }
                $start_time = $this->input->post('start_hour') . ':' . $start_min . ' ' . $this->input->post('start_meridian');
                $period_start_time = date('H:i', strtotime("$start_time"));

                if ($this->input->post('end_minute') < 10) {
                    $end_min = "0" . $this->input->post('end_minute');
                } else {
                    $end_min = $this->input->post('end_minute');
                }
                $end_time = $this->input->post('end_hour') . ':' . $end_min . ' ' . $this->input->post('end_meridian');
                $period_end_time = date('H:i', strtotime("$end_time"));



                $routine_data = array(
                    'day_id' => $this->input->post('day_id'),
                    'semester_id' => $this->input->post('semester_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'period_name' => $this->input->post('period_name'),
                    'subject_id' => $this->input->post('subject_id'),
                    'teacher_id' => $this->input->post('teacher_id'),
                    //'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    //'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'period_start_time' => $period_start_time,
                    'period_end_time' => $period_end_time,
                    'attendance_class' => $attendance_period,
                    'status' => 1,
                );
                //echo "<pre>";print_r($routine_data);die;
                $add_routine_data = $this->my_custom_functions->insert_data($routine_data, TBL_TIMETABLE);
                if ($add_routine_data) {
                    $this->session->set_flashdata("s_message", 'Routine added successfully.');
                    redirect("admin/college/manageTimeTable");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/college/manageTimeTable");
                }
            }
        } else {
            $data['page_title'] = 'Add timetable';
            $data['class_list'] = $this->my_custom_functions->get_multiple_data("tbl_courses", 'and status = 1');
            //$data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['teacher_list'] = $this->my_custom_functions->get_multiple_data('tbl_staff', 'and designation = "2"');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data('tbl_subjects', 'and status = 1');
            $this->load->view('admin/college/add_time_table', $data);
        }
    }
    function get_section_list() {
        $class_id = $this->input->post('class_id');

        $sec_id = $this->input->post('sec_id');
        $get_section_list = $this->my_custom_functions->get_multiple_data('tbl_section', 'and class_id = "' . $class_id . '" and status = 1');

        echo '<option value="">Select Section</option>';
        foreach ($get_section_list as $sec_list) {
            if ($sec_id == $sec_list['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $sec_list['id'] . '" ' . $selected . '>' . $sec_list['section_name'] . '</option>';
        }
    }
    function get_semester_list() {
        $class_id = $this->input->post('class_id');

        $sem_id = $this->input->post('sem_id');
        $get_semester_list = $this->my_custom_functions->get_multiple_data('tbl_semester', ' and course_id = "' . $class_id . '"');

        echo '<option value="">Select Semester</option>';
        foreach ($get_semester_list as $sec_list) {
            if ($sem_id == $sec_list['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $sec_list['id'] . '" ' . $selected . '>' . $sec_list['semester_name'] . '</option>';
        }
    }
    function get_semester_list_tt() {
        $class_id = $this->input->post('class_id');

        $sem_id = $this->input->post('sem_id');
        //$get_semester_list = $this->my_custom_functions->get_multiple_data('tbl_semester', 'and course_id = "' . $class_id . '" ');
        $get_semester_list = $this->my_custom_functions->get_multiple_data('tbl_semester', 'and course_id = "' . $class_id . '" and session_id = "' . $this->session->userdata('session_id') . '"');
        echo '<option value="">Select Semester</option>';
        foreach ($get_semester_list as $sec_list) {
            if ($sem_id == $sec_list['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $sec_list['id'] . '" ' . $selected . '>' . $sec_list['semester_name'] . '</option>';
        }
    }
    function get_semester_list_for_score() {
        $class_id = $this->input->post('class_id');
        $session_id = $this->input->post('session_id');
        $post_sem_id = $this->input->post('semester_id');

        $semester_list = $this->my_custom_functions->get_multiple_data('tbl_semester', 'and course_id = "' . $class_id . '" and session_id = "' . $session_id . '"');

        echo '<option value="">Select Section</option>';
        $selected = '';
        foreach ($semester_list as $semester) {
            if ($post_sem_id == $semester['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $semester['id'] . '" ' . $selected . '>' . $semester['semester_name'] . '</option>';
        }
    }
    function get_term_list() {
        $semester_id = $this->input->post('semester_id');
        $term_list = $this->my_custom_functions->get_multiple_data('tbl_terms', 'and semester_id = "' . $semester_id . '"');
        $post_term_id = $this->input->post('term_id');
        echo '<option value="">Select term</option>';
        $selected = '';
        foreach ($term_list as $term) {
            if ($post_term_id == $term['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $term['id'] . '" ' . $selected . '>' . $term['term_name'] . '</option>';
        }
    }
    function get_exam_list() {

        $term_id = $this->input->post('term_id');
        $post_exam_id = $this->input->post('exam_id');
        $exam_list = $this->College_model->get_exam_list($term_id);
        echo '<option value="">Select Section</option>';
        $selected = '';
        foreach ($exam_list as $exam) {
            if ($post_exam_id == $exam['id']) {
                $selected = 'selected="selected"';
            } else {
                $selected = '';
            }
            echo '<option value="' . $exam['id'] . '" ' . $selected . '>' . $exam['exam_name'] . '</option>';
        }
    }
    //////////////////////////////////  CHECK DUPLICATE PERIOD AJAX ///////////////////////////

    function check_duplicate_period(){
         $day_id = $this->input->post('day_id');
         $class_id = $this->input->post('class_id');
         $semester_id = $this->input->post('semester_id');
         $section_id = $this->input->post('section_id');
         $start_time = date('H:i:s', strtotime($this->input->post('start_time')));
         $end_time = date('H:i:s', strtotime($this->input->post('end_time')));
         $subject_id = $this->input->post('subject_id');
         $teacher_id = $this->input->post('teacher_id');

        //$perion_list = $this->my_custom_functions->get_multiple_data(TBL_TIMETABLE,'and school_id = "' . $this->session->userdata('school_id') . '" and day_id = "'.$day_id.'" and semester_id = "'.$semester_id.'" and class_id = "'.$class_id.'" and section_id = "'.$section_id.'" and ((period_start_time <="'.$start_time.'" and period_end_time >= "'.$start_time.'") OR (period_start_time <="'.$end_time.'" and period_end_time >= "'.$end_time.'")) and subject_id = "'.$subject_id.'" and teacher_id = "'.$teacher_id.'"');
        $perion_list = $this->my_custom_functions->get_multiple_data(TBL_TIMETABLE,'and day_id = "'.$day_id.'"  and ((period_start_time <="'.$start_time.'" and period_end_time >= "'.$start_time.'") OR (period_start_time <="'.$end_time.'" and period_end_time >= "'.$end_time.'"))  and teacher_id = "'.$teacher_id.'"');
        //echo $this->db->last_query();
        if(!empty($perion_list)){
            foreach($perion_list as $period){
                $teacher_name = $this->my_custom_functions->get_particular_field_value('tbl_staff', 'staff_name', 'and id = "' . $period['teacher_id'] . '"');
                $class_name = $this->my_custom_functions->get_particular_field_value('tbl_courses', 'course_name', 'and id = "' . $period['class_id'] . '"');
                $section_name = $this->my_custom_functions->get_particular_field_value('tbl_section', 'section_name', 'and id = "' . $period['section_id'] . '"');
                $day_name = $this->config->item('days_list')[$period['day_id']];
                $class_start_time = date('H:i A',strtotime($period['period_start_time']));
                $class_end_time = date('H:i A',strtotime($period['period_end_time']));
            }

            //$msg = "Teacher ".$teacher_name." is already assigned in ".$class_name."-".$section_name." on ".$day_name." from ".$class_start_time." to ".$class_end_time.". Do you want to proceed?";
              $msg = "class is already assigned to this teacher do you still want to proceed?";
        }else{
            $msg = "Do you want to proceed?";
        }
        echo $msg;


    }
    /*
    | --------------------------------------------------------------------------
    | Edit TimeTable
    | --------------------------------------------------------------------------
    */
    public function editTimeTable() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $routine_id = $this->input->post('routine_id');

            /// tbl_admins contents
            $this->form_validation->set_rules("day_id", "Day", "trim|required");
            $this->form_validation->set_rules("class_id", "Class", "trim|required");
            $this->form_validation->set_rules("section_id", "Section", "trim|required");
            $this->form_validation->set_rules("period_name", "Period Name", "trim|required");
            //$this->form_validation->set_rules("subject_id", "Subject", "trim|required");
            //$this->form_validation->set_rules("teacher_id", "Teacher", "trim|required");
            //$this->form_validation->set_rules("status", "Status", "trim|required");
            //$this->form_validation->set_rules("subject_code", "Subject Code", "trim|required");


            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/editTimeTable/" . $this->ablfunctions->ablEncrypt($routine_id));
            } else { /// Update admin
                if ($this->input->post('attendance_class')) {
                    $attendance_period = 1;
                } else {
                    $attendance_period = 0;
                }

                if ($this->input->post('start_minute') < 10) {
                    $start_min = "0" . $this->input->post('start_minute');
                } else {
                    $start_min = $this->input->post('start_minute');
                }
                $start_time = $this->input->post('start_hour') . ':' . $start_min . ' ' . $this->input->post('start_meridian');
                $period_start_time = date('H:i', strtotime("$start_time"));

                if ($this->input->post('end_minute') < 10) {
                    $end_min = "0" . $this->input->post('end_minute');
                } else {
                    $end_min = $this->input->post('end_minute');
                }
                $end_time = $this->input->post('end_hour') . ':' . $end_min . ' ' . $this->input->post('end_meridian');
                $period_end_time = date('H:i', strtotime("$end_time"));


                $routine_data_update = array(
                    'day_id' => $this->input->post('day_id'),
                    'semester_id' => $this->input->post('semester_id'),
                    'class_id' => $this->input->post('class_id'),
                    'section_id' => $this->input->post('section_id'),
                    'period_name' => $this->input->post('period_name'),
                    'subject_id' => $this->input->post('subject_id'),
                    'teacher_id' => $this->input->post('teacher_id'),
                    //'period_start_time' => date("H:i", strtotime($this->input->post('period_start_time'))),
                    //'period_end_time' => date("H:i", strtotime($this->input->post('period_end_time'))),
                    'period_start_time' => $period_start_time,
                    'period_end_time' => $period_end_time,
                    'attendance_class' => $attendance_period,
                );

                //echo "<pre>";print_r($routine_data_update);die;
                $condition = array(
                    'id' => $routine_id
                );
                $update = $this->my_custom_functions->update_data($routine_data_update, TBL_TIMETABLE, $condition);
                if ($update) {
                    $this->session->set_flashdata("s_message", 'Routine successfully updated.');
                    redirect("admin/college/manageTimeTable");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please try again later');
                    redirect("admin/college/editTimeTable/" . $this->ablfunctions->ablEncrypt($routine_id));
                }
            }
        } else {
            $routine_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
            $data['routine_data'] = $this->my_custom_functions->get_details_from_id($routine_id, TBL_TIMETABLE);
            $data['class_list'] = $this->my_custom_functions->get_multiple_data("tbl_courses", 'and status = 1');
            //$data['period_list'] = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');
            $data['teacher_list'] = $this->my_custom_functions->get_multiple_data('tbl_staff', 'and designation = "2"');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data('tbl_subjects', 'and status = 1');
            $data['page_title'] = 'Edit timetable';
            $this->load->view('admin/college/edit_time_table', $data);
        }
    }

        /*
    | --------------------------------------------------------------------------
    | Delete deleteTimeTable
    | --------------------------------------------------------------------------
    */
    function deleteTimeTable() {
    $encrypted_id = $this->uri->segment(4);
    $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
    $query = $this->ablfunctions->deleteData(TBL_TIMETABLE, array("id" => $page_id));
    if ($query) { // if deleted
    $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
    redirect("admin/college/manageTimeTable");
    } else {
    $this->session->set_flashdata('e_message', 'Record Deletion Failed');
    redirect("admin/college/manageTimeTable");
    }
    }
    /*
    | --------------------------------------------------------------------------
    | Temp teacher Assign
    | --------------------------------------------------------------------------
    */
    function tempTeacherAssign() {
        $data['post_data'] = array();
        if ($this->input->post('submit') && $this->input->post('submit') != '') {

            $data['post_data'] = array('teacher_id' => $this->input->post('teacher_id'), 'post_date' => $this->input->post('class_date'), 'available_teacher' => $this->input->post('available_teacher'));

            $day_num = date('N', strtotime($this->my_custom_functions->database_date($this->input->post('class_date'))));

            $data['classes_list'] = $this->College_model->get_teacher_list_temp_assign($day_num);

        }
        $data['teacher_list'] = $this->my_custom_functions->get_multiple_data('tbl_staff', 'and designation = "2"');
        $data['page_title'] = 'Temporary teacher assign';
        $this->load->view('admin/college/manage_temp_teacher', $data);
    }

    function assignTempTeacher() {
        //echo "<pre>";print_r($_POST);die;
        $teacher_list = $this->input->post('teacher_id');
        $period_id_list = $this->input->post('period_id');
        $class_assign_date = $this->input->post('class_assign_date');
        //echo "<pre>";print_r($teacher_list);
        $day_num = date('N', strtotime($this->my_custom_functions->database_date($class_assign_date)));


        foreach ($teacher_list as $key => $teacher_id) {

            if ($teacher_id != '') {

                $period_detail = $this->my_custom_functions->get_details_from_id($key, TBL_TIMETABLE);
                $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and course_id = "' . $period_detail['class_id'] . '" and session_id = "' . $this->session->userdata('session_id') . '"');
                $check_classes = $this->my_custom_functions->get_perticular_count(TBL_TEMP_TIMETABLE, 'and day_id = "' . $day_num . '" and class_date = "' . $this->my_custom_functions->database_date($class_assign_date) . '" and class_id = "' . $period_detail['class_id'] . '" and semester_id = "' . $semester_id . '" and section_id = "' . $period_detail['section_id'] . '" and subject_id = "' . $period_detail['subject_id'] . '"');
                if ($check_classes > 0) {
                    $update_data = array(
                        'teacher_id' => $teacher_id,
                    );

                    $condition = array(
                        'day_id' => $day_num,
                        'class_id' => $period_detail['class_id'],
                        'semester_id' => $semester_id,
                        'section_id' => $period_detail['section_id'],
                        'subject_id' => $period_detail['subject_id'],
                        'class_date' => $this->my_custom_functions->database_date($class_assign_date)
                    );
                    $update = $this->my_custom_functions->update_data($update_data, TBL_TEMP_TIMETABLE, $condition);

                    //////////////////////////////  SEND PUSH NOTIFICATION FOR TEMPORARY CLASS ASSIGN   /////////////////////////////////////

                    // $class = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $period_detail['class_id'] . '"');
                    // $section = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $period_detail['section_id'] . '"');
                    // $subject_name = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $period_detail['subject_id'] . '"');
                    //
                    // $email_data = $this->config->item("temporary_class");
                    // $from_email_variable = $this->my_custom_functions->get_particular_field_value(TBL_SYSTEM_EMAILS, "from_email_variable", " and variable_name='temporary_class'");
                    // $from_email = $this->config->item($from_email_variable);
                    // $subject = $email_data['subject'];
                    // $addressing_user = $email_data['addressing_user'];
                    // $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                    //     'Class' => $class,
                    //     'Section' => $section,
                    //     'Subject' => $subject_name,
                    //     'date' => $class_assign_date
                    //         )
                    // );
                    //
                    // $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                    //     'unsubscribe' => '<a href="javascript:;" target="_blank">unsubscribe</a>'
                    //         )
                    // );
                    // $institute_name = $this->my_custom_functions->sprintf_email($email_data['institute_name'], array(
                    //     'institute_name' => 'Team Bidyaaly'
                    //         )
                    // );
                    // $system_text = $this->my_custom_functions->sprintf_email($email_data['systemtext'], array(
                    //     'systemtext' => SYSTEM_TEXT
                    //         )
                    // );
                    // $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe, $institute_name, $system_text);
                    //echo $full_mail;die ;

                    //$this->my_custom_functions->SendNotification($teacher_id, array("subject" => $subject, "from" => $from_email, "push_content" => $message, "sms_content" => $message, "email_content" => $full_mail), NOTIFICATION_TYPE_TEMP_CLASS_ASSIGN); // done notification
                    //////////////////////////////  SEND PUSH NOTIFICATION FOR TEMPORARY CLASS ASSIGN END   /////////////////////////////////////
                } else {
                    $class_data = array(
                        'day_id' => $day_num,
                        'semester_id' => $semester_id,
                        'class_id' => $period_detail['class_id'],
                        'section_id' => $period_detail['section_id'],
                        'period_start_time' => $period_detail['period_start_time'],
                        'period_end_time' => $period_detail['period_end_time'],
                        'period_name' => $period_detail['period_name'],
                        'subject_id' => $period_detail['subject_id'],
                        'teacher_id' => $teacher_id,
                        'attendance_class' => $period_detail['attendance_class'],
                        'status' => $period_detail['status'],
                        'class_date' => $this->my_custom_functions->database_date($class_assign_date)
                    );

                    $insert_data = $this->my_custom_functions->insert_data($class_data, TBL_TEMP_TIMETABLE);

                    //////////////////////////////  SEND PUSH NOTIFICATION FOR TEMPORARY CLASS ASSIGN   /////////////////////////////////////
                    //
                    // $class = $this->my_custom_functions->get_particular_field_value(TBL_CLASSES, 'class_name', 'and id = "' . $period_detail['class_id'] . '"');
                    // $section = $this->my_custom_functions->get_particular_field_value(TBL_SECTION, 'section_name', 'and id = "' . $period_detail['section_id'] . '"');
                    // $subject_name = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id = "' . $period_detail['subject_id'] . '"');
                    // $email_data = $this->config->item("temporary_class");
                    // $from_email_variable = $this->my_custom_functions->get_particular_field_value(TBL_SYSTEM_EMAILS, "from_email_variable", " and variable_name='temporary_class'");
                    // $from_email = $this->config->item($from_email_variable);
                    // $subject = $email_data['subject'];
                    // $addressing_user = $email_data['addressing_user'];
                    // $message = $this->my_custom_functions->sprintf_email($email_data['mail_body'], array(
                    //     'Class' => $class,
                    //     'Section' => $section,
                    //     'Subject' => $subject_name,
                    //     'date' => $class_assign_date
                    //         )
                    // );
                    //
                    // $unsubscribe = $this->my_custom_functions->sprintf_email($email_data['unsubscribe'], array(
                    //     'unsubscribe' => '<a href="javascript:;" target="_blank">unsubscribe</a>'
                    //         )
                    // );
                    // $full_mail = $this->my_custom_functions->CreateSystemEmail($addressing_user, $message, $unsubscribe);
                    //
                    //
                    // $this->my_custom_functions->SendNotification($teacher_id, array("subject" => $subject, "from" => $from_email, "push_content" => $message, "sms_content" => $message, "email_content" => $full_mail), NOTIFICATION_TYPE_TEMP_CLASS_ASSIGN); // done notification
                    //////////////////////////////  SEND PUSH NOTIFICATION FOR TEMPORARY CLASS ASSIGN END   /////////////////////////////////////
                }
            }
        }
        $this->session->set_flashdata("s_message", 'Teacher successfully assigned');
        redirect("admin/college/tempTeacherAssign/");
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Score
    | --------------------------------------------------------------------------
    */
    function manageScore() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $class_id = $this->input->post('class_id');
            $section_id = $this->input->post('section_id');
            $semester_id = $this->input->post('semester_id');
            $term_id = $this->input->post('term_id');
            $exam_id = $this->input->post('exam_id');
            $session_id = $this->session->userdata('session_id');
            $data['student_list'] = $this->College_model->get_student_data($semester_id);
            $data['session_list'] = $this->my_custom_functions->get_multiple_data(TBL_SESSION, '');
            $data['class_list'] = $this->my_custom_functions->get_multiple_data('tbl_courses', 'and status = 1');
            $data['post_data'] = array('class_id' => $class_id, 'section_id' => $section_id, 'semester_id' => $semester_id, 'term_id' => $term_id, 'exam_id' => $exam_id);

            $exam_student_list = array();
            foreach ($data['student_list'] as $row) {
                $exam_student_list[] = $row['id'];
            }
            $this->session->set_userdata("exam_student_list", $exam_student_list);
        } else {

            $data['period'] = array();
            $data['post_data'] = array('semester_id' => '', 'class_id' => '', 'section_id' => '', 'term_id' => '', 'exam_id' => '');
            $data['session_list'] = $this->my_custom_functions->get_multiple_data(TBL_SESSION, '');
            $data['class_list'] = $this->my_custom_functions->get_multiple_data('tbl_courses', 'and status = 1');
        }

        $data['page_title'] = 'Manage score';
        $this->load->view('admin/college/manage_score', $data);
    }
    function addScore() {

        if ($this->input->post('submit') == 'Save' || $this->input->post('submit') == 'Save & Next') {

            $student_id = $this->my_custom_functions->ablDecrypt($this->input->post('student_id'));
            $exam_id = $this->my_custom_functions->ablDecrypt($this->input->post('exam_id'));
            $score_data_list = $this->input->post('marks_obtain');
            if (!empty($score_data_list)) {
                foreach ($score_data_list as $score_id => $score) {
                    $check_score_id_exist = $this->my_custom_functions->get_perticular_count(TBL_SCORE, 'and student_id = "' . $student_id . '" and score_id = "' . $score_id . '"');
                    if ($check_score_id_exist > 0) {
                        $update_data = array('score' => $score);
                        $condition = array(
                            //'school_id' => $this->session->userdata('school_id'),
                            'score_id' => $score_id,
                            'student_id' => $student_id
                        );
                        $update_score = $this->my_custom_functions->update_data($update_data, TBL_SCORE, $condition);
                    } else {
                        $score_data = array(
                            //'school_id' => $this->session->userdata('school_id'),
                            'exam_id' => $exam_id,
                            'score_id' => $score_id,
                            'student_id' => $student_id,
                            'score' => $score
                        );
                        $insert_score_data = $this->my_custom_functions->insert_data($score_data, TBL_SCORE);
                    }
                }
            }

            $this->session->set_flashdata("s_message", 'Exam record added successfully.');
            if ($this->input->post('submit') == 'Save') {
                redirect("admin/college/addScore/" . $this->input->post('student_id') . '/' . $this->input->post('exam_id'));
            } else if ($this->input->post('submit') == 'Save & Next') {
                $exam_student_list = $this->session->userdata("exam_student_list");
                $student_id = $this->my_custom_functions->ablEncrypt($exam_student_list[$this->input->post('next_student_index')]);

                if (count($exam_student_list) == $this->input->post('next_student_index')) {
                    redirect("admin/college/manageScore");
                } else {
                    redirect("admin/college/addScore/" . $student_id . '/' . $this->input->post('exam_id'));
                }
            }
        } else {

            $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
            $exam_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(5));
            $data['student_detail'] = $this->College_model->student_detail($student_id);
            $data['score_card'] = $this->College_model->score_card_detail($exam_id);
            $data['page_title'] = 'Add score';

            $exam_student_list = $this->session->userdata("exam_student_list");
            $student_index = array_search($student_id, $exam_student_list);
            $data['next_student_index'] = $student_index + 1;

            $this->load->view('admin/college/score_detail', $data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | View Score
    | --------------------------------------------------------------------------
    */
    function viewScore() {
        $data['page_title'] = 'View score';
        $student_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(4));
        $exam_id = $this->my_custom_functions->ablDecrypt($this->uri->segment(5));
        $data['student_detail'] = $this->College_model->student_detail($student_id);
        $data['score_card'] = $this->College_model->score_card_detail($exam_id);
        $this->load->view('admin/college/view_score_detail', $data);
    }


            function manageSyllabus() {
            $data['page_title'] = 'Manage notice';
            $data['details'] = $this->my_custom_functions->get_multiple_data('tbl_syllabus', 'ORDER BY id DESC');
            $this->load->view('admin/college/manage_syllabus', $data);
        }
            /*
        | --------------------------------------------------------------------------
        | Edit page
        | --------------------------------------------------------------------------
        */
        function addSyllabus() {
            if ($this->input->post('submit') && $this->input->post('submit') != '') {
                //$this->form_validation->set_rules("notice_heading", "Notice Heading", "trim|required");
                $this->form_validation->set_rules("syllabus_text", "Syllabus Text", "trim|required");

                if ($this->form_validation->run() == false) {

                    $this->session->set_flashdata("e_message", validation_errors());
                    redirect("admin/college/addSyllabus");

                }else{
                $data = array(
                        'course_id' => $this->input->post('course_id'),
                        'issue_date' => date('Y-m-d'),
                        'syllabus_text' => $this->input->post('syllabus_text'),
                        'status' => 1
                    );
                $notice_id = $this->my_custom_functions->insert_data_last_id($data, 'tbl_syllabus');

                         if ($notice_id) {
                             if (isset($_FILES['doc_upload'])) {

                            if ($_FILES['doc_upload']['name'] != "") {

                                //$file_name = $student_from_id.'.'.'jpg';
                                //$file_name = $student_from_id.'_'.'jpg';
                                $file_name = $notice_id.'_'.basename($_FILES['doc_upload']['name']);

                                $config = array();
                                $config['upload_path']          = UPLOAD_DIR.SYLLABUS_DOCUMENT;
                                $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                                $config['file_name']            = $file_name;
                                //$config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                                $this->load->library('upload');
                                $this->upload->initialize($config);

                                if (!$this->upload->do_upload('doc_upload')) {

                                    $img_upload_message = $this->upload->display_errors();

                                } else {
                                    $data = array(
                                        "document_url" => $file_name
                                    );

                                    $table = "tbl_syllabus";

                                    $where = array(
                                        "id" => $notice_id
                                    );
                                    $this->ablfunctions->updateData($data, $table, $where);

                                    // $img_upload_message = 'Cover image uploaded successfully.';
                                }
                            }
                        }
                        $this->session->set_flashdata("s_message", "Syllabus  successfully Register.");
                        redirect("admin/college/manageSyllabus");
                        }
                        }
        } else {
                $data['class_list'] = $this->my_custom_functions->get_multiple_data('tbl_courses', 'and status = 1');
                $this->load->view('admin/college/add_syllabus', $data);
            }
        }
        function editSyllabus() {
            if ($this->input->post('submit') && $this->input->post('submit') != '') {
                $encrypted_id = $this->input->post('notice_id');
                $notice_id = $this->ablfunctions->ablDecrypt($encrypted_id);

                //$this->form_validation->set_rules("notice_heading", "Notice Heading", "trim|required");
                $this->form_validation->set_rules("syllabus_text", "Syllabus Text", "trim|required");

                if ($this->form_validation->run() == false) {

                    $this->session->set_flashdata("e_message", validation_errors());
                    redirect("admin/college/editSyllabus/".$encrypted_id."");

                }else{

                 $data = array(
                   'course_id' => $this->input->post('course_id'),
                   'syllabus_text' => $this->input->post('syllabus_text'),
                   'status' => $this->input->post('status')
                    );
                 $condition = array(
                    "id" => $notice_id
                    );
                $update_id = $this->my_custom_functions->update_data($data,'tbl_syllabus', $condition);


                             if (isset($_FILES['doc_upload'])) {
                            if ($update_id) {

                            if ($_FILES['doc_upload']['name'] != "") {

                                //$file_name = $student_from_id.'.'.'jpg';
                                //$file_name = $student_from_id.'_'.'jpg';
                                $file_name_old = $notice_id.'_'.basename($_FILES['doc_upload']['name']);
                                $file_name = str_replace(' ', '_', $file_name_old);

                                $config = array();
                                $config['upload_path']          = UPLOAD_DIR.SYLLABUS_DOCUMENT;
                                $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                                $config['file_name']            = $file_name;
                                //$config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                                $this->load->library('upload');
                                $this->upload->initialize($config);

                                if (!$this->upload->do_upload('doc_upload')) {

                                    $img_upload_message = $this->upload->display_errors();

                                } else {
                                    $data = array(
                                        "document_url" => $file_name
                                    );

                                    $table = "tbl_syllabus";

                                    $where = array(
                                        "id" => $notice_id
                                    );
                                    $this->ablfunctions->updateData($data, $table, $where);

                                    // $img_upload_message = 'Cover image uploaded successfully.';
                                }
                            }
                        }
                        $this->session->set_flashdata("s_message", "Syllabus  successfully Updated.");
                        redirect("admin/college/manageSyllabus");
                        }
                    }
                } else {
                $data['page_title'] = 'Edit notice';
                $notice_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
                $data['class_list'] = $this->my_custom_functions->get_multiple_data('tbl_courses', 'and status = 1');
                $data['details'] = $this->my_custom_functions->get_details_from_id($notice_id, 'tbl_syllabus');
                $this->load->view('admin/college/edit_syllabus', $data);
            }
        }
        function deleteSyllabusFile($file,$file_id){

            $data = array(
                    "document_url" =>'',
                    );
            $where = array(
                    "id" => $file_id
                    );
            $this->ablfunctions->updateData($data,"tbl_syllabus", $where);
            $cript_id = $this->ablfunctions->ablEncrypt($file_id);
            $path=UPLOAD_DIR.SYLLABUS_DOCUMENT.$file;
            @unlink(''.$path.'');
            $this->session->set_flashdata("s_message", "File successfully Deleted.");
            redirect("admin/college/editSyllabus/".$cript_id."");


        }
        /*
        | --------------------------------------------------------------------------
        | Delete Syllabus
        | --------------------------------------------------------------------------
        */
        function deleteSyllabus($file,$file_id){
          $query = $this->ablfunctions->deleteData('tbl_syllabus', array("id" => $file_id));
          $path=UPLOAD_DIR.SYLLABUS_DOCUMENT.$file;
          @unlink(''.$path.'');
        if ($query) { // if deleted
            $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
            redirect('admin/college/manageSyllabus');
        } else {
            $this->session->set_flashdata('e_message', 'Record Deletion Failed');
            redirect('admin/college/manageSyllabus');
        }
        }

    //End
}
