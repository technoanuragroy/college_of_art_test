<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cms extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->checkAdminSecurity();
        $this->load->model("admin/Cms_model");
    }

    /*
    | --------------------------------------------------------------------------
    | Manage CMS
    | --------------------------------------------------------------------------
    */
    function index() {

        // Get the data
        $data['cms_pages'] = $this->Cms_model->get_all_cms_pages();

        foreach ($data['cms_pages'] as $key => $page) {

            $data['cms_pages'][$key]['encrypted_id'] = $this->ablfunctions->ablEncrypt($page['id']);
        }

        $this->load->view("admin/cms/manage_cms", $data);
    }

    /*
    | --------------------------------------------------------------------------
    | Create new page through CMS management
    | --------------------------------------------------------------------------
    */
    function addPage() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("name", "Page Name", "trim|required");
            $this->form_validation->set_rules("url_alias", "URL Alias", "trim|required|is_unique[tbl_cms_pages.url_alias]");
            $this->form_validation->set_rules("page_heading", "Page Heading", "trim|required");
            $this->form_validation->set_rules("page_content", "Page Content", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/cms");
            } else {

                // Add new page data
                $insert_data = array(
                    "name" => $this->input->post("name"),
                    "url_alias" => $this->input->post("url_alias"),
                    "page_heading" => $this->input->post("page_heading"),
                    "page_content" => $this->input->post("page_content"),
                    "page_title" => $this->input->post("page_title"),
                    "meta_keywords" => $this->input->post("meta_keywords"),
                    "meta_description" => $this->input->post("meta_description")
                );

                $page_id = $this->Cms_model->insert_page_data($insert_data);

                if ($page_id) {

                    $this->session->set_flashdata("s_message", "You have successfully added a new page.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the page! Please try again.");
                }

                redirect("admin/cms");
            }
        } else {

            $this->load->view("admin/cms/add_page");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Edit page
    | --------------------------------------------------------------------------
    */
    function editPage() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('page_id');
            $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("name", "Page Name", "trim|required");
            //$this->form_validation->set_rules("url_alias", "URL Alias", "trim|required|is_unique[tbl_cms_pages.url_alias]");
            $this->form_validation->set_rules("page_heading", "Page Heading", "trim|required");
            $this->form_validation->set_rules("page_content", "Page Content", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/cms/editPage/" . $encrypted_id);
            } else {

                // Update page data
                $update_data = array(
                    "name" => $this->input->post("name"),
                    "url_alias" => $this->input->post("url_alias"),
                    "page_heading" => $this->input->post("page_heading"),
                    "page_content" => $this->input->post("page_content"),
                    "page_title" => $this->input->post("page_title"),
                    "meta_keywords" => $this->input->post("meta_keywords"),
                    "meta_description" => $this->input->post("meta_description")
                );

                $update = $this->Cms_model->update_page_data($page_id, $update_data);

                if ($update) {

                    $this->session->set_flashdata("s_message", "Page record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the page record.");
                }

                redirect("admin/cms/editPage/" . $encrypted_id);
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $data['page_details'] = $this->ablfunctions->getDetailsFromId($page_id, "tbl_cms_pages");
            $data['page_details']['encrypted_id'] = $encrypted_id;

            $this->load->view("admin/cms/edit_page", $data);
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Get URL alias auto populated through ajax call from page name text
    | --------------------------------------------------------------------------
    */
    public function getUrlAlias() {

        $name = $this->input->post('name');

        $url_alias = $this->ablfunctions->CreateAlise($name);

        echo $url_alias;
    }

    /*
    | --------------------------------------------------------------------------
    | Validate URL alias through ajax call from add page page
    | --------------------------------------------------------------------------
    */
    public function validateUrlAlias() {

        $url_alias = $this->input->post('url_alias');

        if (isset($url_alias) && $url_alias != "") {

            if($this->input->post("page")) {

                $encrypted_id = $this->input->post("page");
                $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
                $url_alias_count = $this->ablfunctions->getParticularCount("tbl_cms_pages", " and url_alias='" . $url_alias . "' and id!='" . $page_id . "'");
            } else {

                $url_alias_count = $this->ablfunctions->getParticularCount("tbl_cms_pages", " and url_alias='" . $url_alias . "'");
            }

            if ($url_alias_count > 0) {

                echo "URL alias already exists on system";
            } else {

                echo "";
            }
        } else {

            echo "Invalid URL alias";
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Delete page
    | --------------------------------------------------------------------------
    */
    function deletePage() {

        $encrypted_id = $this->uri->segment(4);
        $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);

        // Delete page data
        $this->ablfunctions->deleteData("tbl_cms_pages", array("id" => $page_id));

        $this->session->set_flashdata('s_message', "Page has been deleted successfully.");
        redirect("admin/cms");
    }
}
