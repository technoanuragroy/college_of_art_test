<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class College extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->checkAdminSecurity();
        $this->load->model("admin/College_model");
    }

    /*
    | --------------------------------------------------------------------------
    | Manage CMS
    | --------------------------------------------------------------------------
    */
    function index() {

        // Get the data
        $data['cms_pages'] = $this->Cms_model->get_all_cms_pages();

        foreach ($data['cms_pages'] as $key => $page) {

            $data['cms_pages'][$key]['encrypted_id'] = $this->ablfunctions->ablEncrypt($page['id']);
        }

        $this->load->view("admin/cms/manage_cms", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Staff
    | --------------------------------------------------------------------------
    */
    function manage_staff(){

        // Get the data
        $data['all_staff'] = $this->my_custom_functions->get_multiple_data("tbl_staff",'');

        $this->load->view("admin/college/manage_staff", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add new Staff
    | --------------------------------------------------------------------------
    */
    function addStaff() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("staff_name", "Staff Name", "trim|required");
            $this->form_validation->set_rules("designation", "Designation", "trim|required");
            $this->form_validation->set_rules("scale", "Scale", "trim|required");
            $this->form_validation->set_rules("phone_number", "Phone Number", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            $this->form_validation->set_rules("user_name", "User Name", "trim|required|is_unique[tbl_staff.user_name]");
            $this->form_validation->set_rules("password", "Password", "trim|required|min_length[6]|max_length[32]");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/manage_staff");
            } else {

              // Add new admin data
                $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);
                $insert_data = array(
                    "staff_name" => $this->input->post("staff_name"),
                    "designation" => $this->input->post("designation"),
                    "scale" => $this->input->post("scale"),
                    "phone_number" => $this->input->post("phone_number"),
                    "email" => $this->input->post("email"),
                    "user_name" => $this->input->post("user_name"),
                    "password" => $password,
                    "status" => $this->input->post("status")
                );

                $page_id = $this->my_custom_functions->insert_data($insert_data,"tbl_staff");///////  insert to user table

                if ($page_id) {

                    $this->session->set_flashdata("s_message", "You have successfully added a Staff.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the Staff! Please try again.");
                }

                redirect("admin/college/manage_staff");
            }
        } else {

            $this->load->view("admin/college/add_staff");
        }
    }


    /*
    | --------------------------------------------------------------------------
    | Edit page
    | --------------------------------------------------------------------------
    */
    function editStaff() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('staff_id');
            $staff_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("staff_name", "Staff Name", "trim|required");
            $this->form_validation->set_rules("designation", "Designation", "trim|required");
            $this->form_validation->set_rules("scale", "Scale", "trim|required");
            $this->form_validation->set_rules("phone_number", "Phone Number", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            //$this->form_validation->set_rules("status", "Page Content", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editStaff/".$encrypted_id."");
            } else {

              // Add new page data
              $update_data = array(
                  "staff_name" => $this->input->post("staff_name"),
                  "designation" => $this->input->post("designation"),
                  "scale" => $this->input->post("scale"),
                  "phone_number" => $this->input->post("phone_number"),
                  "email" => $this->input->post("email"),
                  "status" => $this->input->post("status")
              );
              $where = array(
                "id" => $staff_id,
              );
              // Update password if password provided only
              if($this->input->post("password") AND $this->input->post("password") != "") {

                  $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);
                  $update_data["password"] = $password;
              }

                $update = $this->my_custom_functions->update_data($update_data,"tbl_staff",$where);

                if ($update) {

                    $this->session->set_flashdata("s_message", "Staff record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Staff record.");
                }

                redirect("admin/college/manage_staff");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $staff_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            $data['staff_details'] = $this->ablfunctions->getDetailsFromId($staff_id, "tbl_staff");

            $this->load->view("admin/college/edit_staff", $data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Delete Staff
    | --------------------------------------------------------------------------
    */
    function deleteStaff() {

        $encrypted_id = $this->uri->segment(4);
        $staff_id = $this->ablfunctions->ablDecrypt($encrypted_id);

        // Delete page data
        $this->ablfunctions->deleteData("tbl_staff", array("id" => $staff_id));

        $this->session->set_flashdata('s_message', "Deleted successfully.");
        redirect("admin/college/manage_staff");
    }

    /*
    | --------------------------------------------------------------------------
    | Get URL alias auto populated through ajax call from page name text
    | --------------------------------------------------------------------------
    */
    public function getUrlAlias() {

        $name = $this->input->post('name');

        $url_alias = $this->ablfunctions->CreateAlise($name);

        echo $url_alias;
    }

    /*
    | --------------------------------------------------------------------------
    | Validate URL alias through ajax call from add page page
    | --------------------------------------------------------------------------
    */
    public function validateUrlAlias() {

        $url_alias = $this->input->post('url_alias');

        if (isset($url_alias) && $url_alias != "") {

            if($this->input->post("page")) {

                $encrypted_id = $this->input->post("page");
                $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
                $url_alias_count = $this->ablfunctions->getParticularCount("tbl_cms_pages", " and url_alias='" . $url_alias . "' and id!='" . $page_id . "'");
            } else {

                $url_alias_count = $this->ablfunctions->getParticularCount("tbl_cms_pages", " and url_alias='" . $url_alias . "'");
            }

            if ($url_alias_count > 0) {

                echo "URL alias already exists on system";
            } else {

                echo "";
            }
        } else {

            echo "Invalid URL alias";
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Delete page
    | --------------------------------------------------------------------------
    */
    function deletePage() {

        $encrypted_id = $this->uri->segment(4);
        $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);

        // Delete page data
        $this->ablfunctions->deleteData("tbl_cms_pages", array("id" => $page_id));

        $this->session->set_flashdata('s_message', "Deleted successfully.");
        redirect("admin/cms");
    }

    /*
    | --------------------------------------------------------------------------
    | Manage Staff
    | --------------------------------------------------------------------------
    */
    function manage_attendance() {
        // if ($this->input->post('submit') AND ($this->input->post('submit') == "Submit")) {
        //     $month = $this->input->post('month');
        //     $this->session->set_userdata('sess_month', $month);
        //     $year = $this->input->post('year');
        // } else { //if loaded directly
        //     if ($this->session->userdata('sess_month')) {
        //         $month = $this->session->userdata('sess_month');
        //     } else {
        //         $month = date("m") - 1;
        //     }
        //
        //     $year = date("Y");
        // }
        //
        // $all_years = $this->emp_model->get_attendence_year_list();
        // foreach ($all_years as $row) {
        //     $years[$row->att_years] = $row->att_years;
        // }
        //
        // $query = $this->emp_model->manage_attendance($month, $year);
        // $data = array(
        //   "attend" => $query,
        //   'years' => $years,
        //   'month' => $month,
        //   'year' => $year,
        // );
        $this->load->view("admin/college/manage_attendance");
    }
    function addAttendance() {
      if ($this->input->post('submit') AND ($this->input->post('submit') == "Submit")) {

          $emp_id = $this->input->post('emp_id');
          $date_month = $this->input->post('year') . "-" . $this->input->post('month') . "-" . "01";
          $result = $this->College_model->get_attendance_details($emp_id, $date_month);
          if ($result) {

              $this->session->set_flashdata('e_message', 'Attendance already added.');
              redirect('admin/college/addAttendance');
          } else {

              $data = array(
                'emp_id' => $this->input->post('emp_id'),
                'date_month' => $date_month,
                'present' => $this->input->post('present'),
                'absent' => $this->input->post('absent'),
                'pl' => $this->input->post('pl'),
                'cl' => $this->input->post('cl'),
                'sl' => $this->input->post('sl'),
                'opl' => $this->input->post('opl'),
                'working_days' => $this->input->post('working_days'),
                'note' => $this->input->post('note'),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->input->post('admin_name'),
              );
              $employee = $this->my_custom_functions->insert_data($data,"tbl_attendance");///////  insert to user table
              if (isset($employee)) {
                  $this->session->set_flashdata('s_message', 'Successfully Added Attdance');
                  redirect("admin/college/manage_attendance");
              }
          }
      } else {

          $employee = $this->my_custom_functions->get_details_from_id("", "tbl_staff", array("id" => $this->uri->segment(3)));
          //$all_emp = $this->my_custom_functions->get_multiple_data("tbl_staff",'');
          //echo '<pre>';print_r($all_emp);die;
          $all_emp = $this->College_model->get_all_employee_details();
          $cl = $this->College_model->get_cl($this->uri->segment(3));

          $data = array(
            'employee' => $employee,
            'all_emp' => $all_emp,
            'cl' => $cl,
          );
          $this->load->view('admin/college/add_attendance', $data);
      }
        //$this->load->view("admin/college/add_attendance");
    }
    /*
    | --------------------------------------------------------------------------
    | Attendance Report
    | --------------------------------------------------------------------------
    */
    function attendance_report() {

    if ($this->input->post('submit') AND ($this->input->post('submit') == "Search")) {
        //$emp_id = $this->College_model->get_emp_id($this->input->post('emp_name'));
        $emp_id = $this->my_custom_functions->get_particular_field_value("tbl_staff","id", 'and staff_name="'.$this->input->post('emp_name').'"');
        $attend = $this->College_model->get_attendance_report($emp_id);
        //echo '<pre>';print_r($attend);die;
        $data = array(
          'attend' => $attend,
          'emp_name' => $this->input->post('emp_name'),
          'from_date' => $this->input->post('from_date'),
          'to_date' => $this->input->post('to_date'),
        );



        $this->load->view('admin/college/attendance_report.php', $data);
    } else {
        $this->load->view('admin/college/attendance_report.php');
    }
    }
    ///////////////////////////////////////////////////////////////
    function ajax_get_emps()
    {
    $return_arr = array();
    $term = $this->input->get('term');
    $fetch = $this->College_model->get_ajax_emps($term);

    foreach ($fetch->result_array() as $row)
    {
      $row_array['value'] = $row['staff_name'];
      array_push($return_arr,$row_array);
    }

    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Attendance
    | --------------------------------------------------------------------------
    */
    function edit_attendance() {
    if ($this->input->post('submit') AND ($this->input->post('submit') == "Update")) {
        $data = array(
          'emp_id' => $this->input->post('emp_id'),
          'date_month' => $this->input->post('year') . "-" . $this->input->post('month') . "-" . "01",
          'present' => $this->input->post('present'),
          'absent' => $this->input->post('absent'),
          'pl' => $this->input->post('pl'),
          'cl' => $this->input->post('cl'),
          'sl' => $this->input->post('sl'),
          'opl' => $this->input->post('opl'),
          'updated_at' => date('Y-m-d H:i:s'),
          'created_by' => $this->input->post('admin_name'),
        );
        $employee = $this->emp_model->update_attendance($data);
        if (isset($employee)) {
            $this->session->set_flashdata('update_data', 'success');
            redirect('emp/edit_attendance/' . $this->input->post('emp_id') . '/' .$this->input->post('id'));
        }
    } else {

        $emp_id = $this->uri->segment(3);
        $attendance_id = $this->uri->segment(4);

        $employee = $this->emp_model->get_employee_details($emp_id);
        $query = $this->emp_model->get_attendance($attendance_id);

        $dtime = new DateTime($query->date_month);
        $year = $dtime->format("Y");
        $cl = $this->emp_model->get_cl_for_selected_year($year, $emp_id);

        $data = array(
          "employee" => $employee,
          "attend_id" => $attendance_id,
          "attend" => $query,
          'cl' => $cl
        );

        $this->load->view('emp/edit_attendance', $data);
    }
    }
    /*
    | --------------------------------------------------------------------------
    | Delete Attendance
    | --------------------------------------------------------------------------
    */
    function delete_attendance() {
      $id = $this->uri->segment(4);
      $query = $this->ablfunctions->deleteData("tbl_attendance", array("id" => $id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect('admin/college/add_attendance');
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect('admin/college/add_attendance');
    }
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Staff
    | --------------------------------------------------------------------------
    */
    function manageSession(){

        // Get the data
        $data['all_session'] = $this->my_custom_functions->get_multiple_data("tbl_session",'');
        $this->load->view("admin/college/manage_session", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add new Session
    | --------------------------------------------------------------------------
    */
    function addSession() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            //echo "<pre>";print_r ($_POST);die;
            $this->form_validation->set_rules("session_name", "Session Name", "trim|required");
            $this->form_validation->set_rules("from_date", "From Date", "trim|required");
            $this->form_validation->set_rules("to_date", "To Date", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/addSession");
            } else {

                $insert_data = array(
                    "session_name" => $this->input->post("session_name"),
                    "from_date" => $this->input->post("from_date"),
                    "to_date" => $this->input->post("to_date")
                );

                $page_id = $this->my_custom_functions->insert_data($insert_data,"tbl_session");///////  insert to user table

                if ($page_id) {

                    $this->session->set_flashdata("s_message", "You have successfully added a Session.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the Session! Please try again.");
                }

                redirect("admin/college/manageSession");
            }
        } else {

            $this->load->view("admin/college/add_session");
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Session
    | --------------------------------------------------------------------------
    */
    function editSession() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('session_id');
            $session_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("session_name", "Session Name", "trim|required");
            $this->form_validation->set_rules("from_date", "From Date", "trim|required");
            $this->form_validation->set_rules("to_date", "To Date", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editSession/".$encrypted_id."");
            } else {

              $update_data = array(
                  "session_name" => $this->input->post("session_name"),
                  "from_date" => $this->input->post("from_date"),
                  "to_date" => $this->input->post("to_date")
              );
              $where = array(
                "id" => $session_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_session",$where);

                if ($update) {

                    $this->session->set_flashdata("s_message", "Session record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Session record.");
                }

                redirect("admin/college/manageSession");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $session_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            $data['session_details'] = $this->ablfunctions->getDetailsFromId($session_id, "tbl_session");

            $this->load->view("admin/college/edit_session", $data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Delete Session
    | --------------------------------------------------------------------------
    */
    function deleteSession() {
      $encrypted_id = $this->uri->segment(4);
      $session_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_session", array("id" => $session_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect('admin/college/manageSession');
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect('admin/college/manageSession');
    }
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Staff
    | --------------------------------------------------------------------------
    */
    function manageClasses(){

        // Get the data
        $data['all_classes'] = $this->my_custom_functions->get_multiple_data("tbl_classes",'');
        $this->load->view("admin/college/manage_classes", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add new Classes
    | --------------------------------------------------------------------------
    */
    function addClasses() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->form_validation->set_rules("class_name", "Classes Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/addClasses");
            } else {

                $insert_data = array(
                    "class_name" => $this->input->post("class_name"),
                    "status" => $this->input->post("status")
                );

                $page_id = $this->my_custom_functions->insert_data($insert_data,"tbl_classes");///////  insert to user table

                if ($page_id) {

                    $this->session->set_flashdata("s_message", "You have successfully added a Classes.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the Classes! Please try again.");
                }

                redirect("admin/college/manageClasses");
            }
        } else {

            $this->load->view("admin/college/add_classes");
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Classes
    | --------------------------------------------------------------------------
    */
    function editClasses() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('class_id');
            $class_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("class_name", "Classes Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editSession/".$encrypted_id."");
            } else {

              $update_data = array(
                  "class_name" => $this->input->post("class_name"),
                  "status" => $this->input->post("status")
              );
              $where = array(
                "id" => $class_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_classes",$where);

                if ($update) {

                    $this->session->set_flashdata("s_message", "Classes record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Classes record.");
                }

                redirect("admin/college/manageClasses");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $class_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            $data['class_details'] = $this->ablfunctions->getDetailsFromId($class_id, "tbl_classes");

            $this->load->view("admin/college/edit_classes", $data);
        }
    }
        /*
    | --------------------------------------------------------------------------
    | Delete Classes
    | --------------------------------------------------------------------------
    */
    function deleteClasses() {
      $encrypted_id = $this->uri->segment(4);
      $class_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_classes", array("id" => $class_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manageClasses");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manageClasses");
    }
    }
        /*
    | --------------------------------------------------------------------------
    | Manage Sections
    | --------------------------------------------------------------------------
    */

        function manageSections() {
        
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            //$data['post_data'] = array('class_id' => $this->input->post('class'));
            $data['page_title'] = 'Section Search Details';
            $data['all_section'] = $this->my_custom_functions->get_multiple_data('tbl_section',' and class_id = "' . $this->input->post('class'). '" and status = 1');
        } else {
             $data['page_title'] = 'Manage section';
             $data['all_section'] = $this->my_custom_functions->get_multiple_data("tbl_section",'');
            //$data['classes'] = $this->School_user_model->get_section_data();
        }
            // Get the data
            $data['class_list'] = $this->my_custom_functions->get_multiple_data('tbl_classes', 'and status = 1');
        
        $this->load->view("admin/college/manage_sections", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add new Sections
    | --------------------------------------------------------------------------
    */
    function addSections() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->form_validation->set_rules("class_id", "Classes Name", "trim|required");
            $this->form_validation->set_rules("section_name", "Section Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/addSections");
            } else {

                $insert_data = array(
                    "class_id" => $this->input->post("class_id"),
                    "section_name" => $this->input->post("section_name"),
                    "status" => $this->input->post("status")
                );

                $page_id = $this->my_custom_functions->insert_data($insert_data,"tbl_section");///////  insert to user table

                if ($page_id) {

                    $this->session->set_flashdata("s_message", "You have successfully added a Section.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the Section! Please try again.");
                }

                redirect("admin/college/manageSections");
            }
        } else {

            $data['class_list'] = $this->my_custom_functions->get_multiple_data("tbl_classes",'and status="1"');

            $this->load->view("admin/college/add_sections",$data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Sections
    | --------------------------------------------------------------------------
    */
    function editSections() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('section_id');
            $section_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("class_id", "Classes Name", "trim|required");
            $this->form_validation->set_rules("section_name", "Section Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editSections/".$encrypted_id."");
            } else {

              $update_data = array(
                    "class_id" => $this->input->post("class_id"),
                    "section_name" => $this->input->post("section_name"),
                    "status" => $this->input->post("status")
                );
              $where = array(
                "id" => $section_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_section",$where);

                if ($update) {

                    $this->session->set_flashdata("s_message", "Section record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Section record.");
                }

                redirect("admin/college/manageSections");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $section_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            $data['section_details'] = $this->ablfunctions->getDetailsFromId($section_id, "tbl_section");
            $data['class_list'] = $this->my_custom_functions->get_multiple_data("tbl_classes",'and status="1"');

            $this->load->view("admin/college/edit_sections",$data);
        }
    }
        /*
    | --------------------------------------------------------------------------
    | Delete Sections
    | --------------------------------------------------------------------------
    */
    function deleteSections() {
      $encrypted_id = $this->uri->segment(4);
      $section_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_section", array("id" => $section_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manageSections");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manageSections");
    }
    }
        /*
    | --------------------------------------------------------------------------
    | Manage Subjects
    | --------------------------------------------------------------------------
    */
    function manageSubjects(){


        // Get the data
        $data['all_subject'] = $this->my_custom_functions->get_multiple_data("tbl_subjects",'');
        $this->load->view("admin/college/manage_subject", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add new Subjects
    | --------------------------------------------------------------------------
    */
    function addSubjects() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->form_validation->set_rules("subject_name", "Subject Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/addClasses");
            } else {

                $insert_data = array(
                    "subject_name" => $this->input->post("subject_name"),
                    "status" => $this->input->post("status")
                );

                $page_id = $this->my_custom_functions->insert_data($insert_data,"tbl_subjects");///////  insert to user table

                if ($page_id) {

                    $this->session->set_flashdata("s_message", "You have successfully added a Subjects.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the Subjects! Please try again.");
                }

                redirect("admin/college/manageSubjects");
            }
        } else {

            $this->load->view("admin/college/add_subject");
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Subjects
    | --------------------------------------------------------------------------
    */
    function editSubjects() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('subject_id');
            $subject_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("subject_name", "Subject Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editSubjects/".$encrypted_id."");
            } else {

              $update_data = array(
                    "subject_name" => $this->input->post("subject_name"),
                    "status" => $this->input->post("status")
                );

              $where = array(
                "id" => $subject_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_subjects",$where);

                if ($update) {

                    $this->session->set_flashdata("s_message", "Classes record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Classes record.");
                }

                redirect("admin/college/manageSubjects");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $subject_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            $data['details'] = $this->ablfunctions->getDetailsFromId($subject_id, "tbl_subjects");

            $this->load->view("admin/college/edit_subject", $data);
        }
    }
        /*
    | --------------------------------------------------------------------------
    | Delete Subjects
    | --------------------------------------------------------------------------
    */
    function deleteSubjects() {
      $encrypted_id = $this->uri->segment(4);
      $subject_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_subjects", array("id" => $subject_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manageSubjects");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manageSubjects");
    }
    }
     /*
    | --------------------------------------------------------------------------
    | Manage Semester
    | --------------------------------------------------------------------------
    */
    function manageSemester(){

        // Get the data
        $data['all_semester'] = $this->my_custom_functions->get_multiple_data("tbl_semester",'');
        $this->load->view("admin/college/manage_semester", $data);
    }
        /*
    | --------------------------------------------------------------------------
    | Add semester
    | --------------------------------------------------------------------------
    */

    public function addSemester() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules("course_id", "Course Name", "trim|required");
            //$this->form_validation->set_rules("session", "Session Name", "trim|required");
            $this->form_validation->set_rules("semester_name", "Semester Name", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/addSemester");
            } else {

                $chek_unique = $this->my_custom_functions->get_perticular_count('tbl_semester', 'and session_id="' .$this->input->post('session_id') . '" and course_id="' . $this->input->post('course_id') . '"');
                if ($chek_unique > 0) {
                    $this->session->set_flashdata("e_message", 'Semester already exists with selected session and class');
                    redirect("admin/college/addSemester");
                }

                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }

                $insert_data = array(
                    'session_id' => $this->input->post('session_id'),
                    'course_id' => $this->input->post('course_id'),
                    'semester_name' => $this->input->post('semester_name'),
                    'combined_term_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('term_label')
                );
 
                $add_session_data = $this->my_custom_functions->insert_data($insert_data,"tbl_semester");///////  insert

                if ($add_session_data) {
                                                            
                    $this->session->set_flashdata("s_message", 'Semester added successfully.');
                    redirect("admin/college/manageSemester");
                } else {
                    
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/college/addSemester");
                }
            }
        } else {

            $data['session_details']= $this->College_model->get_current_session();
            $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_classes",'');

            $this->load->view('admin/college/add_semester', $data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Semester
    | --------------------------------------------------------------------------
    */
    public function editSemester() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $semester_id = $this->input->post('semester_id');

            $this->form_validation->set_rules("course_id", "Course Name", "trim|required");
            //$this->form_validation->set_rules("session", "Session Name", "trim|required");
            $this->form_validation->set_rules("semester_name", "Semester Name", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/editSemester" . $semester_id);
            } else {

                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }
                $semester_data = array(
                    'session_id' => $this->input->post('session_id'),
                    'course_id' => $this->input->post('course_id'),
                    'semester_name' => $this->input->post('semester_name'),
                    'combined_term_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('term_label')
                );
                $condition = array(
                    'id' => $this->ablfunctions->ablDecrypt($semester_id)
                );
                $edit_class_data = $this->my_custom_functions->update_data($semester_data,"tbl_semester", $condition);

                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Semester edited successfully.');
                    redirect("admin/college/manageSemester");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/college/editSemester/". $semester_id);
                }
            }
        } else {


            $semester_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
            $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_classes",'');
            $data['details'] = $this->ablfunctions->getDetailsFromId($semester_id, "tbl_semester");

            $this->load->view('admin/college/edit_semester', $data);
        }
    }
            /*
    | --------------------------------------------------------------------------
    | Delete Semester
    | --------------------------------------------------------------------------
    */
    function deleteSemester() {
      $encrypted_id = $this->uri->segment(4);
      $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_semester", array("id" => $page_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manageSemester");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manageSemester");
    }
    }

         /*
    | --------------------------------------------------------------------------
    | Manage Terms
    | --------------------------------------------------------------------------
    */
    function manageTerms(){

        // Get the data
        $data['all_terms'] = $this->my_custom_functions->get_multiple_data("tbl_terms",'');
        $this->load->view("admin/college/manage_terms", $data);
    }
    /*
    | --------------------------------------------------------------------------
    | Add semester
    | --------------------------------------------------------------------------
    */

    public function addTerms() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $this->form_validation->set_rules("semester_id", "Semester Name", "trim|required");
            //$this->form_validation->set_rules("session", "Session Name", "trim|required");
            $this->form_validation->set_rules("term_name", "Term Name", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/addTerms");
            } else {


                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }

                $insert_data = array(
                    'semester_id' => $this->input->post('semester_id'),
                    'term_name' => $this->input->post('term_name'),
                    'combined_exam_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('term_label')
                );
 
                $add_session_data = $this->my_custom_functions->insert_data($insert_data,"tbl_terms");///////  insert

                if ($add_session_data) {
                                                            
                    $this->session->set_flashdata("s_message", 'Term added successfully.');
                    redirect("admin/college/manageTerms");
                } else {
                    
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/college/addTerms");
                }
            }
        } else {
            $data['all_semester'] = $this->my_custom_functions->get_multiple_data("tbl_semester",'');

            $this->load->view('admin/college/add_term',$data);
        }
    }
        /*
    | --------------------------------------------------------------------------
    | Edit Terms
    | --------------------------------------------------------------------------
    */
    public function editTerm() {

        if ($this->input->post('submit') AND ( $this->input->post('submit') != "")) {

            $page_id = $this->input->post('page_id');

            $this->form_validation->set_rules("semester_id", "Semester Name", "trim|required");
            //$this->form_validation->set_rules("session", "Session Name", "trim|required");
            $this->form_validation->set_rules("term_name", "Term Name", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/editTerm/". $page_id);
            } else {

                if ($this->input->post('combine_rslt')) {
                    $combine_rslt = 1;
                } else {
                    $combine_rslt = 0;
                }
                $page_data = array(
                    'semester_id' => $this->input->post('semester_id'),
                    'term_name' => $this->input->post('term_name'),
                    'combined_exam_results' => $combine_rslt,
                    'combined_result_name' => $this->input->post('term_label')
                );
                $condition = array(
                    'id' => $this->ablfunctions->ablDecrypt($page_id)
                );
                $edit_class_data = $this->my_custom_functions->update_data($page_data,"tbl_terms", $condition);

                if ($edit_class_data) {
                    $this->session->set_flashdata("s_message", 'Semester edited successfully.');
                    redirect("admin/college/manageTerms");
                } else {
                    $this->session->set_flashdata("e_message", 'Something went wrong, please ty again later.');
                    redirect("admin/college/editTerm/". $page_id);
                }
            }
        } else {


            $term_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
            $data['all_semester'] = $this->my_custom_functions->get_multiple_data("tbl_semester",'');
            $data['details'] = $this->ablfunctions->getDetailsFromId($term_id, "tbl_terms");

            $this->load->view('admin/college/edit_term', $data);
        }
    }
                /*
    | --------------------------------------------------------------------------
    | Delete Semester
    | --------------------------------------------------------------------------
    */
    function deleteTerm() {
      $encrypted_id = $this->uri->segment(4);
      $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_terms", array("id" => $page_id));
    if ($query) { // if deleted
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/manageTerms");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/manageTerms");
    }
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Exam
    | --------------------------------------------------------------------------
    */
    function manageExam() {
        // Get the data
        $data['details'] = $this->my_custom_functions->get_multiple_data("tbl_exam",'');
        $this->load->view('admin/college/manage_exam', $data);
    }
        /*
    | --------------------------------------------------------------------------
    | Add Exam
    | --------------------------------------------------------------------------
    */
    function addExam() {

        if ($this->input->post('submit') && $this->input->post('submit') != '') {

          /// tbl_admins contents
            $this->form_validation->set_rules("term", "Term", "trim|required");
            $this->form_validation->set_rules("exam_name", "Exam name", "trim|required");
            $this->form_validation->set_rules("exam_disp_name", "Exam display name", "trim|required");
            $this->form_validation->set_rules("exam_start_date", "Exam start date", "trim|required");
            $this->form_validation->set_rules("exam_end_date", "Exam end date", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/addExam");
            } else {

                $publish_date = $this->my_custom_functions->database_date($this->input->post('publish_date'));
                $publish_hour_post = $this->input->post('start_hour');
                $publish_minute_post = $this->input->post('start_minute');
                $publish_meridian = $this->input->post('start_meridian');
                if ($publish_hour_post < 10) {
                    $publish_hour = '0' . $publish_hour_post;
                } else {
                    $publish_hour = $publish_hour_post;
                }
                if ($publish_minute_post < 10) {
                    $publish_minute = '0' . $publish_minute_post;
                } else {
                    $publish_minute = $publish_minute_post;
                }

                $publish_date_time = strtotime($publish_date . " " . $publish_hour . ":" . $publish_minute . $publish_meridian);


                $exam_data = array(
                    'term_id' => $this->input->post('term'),
                    'exam_name' => $this->input->post('exam_name'),
                    'exam_display_name' => $this->input->post('exam_disp_name'),
                    'exam_start_date' => $this->my_custom_functions->database_date($this->input->post('exam_start_date')),
                    'exam_end_date' => $this->my_custom_functions->database_date($this->input->post('exam_end_date')),
                    'publish_date' => $publish_date_time,
                );
                $exam_id = $this->my_custom_functions->insert_data_last_id($exam_data,'tbl_exam');
                //$exam_id = 2;

                $subject_list = $this->input->post('subject');
                $fullmarks = $this->input->post('total_marks');
                $passmarks = $this->input->post('pass_marks');

                foreach ($subject_list as $key => $subject_id) {
                    $marks_data = array(
                        'exam_id' => $exam_id,
                        'subject_id' => $subject_id,
                        'full_marks' => $fullmarks[$key],
                        'pass_marks' => $passmarks[$key]
                    );
                    //echo "<pre>";print_r($marks_data);
                    $marks = $this->my_custom_functions->insert_data($marks_data,'tbl_exam_scores');
                }
                $this->session->set_flashdata("s_message", 'Exam record added successfully.');
                redirect("admin/college/manageExam");
            }
        } else {

            $data['page_title'] = 'Add Exam';
            $data['term_list'] = $this->my_custom_functions->get_multiple_data("tbl_terms",'');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data("tbl_subjects",'');
            $this->load->view('admin/college/add_exam', $data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Exam
    | --------------------------------------------------------------------------
    */
    function editExam() {
        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $exam_id = $this->ablfunctions->ablDecrypt($this->input->post('exam_id'));

            /// tbl_admins contents
            $this->form_validation->set_rules("term", "Term", "trim|required");
            $this->form_validation->set_rules("exam_name", "Exam name", "trim|required");
            $this->form_validation->set_rules("exam_disp_name", "Exam display name", "trim|required");
            $this->form_validation->set_rules("exam_start_date", "Exam start date", "trim|required");
            $this->form_validation->set_rules("exam_end_date", "Exam end date", "trim|required");

            if ($this->form_validation->run() == false) { /// Return to register page and show the validation errors
                $this->session->set_flashdata("e_message", validation_errors('<span class="e_message">'));
                redirect("admin/college/editExam/" . $this->input->post('exam_id'));
            } else {
                $publish_date = $this->my_custom_functions->database_date($this->input->post('publish_date'));
                $publish_hour_post = $this->input->post('start_hour');
                $publish_minute_post = $this->input->post('start_minute');
                $publish_meridian = $this->input->post('start_meridian');
                if ($publish_hour_post < 10) {
                    $publish_hour = '0' . $publish_hour_post;
                } else {
                    $publish_hour = $publish_hour_post;
                }
                if ($publish_minute_post < 10) {
                    $publish_minute = '0' . $publish_minute_post;
                } else {
                    $publish_minute = $publish_minute_post;
                }

                $publish_date_time = strtotime($publish_date . " " . $publish_hour . ":" . $publish_minute . $publish_meridian);


                $exam_data = array(
                    'term_id' => $this->input->post('term'),
                    'exam_name' => $this->input->post('exam_name'),
                    'exam_display_name' => $this->input->post('exam_disp_name'),
                    'exam_start_date' => $this->my_custom_functions->database_date($this->input->post('exam_start_date')),
                    'exam_end_date' => $this->my_custom_functions->database_date($this->input->post('exam_end_date')),
                    'publish_date' => $publish_date_time,
                );
                $condition = array('id' => $exam_id);

                $update_exam_data = $this->my_custom_functions->update_data($exam_data,'tbl_exam', $condition);


                $update_score_id = $this->input->post('update_id');
                $update_total_marks = $this->input->post('update_total_marks');
                $update_pass_marks = $this->input->post('update_pass_marks');

                if (!empty($update_score_id)) {
                    foreach ($update_score_id as $key => $score_id) {
                        $update_score = array(
                            'full_marks' => $update_total_marks[$key],
                            'pass_marks' => $update_pass_marks[$key]
                        );
                        $condition_score = array('id' => $score_id);
                        $update_scores = $this->my_custom_functions->update_data($update_score, 'tbl_exam_scores', $condition_score);
                    }
                }
                $subject_list = $this->input->post('subject');
                $fullmarks = $this->input->post('total_marks');
                $passmarks = $this->input->post('pass_marks');
                if (!empty($subject_list)) {
                    foreach ($subject_list as $key => $subject_id) {
                        $marks_data = array(
                            'exam_id' => $exam_id,
                            'subject_id' => $subject_id,
                            'full_marks' => $fullmarks[$key],
                            'pass_marks' => $passmarks[$key]
                        );
                        echo "<pre>";print_r($marks_data);
                        $marks = $this->my_custom_functions->insert_data($marks_data, 'tbl_exam_scores');
                    }
                }
                $this->session->set_flashdata("s_message", 'Exam record updated successfully.');
                redirect("admin/college/manageExam");
            }
        } else {
            $exam_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));
            $data['page_title'] = 'Edit Exam';
            $data['exam_data'] = $this->College_model->get_exam_data_list($exam_id);
           $data['term_list'] = $this->my_custom_functions->get_multiple_data("tbl_terms",'');
            $data['subject_list'] = $this->my_custom_functions->get_multiple_data("tbl_subjects",'');
            $this->load->view('admin/college/edit_exam', $data);
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Delete Exam subject
    | --------------------------------------------------------------------------
    */
        function delete_exam_subject() {
        $ecam_score_id = $this->ablfunctions->ablDecrypt($this->input->post('exam_score_id'));
        $delete_score_condition_one = array('score_id' => $ecam_score_id);
        $delete_score = $this->my_custom_functions->delete_data('tbl_score', $delete_score_condition_one);
        $delete_score_condition_two = array('id' => $ecam_score_id);
        $delete_score = $this->my_custom_functions->delete_data('tbl_exam_scores', $delete_score_condition_two);
        echo "success";
    }
    /*
    | --------------------------------------------------------------------------
    | Manage Student Register
    | --------------------------------------------------------------------------
    */
    function manageStudentRegister(){


        if ($this->input->post('submit') && $this->input->post('submit') != '') {
            $data['post_data'] = array('session_id' => $this->input->post('session'));
            $data['session_active'] = '1';
            $data['all_student'] = $this->my_custom_functions->get_multiple_data('tbl_student_from',' and session_id = "' . $this->input->post('session'). '"');
        }
        else if ($this->input->post('search') && $this->input->post('search') != '') {
            $data['post_data'] = array('status' => $this->input->post('status'));
            $data['all_student'] = $this->my_custom_functions->get_multiple_data('tbl_student_from',' and status = "' . $this->input->post('status'). '" and session_id = "' . $this->input->post('current_session_id'). '"');
            $data['session_active'] = '1';
        } else {
             //$data['page_title'] = 'Manage section';
            //$data['all_student'] = $this->my_custom_functions->get_multiple_data("tbl_student_from",'');
            $data['all_student'] = '';
        }
        // Get the data
        $data['all_session'] = $this->my_custom_functions->get_multiple_data("tbl_session",'');
        $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_classes",'');
        $data['all_status'] = $this->my_custom_functions->get_multiple_data("tbl_student_from",'');
        $this->load->view("admin/college/manage_student_register", $data);
    }
        /*
    | --------------------------------------------------------------------------
    | Edit Student Register
    | --------------------------------------------------------------------------
    */
    function editStudentRegister() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('student_id');
            $student_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("course_id", "Select Course", "required");
            //$this->form_validation->set_rules("session_id", "Select session", "required");
            $this->form_validation->set_rules("student_name", "Name of the Candidate", "trim|required");
            $this->form_validation->set_rules("father_or_mother_name", "Father’s /Mother’s Name", "trim|required");
            $this->form_validation->set_rules("guardian_name", "Name of the Guardian", "trim|required");
            $this->form_validation->set_rules("perm_address", "Permanent Address", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            //$this->form_validation->set_rules("phone_number", "Phone Number", "trim|required|is_unique[tbl_student_from.phone_number]");
            $this->form_validation->set_rules("phone_no", "Phone Number", "trim|required|regex_match[/^[0-9]{10}$/]");
            $this->form_validation->set_rules("corres_address", "Address for correspondence", "trim|required");
            $this->form_validation->set_rules("aadhar_no", "Aadhar No", "trim|required|min_length[12]|max_length[12]");
            $this->form_validation->set_rules("reg_mobile_no", "Registered Mobile No", "trim|required|regex_match[/^[0-9]{10}$/]");
            $this->form_validation->set_rules("dob", "D.O.B", "trim|required");
            
            //$this->form_validation->set_rules("student_passport_photo", "Passport Photo", "required");
            $this->form_validation->set_rules("age", "Age", "trim|required");
            $this->form_validation->set_rules("gender", "Gender", "required");
            $this->form_validation->set_rules("marital_status", "Marital Status", "required");
            $this->form_validation->set_rules("nationality", "nationality", "required");
            $this->form_validation->set_rules("caste", "Caste", "required");
            $this->form_validation->set_rules("local_address", "Local address of the candidate", "trim|required");
            $this->form_validation->set_rules("local_gu_name_adds", "Name & Address of the local guardian", "trim|required");
            //$this->form_validation->set_rules("rules_condition_one", "Condition", "required");
            //$this->form_validation->set_rules("rules_condition_two", "Condition", "required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editStudentRegister/".$encrypted_id."");
            } else {

              $update_data = array(
                    //"course_id" => $this->input->post("course_id"),
                    //"session_id" => $this->input->post("session_id"),
                    "student_name" => $this->input->post("student_name"),
                    "father_or_mother_name" => $this->input->post("father_or_mother_name"),
                    "guardian_name" => $this->input->post("guardian_name"),
                    "perm_address" => $this->input->post("perm_address"),
                    "email" => $this->input->post("email"),
                    "phone_no" => $this->input->post("phone_no"),
                    "corres_address" => $this->input->post("corres_address"),
                    "aadhar_no" => $this->input->post("aadhar_no"),
                    "reg_mobile_no" => $this->input->post("reg_mobile_no"),
                    "dob" => $this->input->post("dob"),
                    "age" => $this->input->post("age"),
                    "gender" => $this->input->post("gender"),
                    "marital_status" => $this->input->post("marital_status"),
                    "nationality" => $this->input->post("nationality"),
                    "caste" => $this->input->post("caste"),
                    "local_address" => $this->input->post("local_address"),
                    "local_gu_name_adds" => $this->input->post("local_gu_name_adds"),
                    "status" => $this->input->post("status")
                );

              $where = array(
                "id" => $student_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_student_from",$where);
                //$update =1;

                if ($update) {
                    $education_details= $this->College_model->get_educational_details($student_id);
                    
                    foreach ($education_details as $row) {

                    if ($this->input->post('exam_pass_'.$row['id'].'')!='' AND $this->input->post('subject_'.$row['id'].'')!='' AND $this->input->post('year_pass_'.$row['id'].'')!='' AND $this->input->post('board_'.$row['id'].'')!='' AND $this->input->post('div_obt_'.$row['id'].'')!='') {

                    $exam_data = array(
                    "exam_passed" => $this->input->post("exam_pass_".$row['id'].""),
                    "subject" => $this->input->post("subject_".$row['id'].""),
                    "year_of_pass" => $this->input->post("year_pass_".$row['id'].""),
                    "board_coun_uni" => $this->input->post("board_".$row['id'].""),
                    "div_obt" => $this->input->post("div_obt_".$row['id']."")
                    );


                    $where = array(
                    "id" => $row['id'],
                    );
                    $edu_table_update = $this->my_custom_functions->update_data($exam_data,"tbl_edu_qualification",$where);
                        if ($edu_table_update) {
                         if (isset($_FILES["document_".$row['id'].""])) { 
                        
                        if ($_FILES["document_".$row['id'].""]['name'] != "") {

                            //$file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name = $row['id'].'_'.basename($_FILES["document_".$row['id'].""]['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');      
                            $this->upload->initialize($config);                         

                            if (!$this->upload->do_upload('document_'.$row['id'].'')) {
                                
                                $img_upload_message = $this->upload->display_errors();

                            } else {                                

                                // Upload thumb image                                
                                // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                                // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;
                                
                                // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                                //Update database with the cover image url
                                $data = array(
                                    "document_url" => $file_name
                                );

                                $table = "tbl_edu_qualification";

                                $where = array(
                                    "id" => $row['id']
                                );
                                $this->ablfunctions->updateData($data, $table, $where);
                                
                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                    }

                    }
                    }
                    ////////////////File Upload///////////////
                    // Upload Passport Photo 
                    $img_upload_message = '';
                    if (isset($_FILES['student_passport_photo'])) { 
                        
                        if ($_FILES['student_passport_photo']['name'] != "") {

                            $file_name = $student_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO;
                            $config['allowed_types']        = ALLOWED_PHOTO_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_PHOTO_IMAGE_SIZE;
                            $config['max_width']  = PASSPORT_WIDTH;
                            $config['max_height']  = PASSPORT_HEIGHT;

                            $this->load->library('upload');      
                            $this->upload->initialize($config);                         

                            if (!$this->upload->do_upload('student_passport_photo')) {
                                
                                $img_upload_message = $this->upload->display_errors();

                            } else {                                

                                //Upload thumb image                                
                                $source = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$file_name;
                                
                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);
                                
                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    } 

                     // Upload Original copies of mark sheets
                    // if (isset($_FILES['mark_sheets_xerox'])) { 
                        
                    //     if ($_FILES['mark_sheets_xerox']['name'] != "") {

                    //         $file_name = $student_from_id.'.'.'jpg';
                    //         //$file_name = $student_from_id.'_'.'jpg';
                    //         //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                    //         $config = array();
                    //         $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                    //         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                    //         $config['file_name']            = $file_name;
                    //         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                    //         $this->load->library('upload');      
                    //         $this->upload->initialize($config);                         

                    //         if (!$this->upload->do_upload('mark_sheets_xerox')) {
                                
                    //             $img_upload_message = $this->upload->display_errors();

                    //         } else {                                

                    //             // Upload thumb image                                
                    //             $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                    //             $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;
                                
                    //             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                    //             // Update database with the cover image url
                    //             // $data = array(
                    //             //     "cover_image_url" => $file_name
                    //             // );

                    //             // $table = "tbl_gallery_albums";

                    //             // $where = array(
                    //             //     "id" => $album_id
                    //             // );
                    //             // $this->ablfunctions->updateData($data, $table, $where);
                                
                    //             // $img_upload_message = 'Cover image uploaded successfully.';
                    //         }
                    //     }
                    // }
                     // Upload Original copies of age verification
                    if (isset($_FILES['age_verification_xerox'])) { 
                        
                        if ($_FILES['age_verification_xerox']['name'] != "") {

                            $file_name = $student_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_AGE_PROOF;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');      
                            $this->upload->initialize($config);                         

                            if (!$this->upload->do_upload('age_verification_xerox')) {
                                
                                $img_upload_message = $this->upload->display_errors();

                            } else {                                

                                // Upload thumb image                                
                                $source = UPLOAD_DIR.STUDENT_AGE_PROOF.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_AGE_PROOF_THUMB.$file_name;
                                
                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);
                                
                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                     // Upload Original copies of SC/ST/OBC/Handicapped Certificate 
                    if (isset($_FILES['caste_certificate_xerox'])) { 
                        
                        if ($_FILES['caste_certificate_xerox']['name'] != "") {

                            $file_name = $student_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_CASTE;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');      
                            $this->upload->initialize($config);                         

                            if (!$this->upload->do_upload('caste_certificate_xerox')) {
                                
                                $img_upload_message = $this->upload->display_errors();

                            } else {                                

                                // Upload thumb image                                
                                $source = UPLOAD_DIR.STUDENT_CASTE.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_CASTE_THUMB.$file_name;
                                
                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);
                                
                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                     // Upload Countersignature of the Guardian
                    if (isset($_FILES['guardian_signature'])) { 
                        
                        if ($_FILES['guardian_signature']['name'] != "") {

                            $file_name = $student_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_GURDAIN_SIGN;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            $config['max_width']  = SIGN_WIDTH;
                            $config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');      
                            $this->upload->initialize($config);                         

                            if (!$this->upload->do_upload('guardian_signature')) {
                                
                                $img_upload_message = $this->upload->display_errors();

                            } else {                                

                                // Upload thumb image                                
                                $source = UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$file_name;
                                
                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);
                                
                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                     // Upload Signature of Candidate
                    if (isset($_FILES['candidate_signature'])) { 
                        
                        if ($_FILES['candidate_signature']['name'] != "") {

                            $file_name = $student_id.'.'.'jpg';
                            //echo $file_name;die;
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            $config['max_width']  = SIGN_WIDTH;
                            $config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');      
                            $this->upload->initialize($config);                         

                            if (!$this->upload->do_upload('candidate_signature')) {
                                
                                $img_upload_message = $this->upload->display_errors();

                            } else {                                

                                //Upload thumb image                                
                                $source = UPLOAD_DIR.STUDENT_SIGN.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_SIGN_THUMB.$file_name;
                                
                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);
                                
                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    } 
                    // Upload Signature of Candidate in full
                    if (isset($_FILES['candidate_signature_full'])) { 
                        
                        if ($_FILES['candidate_signature_full']['name'] != "") {

                            $file_name = $student_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN_FULL;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            $config['max_width']  = SIGN_WIDTH;
                            $config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');      
                            $this->upload->initialize($config);                         

                            if (!$this->upload->do_upload('candidate_signature_full')) {
                                
                                $img_upload_message = $this->upload->display_errors();

                            } else {                                

                                // Upload thumb image                                
                                $source = UPLOAD_DIR.STUDENT_SIGN_FULL.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$file_name;
                                
                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);
                                
                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                                                       
                    }

                    $this->session->set_flashdata("s_message", "Student Admissions record updated successfully.");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update the Student Admissions record.");
                }

                redirect("admin/college/editStudentRegister/".$encrypted_id."");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_classes",'');
            $student_id = $this->ablfunctions->ablDecrypt($encrypted_id);

             $data['education_details']= $this->College_model->get_educational_details($student_id);
             //echo "<pre>";print_r ($data['education_details']);die;
            $data['details'] = $this->ablfunctions->getDetailsFromId($student_id, "tbl_student_from");

            $this->load->view("admin/college/edit_student_register", $data);
        }
    }
         /*
    | --------------------------------------------------------------------------
    | Issue Admit card
    | --------------------------------------------------------------------------
    */
    function issueAdmitCard(){

        // Get the data
        $encrypted_id = $this->uri->segment(4);
        $from_id = $this->ablfunctions->ablDecrypt($encrypted_id);
        $data['card_details'] = $this->ablfunctions->getDetailsFromId($from_id, "tbl_student_from");
        $this->load->view("admin/college/admit_card_view", $data);
    }
    
             /*
    | --------------------------------------------------------------------------
    | delete Image
    | --------------------------------------------------------------------------
    */
    function deletePassportImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$student_id.".jpg";
        $path_thu=UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$student_id.".jpg";
        //echo $path;die;
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
             redirect("admin/college/editStudentRegister/".$cript_id."");


    }
    function deleteAgeproofImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.STUDENT_AGE_PROOF.$student_id.".jpg";
        $path_thu=UPLOAD_DIR.STUDENT_AGE_PROOF_THUMB.$student_id.".jpg";
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
             redirect("admin/college/editStudentRegister/".$cript_id."");


    }
    function deleteCasteImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.STUDENT_CASTE.$student_id.".jpg";
        $path_thu=UPLOAD_DIR.STUDENT_CASTE_THUMB.$student_id.".jpg";
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStudentRegister/".$cript_id."");


    }
        function deleteGdsignImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$student_id.".jpg";
        $path_thu=UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$student_id.".jpg";
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStudentRegister/".$cript_id."");


    }
    function deleteStsignImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.STUDENT_SIGN.$student_id.".jpg";
        $path_thu=UPLOAD_DIR.STUDENT_SIGN_THUMB.$student_id.".jpg";
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStudentRegister/".$cript_id."");


    }
    function deleteStfsignImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.STUDENT_SIGN_FULL.$student_id.".jpg";
        $path_thu=UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$student_id.".jpg";
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStudentRegister/".$cript_id."");


    }
    function deleteMarkSheetImage($document_url,$student_id,$edu_table_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.STUDENT_MARKSHEET.$document_url;
        $path_thu=UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$document_url;
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "document_url" =>'',
              );

              $table = "tbl_edu_qualification";

              $where = array(
                    "id" => $edu_table_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        if ($delete) {
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editStudentRegister/".$cript_id."");
        }
        


    }
    
    function deletePrincipalImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $path=UPLOAD_DIR.PRINCIPAL_SIGN.$student_id.".jpg";
        $path_thu=UPLOAD_DIR.PRINCIPAL_SIGN_THUMB.$student_id.".jpg";
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("admin/college/editPrincipal/".$cript_id."");


    }
        /*
    | --------------------------------------------------------------------------
    | Manage Principal
    | --------------------------------------------------------------------------
    */
    function managePrincipal(){

        // Get the data
        $data['details'] = $this->my_custom_functions->get_multiple_data("tbl_principal_record",'');
        $this->load->view("admin/college/manage_principal", $data);
    }
        /*
    | --------------------------------------------------------------------------
    | Add new Principal
    | --------------------------------------------------------------------------
    */
    function addPrincipal() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $this->form_validation->set_rules("principal_name", "Principal Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/addPrincipal");
            } else {

                $insert_data = array(
                    "principal_name" => $this->input->post("principal_name"),
                    "status" => $this->input->post("status")
                );

                $page_id = $this->my_custom_functions->insert_data($insert_data,"tbl_principal_record");///////  insert to user table

                if ($page_id) {

                    if (isset($_FILES['candidate_signature'])) { 
                        
                        if ($_FILES['candidate_signature']['name'] != "") {

                            $file_name = $page_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.PRINCIPAL_SIGN;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            $config['max_width']  = SIGN_WIDTH;
                            $config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');      
                            $this->upload->initialize($config);                         

                            if (!$this->upload->do_upload('candidate_signature')) {
                                
                                $img_upload_message = $this->upload->display_errors();

                            } else {                                

                                //Upload thumb image                                
                                $source = UPLOAD_DIR.PRINCIPAL_SIGN.$file_name;
                                $destination = UPLOAD_DIR.PRINCIPAL_SIGN_THUMB.$file_name;
                                
                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);
                                
                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    } 

                    $this->session->set_flashdata("s_message", "You have successfully added Principal Record.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the Principal Record! Please try again.");
                }

                redirect("admin/college/managePrincipal");
            }
        } else {

            $this->load->view("admin/college/add_principal");
        }
    }
    /*
    | --------------------------------------------------------------------------
    | Edit Principal
    | --------------------------------------------------------------------------
    */
    function editPrincipal() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('page_id');
            $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $this->form_validation->set_rules("principal_name", "Principal Name", "trim|required");
            $this->form_validation->set_rules("status", "Status", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/college/editPrincipal");
            } else {

                $update_data = array(
                    "principal_name" => $this->input->post("principal_name"),
                    "status" => $this->input->post("status")
                );

                $where = array(
                "id" => $page_id,
              );

                $update = $this->my_custom_functions->update_data($update_data,"tbl_principal_record",$where); 

                if ($update) {

                    if (isset($_FILES['candidate_signature'])) { 
                        
                        if ($_FILES['candidate_signature']['name'] != "") {

                            $file_name = $page_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.PRINCIPAL_SIGN;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            $config['max_width']  = SIGN_WIDTH;
                            $config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');      
                            $this->upload->initialize($config);                         

                            if (!$this->upload->do_upload('candidate_signature')) {
                                
                                $img_upload_message = $this->upload->display_errors();

                            } else {                                

                                //Upload thumb image                                
                                $source = UPLOAD_DIR.PRINCIPAL_SIGN.$file_name;
                                $destination = UPLOAD_DIR.PRINCIPAL_SIGN_THUMB.$file_name;
                                
                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);
                                
                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    } 

                    $this->session->set_flashdata("s_message", "You have successfully Update Principal Record.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to Update the Principal Record! Please try again.");
                }

                redirect("admin/college/managePrincipal");
            }
        } else {

        // Get the data
        $encrypted_id = $this->uri->segment(4);
        $principal_id = $this->ablfunctions->ablDecrypt($encrypted_id);
        $data['details'] = $this->ablfunctions->getDetailsFromId($principal_id, "tbl_principal_record");
        $this->load->view("admin/college/edit_principal", $data);
        }
    }
            /*
    | --------------------------------------------------------------------------
    | Delete Principal
    | --------------------------------------------------------------------------
    */
    function deletePrincipal() {
      $encrypted_id = $this->uri->segment(4);
      $page_id = $this->ablfunctions->ablDecrypt($encrypted_id);
      $query = $this->ablfunctions->deleteData("tbl_principal_record", array("id" => $page_id));
    if ($query) { // if deleted
        $path=UPLOAD_DIR.PRINCIPAL_SIGN.$page_id.".jpg";
        $path_thu=UPLOAD_DIR.PRINCIPAL_SIGN_THUMB.$page_id.".jpg";
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $this->session->set_flashdata('s_message', 'Record Deleted Successfully.');
        redirect("admin/college/managePrincipal");
    } else {
        $this->session->set_flashdata('e_message', 'Record Deletion Failed');
        redirect("admin/college/managePrincipal");
    }
    }
        ////////////////////////////////////////////////////////////////////////////
    // Download Admit Card
    ////////////////////////////////////////////////////////////////////////////
    function downloadAdmitCard() {

        // Get the data
        $encrypted_id = $this->uri->segment(4);
        $from_id = $this->ablfunctions->ablDecrypt($encrypted_id);
        $card_details = $this->ablfunctions->getDetailsFromId($from_id, "tbl_student_from");
        $path_student=base_url().UPLOAD_DIR.STUDENT_SIGN_FULL.$card_details['id'].".jpg";
                                            
        //$this->load->view("admin/college/admit_card_view", $data);


            include(APPPATH . "libraries/mpdf/mpdf.php");

                
            $mpdf = new mPDF('utf-8', 'A4', '', '', '15', '15', '15', '18', '15', '15');
                
            $mpdf->SetTitle('Student Admit Card');
                
            $mpdf->SetDisplayMode('fullpage');

                
            $html =    '<htmlpagefooter name="MyFooter1">
                        <table width="100%" style="vertical-align: bottom;">
                        <tr>
                        <td width="80%">
                        <span style="font-size: 14;">College Of Art & Design</span>
                        </td>                                
                        </tr>
                        </table>
                        </htmlpagefooter>
                        <sethtmlpagefooter name="MyFooter1" value="on" />
                        <htmlpageheader name="MyHeader1"><div style="color: #525252;font-size: 14px;width: 100%;text-align: center;"></div></htmlpageheader>
                                <sethtmlpageheader name="MyHeader1" value="on" />';


                    $html .=   '<div style="width:100%;text-align:center;">            
                    <div style="width: 100%; float: left; display: block; overflow: auto;">

                    <div style="width: 100%; float: left; display: block; padding-bottom: 15px; border-bottom: 4px solid #333;">
                    <div style="width: 20%; float: left; display: inline-block; text-align: left;">
                    <span>
                    <img src="' . $path_student . '" width="96" height="96">
                    </span>    
                    </div>
                    <div style="width: 80%; float: left; display: inline-block; text-align: left; padding-top: 15px;">                                 
                    <span style="font-weight: bold;">
                    ' . $card_details['student_name'] . '
                    </span>  
                    <br>
                    <span style="font-weight: bold;">
                    ' . $card_details['student_name'] . '
                    </span>  
                    <br>
                    <span style="font-weight: bold;">
                    Mobile: ' . $card_details['student_name'] . ', 
                    Email: ' . $card_details['perm_address'] . '    
                    </span>                                                             
                    </div>
                    </div>    

                    <div style="width: 100%; float: left; display: block; margin-top: 25px; text-align: center;">                        
                    <span style="font-weight: bold;">
                                                Payment Receipt
                    </span>                                                       
                    </div>

                    <div style="width: 100%; float: left; display: block; margin-top: 25px;">     
                    <div style="width: 50%; float: left; display: inline-block; text-align: left;">
                    <span style="font-weight: bold;">
                     Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $card_details['student_name'] . '
                    </span>   
                    <br>
                    <span style="font-weight: bold;">
                    Semester: &nbsp;&nbsp;' . $card_details['student_name'] . '
                    </span>
                    <br>
                    <span style="font-weight: bold;">
                    Section: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $card_details['student_name'] . '
                     </span>
                    <br>
                    <span style="font-weight: bold;">
                    Roll No: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $card_details['student_name']. '
                     </span>
                    </div>   
                    <div style="width: 50%; float: right; display: inline-block; text-align: left;">
  
                    <br>
                    <span style="font-weight: bold;">
                    Receipt No: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $card_details['student_name'] . '
                    </span>
                    <br>
                    <span style="font-weight: bold;">                                
                    </span>
                    </div> 
                    </div>

                    <div style="width: 100%; float: left; display: block; line-height: 40px; margin-top: 25px; border: 2px solid #333; border-left: none; border-right: none;"> 
                    <div style="width: 50%; float: left; display: inline-block; text-align: left;">
                    <span style="font-weight: bold;">
                    Fees Term: ' . $card_details['student_name'] . '
                    </span> 
                    </div>  
                    <div style="width: 50%; float: right; display: inline-block; text-align: left;">
                    <span style="font-weight: bold;">
                        Payment Mode: ' . $card_details['student_name'] . '
                    </span>   
                        </div> 
                    </div>

                    <div style="width: 100%; float: left; display: block; margin-top: 25px;">                             
                    <table style="width: 100%; line-height: 40px;" border="0" cellspacing="0">
                    <thead>
                    <tr style="background-color: #eee;">                                               
                    <th scope="col" style="text-align:left; padding: 6px 10px;">
                    Fees Name
                    </th>  
                    <th scope="col" style="text-align:right; padding: 6px 10px;">
                    Amount
                    </th>                                                
                        </tr>
                    </thead>
                    <tbody>';
                    
 

                    $html .=    '<tr>
                    <td style="text-align:left; border-top: 2px solid #333;">
                    <span style="font-weight: bold;">Fees Amount</span> 
                    </td>  
                    <td style="text-align:right; border-top: 2px solid #333;">
   
                    </td>                                                    
                </tr>

                <tr>
                <td style="text-align:left; border-top: 2px solid #333;">
                    <span style="font-weight: bold;">Late Fine</span> 
                </td>  
                <td style="text-align:right; border-top: 2px solid #333;">
   
                     </td>                                                    
                 </tr>';

              /*if ($payment_settings['add_on_school_fees'] > 0 AND $payment_details['service_charges'] > 0) {
              $html .=    '<tr>
                <td style="text-align:left; border-top: 2px solid #333;">
                    <span style="font-weight: bold;">Payment Handling Fees</span> 
                    </td>  
                    <td style="text-align:right; border-top: 2px solid #333;">
                            <span style="font-weight: bold;">
                            ' . INDIAN_CURRENCY_CODE . number_format($payment_details['service_charges'], 2) . '
                                    </span>    
                                </td>                                                    
                            </tr>';
             }*/

            $html .=   '<tr>
            <td style="text-align:left; border-top: 2px solid #333;">
                <span style="font-weight: bold;">Paid Amount</span>                                                                 
            </td>  
            <td style="text-align:right; border-top: 2px solid #333;">
  
                </td>                                                    
            </tr>                                                                                                                                  
            </tbody>
            </table>                                
            </div>                                                
            </div>                      
            </div>';

            $mpdf->WriteHTML($html);

            $file_name = 'temp/Admit-Card' . $this->ablfunctions->ablEncrypt($from_id) . '.pdf';
            $mpdf->Output($file_name, 'F');

            $file_url = base_url() . $file_name;
            header("Location: $file_url");

            exit();

         }

}
