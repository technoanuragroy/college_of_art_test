<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {

    function __construct() {
        parent::__construct();           
        $this->my_custom_functions->checkAdminSecurity();
        $this->load->model("admin/Settings_model");
    }

    /*
    | --------------------------------------------------------------------------
    | Email settings
    | --------------------------------------------------------------------------   
    */
    public function emailSettings() {
        
        if ($this->input->post("submit") && $this->input->post("submit") != "") {
            
            // Save email settings data
            // Settings enabled/disabled flag
            if($this->input->post("gateway_settings")) {
            
                $gateway_settings = 1;
            } else {

                $gateway_settings = 0;
            }

            // Gateway cofiguration in json format
            $config = array(
                "host" => $this->input->post("host"),
                "port" => $this->input->post("port"),
                "username" => $this->input->post("username"),
                "password" => $this->input->post("password"),
            );

            $gateway_config = json_encode($config);

            // Main data
            $gateway_data = array(
                "gateway_label" => $this->input->post("gateway_label"),
                "gateway_config" => $gateway_config,
                "gateway_settings" => $gateway_settings
            );
            
            $this->Settings_model->save_gateway_settings(SETTINGS_EMAIL_KEY, $gateway_data);
            
            $this->session->set_flashdata("s_message", "Email gateway settings updated successfully.");
            redirect("admin/settings/emailSettings");            
        } else {
            
            $email_settings = $this->Settings_model->get_gateway_settings(SETTINGS_EMAIL_KEY);
            
            $data["gateway_settings"] = "";
            $data["gateway_label"] = "";
            $data["host"] = "";
            $data["port"] = "";
            $data["username"] = "";
            $data["password"] = "";
            $data["display"] = "";

            if(!empty($email_settings)) {

                $data["gateway_settings"] = $email_settings['gateway_settings'];
                $data["gateway_label"] = $email_settings['gateway_label'];                        
                $config = json_decode($email_settings['gateway_config'], true);

                if(array_key_exists('host', $config)) {
                    $data["host"] = $config['host'];
                }
                if(array_key_exists('port', $config)) {
                    $data["port"] = $config['port'];
                }
                if(array_key_exists('username', $config)) {
                    $data["username"] = $config['username'];
                }
                if(array_key_exists('password', $config)) {
                    $data["password"] = $config['password'];
                }
            }

            // If settings is disabled, hide the inputs
            if($data["gateway_settings"] == 1) {
                $data["display"] = 'style="display: block;"';
            } else {
                $data["display"] = 'style="display: none;"';
            }

            $this->load->view("admin/settings/email_settings", $data);
        }
    }
    
    /*
    | --------------------------------------------------------------------------
    | SMS settings
    | --------------------------------------------------------------------------  
    */
    public function smsSettings() {
                
        if ($this->input->post("submit") && $this->input->post("submit") != "") {
            
            // Save sms settings data
            // Settings enabled/disabled flag
            if($this->input->post("gateway_settings")) {
            
                $gateway_settings = 1;
            } else {

                $gateway_settings = 0;
            }

            // Gateway cofiguration in json format
            $config = array(
                "sms_url" => $this->input->post("sms_url")            
            );

            if($this->input->post("variable_name") AND $this->input->post("variable_value")) {

                $variable_names = $this->input->post("variable_name");
                $variable_values = $this->input->post("variable_value");

                foreach($variable_names as $key => $variable_name) {

                    $variable_value = $variable_values[$key];                
                    $config[$variable_name] = $variable_value;
                }
            }

            $gateway_config = json_encode($config);

            // Main data
            $gateway_data = array(
                "gateway_label" => $this->input->post("gateway_label"),
                "gateway_config" => $gateway_config,
                "gateway_settings" => $gateway_settings
            );
        
            $this->Settings_model->save_gateway_settings(SETTINGS_SMS_KEY, $gateway_data);
            
            $this->session->set_flashdata("s_message", "SMS gateway settings updated successfully.");
            redirect("admin/settings/smsSettings");            
        } else {
            
            $sms_settings = $this->Settings_model->get_gateway_settings(SETTINGS_SMS_KEY);
            
            $data["gateway_settings"] = "";
            $data["gateway_label"] = "";
            $data["sms_url"] = "";
            $data["variables"] = array();            
            $data["display"] = "";

            if(!empty($sms_settings)) {

                $data["gateway_settings"] = $sms_settings['gateway_settings'];
                $data["gateway_label"] = $sms_settings['gateway_label'];                        
                $config = json_decode($sms_settings['gateway_config'], true);

                if(array_key_exists('sms_url', $config)) {
                    $data["sms_url"] = $config['sms_url'];
                    unset($config['sms_url']);
                }
                
                $data["variables"] = $config;
            }

            // If settings is disabled, hide the inputs
            if($data["gateway_settings"] == 1) {
                $data["display"] = 'style="display: block;"';
            } else {
                $data["display"] = 'style="display: none;"';
            }

            $this->load->view("admin/settings/sms_settings", $data);
        }
    }

    /*
    | --------------------------------------------------------------------------
    | User settings
    | --------------------------------------------------------------------------
    */
    public function userSettings() {
        
        if ($this->input->post("submit") && $this->input->post("submit") != "") {
            
            // Save user settings
            $settings_data = array(
                "email_verification" => $this->input->post("email_verification"),
                "otp_verification" => $this->input->post("otp_verification"),
                "username" => $this->input->post("username")
            );  
            
            $this->Settings_model->save_user_settings($settings_data);
            
            $this->session->set_flashdata("s_message", "User settings updated successfully.");
            redirect("admin/settings/userSettings");            
        } else {
            
            $user_settings = $this->Settings_model->get_user_settings();
                        
            $data["email_verification"] = "";
            $data["otp_verification"] = "";
            $data["username"] = "";            

            if(!empty($user_settings)) {

                $data = $user_settings;
            }
            
            $this->load->view("admin/settings/user_settings", $data);
        }
    }
    
}
