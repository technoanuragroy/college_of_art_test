<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("admin/Main_model");
    }

    /*
    | --------------------------------------------------------------------------
    | Admin login
    | --------------------------------------------------------------------------
    */
    public function index() {

        if ($this->input->post("submit") AND $this->input->post("submit") != "") {

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("username", "Username", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin");
            } else {

                // Verifying the credentials with database
                $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
                $password = $this->input->post('password');

                $verified = $this->Main_model->login_check($username, $password);

                // If credentials match with database data
                if ($verified) {

                    // Set cookie if remember me checkbox is checked
                    if($this->input->post('remember_me_admin')) {

                        $cookie_data = array(
                            'name' => 'admin_id',
                            'value' => $this->ablfunctions->ablEncrypt($this->session->userdata('admin_id')),
                            'expire' => COOKIE_EXPIRY_TIME,
                        );
                        $this->input->set_cookie($cookie_data);
                    }
                    // Delete cookie if remember me checkbox is not checked
                    else {

                        if ($this->input->cookie('admin_id', true)) {

                            delete_cookie('admin_id');
                        }
                    }

                    redirect("admin/user/dashboard");
                }
                // If credentials don't match
                else {

                    $this->session->set_flashdata("e_message", "Invalid Username/Password.");
                    redirect("admin");
                }
            }
        } else {

            // If the admin is already logged in, redirect it to dashboard
            if($this->session->userdata('admin_id')) {

                redirect("admin/user/dashboard");
            }
            // Else check if anything stored in cookie
            else if ($this->input->cookie('admin_id', true)) {

                $admin_id = $this->ablfunctions->ablDecrypt($this->input->cookie('admin_id'));
                $admin_details = $this->ablfunctions->getDetailsFromId($admin_id, "tbl_admins");

                if(!empty($admin_details)) {

                    // Session variables for admin
                    $session_id = '2';
                    $session_data = array(
                        "session_id" => $session_id,
                        "admin_id" => $admin_details["id"],
                        "admin_name" => $admin_details["name"],
                        "admin_email" => $admin_details["email"],
                        "admin_type" => $admin_details["type"],


                    );
                    $this->session->set_userdata($session_data);

                    redirect("admin/user/dashboard");
                }
            }

            $this->load->view("admin/login");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Admin forgot password
    | --------------------------------------------------------------------------
    */
    public function forgotPassword() {

        if($this->input->post('submit') AND $this->input->post('submit') != "") {

            // CI form validation
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required');

            if($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/main/forgotPassword");
            } else {

                // Verifying the username with database
                $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
                $verified = $this->ablfunctions->getParticularCount("tbl_admins", " AND username='" . $username . "' AND status=1");

                // If the username already exists in database
                if($verified) {

                    $admin_details = $this->ablfunctions->getDetailsFromId("", "tbl_admins", array("username" => $username));

                    // Password verification token
                    $verification_token = $this->my_custom_functions->createVerificationToken(USER_TYPE_ADMIN, $admin_details['id'], TOKEN_TYPE_FORGOT_PASSWORD);

                    // Password reset link
                    $reset_link = base_url() . 'admin/main/resetPassword/' . $verification_token;

                    $this->load->helper('file');

                    // Email content
                    $message = "";
                    $message .= read_file("application/views/mail_template/mail_template_header.php");
                    $message .= '<h1>Forgot Password</h1>';
                    $message .= '<p>';
                    $message .= 'Hello '.$admin_details['name'].',<br><br>';
                    $message .= 'You are receiving this email because you have requested for a change of password.<br><br>';
                    $message .= 'You may <a href="'.$reset_link.'">click here</a> to reset your password now.<br><br>Thank you.';
                    $message .= '</p>';
                    $message .= read_file("application/views/mail_template/mail_template_footer.php");

                    // Send email
                    $this->ablfunctions->SendEmail(SITE_EMAIL.','.SITE_NAME, $admin_details['email'], SITE_NAME.' : Recover password', $message);

                    $this->session->set_flashdata("s_message", "A password recovery email has been sent to your email account.");
                    redirect("admin/main/forgotPassword");
                }
                // If the username doesn't exist
                else {

                    $this->session->set_flashdata("e_message", "You are not a registered admin!");
                    redirect("admin/main/forgotPassword");
                }
            }
        } else {

            $this->load->view("admin/forgot_password");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Admin reset password
    | --------------------------------------------------------------------------
    */
    public function resetPassword() {

        $verification_token = $this->uri->segment(4);

        if ($verification_token != "") {

            // Check the token's validity. It will return the token details if token is valid
            $verified = $this->my_custom_functions->validateVerificationToken(USER_TYPE_ADMIN, TOKEN_TYPE_FORGOT_PASSWORD, $verification_token);

            // If the verification link is valid
            if (!empty($verified)) {

                if($this->input->post('submit') AND $this->input->post('submit') != "") {

                    // CI form validation
                    $this->load->library('form_validation');

                    $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[32]');
                    $this->form_validation->set_rules('retype_new_password', 'Retype New Password', 'trim|required|min_length[6]|max_length[32]|matches[new_password]');

                    if ($this->form_validation->run() == FALSE) {

                        $this->session->set_flashdata("e_message", validation_errors());
                        redirect("resetPassword/" . $verification_token);
                    } else {

                        // Update password
                        $data = array(
                            "password" => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)
                        );

                        $table = "tbl_admins";

                        $where = array(
                            "id" => $verified['user_id']
                        );
                        $this->ablfunctions->updateData($data, $table, $where);

                        // Delete verification token
                        $table = "tbl_verification_tokens";

                        $where = array(
                            "id" => $verified['id']
                        );
                        $this->ablfunctions->deleteData($table, $where);

                        $this->session->set_flashdata("s_message", "Your password has been changed successfully, you can login to your account using this new password.");
                        redirect("admin");
                    }
                } else {

                    $this->load->view("admin/reset_password");
                }
            }
            // If the verification link is expired
            else {

                $this->session->set_flashdata("e_message", "The verification link is expired.");
                redirect("admin");
            }
        } else {

            $this->session->set_flashdata("e_message", "Something went wrong. Please make a request to reset the password once again.");
            redirect("admin");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Admin logout
    | --------------------------------------------------------------------------
    */
    public function logout() {

        // Unset sessions
        $session_data = array('admin_id', 'admin_name', 'admin_email', 'admin_type','session_id');
        $this->session->unset_userdata($session_data);

        // Delete cookie
        if ($this->input->cookie('admin_id', true)) {

            delete_cookie('admin_id');
        }

        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("admin");
    }
}
