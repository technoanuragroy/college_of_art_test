<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();           
        $this->my_custom_functions->checkAdminSecurity();
        $this->load->model("admin/User_model");
    }

    /*
    | --------------------------------------------------------------------------
    | Admin dashboard
    | --------------------------------------------------------------------------     
    */
    public function dashboard() {
        
        $this->load->view("admin/dashboard");
    }
    
    /*
    | --------------------------------------------------------------------------
    | Edit profile
    | --------------------------------------------------------------------------  
    */
    function editProfile() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            // CI form validation
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            $this->form_validation->set_rules("phone", "Phone Number", "trim|required|integer");

            if ($this->form_validation->run() == false) { 
                
                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/user/editProfile");
            } else { 
                           
                // Update profile data
                $update_data = array(
                    "name" => $this->input->post("name"),
                    "email" => $this->input->post("email"),
                    "phone" => $this->input->post("phone")            
                );
                
                // Update type for super admin only
                if($this->session->userdata("admin_type") == ADMIN_TYPE_SUPER_ADMIN) {
                    $update_data["type"] = $this->input->post("type");                                    
                }
                
                $update = $this->User_model->update_profile_data($this->session->userdata('admin_id'), $update_data);                               

                if ($update) {
                    
                    $this->session->set_flashdata('s_message', "Successfully updated profile details.");
                    redirect("admin/user/editProfile");
                } else {
                    
                    $this->session->set_flashdata('e_message', "Failed to update profile details.");
                    redirect("admin/user/editProfile");
                }
            }
        } else {
            
            $data['admin_details'] = $this->ablfunctions->getDetailsFromId($this->session->userdata('admin_id'), "tbl_admins");
            
            $this->load->view("admin/edit_profile", $data);
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Change password
    | -------------------------------------------------------------------------- 
    */
    public function changePassword() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            // CI form validation
            $this->load->library("form_validation");
            
            $this->form_validation->set_rules("old_password", "Old Password", "trim|required");
            $this->form_validation->set_rules("new_password", "New Password", "trim|required|min_length[6]|max_length[32]");
            $this->form_validation->set_rules("confirm_new_password", "Confirm New Password", "trim|required|min_length[6]|max_length[32]|matches[new_password]");

            if ($this->form_validation->run() == false) { 
                
                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/user/changePassword");
            } else {

                // Check if given old password matches with existing password
                $old_password_given = $this->input->post('old_password');
                $old_password_existing = $this->ablfunctions->getParticularFieldValue("tbl_admins", "password", " and id='" . $this->session->userdata('admin_id') . "'");

                if (password_verify($old_password_given, $old_password_existing)) {

                    // Update password
                    $data = array(
                        "password" => password_hash($this->input->post("new_password"), PASSWORD_DEFAULT)
                    );

                    $table = "tbl_admins";

                    $where = array(
                        "id" => $this->session->userdata('admin_id')
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Password has been updated successfully.");
                    redirect("admin/user/changePassword");
                } else {
                    
                    $this->session->set_flashdata("e_message", "The given old password is incorrect.");
                    redirect("admin/user/changePassword");
                }
            }
        } else {
            
            $this->load->view("admin/change_password");
        }
    }    

    /*
    | --------------------------------------------------------------------------
    | Create new admin
    | --------------------------------------------------------------------------
    */
    function addAdmin() { 
        
        // Super admin can add other admins
        if ($this->session->userdata('admin_type') == ADMIN_TYPE_SUPER_ADMIN) {
            
            if ($this->input->post("submit") && $this->input->post("submit") != "") {
                
                // CI form validation
                $this->load->library("form_validation");
                          
                $this->form_validation->set_rules("name", "Name", "trim|required");
                $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");  
                $this->form_validation->set_rules("phone", "Phone Number", "trim|required|integer");
                $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|is_unique[tbl_admins.username]");
                $this->form_validation->set_rules("password", "Password", "trim|required|min_length[6]|max_length[32]");
                $this->form_validation->set_rules("confirm_password", "Confirm Password", "trim|required|min_length[6]|max_length[32]|matches[password]");

                if ($this->form_validation->run() == false) { 
                    
                    $this->session->set_flashdata("e_message", validation_errors());
                    redirect("admin/user/manageAdmins");
                } else {  
                    
                    // Add new admin data
                    $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);

                    $insert_data = array(
                        "type" => $this->input->post("type"),
                        "name" => $this->input->post("name"),
                        "username" => $this->input->post("username"),
                        "password" => $password,
                        "email" => $this->input->post("email"),
                        "phone" => $this->input->post("phone"),                        
                        "date_created" => date(DATETIME_FORMAT),            
                        "status" => $this->input->post("status")
                    );
                    
                    $admin_id = $this->User_model->insert_admin_data($insert_data);
                    
                    if ($admin_id) { 
                        
                        $this->session->set_flashdata("s_message", "You have successfully added a new admin.");
                    } else {
                        
                        $this->session->set_flashdata("e_message", "Failed to add the admin! Please try again.");
                    }
                    
                    redirect("admin/user/manageAdmins");
                }
            } else {
               
                $this->load->view("admin/add_admin");
            }
        } else {
            
            $this->session->set_flashdata('e_message', "Sorry, you are not authorised to add an admin.");
            redirect("admin/user/dashboard");
        }
    }
    
    /*
    | --------------------------------------------------------------------------
    | Edit admin
    | --------------------------------------------------------------------------    
    */
    function editAdmin() {
        
        // Super admin can edit other admins
        if ($this->session->userdata('admin_type') == ADMIN_TYPE_SUPER_ADMIN) {
            
            if ($this->input->post("submit") && $this->input->post("submit") != "") {
                
                $encrypted_id = $this->input->post('admin_id');
                $admin_id = $this->ablfunctions->ablDecrypt($encrypted_id);

                // CI form validation
                $this->load->library("form_validation");
                
                $this->form_validation->set_rules("name", "Name", "trim|required");
                $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
                $this->form_validation->set_rules("phone", "Phone Number", "trim|required|integer");
                $this->form_validation->set_rules("password", "Password", "trim|min_length[6]|max_length[32]");
                $this->form_validation->set_rules("confirm_password", "Confirm Password", "trim|min_length[6]|max_length[32]|matches[password]");

                if ($this->form_validation->run() == false) { 
                    
                    $this->session->set_flashdata("e_message", validation_errors());
                    redirect("admin/user/editAdmin/" . $encrypted_id);
                } else { 
                    
                    // Update admin data
                    $update_data = array(
                        "type" => $this->input->post("type"),
                        "name" => $this->input->post("name"),
                        "email" => $this->input->post("email"),
                        "phone" => $this->input->post("phone"),
                        "status" => $this->input->post("status"),                        
                    );

                    // Update password if password provided only
                    if($this->input->post("password") AND $this->input->post("password") != "") {

                        $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);
                        $update_data["password"] = $password;
                    }
                    
                    $update = $this->User_model->update_other_admin_data($admin_id, $update_data);
                                        
                    if ($update) {
                        
                        $this->session->set_flashdata("s_message", "Admin record updated successfully.");
                    } else {
                        
                        $this->session->set_flashdata('e_message', "Failed to update the admin record.");
                    }

                    redirect("admin/user/editAdmin/" . $encrypted_id);
                }
            } else {
                
                $encrypted_id = $this->uri->segment(4);
                $admin_id = $this->ablfunctions->ablDecrypt($encrypted_id);
                
                $data['admin_details'] = $this->ablfunctions->getDetailsFromId($admin_id, "tbl_admins");
                $data['admin_details']['encrypted_id'] = $encrypted_id;
                
                $this->load->view("admin/edit_admin", $data);
            }
        } else {
            
            $this->session->set_flashdata('e_message', "Sorry, you are not authorised to edit an admin.");
            redirect("admin/user/dashboard");
        }
    }
    
    /*
    | --------------------------------------------------------------------------
    | Manage admins
    | --------------------------------------------------------------------------  
    */
    function manageAdmins() {

        // Super admin can manage other admins
        if ($this->session->userdata('admin_type') == ADMIN_TYPE_SUPER_ADMIN) {

            // All the admins except the logged in admin
            $data['admins'] = $this->User_model->get_all_admins();

            foreach ($data['admins'] as $key => $admin) {
               
                if ($admin['status'] == 1) {
                    
                    $data['admins'][$key]['status_text'] = "<span style='color:green;'>Active</span>";
                } else if ($admin['status'] == 0) {
                    
                    $data['admins'][$key]['status_text'] = "<span style='color:red;'>Inactive</span>";
                }

                if ($admin['type'] == ADMIN_TYPE_SUPER_ADMIN) {
                    
                    $data['admins'][$key]['type_text'] = "<span style='color:crimson;'>Super admin</span>";                    
                } else if ($admin['type'] == ADMIN_TYPE_ADMIN) {
                    
                    $data['admins'][$key]['type_text'] = "<span style='color:chocolate;'>Admin</span>";
                }           
                
                $data['admins'][$key]['encrypted_id'] = $this->ablfunctions->ablEncrypt($admin['id']);
            }

            $this->load->view("admin/manage_admins", $data);
        } else {

            $this->session->set_flashdata('e_message', "Sorry, you are not authorised to manage admins.");
            redirect("admin/user/dashboard");
        }
    }   
    
    /*
    | --------------------------------------------------------------------------
    | Validate admin username through ajax call from add admin page
    | --------------------------------------------------------------------------
    */
    public function validateAdmin() {

        $username = $this->input->post('username');
        
        if (isset($username) && $username != "") {
            
            $username_count = $this->ablfunctions->getParticularCount("tbl_admins", " and username='" . $username . "'");
            
            if ($username_count > 0) {
                
                echo "Username already exists on system";
            } else {
                
                echo "";
            }
        } else {
            
            echo "Invalid username";
        }
    }
                   
    /*
    | --------------------------------------------------------------------------
    | Delete admin
    | -------------------------------------------------------------------------- 
    */
    function deleteAdmin() {

        $encrypted_id = $this->uri->segment(4);
        $admin_id = $this->ablfunctions->ablDecrypt($encrypted_id);
        
        // Delete admin data
        $this->ablfunctions->deleteData("tbl_admins", array("id" => $admin_id));
        
        $this->session->set_flashdata('s_message', "Admin has been deleted successfully.");
        redirect("admin/user/manageAdmins");
    }          
}
