<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

    function __construct() {
        parent::__construct();           
        $this->my_custom_functions->checkAdminSecurity();
        $this->load->model("admin/Gallery_model");
    }
        
    /*
    | --------------------------------------------------------------------------
    | Manage albums 
    | --------------------------------------------------------------------------
    */
    function manageAlbums() {
        
        // Get the data
        $data['albums'] = $this->Gallery_model->get_all_albums();
        
        foreach ($data['albums'] as $key => $album) {                           
            
            $data['albums'][$key]['encrypted_id'] = $this->ablfunctions->ablEncrypt($album['id']);
        }                

        $this->load->view("admin/gallery/manage_albums", $data);
    }
        
    /*
    | --------------------------------------------------------------------------
    | Create new album
    | --------------------------------------------------------------------------
    */
    function addAlbum() { 
                
        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("name", "Name", "trim|required");            

            if ($this->form_validation->run() == false) { 

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/gallery");
            } else {  

                // Add new album data                
                $insert_data = array(                    
                    "name" => $this->input->post("name"),  
                    "details" => $this->input->post("details")
                );
                
                // If any parent album is selected
                if($this->input->post('parent_album')) {
                    $insert_data['parent_album'] = $this->ablfunctions->ablDecrypt($this->input->post('parent_album'));
                }

                $album_id = $this->Gallery_model->insert_album_data($insert_data);

                if ($album_id) { 
                    
                    // Upload album cover image if there is any                     
                    $img_upload_message = '';
                    
                    if (isset($_FILES['cover_image_url'])) { 
                        
                        if ($_FILES['cover_image_url']['name'] != "") {

                            $file_name = $album_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.GALLERY_DIR;
                            $config['allowed_types']        = ALLOWED_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_IMAGE_SIZE;

                            $this->load->library('upload');      
                            $this->upload->initialize($config);                         

                            if (!$this->upload->do_upload('cover_image_url')) {
                                
                                $img_upload_message = $this->upload->display_errors();

                            } else {                                

                                // Upload thumb image                                
                                $source = UPLOAD_DIR.GALLERY_DIR.$file_name;
                                $destination = UPLOAD_DIR.GALLERY_THUMB_DIR.$file_name;
                                
                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                                // Update database with the cover image url
                                $data = array(
                                    "cover_image_url" => $file_name
                                );

                                $table = "tbl_gallery_albums";

                                $where = array(
                                    "id" => $album_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);
                                
                                $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }                                     

                    $this->session->set_flashdata("s_message", "You have successfully added a new album. ".$img_upload_message);
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the album! Please try again.");
                }

                redirect("admin/gallery");
            }
        } else {

            $data['albums'] = $this->Gallery_model->get_all_albums();  
            
            foreach ($data['albums'] as $key => $album) {                           
            
                $data['albums'][$key]['encrypted_id'] = $this->ablfunctions->ablEncrypt($album['id']);
            }
            
            $this->load->view("admin/gallery/add_album", $data);
        }        
    }
    
    /*
    | --------------------------------------------------------------------------
    | Edit album
    | --------------------------------------------------------------------------
    */
    function editAlbum() { 
                
        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            $encrypted_id = $this->input->post('album_id');
            $album_id = $this->ablfunctions->ablDecrypt($encrypted_id);
            
            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("name", "Name", "trim|required");            

            if ($this->form_validation->run() == false) { 

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("admin/user/editAlbum/" . $encrypted_id);
            } else {  

                // Update album data                
                $update_data = array(                    
                    "name" => $this->input->post("name"),  
                    "details" => $this->input->post("details")
                );
                
                // If any parent album is selected
                if($this->input->post('parent_album')) {
                    $insert_data['parent_album'] = $this->ablfunctions->ablDecrypt($this->input->post('parent_album'));
                }

                $album_id = $this->Gallery_model->update_album_data($insert_data);

                if ($album_id) { 
                    
                    // Upload album cover image if there is any                     
                    $img_upload_message = '';
                    
                    if (isset($_FILES['cover_image_url'])) { 
                        
                        if ($_FILES['cover_image_url']['name'] != "") {

                            $file_name = $album_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.GALLERY_DIR;
                            $config['allowed_types']        = ALLOWED_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_IMAGE_SIZE;

                            $this->load->library('upload');      
                            $this->upload->initialize($config);                         

                            if (!$this->upload->do_upload('cover_image_url')) {
                                
                                $img_upload_message = $this->upload->display_errors();

                            } else {                                

                                // Upload thumb image                                
                                $source = UPLOAD_DIR.GALLERY_DIR.$file_name;
                                $destination = UPLOAD_DIR.GALLERY_THUMB_DIR.$file_name;
                                
                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                                
                                // Update database with the cover image url
                                $data = array(
                                    "cover_image_url" => $file_name
                                );

                                $table = "tbl_gallery_albums";

                                $where = array(
                                    "id" => $album_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);
                                
                                $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }                                     

                    $this->session->set_flashdata("s_message", "You have successfully added a new album. ".$img_upload_message);
                } else {

                    $this->session->set_flashdata("e_message", "Failed to add the album! Please try again.");
                }

                redirect("admin/gallery");
            }
        } else {

            $encrypted_id = $this->uri->segment(4);
            $album_id = $this->ablfunctions->ablDecrypt($encrypted_id);

            $data['album_details'] = $this->ablfunctions->getDetailsFromId($album_id, "tbl_gallery_albums");
            $data['album_details']['encrypted_id'] = $encrypted_id;
            
            $data['albums'] = $this->Gallery_model->get_all_albums();  
            
            foreach ($data['albums'] as $key => $album) {                           
            
                $data['albums'][$key]['encrypted_id'] = $this->ablfunctions->ablEncrypt($album['id']);
            }
                                    
            $this->load->view("admin/gallery/edit_album", $data);
        }        
    }

    /*
    | --------------------------------------------------------------------------
    | Upload new file
    | --------------------------------------------------------------------------
    */
    function addFile() {
        
        if ($this->input->post("submit") && $this->input->post("submit") != "") {
            
            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("type", "File Type", "trim|required");   
            
            // Setting validation according to file type selected
            // Image
            if($this->input->post("type") == 1) {
                
                if(empty($_FILES['image_url']['name'])) {
                    $this->form_validation->set_rules("image_url", "Select Image", "trim|required");
                }
            }
            // Video(Youtube Url)
            else if($this->input->post("type") == 2) {
                
                $this->form_validation->set_rules("youtube_url", "Video(Youtube Url)", "trim|required");
            }
            // Video File
            else if($this->input->post("type") == 3) {
                
                if(empty($_FILES['video_file_url']['name'])) {
                    $this->form_validation->set_rules("video_file_url", "Select Video File", "trim|required");
                }
                if(empty($_FILES['preview_image_url']['name'])) {
                    $this->form_validation->set_rules("preview_image_url", "Select Preview Image", "trim|required");
                }
            }

            if ($this->form_validation->run() == false) { 

                $this->session->set_flashdata("e_message", validation_errors());
                
            } else {  

                // Add new file data                
                $insert_data = array(                    
                    "title" => $this->input->post("title"),  
                    "details" => $this->input->post("details"),                    
                    "type" => $this->input->post("type")                    
                );
                
                // If any album is selected
                if($this->input->post('album_id')) {
                    $insert_data['album_id'] = $this->ablfunctions->ablDecrypt($this->input->post('album_id'));
                }
                
                $file_id = $this->Gallery_model->insert_file_data($insert_data);
                
                if ($file_id) { 
                    
                    $file_url = "";
                    $preview_image_url = "";
                    $file_upload_status = "false";
                    $file_upload_message = "";                    
                    
                    // Upload files according to the file type selected
                    
                    ////////////////////////////////////////////////////////////
                    // Image
                    ////////////////////////////////////////////////////////////
                    if($this->input->post("type") == 1) {

                        if (isset($_FILES['image_url'])) { 
                        
                            if ($_FILES['image_url']['name'] != "") {

                                $file_name = $file_id.'_'.time().'_'.basename($_FILES['image_url']['name']);

                                $config = array();
                                $config['upload_path']          = UPLOAD_DIR.GALLERY_DIR;
                                $config['allowed_types']        = ALLOWED_IMAGE_TYPES;
                                $config['file_name']            = $file_name;
                                $config['max_size']             = ALLOWED_IMAGE_SIZE;

                                $this->load->library('upload');      
                                $this->upload->initialize($config);                            

                                if (!$this->upload->do_upload('image_url')) {

                                    $error = $this->upload->display_errors();   
                                    $file_upload_message = "Failed to add the file! ".$error;

                                } else {                                

                                    // Upload thumb image                                
                                    $source = UPLOAD_DIR.GALLERY_DIR.$file_name;
                                    $destination = UPLOAD_DIR.GALLERY_THUMB_DIR.$file_name;

                                    $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                    $file_url = $file_name;    
                                    $file_upload_status = "true";
                                    $file_upload_message = "You have successfully added a new file.";
                                }
                            }
                        }     
                    }
                    ////////////////////////////////////////////////////////////
                    // Video(Youtube Url)
                    ////////////////////////////////////////////////////////////
                    else if($this->input->post("type") == 2) {

                        // Parse the youtube url for checking its validity and further operations
                        $youtube_url = $this->input->post('youtube_url');
                        $url_parsed_arr = parse_url($youtube_url);
                        
                        if ($url_parsed_arr['host'] == "www.youtube.com" && $url_parsed_arr['path'] == "/watch" && substr($url_parsed_arr['query'], 0, 2) == "v=" && substr($url_parsed_arr['query'], 2) != "") {
                            
                            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $youtube_url, $match);
                            $youtube_id = $match[1];
                            
                            $file_url = $youtube_url;    
                            $file_upload_status = "true";
                            $file_upload_message = "You have successfully added a new file. ";
                            
                            // Upload preview image if there is any                                                        
                            if (isset($_FILES['preview_image_url'])) { 
                        
                                if ($_FILES['preview_image_url']['name'] != "") {

                                    $file_name = $file_id.'_preview_'.basename($_FILES['preview_image_url']['name']);

                                    $config = array();
                                    $config['upload_path']          = UPLOAD_DIR.GALLERY_DIR;
                                    $config['allowed_types']        = ALLOWED_IMAGE_TYPES;
                                    $config['file_name']            = $file_name;
                                    $config['max_size']             = ALLOWED_IMAGE_SIZE;

                                    $this->load->library('upload');      
                                    $this->upload->initialize($config);                            

                                    if (!$this->upload->do_upload('preview_image_url')) {

                                        $error = $this->upload->display_errors();        
                                        $file_upload_message .= "Failed to upload the preview image! ".$error;

                                    } else {                                

                                        // Upload thumb image                                
                                        $source = UPLOAD_DIR.GALLERY_DIR.$file_name;
                                        $destination = UPLOAD_DIR.GALLERY_THUMB_DIR.$file_name;

                                        $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                        $preview_image_url = $file_name;                                        
                                        $file_upload_message .= "Preview image uploaded successfully.";
                                    }
                                }
                            }  
                            
                            // If no preview image uploaded, set youtube image url as preview image 
                            if($preview_image_url == '') {                                
                                $preview_image_url = 'https://img.youtube.com/vi/'.$youtube_id.'/hqdefault.jpg';
                            }                                                        
                        } else {
                            
                            $file_upload_message = "Failed to add the file! Please provide a valid youtube url.";
                        }
                    }
                    ////////////////////////////////////////////////////////////
                    // Video File
                    ////////////////////////////////////////////////////////////
                    else if($this->input->post("type") == 3) {

                        if (isset($_FILES['video_file_url'])) { 
                        
                            if ($_FILES['video_file_url']['name'] != "") {

                                $file_name = $file_id.'_'.time().'_'.basename($_FILES['video_file_url']['name']);

                                $config = array();
                                $config['upload_path']          = UPLOAD_DIR.GALLERY_DIR;
                                $config['allowed_types']        = ALLOWED_VIDEO_TYPES;
                                $config['file_name']            = $file_name;
                                $config['max_size']             = ALLOWED_VIDEO_SIZE;

                                $this->load->library('upload');      
                                $this->upload->initialize($config);

                                if (!$this->upload->do_upload('video_file_url')) {

                                    $error = $this->upload->display_errors();
                                    $file_upload_message = "Failed to add the file! ".$error;
                                    
                                } else {                                
                                    
                                    $file_url = $file_name;    
                                    $file_upload_status = "true";
                                    $file_upload_message = "You have successfully added a new file. ";
                                    
                                    // Upload preview image if there is any                                    
                                    if (isset($_FILES['preview_image_url'])) { 
                        
                                        if ($_FILES['preview_image_url']['name'] != "") {

                                            $file_name = $file_id.'_preview_'.basename($_FILES['preview_image_url']['name']);

                                            $config = array();
                                            $config['upload_path']          = UPLOAD_DIR.GALLERY_DIR;
                                            $config['allowed_types']        = ALLOWED_IMAGE_TYPES;
                                            $config['file_name']            = $file_name;
                                            $config['max_size']             = ALLOWED_IMAGE_SIZE;

                                            $this->load->library('upload');      
                                            $this->upload->initialize($config);

                                            if (!$this->upload->do_upload('preview_image_url')) {

                                                $error = $this->upload->display_errors();        
                                                $file_upload_message .= "Failed to upload the preview image! ".$error;

                                            } else {                                

                                                // Upload thumb image                                
                                                $source = UPLOAD_DIR.GALLERY_DIR.$file_name;
                                                $destination = UPLOAD_DIR.GALLERY_THUMB_DIR.$file_name;

                                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                                $preview_image_url = $file_name;                                        
                                                $file_upload_message .= "Preview image uploaded successfully.";
                                            }
                                        }
                                    }                                                                         
                                }
                            }
                        }   
                    }  
                    
                    // Update database according to the file upload status
                    if($file_upload_status == "true") {
                        
                        // Update database with the file url, preview image url
                        $data = array(
                            "file_url" => $file_url,
                            "preview_image_url" => $preview_image_url
                        );

                        $table = "tbl_gallery_files";

                        $where = array(
                            "id" => $file_id
                        );
                        $this->ablfunctions->updateData($data, $table, $where);
                        
                        $this->session->set_flashdata("s_message", $file_upload_message);
                        
                    } else {
                    
                        // Delete file data from database
                        $this->ablfunctions->deleteData("tbl_gallery_files", array("id" => $file_id));
                        
                        if($file_upload_message == "") {
                            $file_upload_message = "Failed to add the file!";
                        }
                        $this->session->set_flashdata("e_message", $file_upload_message);
                    }
                } else {
                    
                    $this->session->set_flashdata("e_message", "Failed to add the file!");
                }                                                                                                                                               
            }
        } else {
            
            $data['albums'] = $this->Gallery_model->get_all_albums();  
            
            foreach ($data['albums'] as $key => $album) {                           
            
                $data['albums'][$key]['encrypted_id'] = $this->ablfunctions->ablEncrypt($album['id']);
            }
            
            $data['file_types'] = $this->config->item('gallery_file_types');
            
            $this->load->view("admin/gallery/add_file", $data);
        }
    }
    
    /*
    | --------------------------------------------------------------------------
    | Manage files 
    | --------------------------------------------------------------------------
    */
    function manageFiles() {
        
        // Get the data        
        $data['files'] = $this->Gallery_model->get_all_files();        
        
        foreach ($data['files'] as $key => $file) {                           
            
            $data['files'][$key]['encrypted_id'] = $this->ablfunctions->ablEncrypt($file['id']);
        }

        $this->load->view("admin/gallery/manage_files", $data);
    }
    
    
    
    
    
    
    
   

    
    
    /*
    | --------------------------------------------------------------------------
    | Edit admin
    | --------------------------------------------------------------------------    
    */
    function editAdmin() {
        
        // Super admin can edit other admins
        if ($this->session->userdata('admin_type') == ADMIN_TYPE_SUPER_ADMIN) {
            
            if ($this->input->post("submit") && $this->input->post("submit") != "") {
                
                $encrypted_id = $this->input->post('admin_id');
                $admin_id = $this->ablfunctions->ablDecrypt($encrypted_id);

                // CI form validation
                $this->load->library("form_validation");
                
                $this->form_validation->set_rules("name", "Name", "trim|required");
                $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
                $this->form_validation->set_rules("phone", "Phone Number", "trim|required|integer");
                $this->form_validation->set_rules("password", "Password", "trim|min_length[6]|max_length[32]");
                $this->form_validation->set_rules("confirm_password", "Confirm Password", "trim|min_length[6]|max_length[32]|matches[password]");

                if ($this->form_validation->run() == false) { 
                    
                    $this->session->set_flashdata("e_message", validation_errors());
                    redirect("admin/user/editAdmin/" . $encrypted_id);
                } else { 
                    
                    // Update admin data
                    $update_data = array(
                        "type" => $this->input->post("type"),
                        "name" => $this->input->post("name"),
                        "email" => $this->input->post("email"),
                        "phone" => $this->input->post("phone"),
                        "status" => $this->input->post("status"),                        
                    );

                    // Update password if password provided only
                    if($this->input->post("password") AND $this->input->post("password") != "") {

                        $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);
                        $update_data["password"] = $password;
                    }
                    
                    $update = $this->User_model->update_other_admin_data($admin_id, $update_data);
                                        
                    if ($update) {
                        
                        $this->session->set_flashdata("s_message", "Admin record updated successfully.");
                    } else {
                        
                        $this->session->set_flashdata('e_message', "Failed to update the admin record.");
                    }

                    redirect("admin/user/editAdmin/" . $encrypted_id);
                }
            } else {
                
                $encrypted_id = $this->uri->segment(4);
                $admin_id = $this->ablfunctions->ablDecrypt($encrypted_id);
                
                $data['admin_details'] = $this->ablfunctions->getDetailsFromId($admin_id, "tbl_admins");
                $data['admin_details']['encrypted_id'] = $encrypted_id;
                
                $this->load->view("admin/edit_admin", $data);
            }
        } else {
            
            $this->session->set_flashdata('e_message', "Sorry, you are not authorised to edit an admin.");
            redirect("admin/user/dashboard");
        }
    }
    
    /*
    | --------------------------------------------------------------------------
    | Manage admins
    | --------------------------------------------------------------------------  
    */
    function manageAdmins() {

        // Super admin can manage other admins
        if ($this->session->userdata('admin_type') == ADMIN_TYPE_SUPER_ADMIN) {

            // All the admins except the logged in admin
            $data['admins'] = $this->User_model->get_all_admins();

            foreach ($data['admins'] as $key => $admin) {
               
                if ($admin['status'] == 1) {
                    
                    $data['admins'][$key]['status_text'] = "<span style='color:green;'>Active</span>";
                } else if ($admin['status'] == 0) {
                    
                    $data['admins'][$key]['status_text'] = "<span style='color:red;'>Inactive</span>";
                }

                if ($admin['type'] == ADMIN_TYPE_SUPER_ADMIN) {
                    
                    $data['admins'][$key]['type_text'] = "<span style='color:crimson;'>Super admin</span>";                    
                } else if ($admin['type'] == ADMIN_TYPE_ADMIN) {
                    
                    $data['admins'][$key]['type_text'] = "<span style='color:chocolate;'>Admin</span>";
                }           
                
                $data['admins'][$key]['encrypted_id'] = $this->ablfunctions->ablEncrypt($admin['id']);
            }

            $this->load->view("admin/manage_admins", $data);
        } else {

            $this->session->set_flashdata('e_message', "Sorry, you are not authorised to manage admins.");
            redirect("admin/user/dashboard");
        }
    }   
    
                       
    /*
    | --------------------------------------------------------------------------
    | Delete admin
    | -------------------------------------------------------------------------- 
    */
    function deleteAdmin() {

        $encrypted_id = $this->uri->segment(4);
        $admin_id = $this->ablfunctions->ablDecrypt($encrypted_id);
        
        // Delete admin data
        $this->ablfunctions->deleteData("tbl_admins", array("id" => $admin_id));
        
        $this->session->set_flashdata('s_message', "Admin has been deleted successfully.");
        redirect("admin/user/manageAdmins");
    }          
}
