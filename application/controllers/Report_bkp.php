<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->checkUserSecurity();
        //$this->my_custom_functions->checkUserVerificationStatus();
        $this->load->model("User_model");
        $this->load->model("Main_model");

    }



    ////////////////////////////////////////////////////////////////////////////
    // Download Admit Card
    ////////////////////////////////////////////////////////////////////////////
    function downloadAdmissionForm() {

               // Get the data
        $encrypted_id = $this->uri->segment(3);
        $student_id = $this->ablfunctions->ablDecrypt($encrypted_id);
        $from_id = $this->my_custom_functions->get_particular_field_value("tbl_student_from","id", 'and student_id="'.$student_id.'"');
        $card_details = $this->ablfunctions->getDetailsFromId($from_id, "tbl_student_from");
        $principal = $this->my_custom_functions->get_multiple_data("tbl_staff",'and designation="1" LIMIT 1');
        $guardian_sign=base_url().UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$this->my_custom_functions->get_particular_field_value("tbl_student_from_document","guardian_sign", 'and student_id="'.$card_details['id'].'"');
        $student_sign=base_url().UPLOAD_DIR.STUDENT_SIGN.$this->my_custom_functions->get_particular_field_value("tbl_student_from_document","student_sign", 'and student_id="'.$card_details['id'].'"');
        $student_sign_full=base_url().UPLOAD_DIR.STUDENT_SIGN_FULL.$this->my_custom_functions->get_particular_field_value("tbl_student_from_document","student_sign_full", 'and student_id="'.$card_details['id'].'"');
        $student_passport=base_url().UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$this->my_custom_functions->get_particular_field_value("tbl_student_from_document","passport", 'and student_id="'.$card_details['id'].'"');
        $prin_photo=base_url().UPLOAD_DIR.STAFF_SIGN.$principal[0]['id'].".jpg";
        $college_logo=base_url()."images/college/logo.jpg";
        $college_logo_new=base_url()."images/college/unnamed.jpg";
        $sessin_name = $this->my_custom_functions->get_particular_field_value("tbl_session","session_name", 'and id="'.$card_details['session_id'].'"');
        $course_name = $this->my_custom_functions->get_particular_field_value("tbl_courses","course_name", 'and id="'.$card_details['course_id'].'"');
        if ($card_details['gender']==1) {
          $gender = "Male";
        }else if ($card_details['gender']==2) {
          $gender = "Female";
        }else if ($card_details['gender']==3) {
          $gender = "Others";
        }
        if ($card_details['marital_status']==1) {
          $marital_statuser = "Married";
        }else if ($card_details['marital_status']==2) {
          $marital_statuser = "Single";
        }else if ($card_details['marital_status']==3) {
          $marital_statuser = "Divorced";
        }
        if ($card_details['caste']==1) {
          $caste = "General";
        }else if ($card_details['caste']==2) {
          $caste = "SC";
        }else if ($card_details['caste']==3) {
          $caste = "ST";
        }else if ($card_details['caste']==4) {
          $caste = "OBC";
        }else if ($card_details['caste']==5) {
          $caste = "Physically Handicapped";
        }



        //$this->load->view("admin/college/admit_card_view", $data);


            include(APPPATH . "libraries/mpdf/mpdf.php");


            $mpdf = new mPDF('utf-8', 'A4', '', '', '15', '15', '15', '18', '15', '15');

            $mpdf->SetTitle('Student Admission Form');

            $mpdf->SetDisplayMode('fullpage');


            $html =    '<!DOCTYPE html>
                        <html lang="en">

                        <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta content="width=device-width, initial-scale=1.0" name="viewport">
                        <meta content="" name="keywords">
                        <meta content="" name="description">

                        </head>

                        <body>';


                    $html .=   '<section id="admit-card" style=" width: 100%;height: auto;padding: 0px 0 0px 0;">

                <div class="container" style="width:100%;">
                <form>
                 <div class="add_content" style="width: 100%; height: auto; float: left;padding-bottom: 20px;">

                <div class="row admision_cnt" style=" float: left;width: 80%;">

                    <div class="" style="float: left;font-size:13px;">
                        <label for="fname">Form No:&nbsp;&nbsp;'.$card_details['from_id'].'&nbsp;&nbsp;</label>



                   </div>
                </div>
                <div class="row admision_box" style="float: right; width: 20%;">


                    <div class="" style="font-size:13px;">
                        <label for="fname">Session:&nbsp;&nbsp;'.$sessin_name.'</label>

                        <!--<input type="text" id="fname" name="firstname" placeholder=""
                            style=" border: none; border-bottom: 1px dashed;outline: none;">-->
                    </div>


                </div>

            </div>

            <div class="admit_header" style="width: 100%; height: auto; float: left; text-align: center;">
                <div class="admit_header_content" style="width: 25%;height: auto; float: left;">
                 <div class="admit_image">
                    <img src="'.$college_logo.'" style="  width: 120px;
            height: 120px;">

            </div>
                </div>
                <div class="admit_header_content " style="width: 50%;height: auto; float: left; text-align: center;margin-top:0px;">
                    <h2 style="margin:0;">College of Art & Design</h2>
                    <p style="font-weight: 500;
          width: 100%;
          margin-left: 0%;margin:5px;">(Affiliated to the University of Burdwan)<br>
                        2f, 12(B) approved by the UGC Act 1956
                    </p>
                    <span style="  font-weight: bold;">ADMISSION FORM</span>

                </div>
                <div class="admit_header_content" style="width: 25%;height: auto; float: right;">
                    <div class="admit_image">
                        <img src="'.$student_passport.'" style="  width: 100px;
            height: 100px;">

                    </div>
                </div>

            </div>







 <div class="admit_header_content "
                style="width: 100%;height: auto; float: left; text-align: center;padding-top:20px; padding-bottom:20px;font-weight: bold;font-size:14px;">

                <div class="" style="width: 30%;
                    text-align: left;float: left;">
                    Course applied for –
                </div>
                <div style="width: 55%; float: left;text-align: left;">
                    <label for="vehicle1" style=" margin-top: -5px;
        padding: 0px 6px;width: 80%;float: left; font-size:12px;">'.$course_name.'</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                </div>
            </div>

            <div style="width: 55%;">
            <span style="font-weight:bold;font-size:12px;">To</span><br>
            <span style="font-weight:bold;font-size:12px;">The Principal / Teacher-in-Charge</span><br>
            <span style="font-size:12px;">College of Art & Design<br>
            Amar Mouza, Nawabhat, Suri Road<br>
            P.O. - Fagupur, Dist.- Purba Barddhaman<br>
            PIN – 713104, West Bengal.</span>

            <p style="font-style: italic;font-size:12px;">Contact No.(Phone): +91 9434667350 / 9434311911<br>
            E-mail: contactus@collegeofartanddesign.co.in<br>
            <span style="font-weight:bold;font-style: normal;">Website:</span> www.collegeofartanddesign.co.in</p><br>
            </div>

            <div style="width: 100%;height: auto; float: left; text-align: left;font-weight: 400;">

                    <p style="margin:0; font-size:12px;">Sir,<br> I beg to present myself for admission to the College of Art & Design , Burdwan. I undertake that I shall
                    abide by all the rules and regulations of the College and shall be responsible for the good conduct,
                    attendance, punctual payment of College fees etc.
                    </p>
                    <p style="margin:0;margin-bottom:30px;font-size:12px;">I further vouch for the correctness of the following details regarding myself:
                    </p>

            </div>

             <div style="width: 100%;">

                <div class="row form-content" style="width: 100%; float: left; padding-bottom: 15px;">

                    <div class="content_list" style=" float: left; margin-bottom:10px;">
                        <label for="fname"><span style="padding-right: 15px;">A.</span> Name of the Candidate (in
                            capital letters):&nbsp;&nbsp;'.$card_details['student_name'].' </label>
                    </div>
                    <div class="" style="width: 100%; float: left;">
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
          border-bottom: 1px dashed;
          width: 100%;outline: none;">-->
                    </div>
                </div>

                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

                    <div class="content_list" style=" float: left;margin-bottom:10px;">
                        <label for="fname"><span style="padding-right: 15px;">B.</span> Father’s /Mother’s Name : &nbsp;&nbsp;'.$card_details['father_or_mother_name'].'
                        </label>
                    </div>
                    <div class="" style="width: 100%; float: left;">
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
          border-bottom: 1px dashed;
          width: 100%;outline: none;">-->
                    </div>
                </div>

                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

                    <div class="content_list" style=" float: left;margin-bottom:10px;">
                        <label for="fname"><span style="padding-right: 15px;">C.</span> Name of the Guardian: &nbsp;&nbsp;'.$card_details['guardian_name'].'
                        </label></label>
                    </div>
                    <div class="" style="width: 100%; float: left;">
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
          border-bottom: 1px dashed;
          width: 100%;outline: none;">-->
                    </div>
                </div>



                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

                    <div class="content_list" style=" float: left;margin-bottom:10px;">
                        <label for="fname"><span style="padding-right: 15px;">D.</span> i) Permanent Address: &nbsp;&nbsp;'.$card_details['perm_address'].'</label>
                    </div>
                    <div class="" style="width: 100%; float: left;">
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
          border-bottom: 1px dashed;
          width: 100%;outline: none;">-->
                    </div>
                </div>



                <div class="row form-content " style="width: 100%; float: left;padding-bottom: 15px;">



                    <div class="" style="width: 100%;
                float: left;margin-bottom:10px;">
                        <div class="admit_time">
                            e-mail:&nbsp;&nbsp;'.$card_details['email'].'&nbsp;&nbsp;&nbsp;&nbsp;
                            <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
            border-bottom: 1px dashed;
            width: 40%;outline: none;">--> Phone No: &nbsp;&nbsp;'.$card_details['phone_no'].'&nbsp;&nbsp;&nbsp;&nbsp;<!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
              border-bottom: 1px dashed;
              width: 30%;outline: none;">-->
                        </div>
                    </div>

                </div>


                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

                    <div class="content_list" style=" float: left;padding-left: 20px;margin-bottom:10px;">
                        <label for="fname">ii) Address for correspondence: &nbsp;&nbsp;'.$card_details['corres_address'].'&nbsp;&nbsp;</label>
                    </div>
                    <div class="" style="width: 100%; float: left;">
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
          border-bottom: 1px dashed;
          width: 100%;outline: none;">-->
                    </div>
                </div>


                <div class="row form-content " style="width: 100%; float: left;padding-bottom: 15px;">



                    <div class="" style="width: 100%;
                float: left;
                padding-left: 20px;margin-bottom:10px;">
                        <div class="admit_time">
                            iii) Aadhar No:&nbsp;&nbsp;'.$card_details['aadhar_no'].'&nbsp;&nbsp;&nbsp;&nbsp;
                            <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
            border-bottom: 1px dashed;
            width: 25%;outline: none;">--> Registered Mobile No:&nbsp;&nbsp;'.$card_details['reg_mobile_no'].'&nbsp;&nbsp;&nbsp;&nbsp; <!--<input type="text" id="fname" name="firstname"
                                placeholder="" style=" border: none;
              border-bottom: 1px dashed;
              width: 25%;outline: none;">-->
                        </div>
                    </div>

                </div>


                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

                    <div class="content_list" style=" float: left;margin-bottom:10px;">
                        <label for="fname"><span style="padding-right: 15px;">E.</span> Annual income of the Guardian /
                            Parents alongwith signature:&nbsp;&nbsp;'.$card_details['gud_annual_income'].'&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    </div>
                    <div class="" style="width: 100%; float: left;">
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
          border-bottom: 1px dashed;
          width: 100%;outline: none;">-->
                    </div>
                </div>



                <div class="row form-content " style="width: 100%; float: left;padding-bottom: 15px;">



                    <div class="" style="width: 100%; float: left;">

                        <span style="padding-right: 15px;">F.</span> Date of birth:&nbsp;&nbsp;'.$card_details['dob'].'&nbsp;&nbsp;&nbsp;&nbsp;
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
            border-bottom: 1px dashed;
            width: 30%;outline: none;">-->Age (actual):&nbsp;&nbsp;'.$card_details['age'].'&nbsp;&nbsp;&nbsp;&nbsp; <!--<input type="text" id="fname" name="firstname" placeholder=""
                            style=" border: none;
              border-bottom: 1px dashed;
              width: 30%;outline: none;">-->

                    </div>
                             <div style="margin-top:20px; font-weight:bold; weight:100%;float:right;text-align:right;"></div>
                </div>

                <div class="row form-content " style="width: 100%; float: left;padding-bottom: 15px;">



                    <div class="" style="width: 100%; float: left;">

                        <span style="padding-right: 15px;">G.</span> Sex:&nbsp;&nbsp;'.$gender.'&nbsp;&nbsp;&nbsp;&nbsp;
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
            border-bottom: 1px dashed;
            width: 33%;outline: none;">-->Marital Status:&nbsp;&nbsp;'.$marital_statuser.'&nbsp;&nbsp; <!--<input type="text" id="fname" name="firstname" placeholder=""
                            style=" border: none;
              border-bottom: 1px dashed;
              width:35%;outline: none;">-->

                    </div>

                </div>



                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

                    <div class="content_list" style=" float: left;margin-bottom:10px;">
                        <label for="fname"><span style="padding-right: 15px;">H.</span> Nationality:&nbsp;&nbsp; '.$card_details['nationality'].'&nbsp;&nbsp;</label>
                    </div>
                    <div class="" style="width: 100%; float: left;">
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
          border-bottom: 1px dashed;
          width: 100%;outline: none;">-->
                    </div>
                </div>

                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

                    <div class="content_list" style=" float: left;margin-bottom:10px;">
                        <label for="fname"><span style="padding-right: 15px;">I.</span> Whether SC/ST/OBC/Physically
                            Handicapped: &nbsp;&nbsp;'.$caste.'&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    </div>
                    <div class="" style="width: 100%; float: left;">
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
          border-bottom: 1px dashed;
          width: 100%;outline: none;">-->
                    </div>
                </div>





                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

                    <div class="content_list" style=" float: left;padding-bottom: 20px;margin-bottom:5px;">
                        <label for="fname"><span style="padding-right: 15px;">J.</span> Educational qualification :
                        </label>
                    </div>
                    <div class="" style="width: 100%; float: left;">
                        <table style="width:100%; border: 1px solid black;
                        border-collapse: collapse;">
                            <!-- <caption>Monthly savings</caption> -->
                            <tr>
                                <td style=" border: 1px solid black;
                              border-collapse: collapse; text-align: center; padding: 15px;">Examination passed</th>
                                <td style=" border: 1px solid black;
                              border-collapse: collapse;text-align: center; padding: 15px;">Subject</th>
                                <td style=" border: 1px solid black;
                              border-collapse: collapse;text-align: center; padding: 15px;">Year of passing</th>
                                <td style=" border: 1px solid black;
                              border-collapse: collapse;text-align: center; padding: 15px;">Board/Council/ University
                                </th>
                                <td style=" border: 1px solid black;
                              border-collapse: collapse;text-align: center; padding: 15px;">Division obtained</th>
                            </tr>';

          $qualification = $this->my_custom_functions->get_multiple_data("tbl_edu_qualification",'and student_from_id="'.$card_details['id'].'"');
          foreach($qualification as $edudet) {
                          $html .='<tr>
                                <td style=" border: 1px solid black;
                              border-collapse: collapse;text-align: left; padding: 15px;height:50px;">'.$edudet['exam_passed'].'</td>
                                <td style=" border: 1px solid black;
                              border-collapse: collapse;text-align: left; padding: 15px;height:50px;">'.$edudet['subject'].'</td>
                                <td style=" border: 1px solid black;
                              border-collapse: collapse;text-align: left; padding: 15px;height:50px;">'.$edudet['year_of_pass'].'</td>
                                <td style=" border: 1px solid black;
                              border-collapse: collapse;text-align: left; padding: 15px;height:50px;">'.$edudet['board_coun_uni'].'</td>
                                <td style=" border: 1px solid black;
                              border-collapse: collapse;text-align: left; padding: 15px;height:50px;">'.$edudet['div_obt'].'</td>

                            </tr>';
                          }
                      $html .='
                        </table>
                        <p style="font-size:12px;">Attested Xerox copies of mark sheets, age verification, and SC/ST/OBC/Handicapped
                            Certificate, if any, must be enclosed with the application form.</p>
                    </div>
                </div>





                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

                    <div class="content_list" style=" float: left;margin-bottom:10px;">
                        <label for="fname"><span style="padding-right: 15px;">K.</span> Local address of the
                            candidate:&nbsp;&nbsp;&nbsp; '.$card_details['local_address'].'&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    </div>
                    <div class="" style="width: 100%; float: left;">
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
          border-bottom: 1px dashed;
          width: 100%;outline: none;">-->
                    </div>
                </div>




                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

                    <div class="content_list" style=" float: left;">
                        <label for="fname"><span style="padding-right: 15px;">L.</span> Name & Address of the local
                            guardian:&nbsp;&nbsp;'.$card_details['local_gu_name_adds'].'&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    </div>
                    <div class="" style="width: 100%; float: left;">
                        <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
          border-bottom: 1px dashed;
          width: 100%;outline: none;">-->
                    </div>
                </div>


                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 10px;">
                    <p style="font-weight: bold;font-size:13px;"> I / We aware the money once deposited in any head or as fees in
                        College account will not return back at any stage. I /We shall abide by all the rules and
                        regulations of the College before submitting the Application Form.
                    </p>

                </div>







                <div class="admit_signature" style=" width: 100%;
      height: auto;
      float: left;
      padding-top: 20px;">
                    <div class="signature_lft" style="float: left;width:70%;">

                        <div class="row form-content ">

                        <img src="'.$guardian_sign.'" style="  width: 180px;
                       height: 60px;">
                            <div class="">
                                <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
              border-bottom: 1px dashed;
              width: 40%;outline: none;">-->..........................................................
                            </div>
                            <div class="">
                                <label for="fname" style=" font-weight: 400;text-align: center;"> Countersignature of the Guardian</label>
                            </div>
                        </div>

                    </div>
                    <!--signtaure_lft-->

                    <div class="signtaure_rgt" style="float: right;width:30%;">

                        <div class="row form-content">
                        <img src="'.$student_sign.'" style="  width: 180px;
                       height: 60px;">

                            <div class="">
                                <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
              border-bottom: 1px dashed;
              width: 100%;outline: none;">-->...............................................
                            </div>
                            <div style="text-align: center;">
                                <label for="fname" style=" font-weight: 400;"> Signature of Candidate</label>
                            </div>
                        </div>

                    </div>
                    <!--signtaure_rgt-->

                </div>




                <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px; margin-top: 30px;">

                    <h3 style="font-weight: bold; text-align: center;"> DECLARATION OF THE CANDIDATE
                        </h3>
                        <p style="font-weight: 400;text-align: left;font-size:13px;">I do hereby agree (if admitted) to abide by the rules and
                            regulations of the College as are in force and undertake that so long I shall remain a
                            student of this College, I will do nothing either inside or outside the College that will
                            interfere its orderliness and discipline. Any infringement of the aforesaid conditions by me
                            will entail to accept any punishment prescribed by the authority.
                        </p>

                </div>



                <div class="admit_signature" style=" width: 100%;
      height: auto;
      float: left;
      padding-top: 20px;">
                     <div class="signature_lft" style="float: left; width:65%">

                        <div class="row form-content ">


                            <div class="" style="
                            padding-top: 60px;">
                                Date :&nbsp;&nbsp;'.$card_details['date'].'
                            </div>

                        </div>

                    </div>
                    <!--signtaure_lft-->

                    <div class="signtaure_rgt" style="float: right;width:35%;">

                        <div class="row form-content">

                        <img src="'.$student_sign_full.'" style="  width: 180px;
                       height: 60px;">
                            <div class="">
                                <!--<input type="text" id="fname" name="firstname" placeholder="" style=" border: none;
              border-bottom: 1px dashed;
              width: 100%;outline: none;">-->.......................................................
                            </div>
                            <div style="text-align: center;">
                                <label for="fname" style=" font-weight: 400;"> Signature of Candidate in full</label>
                            </div>
                        </div>

                    </div>
                    <!--signtaure_rgt-->

                </div>




            </div> ';



            $html .=   '</div>

                        </form>
                        </div>

                        </section>

                        </body>

                        </html>';

            $mpdf->WriteHTML($html);

            $file_name = 'temp/Admission-Form.pdf';
            $mpdf->Output($file_name, 'F');

            $file_url = base_url() . $file_name;
            header("Location: $file_url");

            exit();

         }

          // <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike"
                    //     style="width: 10%;float: right;outline: none;">
        //             <label for="vehicle2" style=" margin-top: -5px;
        // padding: 0px 6px;width: 90%;float: left;font-size:12px;"> Bachelor of Fine Art (BFA) [4 Years / 8 SEM] </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        //             <input type="checkbox" id="vehicle2" name="vehicle2" value="Car"
        //                 style="width: 10%;float: right; outline: none;">
        //             <label for="vehicle3" style=" margin-top: -5px;
        // padding: 0px 6px;width: 80%;float: left;font-size:12px;">Diploma in Visual Art (Craft & Design) [2 Years] </label>
        //             <input type="checkbox" id="vehicle3" name="vehicle3" value="Boat"
        //
        //               style="width: 10%;float: right; outline: none;">




        // <tr>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //       </tr>
        //       <tr>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //       </tr>
        //       <tr>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //       </tr>

}
