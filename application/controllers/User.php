<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->my_custom_functions->checkUserSecurity();
        //$this->my_custom_functions->checkUserVerificationStatus();
        $this->load->model("User_model");
        $this->load->model("Main_model");

    }

    /*
    | --------------------------------------------------------------------------
    | User dashboard
    | --------------------------------------------------------------------------
    */
    public function dashboard() {

        $data['title'] = "Welcome Student!";
        $this->load->view("dashboard",$data);
    }

    /*
    | -------------------------------------------------------------------
    | Edit profile
    | -------------------------------------------------------------------
    */
    function editProfile() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("name", "Name", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            $this->form_validation->set_rules("phone", "Phone Number", "trim|required|integer");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("user/editProfile");
            } else {

                // Update profile data
                $update_data = array(
                    "name" => $this->input->post("name"),
                    "email" => $this->input->post("email"),
                    "phone" => $this->input->post("phone")
                );

                $condition = array('id' => $this->session->userdata('user_id'));

                $update = $this->my_custom_functions->update_data($update_data,'tbl_student', $condition);
                //$update = $this->User_model->update_profile_data($this->session->userdata('user_id'), $update_data);

                if ($update) {

                    $this->session->set_flashdata('s_message', "Successfully updated profile details.");
                    redirect("user/editProfile");
                } else {

                    $this->session->set_flashdata('e_message', "Failed to update profile details.");
                    redirect("user/editProfile");
                }
            }
        } else {

            $data['user_details'] = $this->ablfunctions->getDetailsFromId($this->session->userdata('user_id'), "tbl_student");

            $this->load->view("edit_profile", $data);
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Change password
    | --------------------------------------------------------------------------
    */
    public function changePassword() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("old_password", "Old Password", "trim|required");
            $this->form_validation->set_rules("new_password", "New Password", "trim|required|min_length[6]|max_length[32]");
            $this->form_validation->set_rules("confirm_new_password", "Confirm New Password", "trim|required|min_length[6]|max_length[32]|matches[new_password]");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("user/changePassword");
            } else {

                // Check if given old password matches with existing password
                $old_password_given = $this->input->post('old_password');
                $old_password_existing = $this->ablfunctions->getParticularFieldValue("tbl_common_login", "password", " and id='" . $this->session->userdata('user_id') . "'");

                if (password_verify($old_password_given, $old_password_existing)) {

                    // Update password
                    $data = array(
                        "password" => password_hash($this->input->post("new_password"), PASSWORD_DEFAULT)
                    );

                    $table = "tbl_common_login";

                    $where = array(
                        "id" => $this->session->userdata('user_id')
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Password has been updated successfully.");
                    redirect("user/changePassword");
                } else {

                    $this->session->set_flashdata("e_message", "The given old password is incorrect.");
                    redirect("user/changePassword");
                }
            }
        } else {

            $this->load->view("change_password");
        }
    }

            /*
    | --------------------------------------------------------------------------
    | Manage Subjects
    | --------------------------------------------------------------------------
    */

    function checkAdmitCard() {
        $data['admit_card_type'] = $this->ablfunctions->getParticularFieldValue("tbl_student_from","admit_card_type", " and student_id='" . $this->session->userdata('user_id') . "'");
        if ($data['admit_card_type']==0) {
            $this->session->set_flashdata("e_message", "No Admit Card Issued.");
            $this->load->view("check_admit",$data);
        }else if($data['admit_card_type']==1) {
            $this->session->set_flashdata("e_message", "Admission Test Admit Card Issued.");
            $this->load->view("check_admit",$data);
        }else if($data['admit_card_type']==2) {
            $this->session->set_flashdata("e_message", "Direct Admission On Admit Card Issued.");
            $this->load->view("check_admit",$data);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Download Admit Card
    ////////////////////////////////////////////////////////////////////////////
    function downloadAdmitCard() {

        // Get the data
        $encrypted_id = $this->uri->segment(3);
        $from_id = $this->ablfunctions->ablDecrypt($encrypted_id);
        $card_details = $this->ablfunctions->getDetailsFromId($from_id, "tbl_student_from");
        $principal = $this->my_custom_functions->get_multiple_data("tbl_staff",'and designation="1" LIMIT 1');
        $student_sign=base_url().UPLOAD_DIR.STUDENT_SIGN_FULL.$card_details['id'].".jpg";
        $student_passport=base_url().UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$card_details['id'].".jpg";
        $prin_photo=base_url().UPLOAD_DIR.STAFF_SIGN.$principal[0]['id'].".jpg";
        $college_logo=base_url()."images/college/logo.jpg";

        if ($card_details['course_id']==4) {
            $bfa="checked";
        }else if ($card_details['course_id']==5) {
            $dva="checked";
        }else if ($card_details['course_id']==3) {
            $mfa="checked";
        }
        if ($card_details['admit_card_type']==1) {
            $admission_ttype="Admission test";
        }else if ($card_details['admit_card_type']==2) {
            $admission_ttype="Direct admission on";
        }

        //$this->load->view("admin/college/admit_card_view", $data);


            include(APPPATH . "libraries/mpdf/mpdf.php");


            $mpdf = new mPDF('utf-8', 'A4', '', '', '15', '15', '15', '18', '15', '15');

            $mpdf->SetTitle('Student Admit Card');

            $mpdf->SetDisplayMode('fullpage');


            $html =    '<!DOCTYPE html>
                        <html lang="en">

                        <head>
                        <meta charset="utf-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta content="width=device-width, initial-scale=1.0" name="viewport">
                        <meta content="" name="keywords">
                        <meta content="" name="description">

                        </head>

                        <body>';


                    $html .=   '<section id="admit-card" style=" width: 95%;height: auto;padding: 70px 0 70px 0;">

                <div class="container" style="width:80%; margin-left:10%;">
                <div class="add_content" style="width: 100%; height: auto; float: left;padding-bottom: 20px;">

                <div class="row admision_cnt" style=" float: left;width: 50%;">

                    <div class="" style="float: left;">
                        <label for="fname">Form No.</label>
                        <span style="border-bottom: 1px dashed;width: 60%;">
                         <label for="fname" style="width: 60%; float: right;">123456</label>
                    </span>
                    </div>

                </div>
                <div class="row admision_box" style="float: right;">




                <label for="vehicle1" style=" margin-top: -5px;
             padding: 0px 6px;"> MFA</label>
                <input type="checkbox" id="vehicle1" '.$mfa.'>
                <label for="vehicle2" style=" margin-top: -5px;
              padding: 0px 6px;"> BFA</label>
                <input type="checkbox" id="vehicle2" '.$bfa.'>
                <label for="vehicle3" style=" margin-top: -5px;
             padding: 0px 6px;"> DVA</label>
                <input type="checkbox" id="vehicle3" '.$dva.'>


                </div>

            </div>

            <div class="admit_header" style="width: 100%; height: auto; float: left; text-align: center;">
                <div class="admit_header_content" style="width: 25%;height: 150px; float: left;">
                    <img src="'.$college_logo.'">
                </div>
                <div class="admit_header_content " style="width: 50%;height: auto; float: left; text-align: center;">
                    <h3>College of Art & Design</h3>
                    <p style="font-weight: 500;
          width: 60%;
          margin-left: 20%;">(Affiliated to the University of Burdwan)
                        2f, 12(B) approved by the UGC Act 1956
                    </p>
                    <span style="  font-weight: 700;">ADMIT CARD</span>

                </div>
                <div class="admit_header_content" style="width: 25%;height: auto; float: left;">
                    <div class="admit_image">
                        <img src="'.$student_passport.'" style="  width: 180px;
            height: 180px;">
                    </div>
                </div>
            </div>




            <div class="row form-content" style="width: 100%; float: left; padding-bottom: 15px;">

                <div class="content_list" >
                    <label for="fname" style="width: 40%; float: left;">Name (In Capital letters)</label>:
                    <span style="border-bottom: 1px dashed;width: 60%;">
                    <label for="fname" style="width: 60%; float: right;">'.$card_details['student_name'].'</label>
                    </span>
                </div>

            </div>

            <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

              <div class="content_list" >
                    <label for="fname" style="width: 40%; float: left;">Address</label>:
                    <span style="border-bottom: 1px dashed;width:100px;">
                    <label for="fname" style="width: 60%; float: right;">'.$card_details['perm_address'].'</label>
                    </span>
                </div>
            </div>

            <div class="row form-content " style="width: 100%; float: left;padding-bottom: 15px;">


               <div class="content_list" >
                    <label for="fname" style="width: 30%; float: left;">'.$admission_ttype.' :</label>

                    <label for="fname" style="width: 5%; float: right;">at</label>
                    <span style="border-bottom: 1px dashed;width: 30%;">
                    <label for="fname" style="width: 30%; float: right;">'.$card_details['admit_card_issue_date'].'</label>
                     </span>

                </div>
            </div>


            <div class="admit_signature" style="width: 100%;
              height: auto;
              padding-top: 75px;">
                <div class="signature_lft" style="float: left;width: 50%;">

                    <div class="row form-content ">


                        <div class="" style=" ">
                            <img src="'.$student_sign.'" style="  width: 150px;
                           height: 60px; border: none;
                    border-bottom: 1px dashed;">

                        </div>
                        <div class="">
                            <label for="fname" style=" font-weight: 600;">Signature of Candidate</label>
                        </div>
                    </div>

                </div>




                <div class="signtaure_rgt" style="float: right;width: 50%;">

                    <div class="row form-content" style="
          text-align: right;
          margin-left: 20%;">

                        <div class="" style="">
                            <img src="'.$prin_photo.'" style="  width: 150px;
                           height: 60px; border: none;
              border-bottom: 1px dashed;">


                        <div class="">
                            <label for="fname" style=" text-align: center;
              ;font-weight: 600;"> Principal / Teacher-in-charge
                                College of Art & Design
                            </label>
                        </div>
                    </div>

                </div>';



            $html .=   '</div>

                        </form>
                        </div>

                        </section>

                        </body>

                        </html>';

            $mpdf->WriteHTML($html);

            $file_name = 'temp/Admit-Card' . $this->ablfunctions->ablEncrypt($from_id) . '.pdf';
            $mpdf->Output($file_name, 'F');

            $file_url = base_url() . $file_name;
            header("Location: $file_url");

            exit();

         }
         function manageNoticeView() {
         $data['page_title'] = 'Manage notice';
         $data['details'] = $this->my_custom_functions->get_multiple_data('tbl_notice', 'ORDER BY issue_date DESC');
         $this->load->view('manage_notice', $data);
     }
     function viewNotice() {

             $data['page_title'] = 'Edit notice';
             $notice_id = $this->ablfunctions->ablDecrypt($this->uri->segment(4));

             $data['notice_detail'] = $this->my_custom_functions->get_details_from_id($notice_id, TBL_NOTICE);
             $this->load->view('edit_notice', $data);

     }
     /*
 | --------------------------------------------------------------------------
 | Add Student From Register
 | --------------------------------------------------------------------------
 */
 function studentFromRegister() {

     if ($this->input->post("submit") && $this->input->post("submit") != "") {

         //echo "<pre>";print_r ($_POST);die;


         $this->form_validation->set_rules("course_id", "Select Course", "required");
         //$this->form_validation->set_rules("session_id", "Select session", "required");
         $this->form_validation->set_rules("student_name", "Name of the Candidate", "trim|required");
         $this->form_validation->set_rules("father_or_mother_name", "Father’s /Mother’s Name", "trim|required");
         $this->form_validation->set_rules("guardian_name", "Name of the Guardian", "trim|required");
         $this->form_validation->set_rules("perm_address", "Permanent Address", "trim|required");
         $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
         //$this->form_validation->set_rules("phone_number", "Phone Number", "trim|required|is_unique[tbl_student_from.phone_number]");
         $this->form_validation->set_rules("phone_no", "Phone Number", "trim|required|regex_match[/^[0-9]{10}$/]");
         $this->form_validation->set_rules("corres_address", "Address for correspondence", "trim|required");
         $this->form_validation->set_rules("aadhar_no", "Aadhar No", "trim|required|min_length[12]|max_length[12]");
         $this->form_validation->set_rules("reg_mobile_no", "Registered Mobile No", "trim|required|regex_match[/^[0-9]{10}$/]");
         $this->form_validation->set_rules("dob", "D.O.B", "trim|required");

         //$this->form_validation->set_rules("student_passport_photo", "Passport Photo", "required");
         $this->form_validation->set_rules("age", "Age", "trim|required");
         $this->form_validation->set_rules("gender", "Gender", "required");
         $this->form_validation->set_rules("marital_status", "Marital Status", "required");
         $this->form_validation->set_rules("nationality", "nationality", "required");
         $this->form_validation->set_rules("caste", "Caste", "required");
         $this->form_validation->set_rules("gud_annual_income", "Annual income of the Guardian", "required");

         $this->form_validation->set_rules("local_address", "Local address of the candidate", "trim|required");
         $this->form_validation->set_rules("local_gu_name_adds", "Name & Address of the local guardian", "trim|required");
         $this->form_validation->set_rules("rules_condition_one", "Condition", "required");
         $this->form_validation->set_rules("rules_condition_two", "Condition", "required");
         $this->form_validation->set_rules("rules_condition_first", "Condition", "required");

         if ($this->form_validation->run() == false) {

             $this->session->set_flashdata("e_message", validation_errors());
         // $data['session_details']= $this->Main_model->get_current_session();
         // $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_classes",'');
         // $this->load->view("student_from", $data);
         redirect("User/studentFromRegister");
         } else {

             if ($this->input->post('rules_condition_first')!='') {
                  $rules_condition_first=1;
               }else{ $rules_condition_one=0; }
             if ($this->input->post('rules_condition_one')!='') {
                  $rules_condition_one=1;
               }else{ $rules_condition_one=0; }
             if ($this->input->post('rules_condition_two')!='') {
                  $rules_condition_two=1;
               }else{ $rules_condition_two=0; }

             $insert_data = array(
                 "student_id" => $this->session->userdata('user_id'),
                 "from_id" => $this->input->post("from_id"),
                 "course_id" => $this->input->post("course_id"),
                 "session_id" => $this->input->post("session_id"),
                 "student_name" => $this->input->post("student_name"),
                 "father_or_mother_name" => $this->input->post("father_or_mother_name"),
                 "guardian_name" => $this->input->post("guardian_name"),
                 "perm_address" => $this->input->post("perm_address"),
                 "email" => $this->input->post("email"),
                 "phone_no" => $this->input->post("phone_no"),
                 "corres_address" => $this->input->post("corres_address"),
                 "aadhar_no" => $this->input->post("aadhar_no"),
                 "reg_mobile_no" => $this->input->post("reg_mobile_no"),
                 "gud_annual_income" => $this->input->post("gud_annual_income"),
                 "dob" => $this->input->post("dob"),
                 "age" => $this->input->post("age"),
                 "gender" => $this->input->post("gender"),
                 "marital_status" => $this->input->post("marital_status"),
                 "nationality" => $this->input->post("nationality"),
                 "caste" => $this->input->post("caste"),
                 "local_address" => $this->input->post("local_address"),
                 "local_gu_name_adds" => $this->input->post("local_gu_name_adds"),
                 "date" => date(DATE_FORMAT),
                 "rules_condition_first" => $rules_condition_first,
                 "rules_condition_one" => $rules_condition_one,
                 "rules_condition_two" => $rules_condition_two,
                 "admit_card_type" =>0,
                 "status" => 1
             );

             $student_from_id = $this->my_custom_functions->insert_data($insert_data,"tbl_student_from");///////  insert to user table

             $student_semester_link_data = array(
                 'student_id' => $student_from_id,
                 'class_id' => $this->input->post("course_id"),
                 'semester_id' => "",
                 'roll_no' => "",
                 'enrollment_time' => time()
             );
             $this->my_custom_functions->insert_data($student_semester_link_data, TBL_STUDENT_ENROLLMENT);


             if ($student_from_id) {

                 if ($this->input->post('exam_pass_one')!='' AND $this->input->post('subject_one')!='' AND $this->input->post('year_pass_one')!='' AND $this->input->post('board_one')!='' AND$this->input->post('div_obt_one')!='') {

                     $exam_data = array(
                 "student_from_id" => $student_from_id,
                 "exam_passed" => $this->input->post("exam_pass_one"),
                 "subject" => $this->input->post("subject_one"),
                 "year_of_pass" => $this->input->post("year_pass_one"),
                 "board_coun_uni" => $this->input->post("board_one"),
                 "div_obt" => $this->input->post("div_obt_one")
                 );

                 $edu_table_id =$this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
                     if ($edu_table_id) {
                      if (isset($_FILES['student_document_one'])) {

                     if ($_FILES['student_document_one']['name'] != "") {

                         //$file_name = $student_from_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $edu_table_id.'_'.basename($_FILES['student_document_one']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('student_document_one')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             // Upload thumb image
                             // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                             // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                             // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             //Update database with the cover image url
                             $data = array(
                                 "document_url" => $file_name
                             );

                             $table = "tbl_edu_qualification";

                             $where = array(
                                 "id" => $edu_table_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }
                 }
                 }
                 }

                 if ($this->input->post('exam_pass_two')!='' AND $this->input->post('subject_two')!='' AND $this->input->post('year_pass_two')!='' AND $this->input->post('board_two')!='' AND$this->input->post('div_obt_two')!='') {

                     $exam_data = array(
                 "student_from_id" => $student_from_id,
                 "exam_passed" => $this->input->post("exam_pass_two"),
                 "subject" => $this->input->post("subject_two"),
                 "year_of_pass" => $this->input->post("year_pass_two"),
                 "board_coun_uni" => $this->input->post("board_two"),
                 "div_obt" => $this->input->post("div_obt_two")
                 );

                 $edu_table_id = $this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
                     if ($edu_table_id) {
                      if (isset($_FILES['student_document_two'])) {

                     if ($_FILES['student_document_two']['name'] != "") {

                         //$file_name = $student_from_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $edu_table_id.'_'.basename($_FILES['student_document_two']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('student_document_two')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             // Upload thumb image
                             // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                             // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                             // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             //Update database with the cover image url
                             $data = array(
                                 "document_url" => $file_name
                             );

                             $table = "tbl_edu_qualification";

                             $where = array(
                                 "id" => $edu_table_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }
                 }
                 }
                 }

                 if ($this->input->post('exam_pass_three')!='' AND $this->input->post('subject_three')!='' AND $this->input->post('year_pass_three')!='' AND $this->input->post('board_three')!='' AND$this->input->post('div_obt_three')!='') {

                     $exam_data = array(
                 "student_from_id" => $student_from_id,
                 "exam_passed" => $this->input->post("exam_pass_three"),
                 "subject" => $this->input->post("subject_three"),
                 "year_of_pass" => $this->input->post("year_pass_three"),
                 "board_coun_uni" => $this->input->post("board_three"),
                 "div_obt" => $this->input->post("div_obt_three")
                 );

                 $edu_table_id = $this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
                     if ($edu_table_id) {
                      if (isset($_FILES['student_document_three'])) {

                     if ($_FILES['student_document_three']['name'] != "") {

                         //$file_name = $student_from_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $edu_table_id.'_'.basename($_FILES['student_document_three']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('student_document_three')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             // Upload thumb image
                             // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                             // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                             // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             //Update database with the cover image url
                             $data = array(
                                 "document_url" => $file_name
                             );

                             $table = "tbl_edu_qualification";

                             $where = array(
                                 "id" => $edu_table_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }
                 }
                 }
                 }

                 if ($this->input->post('exam_pass_four')!='' AND $this->input->post('subject_four')!='' AND $this->input->post('year_pass_four')!='' AND $this->input->post('board_four')!='' AND$this->input->post('div_obt_four')!='') {

                     $exam_data = array(
                 "student_from_id" => $student_from_id,
                 "exam_passed" => $this->input->post("exam_pass_four"),
                 "subject" => $this->input->post("subject_four"),
                 "year_of_pass" => $this->input->post("year_pass_four"),
                 "board_coun_uni" => $this->input->post("board_four"),
                 "div_obt" => $this->input->post("div_obt_four")
                 );

                 $edu_table_id = $this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
                     if ($edu_table_id) {
                      if (isset($_FILES['student_document_four'])) {

                     if ($_FILES['student_document_four']['name'] != "") {

                         //$file_name = $student_from_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $edu_table_id.'_'.basename($_FILES['student_document_four']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('student_document_four')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             // Upload thumb image
                             // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                             // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                             // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             //Update database with the cover image url
                             $data = array(
                                 "document_url" => $file_name
                             );

                             $table = "tbl_edu_qualification";

                             $where = array(
                                 "id" => $edu_table_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }
                 }
                 }
                 }


                 // Upload Passport Photo
                 $img_upload_message = '';
                 if (isset($_FILES['student_passport'])) {

                     if ($_FILES['student_passport']['name'] != "") {

                         //$file_name = $student_from_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $student_from_id.'_'.basename($_FILES['student_passport']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO;
                         $config['allowed_types']        = ALLOWED_PHOTO_IMAGE_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_PHOTO_IMAGE_SIZE;
                         // $config['max_width']  = PASSPORT_WIDTH;
                         // $config['max_height']  = PASSPORT_HEIGHT;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('student_passport')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             //Upload thumb image
                             $source = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$file_name;
                             $destination = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$file_name;

                             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             //Update database with the cover image url
                             $file_data = array(
                                 "student_id" => $student_from_id,
                                 "passport" => $file_name,
                                 "age_photo" => '',
                                 "caste_photo" => '',
                                 "guardian_sign" => '',
                                 "student_sign" => '',
                                 "student_sign_full" => ''
                             );
                             //echo "<pre>";print_r($file_data);die;

                             $this->my_custom_functions->insert_data($file_data,"tbl_student_from_document");

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }
                 }

                  // Upload Original copies of mark sheets
                 // if (isset($_FILES['mark_sheets_xerox'])) {

                 //     if ($_FILES['mark_sheets_xerox']['name'] != "") {

                 //         $file_name = $student_from_id.'.'.'jpg';
                 //         //$file_name = $student_from_id.'_'.'jpg';
                 //         //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                 //         $config = array();
                 //         $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                 //         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                 //         $config['file_name']            = $file_name;
                 //         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                 //         $this->load->library('upload');
                 //         $this->upload->initialize($config);

                 //         if (!$this->upload->do_upload('mark_sheets_xerox')) {

                 //             $img_upload_message = $this->upload->display_errors();

                 //         } else {

                 //             // Upload thumb image
                 //             $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                 //             $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                 //             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                 //             // Update database with the cover image url
                 //             // $data = array(
                 //             //     "cover_image_url" => $file_name
                 //             // );

                 //             // $table = "tbl_gallery_albums";

                 //             // $where = array(
                 //             //     "id" => $album_id
                 //             // );
                 //             // $this->ablfunctions->updateData($data, $table, $where);

                 //             // $img_upload_message = 'Cover image uploaded successfully.';
                 //         }
                 //     }
                 // }
                  // Upload Original copies of age verification
                 if (isset($_FILES['student_age_verification'])) {

                     if ($_FILES['student_age_verification']['name'] != "") {

                         //$file_name = $student_from_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $student_from_id.'_'.basename($_FILES['student_age_verification']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_AGE_PROOF;
                         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('student_age_verification')) {

                             $img_upload_message = $this->upload->display_errors();

                         }
                         else {

                             //Upload thumb image
                             // $source = UPLOAD_DIR.STUDENT_AGE_PROOF.$file_name;
                             // $destination = UPLOAD_DIR.STUDENT_AGE_PROOF_THUMB.$file_name;
                             //
                             // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             //Update database with the cover image url
                             $data = array(
                                 "age_photo" => $file_name
                             );

                             $table = "tbl_student_from_document";

                             $where = array(
                                 "student_id" => $student_from_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                         }
                     }
                 }
                  // Upload Original copies of SC/ST/OBC/Handicapped Certificate
                 if (isset($_FILES['student_caste_cf'])) {

                     if ($_FILES['student_caste_cf']['name'] != "") {

                         //$file_name = $student_from_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $student_from_id.'_'.basename($_FILES['student_caste_cf']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_CASTE;
                         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('student_caste_cf')) {

                             $img_upload_message = $this->upload->display_errors();

                         }
                         else {

                             // Upload thumb image
                             // $source = UPLOAD_DIR.STUDENT_CASTE.$file_name;
                             // $destination = UPLOAD_DIR.STUDENT_CASTE_THUMB.$file_name;
                             //
                             // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             //Update database with the cover image url
                             $data = array(
                                 "caste_photo" => $file_name
                             );

                             $table = "tbl_student_from_document";

                             $where = array(
                                 "student_id" => $student_from_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);


                         }
                     }
                 }
                  // Upload Countersignature of the Guardian
                 if (isset($_FILES['student_guardian_sign'])) {

                     if ($_FILES['student_guardian_sign']['name'] != "") {

                         $file_name_old = $student_from_id.'_'.basename($_FILES['student_guardian_sign']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_GURDAIN_SIGN;
                         $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                         // $config['max_width']  = SIGN_WIDTH;
                         // $config['max_height']  = SIGN_HEIGHT;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('student_guardian_sign')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             // Upload thumb image
                             $source = UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$file_name;
                             $destination = UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$file_name;

                             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                             //Update database with the cover image url
                             $data = array(
                                 "guardian_sign" => $file_name
                             );

                             $table = "tbl_student_from_document";

                             $where = array(
                                 "student_id" => $student_from_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);


                         }
                     }
                 }
                  // Upload Signature of Candidate
                 if (isset($_FILES['student_candidate_sign'])) {

                     if ($_FILES['student_candidate_sign']['name'] != "") {

                         //$file_name = $student_from_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $student_from_id.'_'.basename($_FILES['student_candidate_sign']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN;
                         $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                         //$config['max_width']  = SIGN_WIDTH;
                         //$config['max_height']  = SIGN_HEIGHT;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('student_candidate_sign')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             //Upload thumb image
                             $source = UPLOAD_DIR.STUDENT_SIGN.$file_name;
                             $destination = UPLOAD_DIR.STUDENT_SIGN_THUMB.$file_name;

                             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                             //Update database with the cover image url
                             $data = array(
                                 "student_sign" => $file_name
                             );

                             $table = "tbl_student_from_document";

                             $where = array(
                                 "student_id" => $student_from_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);


                         }
                     }
                 }
                 // Upload Signature of Candidate in full
                 if (isset($_FILES['student_candidate_sign_full'])) {

                     if ($_FILES['student_candidate_sign_full']['name'] != "") {

                         $file_name_old = $student_from_id.'_'.basename($_FILES['student_candidate_sign_full']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN_FULL;
                         $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                         //$config['max_width']  = SIGN_WIDTH;
                         //$config['max_height']  = SIGN_HEIGHT;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('student_candidate_sign_full')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             // Upload thumb image
                             $source = UPLOAD_DIR.STUDENT_SIGN_FULL.$file_name;
                             $destination = UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$file_name;

                             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             //Update database with the cover image url
                             $data = array(
                                 "student_sign_full" => $file_name
                             );

                             $table = "tbl_student_from_document";

                             $where = array(
                                 "student_id" => $student_from_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);


                         }
                     }

                 }
                 $img_upload_message['title'] ="Thanks student!";
                 $this->session->set_flashdata("s_message", "You have successfully Register.".$img_upload_message);

                 //$this->session->set_flashdata("s_message", "You have successfully Register.");
             } else {

                 $this->session->set_flashdata("e_message", "Failed to Register! Please try again.");
             }
             //redirect("main/studentFromRegister",$img_upload_message);
             redirect("user/dashboard",$img_upload_message);
         }
     } else {
         $from_check = $this->my_custom_functions->get_perticular_count("tbl_student_from", " and  student_id='".$this->session->userdata('user_id')."'");
         if ($from_check > 0) {
           $data['title'] ="You have already submitted your admission form.";
           $data['note'] ="Note: For any query, please contact college authority.";
           //$this->session->set_flashdata("e_message", " You have already submitted your admission form.<br> For any query, please contact college authority.");
           $this->load->view("pay_admission_fees",$data);
         }else{
         $randomid = mt_rand(100000,999999);
         $from_id = "CAD".date("Y").$randomid;
         $exists = $this->my_custom_functions->get_perticular_count("tbl_student_from", " and  from_id='".$from_id."'");
         if ($exists > 0) {
           redirect("user/studentFromRegister");
         }else {
           $data['from_id'] = $from_id;
           $data['session_details']= $this->Main_model->get_current_session();
           //echo "<pre>";print_r ($data['session_details']);die;
           //$data['session_details'] = $this->my_custom_functions->get_multiple_data("tbl_session",'');
           $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
           $this->load->view("student_from", $data);
         }
         }

     }
 }

 ////////////////////////////////////////////////////////////////////////////
 // Fees structure
 ////////////////////////////////////////////////////////////////////////////
 function feeStructure() {

     $data['page_title'] = "Fees structure";
     $data['back_url'] = '';
     //echo $this->session->userdata('student_id');die;
     $data['payment_gateways'] = $this->User_model->get_active_payment_gateways();
     $data['payment_settings'] = $this->my_custom_functions->get_details_from_id("", TBL_COLLEGE_PAYMENT_SETTINGS, array("id" => "1"));
     $data['fees_data'] = $this->User_model->get_fees_detail();

     if ($this->input->post('break_ups')) {
         $data['breakup_details'] = $this->User_model->get_fees_breakup_details($this->input->post('break_ups'));
     }

     $this->load->view('fees_structure', $data);
 }

     public function payAdmissionFees() {
         $data['title'] ="Apply For Admission";
         $data['note'] ="Note: Click on the apply online button to fill up the admission form and submit online.";
         $this->load->view("pay_admission_fees",$data);
     }
     function admissionForm_download(){

         $this->load->view("admissionForm_download");
     }
     ///////////////////////////////// VIEW RESULT ///////////////////////////////////////

     function viewResult() {

         $student_id = $this->my_custom_functions->get_particular_field_value('tbl_student_from', 'id', 'and student_id="' . $this->session->userdata('student_id') . '"');
         $class_id = $this->my_custom_functions->get_particular_field_value('tbl_student_from', 'course_id', 'and student_id="' . $this->session->userdata('student_id') . '"');
         if (($student_id!='') AND ($class_id!='')) {
           $data['result'] = $this->User_model->get_student_result($student_id,$class_id);
           //$data['school_details'] = $this->my_custom_functions->get_details_from_id($this->session->userdata("school_id"), TBL_SCHOOL);
           $data['student_details'] = $this->my_custom_functions->get_semesters_of_a_student($student_id, $this->session->userdata('session_id'));
           //$enrollment_detail = $this->my_custom_functions->get_semesters_of_a_student($student_id,$this->session->userdata('session_id'));
           //echo "<pre>";print_r($data['result']);die;
           $data['page_title'] = "View Result";
           $data['back_url'] = '';
           $this->load->view('student/view_result', $data);
         }else {
           $this->session->set_flashdata('s_message', 'Student Enrollment Process Not Done.');
           redirect('user/dashboard');
         }

     }
     /*
 | --------------------------------------------------------------------------
 | Edit Student Register
 | --------------------------------------------------------------------------
 */
 function editStudentRegisterPortal() {

     if ($this->input->post("submit") && $this->input->post("submit") != "") {

         // $encrypted_id = $this->input->post('student_id');
         // $student_id = $this->ablfunctions->ablDecrypt($encrypted_id);
         $user_id = $this->session->userdata('user_id');
         $student_id = $this->my_custom_functions->get_particular_field_value("tbl_student_from","id", 'and student_id="'.$user_id.'"');

         $this->form_validation->set_rules("course_id", "Select Course", "required");
         //$this->form_validation->set_rules("session_id", "Select session", "required");
         $this->form_validation->set_rules("student_name", "Name of the Candidate", "trim|required");
         $this->form_validation->set_rules("father_or_mother_name", "Father’s /Mother’s Name", "trim|required");
         $this->form_validation->set_rules("guardian_name", "Name of the Guardian", "trim|required");
         $this->form_validation->set_rules("perm_address", "Permanent Address", "trim|required");
         $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
         //$this->form_validation->set_rules("phone_number", "Phone Number", "trim|required|is_unique[tbl_student_from.phone_number]");
         $this->form_validation->set_rules("phone_no", "Phone Number", "trim|required|regex_match[/^[0-9]{10}$/]");
         $this->form_validation->set_rules("corres_address", "Address for correspondence", "trim|required");
         $this->form_validation->set_rules("aadhar_no", "Aadhar No", "trim|required|min_length[12]|max_length[12]");
         $this->form_validation->set_rules("reg_mobile_no", "Registered Mobile No", "trim|required|regex_match[/^[0-9]{10}$/]");
         $this->form_validation->set_rules("dob", "D.O.B", "trim|required");
         $this->form_validation->set_rules("gud_annual_income", "Annual income of the Guardian", "required");
         //$this->form_validation->set_rules("student_passport_photo", "Passport Photo", "required");
         $this->form_validation->set_rules("age", "Age", "trim|required");
         $this->form_validation->set_rules("gender", "Gender", "required");
         $this->form_validation->set_rules("marital_status", "Marital Status", "required");
         $this->form_validation->set_rules("nationality", "nationality", "required");
         $this->form_validation->set_rules("caste", "Caste", "required");
         $this->form_validation->set_rules("local_address", "Local address of the candidate", "trim|required");
         $this->form_validation->set_rules("local_gu_name_adds", "Name & Address of the local guardian", "trim|required");
         //$this->form_validation->set_rules("rules_condition_one", "Condition", "required");
         //$this->form_validation->set_rules("rules_condition_two", "Condition", "required");

         if ($this->form_validation->run() == false) {

             $this->session->set_flashdata("e_message", validation_errors());
             redirect("User/editStudentRegisterPortal/");
         } else {

           $update_data = array(
                 "course_id" => $this->input->post("course_id"),
                 //"session_id" => $this->input->post("session_id"),
                 "student_name" => $this->input->post("student_name"),
                 "father_or_mother_name" => $this->input->post("father_or_mother_name"),
                 "guardian_name" => $this->input->post("guardian_name"),
                 "perm_address" => $this->input->post("perm_address"),
                 "email" => $this->input->post("email"),
                 "phone_no" => $this->input->post("phone_no"),
                 "corres_address" => $this->input->post("corres_address"),
                 "aadhar_no" => $this->input->post("aadhar_no"),
                 "reg_mobile_no" => $this->input->post("reg_mobile_no"),
                 "gud_annual_income" => $this->input->post("gud_annual_income"),
                 "dob" => $this->input->post("dob"),
                 "age" => $this->input->post("age"),
                 "gender" => $this->input->post("gender"),
                 "marital_status" => $this->input->post("marital_status"),
                 "nationality" => $this->input->post("nationality"),
                 "caste" => $this->input->post("caste"),
                 "local_address" => $this->input->post("local_address"),
                 "local_gu_name_adds" => $this->input->post("local_gu_name_adds"),
                 //"status" => $this->input->post("status")
             );

           $where = array(
             "id" => $student_id,
           );

             $update = $this->my_custom_functions->update_data($update_data,"tbl_student_from",$where);
             //$update =1;

             if ($update) {
                 $education_details= $this->User_model->get_educational_details($student_id);

                 foreach ($education_details as $row) {

                 if ($this->input->post('exam_pass_'.$row['id'].'')!='' AND $this->input->post('subject_'.$row['id'].'')!='' AND $this->input->post('year_pass_'.$row['id'].'')!='' AND $this->input->post('board_'.$row['id'].'')!='' AND $this->input->post('div_obt_'.$row['id'].'')!='') {

                 $exam_data = array(
                 "exam_passed" => $this->input->post("exam_pass_".$row['id'].""),
                 "subject" => $this->input->post("subject_".$row['id'].""),
                 "year_of_pass" => $this->input->post("year_pass_".$row['id'].""),
                 "board_coun_uni" => $this->input->post("board_".$row['id'].""),
                 "div_obt" => $this->input->post("div_obt_".$row['id']."")
                 );


                 $where = array(
                 "id" => $row['id'],
                 );
                 $edu_table_update = $this->my_custom_functions->update_data($exam_data,"tbl_edu_qualification",$where);
                     if ($edu_table_update) {
                      if (isset($_FILES["document_".$row['id'].""])) {

                     if ($_FILES["document_".$row['id'].""]['name'] != "") {

                         //$file_name = $student_from_id.'.'.'jpg';
                         $file_name_old = $row['id'].'_'.basename($_FILES["document_".$row['id'].""]['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('document_'.$row['id'].'')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             // Upload thumb image
                             // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                             // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                             // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             //Update database with the cover image url
                             $data = array(
                                 "document_url" => $file_name
                             );

                             $table = "tbl_edu_qualification";

                             $where = array(
                                 "id" => $row['id']
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }
                 }
                 }

                 }
                 }
                 ////////////////File Upload///////////////
                 // Upload Passport Photo
                 $img_upload_message = '';
                 if (isset($_FILES['student_passport_photo'])) {

                     if ($_FILES['student_passport_photo']['name'] != "") {

                         //$file_name = $student_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $student_id.'_'.basename($_FILES['cover_image_url']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO;
                         $config['allowed_types']        = ALLOWED_PHOTO_IMAGE_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_PHOTO_IMAGE_SIZE;
                         // $config['max_width']  = PASSPORT_WIDTH;
                         // $config['max_height']  = PASSPORT_HEIGHT;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('student_passport_photo')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             //Upload thumb image
                             $source = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$file_name;
                             $destination = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$file_name;

                             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             // Update database with the cover image url
                             $data = array(
                                 "passport" => $file_name
                             );

                             $table = "tbl_student_from_document";

                             $where = array(
                                 "student_id" => $student_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }
                 }

                  // Upload Original copies of age verification
                 if (isset($_FILES['age_verification_xerox'])) {

                     if ($_FILES['age_verification_xerox']['name'] != "") {

                         //$file_name = $student_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $student_id.'_'.basename($_FILES['age_verification_xerox']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_AGE_PROOF;
                         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('age_verification_xerox')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             // Upload thumb image
                             // $source = UPLOAD_DIR.STUDENT_AGE_PROOF.$file_name;
                             // $destination = UPLOAD_DIR.STUDENT_AGE_PROOF_THUMB.$file_name;
                             //
                             // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             // Update database with the cover image url
                             $data = array(
                                 "age_photo" => $file_name
                             );

                             $table = "tbl_student_from_document";

                             $where = array(
                                 "student_id" => $student_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }
                 }
                  // Upload Original copies of SC/ST/OBC/Handicapped Certificate
                 if (isset($_FILES['caste_certificate_xerox'])) {

                     if ($_FILES['caste_certificate_xerox']['name'] != "") {

                         $file_name = $student_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);
                         //$file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_CASTE;
                         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('caste_certificate_xerox')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             // Upload thumb image
                             // $source = UPLOAD_DIR.STUDENT_CASTE.$file_name;
                             // $destination = UPLOAD_DIR.STUDENT_CASTE_THUMB.$file_name;
                             //
                             // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             // Update database with the cover image url
                             $data = array(
                                 "caste_photo" => $file_name
                             );

                             $table = "tbl_student_from_document";

                             $where = array(
                                 "student_id" => $student_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }
                 }
                  // Upload Countersignature of the Guardian
                 if (isset($_FILES['guardian_signature'])) {

                     if ($_FILES['guardian_signature']['name'] != "") {

                         //$file_name = $student_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $student_id.'_'.basename($_FILES['guardian_signature']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_GURDAIN_SIGN;
                         $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                         // $config['max_width']  = SIGN_WIDTH;
                         // $config['max_height']  = SIGN_HEIGHT;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('guardian_signature')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             // Upload thumb image
                             $source = UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$file_name;
                             $destination = UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$file_name;

                             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             // Update database with the cover image url
                             $data = array(
                                 "guardian_sign" => $file_name
                             );

                             $table = "tbl_student_from_document";

                             $where = array(
                                 "student_id" => $student_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }
                 }
                  // Upload Signature of Candidate
                 if (isset($_FILES['candidate_signature'])) {

                     if ($_FILES['candidate_signature']['name'] != "") {

                         //$file_name = $student_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $student_id.'_'.basename($_FILES['candidate_signature']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN;
                         $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                         // $config['max_width']  = SIGN_WIDTH;
                         // $config['max_height']  = SIGN_HEIGHT;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('candidate_signature')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             //Upload thumb image
                             $source = UPLOAD_DIR.STUDENT_SIGN.$file_name;
                             $destination = UPLOAD_DIR.STUDENT_SIGN_THUMB.$file_name;

                             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             // Update database with the cover image url
                             $data = array(
                                 "student_sign" => $file_name
                             );

                             $table = "tbl_student_from_document";

                             $where = array(
                                 "student_id" => $student_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }
                 }
                 // Upload Signature of Candidate in full
                 if (isset($_FILES['candidate_signature_full'])) {

                     if ($_FILES['candidate_signature_full']['name'] != "") {

                         //$file_name = $student_id.'.'.'jpg';
                         //$file_name = $student_from_id.'_'.'jpg';
                         $file_name_old = $student_id.'_'.basename($_FILES['candidate_signature_full']['name']);
                         $file_name = str_replace(' ', '_', $file_name_old);

                         $config = array();
                         $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN_FULL;
                         $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                         $config['file_name']            = $file_name;
                         $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                         // $config['max_width']  = SIGN_WIDTH;
                         // $config['max_height']  = SIGN_HEIGHT;

                         $this->load->library('upload');
                         $this->upload->initialize($config);

                         if (!$this->upload->do_upload('candidate_signature_full')) {

                             $img_upload_message = $this->upload->display_errors();

                         } else {

                             // Upload thumb image
                             $source = UPLOAD_DIR.STUDENT_SIGN_FULL.$file_name;
                             $destination = UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$file_name;

                             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                             // Update database with the cover image url
                             $data = array(
                                 "student_sign_full" => $file_name
                             );

                             $table = "tbl_student_from_document";

                             $where = array(
                                 "student_id" => $student_id
                             );
                             $this->ablfunctions->updateData($data, $table, $where);

                             // $img_upload_message = 'Cover image uploaded successfully.';
                         }
                     }

                 }

                 $this->session->set_flashdata("s_message", "Student Admissions record updated successfully.");
             } else {

                 $this->session->set_flashdata('e_message', "Failed to update the Student Admissions record.");
             }

             redirect("user/editStudentRegisterPortal");
         }
     } else {

         //$encrypted_id = $this->uri->segment(4);
         $student_id = $this->session->userdata('user_id');
         $from_id = $this->my_custom_functions->get_particular_field_value("tbl_student_from","id", 'and student_id="'.$student_id.'"');
         $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
         //$student_id = $this->ablfunctions->ablDecrypt($encrypted_id);

          $data['education_details']= $this->User_model->get_educational_details($from_id);
          //echo "<pre>";print_r ($data['education_details']);die;
         $data['details'] = $this->ablfunctions->getDetailsFromId($from_id, "tbl_student_from");

         $this->load->view("student/edit_student_register_student", $data);
     }
     }
             /*
        | --------------------------------------------------------------------------
        | delete Image
        | --------------------------------------------------------------------------
        */
        function deletePassportImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
        $path=UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$document_details['passport'];
        $path_thu=UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$document_details['passport'];
        //echo $path;die;
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "passport" =>'',
              );

              $table = "tbl_student_from_document";

              $where = array(
                    "student_id" =>$student_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
             redirect("user/editStudentRegisterPortal");


        }
        function deleteAgeproofImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
        $path=UPLOAD_DIR.STUDENT_AGE_PROOF.$document_details['age_photo'];
        $path_thu=UPLOAD_DIR.STUDENT_AGE_PROOF_THUMB.$document_details['age_photo'];
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "age_photo" =>'',
              );

              $table = "tbl_student_from_document";

              $where = array(
                    "student_id" =>$student_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
             redirect("user/editStudentRegisterPortal");


        }
        function deleteCasteImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
        $path=UPLOAD_DIR.STUDENT_CASTE.$document_details['caste_photo'];
        $path_thu=UPLOAD_DIR.STUDENT_CASTE_THUMB.$document_details['caste_photo'];
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "caste_photo" =>'',
              );

              $table = "tbl_student_from_document";

              $where = array(
                    "student_id" =>$student_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("user/editStudentRegisterPortal");


        }
        function deleteGdsignImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
        $path=UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$document_details['guardian_sign'];
        $path_thu=UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$document_details['guardian_sign'];
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "guardian_sign" =>'',
              );

              $table = "tbl_student_from_document";

              $where = array(
                    "student_id" =>$student_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("user/editStudentRegisterPortal");


        }
        function deleteStsignImage($student_id){

        $cript_id = $this->ablfunctions->ablEncrypt($student_id);
        $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
        $path=UPLOAD_DIR.STUDENT_SIGN.$document_details['student_sign'];
        $path_thu=UPLOAD_DIR.STUDENT_SIGN_THUMB.$document_details['student_sign'];
        @unlink(''.$path.'');
        @unlink(''.$path_thu.'');
        $data = array(
                  "student_sign" =>'',
              );

              $table = "tbl_student_from_document";

              $where = array(
                    "student_id" =>$student_id
              );
              $delete = $this->ablfunctions->updateData($data, $table, $where);
        $this->session->set_flashdata("s_message", "Image successfully Deleted.");
        redirect("user/editStudentRegisterPortal");


        }
        function deleteMarkSheetImage($document_url,$student_id,$edu_table_id){

            $cript_id = $this->ablfunctions->ablEncrypt($student_id);
            $path=UPLOAD_DIR.STUDENT_MARKSHEET.$document_url;
            $path_thu=UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$document_url;
            @unlink(''.$path.'');
            @unlink(''.$path_thu.'');
            $data = array(
                      "document_url" =>'',
                  );

                  $table = "tbl_edu_qualification";

                  $where = array(
                        "id" => $edu_table_id
                  );
                  $delete = $this->ablfunctions->updateData($data, $table, $where);
            if ($delete) {
            $this->session->set_flashdata("s_message", "Image successfully Deleted.");
            redirect("user/editStudentRegisterPortal");
            }

        }
        function deleteStfsignImage($student_id){

            $cript_id = $this->ablfunctions->ablEncrypt($student_id);
            $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
            $path=UPLOAD_DIR.STUDENT_SIGN_FULL.$document_details['student_sign_full'];
            $path_thu=UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$document_details['student_sign_full'];
            @unlink(''.$path.'');
            @unlink(''.$path_thu.'');
            $data = array(
                      "student_sign_full" =>'',
                  );

                  $table = "tbl_student_from_document";

                  $where = array(
                        "student_id" =>$student_id
                  );
                  $delete = $this->ablfunctions->updateData($data, $table, $where);
            $this->session->set_flashdata("s_message", "Image successfully Deleted.");
            redirect("user/editStudentRegisterPortal");

        }



}
