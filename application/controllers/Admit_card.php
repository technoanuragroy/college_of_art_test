<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admit_card extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("User_model");
        $this->load->model("Main_model");

    }


    ////////////////////////////////////////////////////////////////////////////
// Download Admit Card
////////////////////////////////////////////////////////////////////////////
function downloadAdmitCard() {

    // Get the data
    $encrypted_id = $this->uri->segment(3);
    $from_id = $this->ablfunctions->ablDecrypt($encrypted_id);
    $card_details = $this->ablfunctions->getDetailsFromId($from_id, "tbl_student_from");
    $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$card_details['id']));
    $principal = $this->my_custom_functions->get_multiple_data("tbl_staff",'and designation="1" LIMIT 1');
    $student_sign=base_url().UPLOAD_DIR.STUDENT_SIGN_FULL.$document_details['student_sign_full'];
    $student_passport=base_url().UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$document_details['passport'];
    $prin_photo=base_url().UPLOAD_DIR.STAFF_SIGN.$principal[0]['id'].".jpg";
    $college_logo=base_url()."images/college/logo.jpg";
    $publish_date_time = date('d/m/Y h:i A', $card_details['admit_card_issue_date']);
    $publish_date = explode(" ", $publish_date_time);

    $publish_time = explode(":", $publish_date[1]);
    $hour = (int) $publish_time[0];
    $minute = (int) $publish_time[1];
    //$course_name = $this->my_custom_functions->get_particular_field_value("tbl_courses","course_name", 'and id="'.$card_details['course_id'].'"');
    if ($card_details['course_id']==2) {
        $course_name="BFA";
    }else if ($card_details['course_id']==3) {
        $course_name="MFA";
    }else{
        $course_name="Craft & Design";
    }
    if ($card_details['admit_card_type']==1) {
        $admission_ttype="Admission Test Date";
    }else if ($card_details['admit_card_type']==2) {
        $admission_ttype="Direct admission on";
    }

    //$this->load->view("admin/college/admit_card_view", $data);


        include(APPPATH . "libraries/mpdf/mpdf.php");


        $mpdf = new mPDF('utf-8', 'A4', '', '', '15', '15', '15', '18', '15', '15');

        $mpdf->SetTitle('Student Admit Card');

        $mpdf->SetDisplayMode('fullpage');


        $html =    '<!DOCTYPE html>
                    <html lang="en">

                    <head>
                    <meta charset="utf-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta content="width=device-width, initial-scale=1.0" name="viewport">
                    <meta content="" name="keywords">
                    <meta content="" name="description">

                    </head>

                    <body>';


                $html .=   '<section id="admit-card" style=" width: 95%;height: auto;padding: 10px 0 10px 0;">

            <div class="container" style="width:100%;">
            <div class="add_content" style="width: 100%; height: auto; float: left;padding-bottom: 20px;">

            <div class="row admision_cnt" style=" float: left;width: 70%;">

                <div class="" style="float: left;">
                    <label for="fname">Form No:</label>
                    <span style="border-bottom: 1px dotted;width: 60%;">
                     <label for="fname" style="width: 60%; float: right;">'.$card_details['from_id'].'</label>
                </span>
                </div>

            </div>
            <div class="row admision_box" style="float: right;width:25%; text align:right;">




            <label for="vehicle1" style=" margin-top: -5px;
         padding: 0px 6px; "> Course:&nbsp;'.$course_name.'</label>

            <label for="vehicle2" style=" margin-top: -5px;
          padding: 0px 6px;"> </label>



            </div>

        </div>

        <div class="admit_header" style="width: 100%; height: auto; float: left; text-align: center;">

            <div class="admit_header_content" style="width: 20%;height: auto; float: left;">
            <div class="admit_image">
                <img src="'.$college_logo.'" style="  width: 120px;
        height: 120px;">
            </div>
            </div>
            <div class="admit_header_content " style="width: 60%;height: auto; float: left; text-align: center;">
                <h2 style="margin:0;">College of Art & Design</h2>
                <p style="font-weight: 500; margin:5px;
      width: 80%;
      margin-left: 10%;">(Affiliated to the University of Burdwan)<br>
                    2f, 12(B) approved by the UGC Act 1956
                </p>
                <span style="  font-weight: bold;">ADMIT CARD</span>

            </div>
            <div class="admit_header_content" style="width: 20%;height: auto; float: left;">
                <div class="admit_image">
                    <img src="'.$student_passport.'" style="  width: 120px;
        height: 120px;">
                </div>
            </div>
        </div>




        <div class="row form-content" style="width: 100%; float: left; padding-bottom: 15px; margin-top:40px;">

            <div class="content_list" style="width: 30%; float: left;">
                <label for="fname" style="width: 50%; float: left;">Name (In Capital letters)</label>:
                  </div>
                <div class="" style="width: 70%; float: left; border-bottom: 1px dotted;">
                <label for="fname"  style=" border: none;

          width: 100%;">'.$card_details['student_name'].'</label>

            </div>

        </div>

        <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

          <div class="content_list" style="width: 30%; float: left;">
                <label for="fname" style="width: 50%; float: left;">Address</label>:
                 </div>
                 <div class="" style="width: 70%; float: left; border-bottom: 1px dotted;">
                <label for="fname" style=" border: none;

          width: 100%;">'.$card_details['perm_address'].'</label>

            </div>
        </div>

        <div class="row form-content " style="width: 100%; float: left;padding-bottom: 15px;">


           <div class="content_list" style="width: 30%; float: left;">
                <label for="fname" style="width: 50%; float: left;">'.$admission_ttype.' :</label>
 </div>
  <div class="" style="width: 70%; float: left; border-bottom: 1px dotted;">
                <label for="fname" style="width: 5%; float: right;"></label>
                <span style="width: 30%;">
                <label for="fname" style="width: 30%; float: right;">'.$publish_date_time.'</label>
                 </span>

            </div>
        </div>


        <div class="admit_signature" style="width: 100%;
          height: auto;
          padding-top: 25px;">
            <div class="signature_lft" style="float: left;width: 50%;">

                <div class="row form-content ">


                    <div class="" style=" ">
                        <img src="'.$student_sign.'" style="  width: 150px;
                       height: 60px; border: none;
                border-bottom: 1px dotted;">

                    </div>
                    <div class="">
                        <label for="fname" style=" font-weight: 600;">Signature of Candidate</label>
                    </div>
                </div>

            </div>




            <div class="signtaure_rgt" style="float: right;width: 50%;">

                <div class="row form-content" style="
      text-align: center;
      margin-left: 20%;">

                    <div class="" style="">
                        <img src="'.$prin_photo.'" style="  width: 150px;
                       height: 60px; border: none;
          border-bottom: 1px dotted;">


                    <div class="">
                        <label for="fname" style=" text-align: center;
          ;font-weight: 600;"> Principal / Teacher-in-charge
                            College of Art & Design
                        </label>
                    </div>
                </div>

            </div>';



        $html .=   '</div>

                    </form>
                    </div>

                    </section>

                    </body>

                    </html>';

        $mpdf->WriteHTML($html);

        $file_name = 'temp/Admit-Card' . $this->ablfunctions->ablEncrypt($from_id) . '.pdf';
        $mpdf->Output($file_name, 'F');

        $file_url = base_url() . $file_name;
        header("Location: $file_url");

        exit();

     }

          // <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike"
                    //     style="width: 10%;float: right;outline: none;">
        //             <label for="vehicle2" style=" margin-top: -5px;
        // padding: 0px 6px;width: 90%;float: left;font-size:12px;"> Bachelor of Fine Art (BFA) [4 Years / 8 SEM] </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        //             <input type="checkbox" id="vehicle2" name="vehicle2" value="Car"
        //                 style="width: 10%;float: right; outline: none;">
        //             <label for="vehicle3" style=" margin-top: -5px;
        // padding: 0px 6px;width: 80%;float: left;font-size:12px;">Diploma in Visual Art (Craft & Design) [2 Years] </label>
        //             <input type="checkbox" id="vehicle3" name="vehicle3" value="Boat"
        //
        //               style="width: 10%;float: right; outline: none;">




        // <tr>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //       </tr>
        //       <tr>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //       </tr>
        //       <tr>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //       </tr>

}
