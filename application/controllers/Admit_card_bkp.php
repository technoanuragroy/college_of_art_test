<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admit_card extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("User_model");
        $this->load->model("Main_model");

    }


    ////////////////////////////////////////////////////////////////////////////
// Download Admit Card
////////////////////////////////////////////////////////////////////////////
function downloadAdmitCard() {

    // Get the data
    $encrypted_id = $this->uri->segment(3);
    $from_id = $this->ablfunctions->ablDecrypt($encrypted_id);
    $card_details = $this->ablfunctions->getDetailsFromId($from_id, "tbl_student_from");
    $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$card_details['id']));
    $principal = $this->my_custom_functions->get_multiple_data("tbl_staff",'and designation="1" LIMIT 1');
    $student_sign=base_url().UPLOAD_DIR.STUDENT_SIGN_FULL.$document_details['student_sign_full'];
    $student_passport=base_url().UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$document_details['passport'];
    $prin_photo=base_url().UPLOAD_DIR.STAFF_SIGN.$principal[0]['id'].".jpg";
    $college_logo=base_url()."images/college/logo.jpg";

    if ($card_details['course_id']==4) {
        $bfa="checked";
    }else if ($card_details['course_id']==5) {
        $dva="checked";
    }else if ($card_details['course_id']==3) {
        $mfa="checked";
    }
    if ($card_details['admit_card_type']==1) {
        $admission_ttype="Admission test";
    }else if ($card_details['admit_card_type']==2) {
        $admission_ttype="Direct admission on";
    }

    //$this->load->view("admin/college/admit_card_view", $data);


        include(APPPATH . "libraries/mpdf/mpdf.php");


        $mpdf = new mPDF('utf-8', 'A4', '', '', '15', '15', '15', '18', '15', '15');

        $mpdf->SetTitle('Student Admit Card');

        $mpdf->SetDisplayMode('fullpage');


        $html =    '<!DOCTYPE html>
                    <html lang="en">

                    <head>
                    <meta charset="utf-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta content="width=device-width, initial-scale=1.0" name="viewport">
                    <meta content="" name="keywords">
                    <meta content="" name="description">

                    </head>

                    <body>';


                $html .=   '<section id="admit-card" style=" width: 95%;height: auto;padding: 70px 0 70px 0;">

            <div class="container" style="width:80%; margin-left:10%;">
            <div class="add_content" style="width: 100%; height: auto; float: left;padding-bottom: 20px;">

            <div class="row admision_cnt" style=" float: left;width: 50%;">

                <div class="" style="float: left;">
                    <label for="fname">Form No.</label>
                    <span style="border-bottom: 1px dashed;width: 60%;">
                     <label for="fname" style="width: 60%; float: right;">123456</label>
                </span>
                </div>

            </div>
            <div class="row admision_box" style="float: right;">




            <label for="vehicle1" style=" margin-top: -5px;
         padding: 0px 6px;"> MFA</label>
            <input type="checkbox" id="vehicle1" '.$mfa.'>
            <label for="vehicle2" style=" margin-top: -5px;
          padding: 0px 6px;"> BFA</label>
            <input type="checkbox" id="vehicle2" '.$bfa.'>
            <label for="vehicle3" style=" margin-top: -5px;
         padding: 0px 6px;"> DVA</label>
            <input type="checkbox" id="vehicle3" '.$dva.'>


            </div>

        </div>

        <div class="admit_header" style="width: 100%; height: auto; float: left; text-align: center;">
            <div class="admit_header_content" style="width: 25%;height: 150px; float: left;">
                <img src="'.$college_logo.'">
            </div>
            <div class="admit_header_content " style="width: 50%;height: auto; float: left; text-align: center;">
                <h3>College of Art & Design</h3>
                <p style="font-weight: 500;
      width: 60%;
      margin-left: 20%;">(Affiliated to the University of Burdwan)
                    2f, 12(B) approved by the UGC Act 1956
                </p>
                <span style="  font-weight: 700;">ADMIT CARD</span>

            </div>
            <div class="admit_header_content" style="width: 25%;height: auto; float: left;">
                <div class="admit_image">
                    <img src="'.$student_passport.'" style="  width: 180px;
        height: 180px;">
                </div>
            </div>
        </div>




        <div class="row form-content" style="width: 100%; float: left; padding-bottom: 15px;">

            <div class="content_list" >
                <label for="fname" style="width: 40%; float: left;">Name (In Capital letters)</label>:
                <span style="border-bottom: 1px dashed;width: 60%;">
                <label for="fname" style="width: 60%; float: right;">'.$card_details['student_name'].'</label>
                </span>
            </div>

        </div>

        <div class="row form-content" style="width: 100%; float: left;padding-bottom: 15px;">

          <div class="content_list" >
                <label for="fname" style="width: 40%; float: left;">Address</label>:
                <span style="border-bottom: 1px dashed;width:100px;">
                <label for="fname" style="width: 60%; float: right;">'.$card_details['perm_address'].'</label>
                </span>
            </div>
        </div>

        <div class="row form-content " style="width: 100%; float: left;padding-bottom: 15px;">


           <div class="content_list" >
                <label for="fname" style="width: 30%; float: left;">'.$admission_ttype.' :</label>

                <label for="fname" style="width: 5%; float: right;">at</label>
                <span style="border-bottom: 1px dashed;width: 30%;">
                <label for="fname" style="width: 30%; float: right;">'.$card_details['admit_card_issue_date'].'</label>
                 </span>

            </div>
        </div>


        <div class="admit_signature" style="width: 100%;
          height: auto;
          padding-top: 75px;">
            <div class="signature_lft" style="float: left;width: 50%;">

                <div class="row form-content ">


                    <div class="" style=" ">
                        <img src="'.$student_sign.'" style="  width: 150px;
                       height: 60px; border: none;
                border-bottom: 1px dashed;">

                    </div>
                    <div class="">
                        <label for="fname" style=" font-weight: 600;">Signature of Candidate</label>
                    </div>
                </div>

            </div>




            <div class="signtaure_rgt" style="float: right;width: 50%;">

                <div class="row form-content" style="
      text-align: right;
      margin-left: 20%;">

                    <div class="" style="">
                        <img src="'.$prin_photo.'" style="  width: 150px;
                       height: 60px; border: none;
          border-bottom: 1px dashed;">


                    <div class="">
                        <label for="fname" style=" text-align: center;
          ;font-weight: 600;"> Principal / Teacher-in-charge
                            College of Art & Design
                        </label>
                    </div>
                </div>

            </div>';



        $html .=   '</div>

                    </form>
                    </div>

                    </section>

                    </body>

                    </html>';

        $mpdf->WriteHTML($html);

        $file_name = 'temp/Admit-Card' . $this->ablfunctions->ablEncrypt($from_id) . '.pdf';
        $mpdf->Output($file_name, 'F');

        $file_url = base_url() . $file_name;
        header("Location: $file_url");

        exit();

     }

          // <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike"
                    //     style="width: 10%;float: right;outline: none;">
        //             <label for="vehicle2" style=" margin-top: -5px;
        // padding: 0px 6px;width: 90%;float: left;font-size:12px;"> Bachelor of Fine Art (BFA) [4 Years / 8 SEM] </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        //             <input type="checkbox" id="vehicle2" name="vehicle2" value="Car"
        //                 style="width: 10%;float: right; outline: none;">
        //             <label for="vehicle3" style=" margin-top: -5px;
        // padding: 0px 6px;width: 80%;float: left;font-size:12px;">Diploma in Visual Art (Craft & Design) [2 Years] </label>
        //             <input type="checkbox" id="vehicle3" name="vehicle3" value="Boat"
        //
        //               style="width: 10%;float: right; outline: none;">




        // <tr>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //         border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //       </tr>
        //       <tr>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //       </tr>
        //       <tr>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //           <td style=" border: 1px solid black;
        //           border-collapse: collapse;text-align: left; padding: 15px;height:50px;"></td>
        //       </tr>

}
