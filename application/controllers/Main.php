<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Main_model");
    }

    /*
    | --------------------------------------------------------------------------
    | User login
    | Login can be done using email/mobile/username according to user settings
    | --------------------------------------------------------------------------
    */
    public function index() {

        // Internally get the IP address of the user
        $ip = $this->my_custom_functions->getUserIP();

        if ($this->input->post("submit") AND $this->input->post("submit") != "") {

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("username", "Username", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("main");
            } else {

                // Verifying the credentials with database
                $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
                $password = $this->input->post('password');

                $verified = $this->Main_model->login_check($ip, $username, $password);


                // If credentials match with database data
                if ($verified) {

                    // Update last login data in login table
                    $data = array(
                        "last_login_date" => date(DATETIME_FORMAT),
                        "last_login_ip"=> $ip
                    );

                    $table = "tbl_common_login";

                    $where = array(
                        "id" => $this->session->userdata('user_id')
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    // Set cookie if remember me checkbox is checked
                    if($this->input->post('remember_me')) {

                        $cookie_data = array(
                            'name' => 'user_id',
                            'value' => $this->ablfunctions->ablEncrypt($this->session->userdata('user_id')),
                            'expire' => COOKIE_EXPIRY_TIME,
                        );

                        $this->input->set_cookie($cookie_data);
                    }
                    // Delete cookie if remember me checkbox is not checked
                    else {

                        if ($this->input->cookie('user_id', true)) {

                            delete_cookie('user_id');
                        }
                    }

                    redirect("user/dashboard");
                }
                // If credentials don't match
                else {

                    $this->session->set_flashdata("e_message", "Invalid Username/Password.");
                    redirect("main");
                }
            }
        } else {

            // If the user is already logged in, redirect it to dashboard
            if($this->session->userdata('user_id')) {

                redirect("user/dashboard");
            }
            // Else check if anything stored in cookie
            else if ($this->input->cookie('user_id', true)) {

                $user_id = $this->ablfunctions->ablDecrypt($this->input->cookie('user_id'));
                $login_data = $this->ablfunctions->getDetailsFromId($user_id, "tbl_common_login");
                $user_details = $this->ablfunctions->getDetailsFromId($login_data['id'], "tbl_student");

                if(!empty($user_details)) {

                    // Session variables for user
                    $session_data = array(
                        "user_id" => $login_data['id'],
                        "student_id" => $login_data['id'],
                        "user_name" => $user_details["name"],
                        "user_email" => $user_details["email"],
                        "user_type" => $login_data["type"],
                    );
                    $this->session->set_userdata($session_data);

                    // Update last login data in login table
                    $data = array(
                        "last_login_date" => date(DATETIME_FORMAT),
                        "last_login_ip"=> $ip
                    );

                    $table = "tbl_common_login";

                    $where = array(
                        "id" => $login_data["id"]
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    redirect("user/dashboard");
                }
            }

            $this->load->view("login");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | User registration
    | --------------------------------------------------------------------------
    */
    public function registration() {

        // Getting common user settings set by admin
        $user_settings = $this->Main_model->get_user_settings();

        if ($this->input->post("submit") && $this->input->post('submit') != "") {

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("name", "Name", "required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email|is_unique[tbl_student.email]");
            $this->form_validation->set_rules("phone", "Phone", "trim|required|integer|is_unique[tbl_student.phone]");

            // If username type is email, username input would have a email validation
            if($user_settings['username'] == USERNAME_TYPE_EMAIL) {
                $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|valid_email|is_unique[tbl_common_login.username]");
            }
            // If username type is mobile, username input would have a integer validation
            else if($user_settings['username'] == USERNAME_TYPE_MOBILE) {
                $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|integer|is_unique[tbl_common_login.username]");
            }
            // If username type is general, username input would have a normal validation
            else if($user_settings['username'] == USERNAME_TYPE_GENERAL) {
                $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|is_unique[tbl_common_login.username]");
            }

            $this->form_validation->set_rules("password", "Password", "trim|required|min_length[6]|max_length[32]");
            $this->form_validation->set_rules("confirm_password", "Confirm Password", "trim|required|min_length[6]|max_length[32]|matches[password]");
             $this->form_validation->set_message('is_unique', 'This Mobile Number Or Email Id Already Registered.');

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("main/registration");
            } else {

                // Internally get the IP address of the user
                $ip = $this->my_custom_functions->getUserIP();

                // Add new user data
                $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);

                // Login data for common login table
                $login_data = array(
                    "username" => $this->input->post("username"),
                    "password" => $password,
                    "type" => LOGIN_TYPE_USER,
                    "last_login_date" => '0000-00-00 00:00:00',
                    "last_login_ip" => '',
                    "register_ip" => $ip
                );

                // User main data
                $user_data = array(
                    "name" => $this->input->post("name"),
                    "email" => $this->input->post("email"),
                    "phone" => $this->input->post("phone"),
                    "join_date" => date(DATETIME_FORMAT)
                );

                $user_id = $this->Main_model->insert_user_data($login_data, $user_data);
                //$user_id = $this->Main_model->insert_student_data($login_data, $user_data);


                // Verification process according to user settings set by admin
                // If email verification process is enabled

                $login_data = $this->ablfunctions->getDetailsFromId($user_id, "tbl_common_login");
                $user_details = $this->ablfunctions->getDetailsFromId($login_data['id'], "tbl_student");

                if(!empty($user_details)) {

                    // Session variables for user
                    $session_data = array(
                        "user_id" => $login_data['id'],
                        "student_id" => $login_data['id'],
                        "user_name" => $user_details["name"],
                        "user_email" => $user_details["email"],
                        "user_type" => $login_data["type"],
                    );
                    $this->session->set_userdata($session_data);
                }
                if($user_settings['email_verification'] == 1) {

                    $user_details = $this->ablfunctions->getDetailsFromId($user_id, "tbl_users");

                    // Email verification token
                    $verification_token = $this->my_custom_functions->createVerificationToken(USER_TYPE_USER, $user_id, TOKEN_TYPE_EMAIL_VERIFICATION);

                    // Email verification link
                    $verification_link = base_url() . 'main/emailVerification/' . $verification_token;

                    $this->load->helper('file');

                    // Email content
                    $message = "";
                    $message .= read_file("application/views/mail_template/mail_template_header.php");
                    $message .= '<h1>Account Verification</h1>';
                    $message .= '<p>';
                    $message .= 'Hello '.$user_details['name'].',<br><br>';
                    $message .= 'Please verify your account to use all the features.<br><br>';
                    $message .= 'Please <a href="'.$verification_link.'">click here</a> to verify your account now.<br><br>Thank you.';
                    $message .= '</p>';
                    $message .= read_file("application/views/mail_template/mail_template_footer.php");

                    // Send email
                    $this->ablfunctions->SendEmail(SITE_EMAIL.','.SITE_NAME, $user_details['email'], SITE_NAME.' : Verify user account', $message);

                    $this->session->set_flashdata("s_message", "An email has been sent to you to verify your account.");
                    redirect('user/dashboard');
                }

                $this->session->set_flashdata("s_message", "You are successfully registered.");
                redirect('user/dashboard');
            }
        } else {

            if ($this->session->userdata("user_id")) {

                redirect('user/dashboard');
            } else {

                // Custom label for username according to user settings
                $user_settings["username_label"] = "";

                if($user_settings['username'] == USERNAME_TYPE_EMAIL) {

                    $user_settings["username_label"] = "(Email)";
                } else if($user_settings['username'] == USERNAME_TYPE_MOBILE) {

                    $user_settings["username_label"] = "(Phone Number)";
                }

                $data["user_settings"] = $user_settings;
                $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
                $this->load->view("registration", $data);
            }
        }
    }

        /*
    | --------------------------------------------------------------------------
    | User registration
    | --------------------------------------------------------------------------
    */
    public function student_registration() {

        // Getting common user settings set by admin
        $user_settings = $this->Main_model->get_user_settings();

        if ($this->input->post("submit") && $this->input->post('submit') != "") {


            $this->load->library("form_validation");

            $this->form_validation->set_rules("name", "Name", "required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email|is_unique[tbl_student.email]");
            $this->form_validation->set_rules("phone", "Phone", "trim|required|is_unique[tbl_student.phone]");

            // If username type is email, username input would have a email validation
            // if($user_settings['username'] == USERNAME_TYPE_EMAIL) {
            //     $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|valid_email|is_unique[tbl_common_login.username]");
            // }
            // // If username type is mobile, username input would have a integer validation
            // else if($user_settings['username'] == USERNAME_TYPE_MOBILE) {
            //     $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|integer|is_unique[tbl_common_login.username]");
            // }
            // // If username type is general, username input would have a normal validation
            // else if($user_settings['username'] == USERNAME_TYPE_GENERAL) {
            //     $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|is_unique[tbl_common_login.username]");
            // }

            $this->form_validation->set_rules("password", "Password", "trim|required|min_length[6]|max_length[32]");
            $this->form_validation->set_rules("confirm_password", "Confirm Password", "trim|required|min_length[6]|max_length[32]|matches[password]");
            $this->form_validation->set_message('is_unique', 'This Mobile Number Or Email Id Already Registered.');
            //echo '<pre>';print_r($_POST);die;
            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("main/student_registration");
            } else {

                // Internally get the IP address of the user
                $ip = $this->my_custom_functions->getUserIP();

                // Add new user data
                $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);

                // Login data for common login table
                $login_data = array(
                    "username" => $this->input->post("username"),
                    "password" => $password,
                    "type" => LOGIN_TYPE_USER,
                    "last_login_date" => '0000-00-00 00:00:00',
                    "last_login_ip" => '',
                    "register_ip" => $ip
                );

                // User main data
                $user_data = array(
                    "name" => $this->input->post("name"),
                    "email" => $this->input->post("email"),
                    "phone" => $this->input->post("phone"),
                    "course_id" => $this->input->post("course_id"),
                    "join_date" => date(DATETIME_FORMAT),
                );

                //$user_id = $this->Main_model->insert_user_data($login_data, $user_data);
                $user_id = $this->Main_model->insert_student_data($login_data, $user_data);

                if ($user_id) {

                    $verification_token = $this->my_custom_functions->createVerificationToken(USER_TYPE_USER, $user_id, TOKEN_TYPE_SMS_VERIFICATION);
                    $otp_data = array(
                    "user_id" => $user_id,
                    "otp" => $verification_token,
                    "otp_sending_time" => date(DATETIME_FORMAT)
                );
                $otp_id = $this->my_custom_functions->insert_data($otp_data,"tbl_user_otp");

                $this->session->set_flashdata("s_message", "Otp Send successfully registered Mobile Number.");
                $userotp_id = $this->ablfunctions->ablEncrypt($user_id);

                $user_details = $this->ablfunctions->getDetailsFromId($user_id, "tbl_student");

                // Email verification token
                //$verification_token = $this->my_custom_functions->createVerificationToken(USER_TYPE_USER, $user_id, TOKEN_TYPE_EMAIL_VERIFICATION);

                // Email verification link
                //$verification_link = base_url() . 'main/emailVerification/' . $verification_token;

                $this->load->helper('file');

                // Email content
                $message = "";
                $message .= read_file("application/views/mail_template/mail_template_header.php");
                $message .= '<h1>Account Verification</h1>';
                $message .= '<p>';
                $message .= 'Hello '.$user_details['name'].',<br><br>';
                $message .= 'Please verify your account to use this OTP.<br><br>';
                //$message .= 'Please <a href="'.$verification_link.'">click here</a> to verify your account now.<br><br>Thank you.';
                $message .= 'Please Enter This OTP : '.$verification_token.'';
                $message .= '</p>';
                $message .= read_file("application/views/mail_template/mail_template_footer.php");

                // Send email
                $this->ablfunctions->SendEmail(SITE_EMAIL.','.SITE_NAME, $user_details['email'], SITE_NAME.' : Verify user account', $message);

                $this->session->set_flashdata("s_message", "An email has been sent to you to verify your account.");
                redirect('Main/studentOtp/'.$userotp_id);
                //redirect('Main/studentOtp/'.$userotp_id.'/'.$verification_token.'');
                }
                // Verification process according to user settings set by admin
                // If email verification process is enabled




            }
        } else {

            if ($this->session->userdata("user_id")) {

                redirect('user/dashboard');
            } else {

                // Custom label for username according to user settings
                $user_settings["username_label"] = "";

                if($user_settings['username'] == USERNAME_TYPE_EMAIL) {

                    $user_settings["username_label"] = "(Email)";
                } else if($user_settings['username'] == USERNAME_TYPE_MOBILE) {

                    $user_settings["username_label"] = "(Phone Number)";
                }

                $data["user_settings"] = $user_settings;
                $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
                $this->load->view("registration", $data);
            }
        }
    }

    public function studentOtp() {


        if ($this->input->post("submit") AND $this->input->post("submit") != "") {
            $user_id = $this->input->post('user_id');
            $enter_otp = $this->input->post('otp');
            $student_otp = $this->my_custom_functions->get_particular_field_value("tbl_user_otp","otp",'and user_id="'.$user_id.'"');
            if ($student_otp==$enter_otp) {
                $this->session->set_flashdata("s_message", "Register successfully.");
                    $session_data = array(
                        "user_id" => $user_id
                    );
                    $this->session->set_userdata($session_data);
                    // Update phone number
                    $update_data = array(
                        "otp_verification" => 1
                    );

                    $where = array(
                        "id" => $user_id,
                    );
                    $this->ablfunctions->updateData($update_data,"tbl_common_login", $where);
                redirect('user/dashboard');
            }else{

                $this->ablfunctions->deleteData("tbl_user_otp", array("user_id" => $user_id));
                $this->ablfunctions->deleteData("tbl_student", array("id" => $user_id));
                $this->session->set_flashdata("e_message", "Wrong Otp Entered.Please Try again");
                //$this->load->view("registration");
                redirect('Main/student_registration');

            }
           }
        else{
            $data['user_id'] = $this->ablfunctions->ablDecrypt($this->uri->segment(3));
            $this->load->view("student_otp",$data);
        }

    }

    public function checkOtpExists()
    {
      $user_id = $this->input->post('user_id');
      $enter_otp = $this->input->post('otp');
      $student_otp = $this->my_custom_functions->get_particular_field_value("tbl_user_otp","otp",'and user_id="'.$user_id.'"');
      if ($student_otp==$enter_otp) {
        echo "<p class='s_message'>OTP Match Successfully</p>";
      }else {
        echo "<p class='e_message'>Wrong OTP Entered</p>";
      }
    }
    /*
    | --------------------------------------------------------------------------
    | Validate user username through ajax call from user registration page
    | --------------------------------------------------------------------------
    */
    public function validateUsername() {

        $username = $this->input->post('username');

        if (isset($username) && $username != "") {

            $username_count = $this->ablfunctions->getParticularCount("tbl_common_login", " and username='" . $username . "'");

            if ($username_count > 0) {

                echo "Username already exists on system";
            } else {

                echo "";
            }
        } else {

            echo "Invalid username";
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Verification
    | Through email, otp
    | --------------------------------------------------------------------------
    */
    public function verifyAccount() {

        // Only logged in users can verify account
        if($this->session->userdata('user_id')) {

            if ($this->input->post('submit') && $this->input->post('submit') != "") {

                $user_id = $this->session->userdata('user_id');
                $db_otp = $this->ablfunctions->getParticularFieldValue("tbl_user_otp", "otp", " and user_id='".$user_id."'");

                // Compare the given otp with the database otp
                if ($db_otp == $this->input->post('otp')) {

                    // Update OTP verification status
                    $data = array(
                        "otp_verification" => 1
                    );

                    $table = "tbl_common_login";

                    $where = array(
                        'id' => $user_id
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    // Delete the OTP from db
                    $table = "tbl_user_otp";

                    $where = array(
                        "user_id" => $user_id
                    );
                    $this->ablfunctions->deleteData($table, $where);

                    $this->session->set_flashdata("s_message", "Verified account through OTP successfully.");
                    redirect("main/verifyAccount");
                } else {

                    $this->session->set_flashdata("e_message", "Incorrect OTP, please try again.");
                    redirect("main/verifyAccount");
                }
            } else {

                $data["user_settings"] = $this->ablfunctions->getDetailsFromId("1", "tbl_user_settings");
                $data["login_data"] = $this->ablfunctions->getDetailsFromId($this->session->userdata('user_id'), "tbl_common_login");
                $data["user_details"] = $this->ablfunctions->getDetailsFromId($this->session->userdata('user_id'), "tbl_student");

                // If the verification already done, redirect to user dashboard page
                if($data["user_settings"]['otp_verification'] == $data["login_data"]['otp_verification'] AND $data["user_settings"]['email_verification'] == $data["login_data"]['email_verification']) {

                    $this->session->keep_flashdata('s_message');
                    redirect("user/dashboard");
                }

                $this->load->view("verify_account", $data);
            }
        } else {

            $this->session->set_flashdata("s_message", "Login time expired.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Change mobile number from verify account page
    | --------------------------------------------------------------------------
    */
    public function changeMobileNumber() {

        // Only logged in users can change the mobile number
        if($this->session->userdata('user_id')) {

            if (filter_var($this->input->post("phone"), FILTER_VALIDATE_INT)) {

                $user_id = $this->session->userdata('user_id');
                $login_data = $this->ablfunctions->getDetailsFromId($user_id, "tbl_common_login");
                $user_details = $this->ablfunctions->getDetailsFromId($login_data['id'], "tbl_users");

                if(!empty($user_details)) {

                    // Update phone number
                    $data = array(
                        "phone" => $this->input->post('phone')
                    );

                    $table = "tbl_users";

                    $where = array(
                        "id" => $login_data['id']
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Mobile number successfully updated.");
                    redirect("main/verifyAccount");
                }
            } else {

                $this->session->set_flashdata("e_message", "Invalid mobile number provided.");
                redirect("main/verifyAccount");
            }
        } else {

            $this->session->set_flashdata("s_message", "Login time expired.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Change email from verify account page
    | --------------------------------------------------------------------------
    */
    public function changeEmailAddress() {

        // Only logged in users can change the email
        if($this->session->userdata('user_id')) {

            if (filter_var($this->input->post("email"), FILTER_VALIDATE_EMAIL)) {

                $user_id = $this->session->userdata('user_id');
                $login_data = $this->ablfunctions->getDetailsFromId($user_id, "tbl_common_login");
                $user_details = $this->ablfunctions->getDetailsFromId($login_data['id'], "tbl_users");

                if(!empty($user_details)) {

                    // Update phone number
                    $data = array(
                        "email" => $this->input->post('email')
                    );

                    $table = "tbl_users";

                    $where = array(
                        "id" => $login_data['id']
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Email address successfully updated.");
                    redirect("main/verifyAccount");
                }
            } else {

                $this->session->set_flashdata("e_message", "Invalid email address provided.");
                redirect("main/verifyAccount");
            }
        } else {

            $this->session->set_flashdata("s_message", "Login time expired.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Send OTP to mobile
    | --------------------------------------------------------------------------
    */
    function sendOTP() {

        // Only logged in users can change the email
        if($this->session->userdata('user_id')) {

            $user_id = $this->session->userdata('user_id');
            $login_data = $this->ablfunctions->getDetailsFromId($user_id, "tbl_common_login");

            // Last OTP details if any
            $OTP_data = $this->ablfunctions->getDetailsFromId("", "tbl_user_otp", array("user_id" => $user_id));

            $phone = $this->ablfunctions->getParticularFieldValue("tbl_users", "phone", " and id='".$user_id."'");

            // If there already exists an OTP
            if(!empty($OTP_data)) {

                // Get the times to compare
                $last_otp_sending_time = $OTP_data['otp_sending_time'];
                $current_time = time();

                $diff = $current_time - $last_otp_sending_time;
                $minutes = floor($diff / 60);

                // If 10 minutes have passed since last OTP sent, create and send a new OTP, else send the old OTP
                if ($minutes > 10) {

                    // New OTP
                    $otp = $this->my_custom_functions->generateOTP();

                    // Update with new OTP
                    $data = array(
                        "otp" => $otp,
                        "otp_sending_time" => time()
                    );

                    $table = "tbl_user_otp";

                    $where = array(
                        "user_id" => $user_id
                    );
                    $this->ablfunctions->updateData($data, $table, $where);
                } else {

                    // Old OTP
                    $otp = $OTP_data['otp'];
                }
            }
            // If no old OTP exists
            else {

                // New OTP
                $otp = $this->my_custom_functions->generateOTP();

                // Insert a new OTP record
                $data = array(
                    "user_id" => $user_id,
                    "otp" => $otp,
                    "otp_sending_time" => time()
                );

                $table = "tbl_user_otp";
                $this->ablfunctions->insertData($data, $table);
            }

            // SMS content
            $message = $otp . ' is your one time password for verification.';

            $sms_url = $this->my_custom_functions->getSMSUrl();
            $this->ablfunctions->sendSMS($sms_url, $phone, $message);

            echo 1;
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Resend account verification email
    | --------------------------------------------------------------------------
    */
    public function sendVerificationEmail() {

        // Only logged in users can change the email
        if($this->session->userdata('user_id')) {

            $user_id = $this->session->userdata("user_id");
            $user_details = $this->ablfunctions->getDetailsFromId($user_id, "tbl_users");

            // Email verification token
            $verification_token = $this->my_custom_functions->createVerificationToken(USER_TYPE_USER, $user_id, TOKEN_TYPE_EMAIL_VERIFICATION);

            // Email verification link
            $verification_link = base_url() . 'main/emailVerification/' . $verification_token;

            $this->load->helper('file');

            // Email content
            $message = "";
            $message .= read_file("application/views/mail_template/mail_template_header.php");
            $message .= '<h1>Account Verification</h1>';
            $message .= '<p>';
            $message .= 'Hello '.$user_details['name'].',<br><br>';
            $message .= 'Please verify your account to use all the features.<br><br>';
            $message .= 'Please <a href="'.$verification_link.'">click here</a> to verify your account now.<br><br>Thank you.';
            $message .= '</p>';
            $message .= read_file("application/views/mail_template/mail_template_footer.php");

            // Send email
            $this->ablfunctions->SendEmail(SITE_EMAIL.','.SITE_NAME, $user_details['email'], SITE_NAME.' : Verify user account', $message);

            $this->session->set_flashdata("s_message", "An email has been sent to you to verify your account.");
            redirect("main/verifyAccount");
        } else {

            $this->session->set_flashdata("s_message", "Login time expired.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Email verification
    | --------------------------------------------------------------------------
    */
    public function emailVerification() {

        $verification_token = $this->uri->segment(3);

        if ($verification_token != "") {

            // Check the token's validity. It will return the token details if token is valid
            $verified = $this->my_custom_functions->validateVerificationToken(USER_TYPE_USER, TOKEN_TYPE_EMAIL_VERIFICATION, $verification_token);

            // If the verification link is valid
            if (!empty($verified)) {

                // Update email verification status
                $data = array(
                    "email_verification" => 1
                );

                $table = "tbl_common_login";

                $where = array(
                    "id" => $verified['user_id']
                );
                $updated = $this->ablfunctions->updateData($data, $table, $where);

                if ($updated) {

                    // Delete verification token
                    $table = "tbl_verification_tokens";

                    $where = array(
                        "id" => $verified['id']
                    );
                    $this->ablfunctions->deleteData($table, $where);

                    $this->session->set_flashdata("s_message", 'Your account has been verified successfully. You can access your account');
                    redirect("main");
                } else {

                    $this->session->set_flashdata("e_message", "Something went wrong. Please make a request to verify email address once again.");
                    redirect("main");
                }
            }
            // If the verification link is expired
            else {

                $this->session->set_flashdata("e_message", "The verification link is expired.");
                redirect("main");
            }
        } else {

            $this->session->set_flashdata("e_message", "Something went wrong. Please make a request to verify email address once again.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | User forgot password
    | --------------------------------------------------------------------------
    */
    public function forgotPassword() {

        if($this->input->post('submit') AND $this->input->post('submit') != "") {

            // CI form validation
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required');

            if($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("main/forgotPassword");
            } else {

                // Verifying the username with database
                $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
                $verified = $this->ablfunctions->getParticularCount("tbl_common_login", " AND username='" . $username . "' AND status=1");

                // If the username already exists in database
                if($verified) {

                    $login_data = $this->ablfunctions->getDetailsFromId("", "tbl_common_login", array("username" => $username));
                    $user_details = $this->ablfunctions->getDetailsFromId($login_data['id'], "tbl_student");

                    // Password verification token
                    $verification_token = $this->my_custom_functions->createVerificationToken(USER_TYPE_USER, $user_details['id'], TOKEN_TYPE_FORGOT_PASSWORD);

                    // Password reset link
                    $reset_link = base_url() . 'main/resetPassword/' . $verification_token;

                    $this->load->helper('file');

                    // Email content
                    $message = "";
                    $message .= read_file("application/views/mail_template/mail_template_header.php");
                    $message .= '<h1>Forgot Password</h1>';
                    $message .= '<p>';
                    $message .= 'Hello '.$user_details['name'].',<br><br>';
                    $message .= 'You are receiving this email because you have requested for a change of password.<br><br>';
                    $message .= 'You may <a href="'.$reset_link.'">click here</a> to reset your password now.<br><br>Thank you.';
                    $message .= '</p>';
                    $message .= read_file("application/views/mail_template/mail_template_footer.php");

                    // Send email
                    $this->ablfunctions->SendEmail(SITE_EMAIL.','.SITE_NAME, $user_details['email'], SITE_NAME.' : Recover password', $message);

                    $this->session->set_flashdata("s_message", "A password recovery email has been sent to your email account.");
                    redirect("main/forgotPassword");
                }
                // If the username doesn't exist
                else {

                    $this->session->set_flashdata("e_message", "You are not a registered user!");
                    redirect("main/forgotPassword");
                }
            }
        } else {

            $this->load->view("forgot_password");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | User reset password
    | --------------------------------------------------------------------------
    */
    public function resetPassword() {

        $verification_token = $this->uri->segment(3);

        if ($verification_token != "") {

            // Check the token's validity. It will return the token details if token is valid
            $verified = $this->my_custom_functions->validateVerificationToken(USER_TYPE_USER, TOKEN_TYPE_FORGOT_PASSWORD, $verification_token);

            // If the verification link is valid
            if (!empty($verified)) {

                if($this->input->post('submit') AND $this->input->post('submit') != "") {

                    // CI form validation
                    $this->load->library('form_validation');

                    $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[32]');
                    $this->form_validation->set_rules('retype_new_password', 'Retype New Password', 'trim|required|min_length[6]|max_length[32]|matches[new_password]');

                    if ($this->form_validation->run() == FALSE) {

                        $this->session->set_flashdata("e_message", validation_errors());
                        redirect("resetPassword/" . $verification_token);
                    } else {

                        // Update password
                        $data = array(
                            "password" => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)
                        );

                        $table = "tbl_common_login";

                        $where = array(
                            "id" => $verified['user_id']
                        );
                        $this->ablfunctions->updateData($data, $table, $where);

                        // Delete verification token
                        $table = "tbl_verification_tokens";

                        $where = array(
                            "id" => $verified['id']
                        );
                        $this->ablfunctions->deleteData($table, $where);

                        $this->session->set_flashdata("s_message", "Your password has been changed successfully, you can login to your account using this new password.");
                        redirect("main");
                    }
                } else {

                    $this->load->view("reset_password");
                }
            }
            // If the verification link is expired
            else {

                $this->session->set_flashdata("e_message", "The verification link is expired.");
                redirect("main");
            }
        } else {

            $this->session->set_flashdata("e_message", "Something went wrong. Please make a request to reset the password once again.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | User logout
    | --------------------------------------------------------------------------
    */
    public function logout() {

        // Unset sessions
        $session_data = array('user_id','student_id','user_name', 'user_email', 'user_type');
        $this->session->unset_userdata($session_data);

        // Delete cookie
        if ($this->input->cookie('user_id', true)) {

            delete_cookie('user_id');
        }

        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("main");
    }

}
