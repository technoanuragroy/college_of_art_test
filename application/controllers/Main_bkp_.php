<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Main_model");
    }

    /*
    | --------------------------------------------------------------------------
    | User login
    | Login can be done using email/mobile/username according to user settings
    | --------------------------------------------------------------------------
    */
    public function index() {

        // Internally get the IP address of the user
        $ip = $this->my_custom_functions->getUserIP();

        if ($this->input->post("submit") AND $this->input->post("submit") != "") {

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("username", "Username", "trim|required");
            $this->form_validation->set_rules("password", "Password", "trim|required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("main");
            } else {

                // Verifying the credentials with database
                $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
                $password = $this->input->post('password');

                $verified = $this->Main_model->login_check($ip, $username, $password);


                // If credentials match with database data
                if ($verified) {

                    // Update last login data in login table
                    $data = array(
                        "last_login_date" => date(DATETIME_FORMAT),
                        "last_login_ip"=> $ip
                    );

                    $table = "tbl_common_login";

                    $where = array(
                        "id" => $this->session->userdata('user_id')
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    // Set cookie if remember me checkbox is checked
                    if($this->input->post('remember_me')) {

                        $cookie_data = array(
                            'name' => 'user_id',
                            'value' => $this->ablfunctions->ablEncrypt($this->session->userdata('user_id')),
                            'expire' => COOKIE_EXPIRY_TIME,
                        );
                        $this->input->set_cookie($cookie_data);
                    }
                    // Delete cookie if remember me checkbox is not checked
                    else {

                        if ($this->input->cookie('user_id', true)) {

                            delete_cookie('user_id');
                        }
                    }

                    redirect("user/dashboard");
                }
                // If credentials don't match
                else {

                    $this->session->set_flashdata("e_message", "Invalid Username/Password.");
                    redirect("main");
                }
            }
        } else {

            // If the user is already logged in, redirect it to dashboard
            if($this->session->userdata('user_id')) {

                redirect("user/dashboard");
            }
            // Else check if anything stored in cookie
            else if ($this->input->cookie('user_id', true)) {

                $user_id = $this->ablfunctions->ablDecrypt($this->input->cookie('user_id'));
                $login_data = $this->ablfunctions->getDetailsFromId($user_id, "tbl_common_login");
                $user_details = $this->ablfunctions->getDetailsFromId($login_data['id'], "tbl_student");

                if(!empty($user_details)) {

                    // Session variables for user
                    $session_data = array(
                        "user_id" => $login_data['id'],
                        "user_name" => $user_details["name"],
                        "user_email" => $user_details["email"],
                        "user_type" => $login_data["type"],
                    );
                    $this->session->set_userdata($session_data);

                    // Update last login data in login table
                    $data = array(
                        "last_login_date" => date(DATETIME_FORMAT),
                        "last_login_ip"=> $ip
                    );

                    $table = "tbl_common_login";

                    $where = array(
                        "id" => $login_data["id"]
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    redirect("user/dashboard");
                }
            }

            $this->load->view("login");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | User registration
    | --------------------------------------------------------------------------
    */
    public function registration() {

        // Getting common user settings set by admin
        $user_settings = $this->Main_model->get_user_settings();

        if ($this->input->post("submit") && $this->input->post('submit') != "") {

            // CI form validation
            $this->load->library("form_validation");

            $this->form_validation->set_rules("name", "Name", "required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            $this->form_validation->set_rules("phone", "Phone", "trim|required|integer");

            // If username type is email, username input would have a email validation
            if($user_settings['username'] == USERNAME_TYPE_EMAIL) {
                $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|valid_email|is_unique[tbl_common_login.username]");
            }
            // If username type is mobile, username input would have a integer validation
            else if($user_settings['username'] == USERNAME_TYPE_MOBILE) {
                $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|integer|is_unique[tbl_common_login.username]");
            }
            // If username type is general, username input would have a normal validation
            else if($user_settings['username'] == USERNAME_TYPE_GENERAL) {
                $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|is_unique[tbl_common_login.username]");
            }

            $this->form_validation->set_rules("password", "Password", "trim|required|min_length[6]|max_length[32]");
            $this->form_validation->set_rules("confirm_password", "Confirm Password", "trim|required|min_length[6]|max_length[32]|matches[password]");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("main/registration");
            } else {

                // Internally get the IP address of the user
                $ip = $this->my_custom_functions->getUserIP();

                // Add new user data
                $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);

                // Login data for common login table
                $login_data = array(
                    "username" => $this->input->post("username"),
                    "password" => $password,
                    "type" => LOGIN_TYPE_USER,
                    "last_login_date" => '0000-00-00 00:00:00',
                    "last_login_ip" => '',
                    "register_ip" => $ip
                );

                // User main data
                $user_data = array(
                    "name" => $this->input->post("name"),
                    "email" => $this->input->post("email"),
                    "phone" => $this->input->post("phone"),
                    "join_date" => date(DATETIME_FORMAT)
                );

                $user_id = $this->Main_model->insert_user_data($login_data, $user_data);
                //$user_id = $this->Main_model->insert_student_data($login_data, $user_data);


                // Verification process according to user settings set by admin
                // If email verification process is enabled
                if($user_settings['email_verification'] == 1) {

                    $user_details = $this->ablfunctions->getDetailsFromId($user_id, "tbl_users");

                    // Email verification token
                    $verification_token = $this->my_custom_functions->createVerificationToken(USER_TYPE_USER, $user_id, TOKEN_TYPE_EMAIL_VERIFICATION);

                    // Email verification link
                    $verification_link = base_url() . 'main/emailVerification/' . $verification_token;

                    $this->load->helper('file');

                    // Email content
                    $message = "";
                    $message .= read_file("application/views/mail_template/mail_template_header.php");
                    $message .= '<h1>Account Verification</h1>';
                    $message .= '<p>';
                    $message .= 'Hello '.$user_details['name'].',<br><br>';
                    $message .= 'Please verify your account to use all the features.<br><br>';
                    $message .= 'Please <a href="'.$verification_link.'">click here</a> to verify your account now.<br><br>Thank you.';
                    $message .= '</p>';
                    $message .= read_file("application/views/mail_template/mail_template_footer.php");

                    // Send email
                    $this->ablfunctions->SendEmail(SITE_EMAIL.','.SITE_NAME, $user_details['email'], SITE_NAME.' : Verify user account', $message);

                    $this->session->set_flashdata("s_message", "An email has been sent to you to verify your account.");
                    redirect('user/dashboard');
                }

                $this->session->set_flashdata("s_message", "You are successfully registered.");
                redirect('user/dashboard');
            }
        } else {

            if ($this->session->userdata("user_id")) {

                redirect('user/dashboard');
            } else {

                // Custom label for username according to user settings
                $user_settings["username_label"] = "";

                if($user_settings['username'] == USERNAME_TYPE_EMAIL) {

                    $user_settings["username_label"] = "(Email)";
                } else if($user_settings['username'] == USERNAME_TYPE_MOBILE) {

                    $user_settings["username_label"] = "(Phone Number)";
                }

                $data["user_settings"] = $user_settings;

                $this->load->view("registration", $data);
            }
        }
    }

        /*
    | --------------------------------------------------------------------------
    | User registration
    | --------------------------------------------------------------------------
    */
    public function student_registration() {

        // Getting common user settings set by admin
        $user_settings = $this->Main_model->get_user_settings();

        if ($this->input->post("submit") && $this->input->post('submit') != "") {


            $this->load->library("form_validation");

            $this->form_validation->set_rules("name", "Name", "required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            $this->form_validation->set_rules("phone", "Phone", "trim|required|integer");

            // // If username type is email, username input would have a email validation
            // if($user_settings['username'] == USERNAME_TYPE_EMAIL) {
            //     $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|valid_email|is_unique[tbl_common_login.username]");
            // }
            // // If username type is mobile, username input would have a integer validation
            // else if($user_settings['username'] == USERNAME_TYPE_MOBILE) {
            //     $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|integer|is_unique[tbl_common_login.username]");
            // }
            // // If username type is general, username input would have a normal validation
            // else if($user_settings['username'] == USERNAME_TYPE_GENERAL) {
            //     $this->form_validation->set_rules("username", "Username", "trim|required|min_length[5]|is_unique[tbl_common_login.username]");
            // }

            $this->form_validation->set_rules("password", "Password", "trim|required|min_length[6]|max_length[32]");
            $this->form_validation->set_rules("confirm_password", "Confirm Password", "trim|required|min_length[6]|max_length[32]|matches[password]");
            //echo '<pre>';print_r($_POST);die;
            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("main/registration");
            } else {

                // Internally get the IP address of the user
                $ip = $this->my_custom_functions->getUserIP();

                // Add new user data
                $password = password_hash($this->input->post("password"), PASSWORD_DEFAULT);

                // Login data for common login table
                $login_data = array(
                    "username" => $this->input->post("username"),
                    "password" => $password,
                    "type" => LOGIN_TYPE_USER,
                    "last_login_date" => '0000-00-00 00:00:00',
                    "last_login_ip" => '',
                    "register_ip" => $ip
                );

                // User main data
                $user_data = array(
                    "name" => $this->input->post("name"),
                    "email" => $this->input->post("email"),
                    "phone" => $this->input->post("phone"),
                    "join_date" => date(DATETIME_FORMAT)
                );

                //$user_id = $this->Main_model->insert_user_data($login_data, $user_data);
                $user_id = $this->Main_model->insert_student_data($login_data, $user_data);


                // Verification process according to user settings set by admin
                // If email verification process is enabled
                // if($user_settings['email_verification'] == 1) {

                //     $user_details = $this->ablfunctions->getDetailsFromId($user_id, "tbl_users");

                //     // Email verification token
                //     $verification_token = $this->my_custom_functions->createVerificationToken(USER_TYPE_USER, $user_id, TOKEN_TYPE_EMAIL_VERIFICATION);

                //     // Email verification link
                //     $verification_link = base_url() . 'main/emailVerification/' . $verification_token;

                //     $this->load->helper('file');

                //     // Email content
                //     $message = "";
                //     $message .= read_file("application/views/mail_template/mail_template_header.php");
                //     $message .= '<h1>Account Verification</h1>';
                //     $message .= '<p>';
                //     $message .= 'Hello '.$user_details['name'].',<br><br>';
                //     $message .= 'Please verify your account to use all the features.<br><br>';
                //     $message .= 'Please <a href="'.$verification_link.'">click here</a> to verify your account now.<br><br>Thank you.';
                //     $message .= '</p>';
                //     $message .= read_file("application/views/mail_template/mail_template_footer.php");

                //     // Send email
                //     $this->ablfunctions->SendEmail(SITE_EMAIL.','.SITE_NAME, $user_details['email'], SITE_NAME.' : Verify user account', $message);

                //     $this->session->set_flashdata("s_message", "An email has been sent to you to verify your account.");
                //     redirect('user/dashboard');
                // }
                if ($user_id) {

                    $verification_token = $this->my_custom_functions->createVerificationToken(USER_TYPE_USER, $user_id, TOKEN_TYPE_SMS_VERIFICATION);
                    $otp_data = array(
                    "user_id" => $user_id,
                    "otp" => $verification_token,
                    "otp_sending_time" => date(DATETIME_FORMAT)
                );
                $otp_id = $this->my_custom_functions->insert_data($otp_data,"tbl_user_otp");

                    $this->session->set_flashdata("s_message", "Otp Send successfully registered Mobile Number.");
                $userotp_id = $this->ablfunctions->ablEncrypt($user_id);
                redirect('Main/studentOtp/'.$userotp_id.'/'.$verification_token.'');
                }


            }
        } else {

            if ($this->session->userdata("user_id")) {

                redirect('user/dashboard');
            } else {

                // Custom label for username according to user settings
                $user_settings["username_label"] = "";

                if($user_settings['username'] == USERNAME_TYPE_EMAIL) {

                    $user_settings["username_label"] = "(Email)";
                } else if($user_settings['username'] == USERNAME_TYPE_MOBILE) {

                    $user_settings["username_label"] = "(Phone Number)";
                }

                $data["user_settings"] = $user_settings;

                $this->load->view("registration", $data);
            }
        }
    }

    public function studentOtp() {


        if ($this->input->post("submit") AND $this->input->post("submit") != "") {
            $user_id = $this->input->post('user_id');
            $enter_otp = $this->input->post('otp');
            $student_otp = $this->my_custom_functions->get_particular_field_value("tbl_user_otp","otp",'and user_id="'.$user_id.'"');
            if ($student_otp==$enter_otp) {
                $this->session->set_flashdata("s_message", "Otp Match successfully.");
                    $session_data = array(
                        "user_id" => $user_id
                    );
                    $this->session->set_userdata($session_data);
                redirect('user/dashboard');
            }else{

                $this->ablfunctions->deleteData("tbl_user_otp", array("user_id" => $user_id));
                $this->ablfunctions->deleteData("tbl_student", array("id" => $user_id));
                $this->session->set_flashdata("e_message", "Wrong Otp Entered.Please Try again");
                //$this->load->view("registration");
                redirect('Main/student_registration');

            }
           }
        else{
            $data['user_id'] = $this->ablfunctions->ablDecrypt($this->uri->segment(3));
            $this->load->view("student_otp",$data);
        }

    }
    /*
    | --------------------------------------------------------------------------
    | Validate user username through ajax call from user registration page
    | --------------------------------------------------------------------------
    */
    public function validateUsername() {

        $username = $this->input->post('username');

        if (isset($username) && $username != "") {

            $username_count = $this->ablfunctions->getParticularCount("tbl_common_login", " and username='" . $username . "'");

            if ($username_count > 0) {

                echo "Username already exists on system";
            } else {

                echo "";
            }
        } else {

            echo "Invalid username";
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Verification
    | Through email, otp
    | --------------------------------------------------------------------------
    */
    public function verifyAccount() {

        // Only logged in users can verify account
        if($this->session->userdata('user_id')) {

            if ($this->input->post('submit') && $this->input->post('submit') != "") {

                $user_id = $this->session->userdata('user_id');
                $db_otp = $this->ablfunctions->getParticularFieldValue("tbl_user_otp", "otp", " and user_id='".$user_id."'");

                // Compare the given otp with the database otp
                if ($db_otp == $this->input->post('otp')) {

                    // Update OTP verification status
                    $data = array(
                        "otp_verification" => 1
                    );

                    $table = "tbl_common_login";

                    $where = array(
                        'id' => $user_id
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    // Delete the OTP from db
                    $table = "tbl_user_otp";

                    $where = array(
                        "user_id" => $user_id
                    );
                    $this->ablfunctions->deleteData($table, $where);

                    $this->session->set_flashdata("s_message", "Verified account through OTP successfully.");
                    redirect("main/verifyAccount");
                } else {

                    $this->session->set_flashdata("e_message", "Incorrect OTP, please try again.");
                    redirect("main/verifyAccount");
                }
            } else {

                $data["user_settings"] = $this->ablfunctions->getDetailsFromId("1", "tbl_user_settings");
                $data["login_data"] = $this->ablfunctions->getDetailsFromId($this->session->userdata('user_id'), "tbl_common_login");
                $data["user_details"] = $this->ablfunctions->getDetailsFromId($this->session->userdata('user_id'), "tbl_student");

                // If the verification already done, redirect to user dashboard page
                if($data["user_settings"]['otp_verification'] == $data["login_data"]['otp_verification'] AND $data["user_settings"]['email_verification'] == $data["login_data"]['email_verification']) {

                    $this->session->keep_flashdata('s_message');
                    redirect("user/dashboard");
                }

                $this->load->view("verify_account", $data);
            }
        } else {

            $this->session->set_flashdata("s_message", "Login time expired.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Change mobile number from verify account page
    | --------------------------------------------------------------------------
    */
    public function changeMobileNumber() {

        // Only logged in users can change the mobile number
        if($this->session->userdata('user_id')) {

            if (filter_var($this->input->post("phone"), FILTER_VALIDATE_INT)) {

                $user_id = $this->session->userdata('user_id');
                $login_data = $this->ablfunctions->getDetailsFromId($user_id, "tbl_common_login");
                $user_details = $this->ablfunctions->getDetailsFromId($login_data['id'], "tbl_users");

                if(!empty($user_details)) {

                    // Update phone number
                    $data = array(
                        "phone" => $this->input->post('phone')
                    );

                    $table = "tbl_users";

                    $where = array(
                        "id" => $login_data['id']
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Mobile number successfully updated.");
                    redirect("main/verifyAccount");
                }
            } else {

                $this->session->set_flashdata("e_message", "Invalid mobile number provided.");
                redirect("main/verifyAccount");
            }
        } else {

            $this->session->set_flashdata("s_message", "Login time expired.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Change email from verify account page
    | --------------------------------------------------------------------------
    */
    public function changeEmailAddress() {

        // Only logged in users can change the email
        if($this->session->userdata('user_id')) {

            if (filter_var($this->input->post("email"), FILTER_VALIDATE_EMAIL)) {

                $user_id = $this->session->userdata('user_id');
                $login_data = $this->ablfunctions->getDetailsFromId($user_id, "tbl_common_login");
                $user_details = $this->ablfunctions->getDetailsFromId($login_data['id'], "tbl_users");

                if(!empty($user_details)) {

                    // Update phone number
                    $data = array(
                        "email" => $this->input->post('email')
                    );

                    $table = "tbl_users";

                    $where = array(
                        "id" => $login_data['id']
                    );
                    $this->ablfunctions->updateData($data, $table, $where);

                    $this->session->set_flashdata("s_message", "Email address successfully updated.");
                    redirect("main/verifyAccount");
                }
            } else {

                $this->session->set_flashdata("e_message", "Invalid email address provided.");
                redirect("main/verifyAccount");
            }
        } else {

            $this->session->set_flashdata("s_message", "Login time expired.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Send OTP to mobile
    | --------------------------------------------------------------------------
    */
    function sendOTP() {

        // Only logged in users can change the email
        if($this->session->userdata('user_id')) {

            $user_id = $this->session->userdata('user_id');
            $login_data = $this->ablfunctions->getDetailsFromId($user_id, "tbl_common_login");

            // Last OTP details if any
            $OTP_data = $this->ablfunctions->getDetailsFromId("", "tbl_user_otp", array("user_id" => $user_id));

            $phone = $this->ablfunctions->getParticularFieldValue("tbl_users", "phone", " and id='".$user_id."'");

            // If there already exists an OTP
            if(!empty($OTP_data)) {

                // Get the times to compare
                $last_otp_sending_time = $OTP_data['otp_sending_time'];
                $current_time = time();

                $diff = $current_time - $last_otp_sending_time;
                $minutes = floor($diff / 60);

                // If 10 minutes have passed since last OTP sent, create and send a new OTP, else send the old OTP
                if ($minutes > 10) {

                    // New OTP
                    $otp = $this->my_custom_functions->generateOTP();

                    // Update with new OTP
                    $data = array(
                        "otp" => $otp,
                        "otp_sending_time" => time()
                    );

                    $table = "tbl_user_otp";

                    $where = array(
                        "user_id" => $user_id
                    );
                    $this->ablfunctions->updateData($data, $table, $where);
                } else {

                    // Old OTP
                    $otp = $OTP_data['otp'];
                }
            }
            // If no old OTP exists
            else {

                // New OTP
                $otp = $this->my_custom_functions->generateOTP();

                // Insert a new OTP record
                $data = array(
                    "user_id" => $user_id,
                    "otp" => $otp,
                    "otp_sending_time" => time()
                );

                $table = "tbl_user_otp";
                $this->ablfunctions->insertData($data, $table);
            }

            // SMS content
            $message = $otp . ' is your one time password for verification.';

            $sms_url = $this->my_custom_functions->getSMSUrl();
            $this->ablfunctions->sendSMS($sms_url, $phone, $message);

            echo 1;
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Resend account verification email
    | --------------------------------------------------------------------------
    */
    public function sendVerificationEmail() {

        // Only logged in users can change the email
        if($this->session->userdata('user_id')) {

            $user_id = $this->session->userdata("user_id");
            $user_details = $this->ablfunctions->getDetailsFromId($user_id, "tbl_users");

            // Email verification token
            $verification_token = $this->my_custom_functions->createVerificationToken(USER_TYPE_USER, $user_id, TOKEN_TYPE_EMAIL_VERIFICATION);

            // Email verification link
            $verification_link = base_url() . 'main/emailVerification/' . $verification_token;

            $this->load->helper('file');

            // Email content
            $message = "";
            $message .= read_file("application/views/mail_template/mail_template_header.php");
            $message .= '<h1>Account Verification</h1>';
            $message .= '<p>';
            $message .= 'Hello '.$user_details['name'].',<br><br>';
            $message .= 'Please verify your account to use all the features.<br><br>';
            $message .= 'Please <a href="'.$verification_link.'">click here</a> to verify your account now.<br><br>Thank you.';
            $message .= '</p>';
            $message .= read_file("application/views/mail_template/mail_template_footer.php");

            // Send email
            $this->ablfunctions->SendEmail(SITE_EMAIL.','.SITE_NAME, $user_details['email'], SITE_NAME.' : Verify user account', $message);

            $this->session->set_flashdata("s_message", "An email has been sent to you to verify your account.");
            redirect("main/verifyAccount");
        } else {

            $this->session->set_flashdata("s_message", "Login time expired.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Email verification
    | --------------------------------------------------------------------------
    */
    public function emailVerification() {

        $verification_token = $this->uri->segment(3);

        if ($verification_token != "") {

            // Check the token's validity. It will return the token details if token is valid
            $verified = $this->my_custom_functions->validateVerificationToken(USER_TYPE_USER, TOKEN_TYPE_EMAIL_VERIFICATION, $verification_token);

            // If the verification link is valid
            if (!empty($verified)) {

                // Update email verification status
                $data = array(
                    "email_verification" => 1
                );

                $table = "tbl_common_login";

                $where = array(
                    "id" => $verified['user_id']
                );
                $updated = $this->ablfunctions->updateData($data, $table, $where);

                if ($updated) {

                    // Delete verification token
                    $table = "tbl_verification_tokens";

                    $where = array(
                        "id" => $verified['id']
                    );
                    $this->ablfunctions->deleteData($table, $where);

                    $this->session->set_flashdata("s_message", 'Your account has been verified successfully. You can access your account');
                    redirect("main");
                } else {

                    $this->session->set_flashdata("e_message", "Something went wrong. Please make a request to verify email address once again.");
                    redirect("main");
                }
            }
            // If the verification link is expired
            else {

                $this->session->set_flashdata("e_message", "The verification link is expired.");
                redirect("main");
            }
        } else {

            $this->session->set_flashdata("e_message", "Something went wrong. Please make a request to verify email address once again.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | User forgot password
    | --------------------------------------------------------------------------
    */
    public function forgotPassword() {

        if($this->input->post('submit') AND $this->input->post('submit') != "") {

            // CI form validation
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required');

            if($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata("e_message", validation_errors());
                redirect("main/forgotPassword");
            } else {

                // Verifying the username with database
                $username = $this->db->escape_str(strip_tags(trim($this->input->post("username"))));
                $verified = $this->ablfunctions->getParticularCount("tbl_common_login", " AND username='" . $username . "' AND status=1");

                // If the username already exists in database
                if($verified) {

                    $login_data = $this->ablfunctions->getDetailsFromId("", "tbl_common_login", array("username" => $username));
                    $user_details = $this->ablfunctions->getDetailsFromId($login_data['id'], "tbl_users");

                    // Password verification token
                    $verification_token = $this->my_custom_functions->createVerificationToken(USER_TYPE_USER, $user_details['id'], TOKEN_TYPE_FORGOT_PASSWORD);

                    // Password reset link
                    $reset_link = base_url() . 'main/resetPassword/' . $verification_token;

                    $this->load->helper('file');

                    // Email content
                    $message = "";
                    $message .= read_file("application/views/mail_template/mail_template_header.php");
                    $message .= '<h1>Forgot Password</h1>';
                    $message .= '<p>';
                    $message .= 'Hello '.$user_details['name'].',<br><br>';
                    $message .= 'You are receiving this email because you have requested for a change of password.<br><br>';
                    $message .= 'You may <a href="'.$reset_link.'">click here</a> to reset your password now.<br><br>Thank you.';
                    $message .= '</p>';
                    $message .= read_file("application/views/mail_template/mail_template_footer.php");

                    // Send email
                    $this->ablfunctions->SendEmail(SITE_EMAIL.','.SITE_NAME, $user_details['email'], SITE_NAME.' : Recover password', $message);

                    $this->session->set_flashdata("s_message", "A password recovery email has been sent to your email account.");
                    redirect("main/forgotPassword");
                }
                // If the username doesn't exist
                else {

                    $this->session->set_flashdata("e_message", "You are not a registered user!");
                    redirect("main/forgotPassword");
                }
            }
        } else {

            $this->load->view("forgot_password");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | User reset password
    | --------------------------------------------------------------------------
    */
    public function resetPassword() {

        $verification_token = $this->uri->segment(3);

        if ($verification_token != "") {

            // Check the token's validity. It will return the token details if token is valid
            $verified = $this->my_custom_functions->validateVerificationToken(USER_TYPE_USER, TOKEN_TYPE_FORGOT_PASSWORD, $verification_token);

            // If the verification link is valid
            if (!empty($verified)) {

                if($this->input->post('submit') AND $this->input->post('submit') != "") {

                    // CI form validation
                    $this->load->library('form_validation');

                    $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[32]');
                    $this->form_validation->set_rules('retype_new_password', 'Retype New Password', 'trim|required|min_length[6]|max_length[32]|matches[new_password]');

                    if ($this->form_validation->run() == FALSE) {

                        $this->session->set_flashdata("e_message", validation_errors());
                        redirect("resetPassword/" . $verification_token);
                    } else {

                        // Update password
                        $data = array(
                            "password" => password_hash($this->input->post('new_password'), PASSWORD_DEFAULT)
                        );

                        $table = "tbl_common_login";

                        $where = array(
                            "id" => $verified['user_id']
                        );
                        $this->ablfunctions->updateData($data, $table, $where);

                        // Delete verification token
                        $table = "tbl_verification_tokens";

                        $where = array(
                            "id" => $verified['id']
                        );
                        $this->ablfunctions->deleteData($table, $where);

                        $this->session->set_flashdata("s_message", "Your password has been changed successfully, you can login to your account using this new password.");
                        redirect("main");
                    }
                } else {

                    $this->load->view("reset_password");
                }
            }
            // If the verification link is expired
            else {

                $this->session->set_flashdata("e_message", "The verification link is expired.");
                redirect("main");
            }
        } else {

            $this->session->set_flashdata("e_message", "Something went wrong. Please make a request to reset the password once again.");
            redirect("main");
        }
    }

    /*
    | --------------------------------------------------------------------------
    | User logout
    | --------------------------------------------------------------------------
    */
    public function logout() {

        // Unset sessions
        $session_data = array('user_id', 'user_name', 'user_email', 'user_type');
        $this->session->unset_userdata($session_data);

        // Delete cookie
        if ($this->input->cookie('user_id', true)) {

            delete_cookie('user_id');
        }

        $this->session->set_flashdata("s_message", 'You are successfully logged out.');
        redirect("main");
    }
        /*
    | --------------------------------------------------------------------------
    | Add Student From Register
    | --------------------------------------------------------------------------
    */
    function studentFromRegister() {

        if ($this->input->post("submit") && $this->input->post("submit") != "") {

            //echo "<pre>";print_r ($_POST);die;


            $this->form_validation->set_rules("course_id", "Select Course", "required");
            //$this->form_validation->set_rules("session_id", "Select session", "required");
            $this->form_validation->set_rules("student_name", "Name of the Candidate", "trim|required");
            $this->form_validation->set_rules("father_or_mother_name", "Father’s /Mother’s Name", "trim|required");
            $this->form_validation->set_rules("guardian_name", "Name of the Guardian", "trim|required");
            $this->form_validation->set_rules("perm_address", "Permanent Address", "trim|required");
            $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
            //$this->form_validation->set_rules("phone_number", "Phone Number", "trim|required|is_unique[tbl_student_from.phone_number]");
            $this->form_validation->set_rules("phone_no", "Phone Number", "trim|required|regex_match[/^[0-9]{10}$/]");
            $this->form_validation->set_rules("corres_address", "Address for correspondence", "trim|required");
            $this->form_validation->set_rules("aadhar_no", "Aadhar No", "trim|required|min_length[12]|max_length[12]");
            $this->form_validation->set_rules("reg_mobile_no", "Registered Mobile No", "trim|required|regex_match[/^[0-9]{10}$/]");
            $this->form_validation->set_rules("dob", "D.O.B", "trim|required");

            //$this->form_validation->set_rules("student_passport_photo", "Passport Photo", "required");
            $this->form_validation->set_rules("age", "Age", "trim|required");
            $this->form_validation->set_rules("gender", "Gender", "required");
            $this->form_validation->set_rules("marital_status", "Marital Status", "required");
            $this->form_validation->set_rules("nationality", "nationality", "required");
            $this->form_validation->set_rules("caste", "Caste", "required");
            $this->form_validation->set_rules("local_address", "Local address of the candidate", "trim|required");
            $this->form_validation->set_rules("local_gu_name_adds", "Name & Address of the local guardian", "trim|required");
            $this->form_validation->set_rules("rules_condition_one", "Condition", "required");
            $this->form_validation->set_rules("rules_condition_two", "Condition", "required");
            $this->form_validation->set_rules("rules_condition_first", "Condition", "required");

            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata("e_message", validation_errors());
            // $data['session_details']= $this->Main_model->get_current_session();
            // $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_classes",'');
            // $this->load->view("student_from", $data);
            redirect("main/studentFromRegister");
            } else {

                if ($this->input->post('rules_condition_first')!='') {
                     $rules_condition_first=1;
                  }else{ $rules_condition_one=0; }
                if ($this->input->post('rules_condition_one')!='') {
                     $rules_condition_one=1;
                  }else{ $rules_condition_one=0; }
                if ($this->input->post('rules_condition_two')!='') {
                     $rules_condition_two=1;
                  }else{ $rules_condition_two=0; }

                $insert_data = array(
                    "student_id" => $this->session->userdata('user_id'),
                    "course_id" => $this->input->post("course_id"),
                    "session_id" => $this->input->post("session_id"),
                    "student_name" => $this->input->post("student_name"),
                    "father_or_mother_name" => $this->input->post("father_or_mother_name"),
                    "guardian_name" => $this->input->post("guardian_name"),
                    "perm_address" => $this->input->post("perm_address"),
                    "email" => $this->input->post("email"),
                    "phone_no" => $this->input->post("phone_no"),
                    "corres_address" => $this->input->post("corres_address"),
                    "aadhar_no" => $this->input->post("aadhar_no"),
                    "reg_mobile_no" => $this->input->post("reg_mobile_no"),
                    "dob" => $this->input->post("dob"),
                    "age" => $this->input->post("age"),
                    "gender" => $this->input->post("gender"),
                    "marital_status" => $this->input->post("marital_status"),
                    "nationality" => $this->input->post("nationality"),
                    "caste" => $this->input->post("caste"),
                    "local_address" => $this->input->post("local_address"),
                    "local_gu_name_adds" => $this->input->post("local_gu_name_adds"),
                    "date" => date(DATE_FORMAT),
                    "rules_condition_first" => $rules_condition_first,
                    "rules_condition_one" => $rules_condition_one,
                    "rules_condition_two" => $rules_condition_two,
                    "admit_card_type" =>0,
                    "status" => 1
                );

                $student_from_id = $this->my_custom_functions->insert_data($insert_data,"tbl_student_from");///////  insert to user table


                if ($student_from_id) {

                    if ($this->input->post('exam_pass_one')!='' AND $this->input->post('subject_one')!='' AND $this->input->post('year_pass_one')!='' AND $this->input->post('board_one')!='' AND$this->input->post('div_obt_one')!='') {

                        $exam_data = array(
                    "student_from_id" => $student_from_id,
                    "exam_passed" => $this->input->post("exam_pass_one"),
                    "subject" => $this->input->post("subject_one"),
                    "year_of_pass" => $this->input->post("year_pass_one"),
                    "board_coun_uni" => $this->input->post("board_one"),
                    "div_obt" => $this->input->post("div_obt_one")
                    );

                    $edu_table_id =$this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
                        if ($edu_table_id) {
                         if (isset($_FILES['student_document_one'])) {

                        if ($_FILES['student_document_one']['name'] != "") {

                            //$file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name = $edu_table_id.'_'.basename($_FILES['student_document_one']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('student_document_one')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                // Upload thumb image
                                // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                                // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                                // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                //Update database with the cover image url
                                $data = array(
                                    "document_url" => $file_name
                                );

                                $table = "tbl_edu_qualification";

                                $where = array(
                                    "id" => $edu_table_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                    }
                    }

                    if ($this->input->post('exam_pass_two')!='' AND $this->input->post('subject_two')!='' AND $this->input->post('year_pass_two')!='' AND $this->input->post('board_two')!='' AND$this->input->post('div_obt_two')!='') {

                        $exam_data = array(
                    "student_from_id" => $student_from_id,
                    "exam_passed" => $this->input->post("exam_pass_two"),
                    "subject" => $this->input->post("subject_two"),
                    "year_of_pass" => $this->input->post("year_pass_two"),
                    "board_coun_uni" => $this->input->post("board_two"),
                    "div_obt" => $this->input->post("div_obt_two")
                    );

                    $edu_table_id = $this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
                        if ($edu_table_id) {
                         if (isset($_FILES['student_document_two'])) {

                        if ($_FILES['student_document_two']['name'] != "") {

                            //$file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name = $edu_table_id.'_'.basename($_FILES['student_document_two']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('student_document_two')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                // Upload thumb image
                                // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                                // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                                // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                //Update database with the cover image url
                                $data = array(
                                    "document_url" => $file_name
                                );

                                $table = "tbl_edu_qualification";

                                $where = array(
                                    "id" => $edu_table_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                    }
                    }

                    if ($this->input->post('exam_pass_three')!='' AND $this->input->post('subject_three')!='' AND $this->input->post('year_pass_three')!='' AND $this->input->post('board_three')!='' AND$this->input->post('div_obt_three')!='') {

                        $exam_data = array(
                    "student_from_id" => $student_from_id,
                    "exam_passed" => $this->input->post("exam_pass_three"),
                    "subject" => $this->input->post("subject_three"),
                    "year_of_pass" => $this->input->post("year_pass_three"),
                    "board_coun_uni" => $this->input->post("board_three"),
                    "div_obt" => $this->input->post("div_obt_three")
                    );

                    $edu_table_id = $this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
                        if ($edu_table_id) {
                         if (isset($_FILES['student_document_three'])) {

                        if ($_FILES['student_document_three']['name'] != "") {

                            //$file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name = $edu_table_id.'_'.basename($_FILES['student_document_three']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('student_document_three')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                // Upload thumb image
                                // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                                // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                                // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                //Update database with the cover image url
                                $data = array(
                                    "document_url" => $file_name
                                );

                                $table = "tbl_edu_qualification";

                                $where = array(
                                    "id" => $edu_table_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                    }
                    }

                    if ($this->input->post('exam_pass_four')!='' AND $this->input->post('subject_four')!='' AND $this->input->post('year_pass_four')!='' AND $this->input->post('board_four')!='' AND$this->input->post('div_obt_four')!='') {

                        $exam_data = array(
                    "student_from_id" => $student_from_id,
                    "exam_passed" => $this->input->post("exam_pass_four"),
                    "subject" => $this->input->post("subject_four"),
                    "year_of_pass" => $this->input->post("year_pass_four"),
                    "board_coun_uni" => $this->input->post("board_four"),
                    "div_obt" => $this->input->post("div_obt_four")
                    );

                    $edu_table_id = $this->my_custom_functions->insert_data($exam_data,"tbl_edu_qualification");
                        if ($edu_table_id) {
                         if (isset($_FILES['student_document_four'])) {

                        if ($_FILES['student_document_four']['name'] != "") {

                            $file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            $file_name = $edu_table_id.'_'.basename($_FILES['student_document_four']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('student_document_four')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                // Upload thumb image
                                // $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                                // $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                                // $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                //Update database with the cover image url
                                $data = array(
                                    "document_url" => $file_name
                                );

                                $table = "tbl_edu_qualification";

                                $where = array(
                                    "id" => $edu_table_id
                                );
                                $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }
                    }
                    }


                    // Upload Passport Photo
                    $img_upload_message = '';
                    if (isset($_FILES['student_passport'])) {

                        if ($_FILES['student_passport']['name'] != "") {

                            $file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO;
                            $config['allowed_types']        = ALLOWED_PHOTO_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_PHOTO_IMAGE_SIZE;
                            // $config['max_width']  = PASSPORT_WIDTH;
                            // $config['max_height']  = PASSPORT_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('student_passport')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                //Upload thumb image
                                $source = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                                // Update database with the cover image url
                                // $data = array(
                                //     "cover_image_url" => $file_name
                                // );

                                // $table = "tbl_gallery_albums";

                                // $where = array(
                                //     "id" => $album_id
                                // );
                                // $this->ablfunctions->updateData($data, $table, $where);

                                // $img_upload_message = 'Cover image uploaded successfully.';
                            }
                        }
                    }

                     // Upload Original copies of mark sheets
                    // if (isset($_FILES['mark_sheets_xerox'])) {

                    //     if ($_FILES['mark_sheets_xerox']['name'] != "") {

                    //         $file_name = $student_from_id.'.'.'jpg';
                    //         //$file_name = $student_from_id.'_'.'jpg';
                    //         //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                    //         $config = array();
                    //         $config['upload_path']          = UPLOAD_DIR.STUDENT_MARKSHEET;
                    //         $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                    //         $config['file_name']            = $file_name;
                    //         $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                    //         $this->load->library('upload');
                    //         $this->upload->initialize($config);

                    //         if (!$this->upload->do_upload('mark_sheets_xerox')) {

                    //             $img_upload_message = $this->upload->display_errors();

                    //         } else {

                    //             // Upload thumb image
                    //             $source = UPLOAD_DIR.STUDENT_MARKSHEET.$file_name;
                    //             $destination = UPLOAD_DIR.STUDENT_MARKSHEET_THUMB.$file_name;

                    //             $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);

                    //             // Update database with the cover image url
                    //             // $data = array(
                    //             //     "cover_image_url" => $file_name
                    //             // );

                    //             // $table = "tbl_gallery_albums";

                    //             // $where = array(
                    //             //     "id" => $album_id
                    //             // );
                    //             // $this->ablfunctions->updateData($data, $table, $where);

                    //             // $img_upload_message = 'Cover image uploaded successfully.';
                    //         }
                    //     }
                    // }
                     // Upload Original copies of age verification
                    if (isset($_FILES['student_age_verification'])) {

                        if ($_FILES['student_age_verification']['name'] != "") {

                            $file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_AGE_PROOF;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('student_age_verification')) {

                                $img_upload_message = $this->upload->display_errors();

                            }
                            // else {
                            //
                            //     // Upload thumb image
                            //     $source = UPLOAD_DIR.STUDENT_AGE_PROOF.$file_name;
                            //     $destination = UPLOAD_DIR.STUDENT_AGE_PROOF_THUMB.$file_name;
                            //
                            //     $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                            //
                            // }
                        }
                    }
                     // Upload Original copies of SC/ST/OBC/Handicapped Certificate
                    if (isset($_FILES['student_caste_cf'])) {

                        if ($_FILES['student_caste_cf']['name'] != "") {

                            $file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_CASTE;
                            $config['allowed_types']        = ALLOWED_DOCUMENT_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_DOCUMENT_IMAGE_SIZE;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('student_caste_cf')) {

                                $img_upload_message = $this->upload->display_errors();

                            }
                            // else {
                            //
                            //     // Upload thumb image
                            //     $source = UPLOAD_DIR.STUDENT_CASTE.$file_name;
                            //     $destination = UPLOAD_DIR.STUDENT_CASTE_THUMB.$file_name;
                            //
                            //     $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);
                            //
                            //
                            // }
                        }
                    }
                     // Upload Countersignature of the Guardian
                    if (isset($_FILES['student_guardian_sign'])) {

                        if ($_FILES['student_guardian_sign']['name'] != "") {

                            $file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_GURDAIN_SIGN;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            // $config['max_width']  = SIGN_WIDTH;
                            // $config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('student_guardian_sign')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                // Upload thumb image
                                $source = UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);


                            }
                        }
                    }
                     // Upload Signature of Candidate
                    if (isset($_FILES['student_candidate_sign'])) {

                        if ($_FILES['student_candidate_sign']['name'] != "") {

                            $file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            //$config['max_width']  = SIGN_WIDTH;
                            //$config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('student_candidate_sign')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                //Upload thumb image
                                $source = UPLOAD_DIR.STUDENT_SIGN.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_SIGN_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);


                            }
                        }
                    }
                    // Upload Signature of Candidate in full
                    if (isset($_FILES['student_candidate_sign_full'])) {

                        if ($_FILES['student_candidate_sign_full']['name'] != "") {

                            $file_name = $student_from_id.'.'.'jpg';
                            //$file_name = $student_from_id.'_'.'jpg';
                            //$file_name = $student_from_id.'_'.basename($_FILES['cover_image_url']['name']);

                            $config = array();
                            $config['upload_path']          = UPLOAD_DIR.STUDENT_SIGN_FULL;
                            $config['allowed_types']        = ALLOWED_SIGN_IMAGE_TYPES;
                            $config['file_name']            = $file_name;
                            $config['max_size']             = ALLOWED_SIGN_IMAGE_SIZE;
                            //$config['max_width']  = SIGN_WIDTH;
                            //$config['max_height']  = SIGN_HEIGHT;

                            $this->load->library('upload');
                            $this->upload->initialize($config);

                            if (!$this->upload->do_upload('student_candidate_sign_full')) {

                                $img_upload_message = $this->upload->display_errors();

                            } else {

                                // Upload thumb image
                                $source = UPLOAD_DIR.STUDENT_SIGN_FULL.$file_name;
                                $destination = UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$file_name;

                                $this->ablfunctions->CreateFixedSizedImage($source, $destination, THUMB_WIDTH, THUMB_HEIGHT);


                            }
                        }

                    }

                    $this->session->set_flashdata("s_message", "You have successfully Register.".$img_upload_message);

                    //$this->session->set_flashdata("s_message", "You have successfully Register.");
                } else {

                    $this->session->set_flashdata("e_message", "Failed to Register! Please try again.");
                }
                echo "string";die;
                //redirect("main/studentFromRegister",$img_upload_message);
                redirect("user/dashboard",$img_upload_message);
            }
        } else {

            $data['session_details']= $this->Main_model->get_current_session();
            //echo "<pre>";print_r ($data['session_details']);die;
            //$data['session_details'] = $this->my_custom_functions->get_multiple_data("tbl_session",'');
            $data['all_course'] = $this->my_custom_functions->get_multiple_data("tbl_courses",'');
            $this->load->view("student_from", $data);
        }
    }
}
