<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /* ************************************************************************ */
    /* ************************************************************************ */
    
    function get_gateway_settings($gateway_key) {
                
        $this->db->where('gateway_key', $gateway_key);             
        $query = $this->db->get("tbl_gateway_settings");

        $data = array();
        if ($query->num_rows() > 0) {            
            $data = $query->row_array();            
        }

        return $data;
    }
    
    /* ************************************************************************ */
    /* ************************************************************************ */
    
    function save_gateway_settings($gateway_key, $gateway_data) {
                        
        // Check if the settings already present
        $this->db->where('gateway_key', $gateway_key);             
        $query = $this->db->get("tbl_gateway_settings");
                
        // Update existing settings
        if ($query->num_rows() > 0) {    
            
            $this->db->where('gateway_key', $gateway_key);
            $this->db->update('tbl_gateway_settings', $gateway_data);
        } 
        // Insert new settings
        else {
            
            $gateway_data['gateway_key'] = $gateway_key;
            $this->db->insert("tbl_gateway_settings", $gateway_data);
        }
        
        return true;
    }
                   
    /* ************************************************************************ */
    /* ************************************************************************ */
    
    function get_user_settings() {
        
        $this->db->where('id', '1');             
        $query = $this->db->get("tbl_user_settings");        

        $data = array();
        if ($query->num_rows() > 0) {            
            $data = $query->row_array();            
        }

        return $data;
    }
    
    /* ************************************************************************ */
    /* ************************************************************************ */
    
    function save_user_settings($settings_data) {                                            
        
        // Check if the settings already present
        $this->db->where('id', '1');             
        $query = $this->db->get("tbl_user_settings");    
        
        // Update existing settings
        if ($query->num_rows() > 0) {    
            
            $this->db->where('id', 1);
            $this->db->update('tbl_user_settings', $settings_data);
        } 
        // Insert new settings
        else {
                        
            $this->db->insert("tbl_user_settings", $settings_data);
        }
        
        return true;
    }
}

?>
