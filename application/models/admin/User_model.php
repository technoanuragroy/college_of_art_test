<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function update_profile_data($admin_id, $update_data) {

        $this->db->where('id', $admin_id);
        $this->db->update('tbl_admins', $update_data);

        return true;
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function insert_admin_data($insert_data) {

        $this->db->insert("tbl_admins", $insert_data);
        $admin_id = $this->db->insert_id();

        return $admin_id;
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function update_other_admin_data($admin_id, $update_data) {

        $this->db->where('id', $admin_id);
        $this->db->update('tbl_admins', $update_data);

        return true;
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function get_all_admins() {

        $this->db->where('id !=', $this->session->userdata('admin_id'));
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get("tbl_admins");

        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }

        return $data;
    }


}

?>
