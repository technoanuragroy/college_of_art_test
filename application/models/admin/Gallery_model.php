<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    /* ************************************************************************ */
    /* ************************************************************************ */

    function get_all_albums() {
                        
        $this->db->order_by('id', 'DESC');        
        $query = $this->db->get("tbl_gallery_albums");

        $data = array();
        if ($query->num_rows() > 0) {            
            $data = $query->result_array();            
        }

        return $data;
    }  
    
    /* ************************************************************************ */
    /* ************************************************************************ */

    function get_all_files() {
                        
        $this->db->order_by('id', 'DESC');        
        $query = $this->db->get("tbl_gallery_files");

        $data = array();
        if ($query->num_rows() > 0) {            
            $data = $query->result_array();            
        }

        return $data;
    } 
    
    /* ************************************************************************ */
    /* ************************************************************************ */

    function insert_album_data($insert_data) {
                
        $this->db->insert("tbl_gallery_albums", $insert_data);
        $album_id = $this->db->insert_id();

        return $album_id;
    }
    
    /* ************************************************************************ */
    /* ************************************************************************ */
    
    function insert_file_data($insert_data) {
        
        $this->db->insert("tbl_gallery_files", $insert_data);
        $file_id = $this->db->insert_id();

        return $file_id;
    }
    
    
    
    
    
    
    
    
    

    /* ************************************************************************ */
    /* ************************************************************************ */

    function update_profile_data($admin_id, $update_data) {                                
        
        $this->db->where('id', $admin_id);
        $this->db->update('tbl_admins', $update_data);

        return true;
    }                     
}

?>
