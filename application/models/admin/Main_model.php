<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function login_check($username, $password) {

        $this->db->where(
            array(
                "username" => $username,
                "status" => '1'
            )
        );
        $query = $this->db->get("tbl_admins");

        $record = array();
        if ($query->num_rows() > 0) {

            $record = $query->row_array();

            if(password_verify($password, $record['password'])) {

                // Session variables for admin
                $session_data = array(
                    "admin_id" => $record["id"],
                    "admin_name" => $record["name"],
                    "admin_email" => $record["email"],
                    "admin_type" => $record["type"],
                    "session_id" => "2",
                );
                $this->session->set_userdata($session_data);

                return TRUE;
            } else {

                return FALSE;
            }
        } else {

            return FALSE;
        }
    }
}

?>
