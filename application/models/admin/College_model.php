<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class College_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function get_all_cms_pages() {

        $this->db->order_by('id', 'DESC');
        $query = $this->db->get("tbl_cms_pages");

        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        }

        return $data;
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function insert_page_data($insert_data) {

        $this->db->insert("tbl_cms_pages", $insert_data);
        $page_id = $this->db->insert_id();

        return $page_id;
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function update_page_data($page_id, $update_data) {

        $this->db->where('id', $page_id);
        $this->db->update('tbl_cms_pages', $update_data);

        return true;
    }
    //////////////////////////////////////////////////////////////////

    function get_cl($e_id) {
        $this->db->where('emp_id', $e_id);
        $year =  date('Y');
        if(date("m")==01){ $year=$year-1; }
        $this->db->where('date_month >=', $year . "-01-01");
        $this->db->where('date_month <=', $year . "-12-31");
        $query = $this->db->get('tbl_attendance');
        return $query->result();
    }
    //////////////////////////////////////////////////////////////////
    function get_all_employee_details() {
    $this->db->where('status', 1);
    $this->db->order_by("staff_name", "asc");
    $query = $this->db->get('tbl_staff');
    return $query->result();
    }
    /////////////////////////////////////////////////////
    function get_attendance_details($emp_id, $date_month) {
    $this->db->where('emp_id', $emp_id);
    $this->db->where('date_month', $date_month);
    $query = $this->db->get('tbl_attendance');
    return $query->row();
    }
    //////////////////////////////////////////////////////////////////
    function get_ajax_emps($term) {
        return $query = $this->db->query("SELECT * FROM tbl_staff WHERE staff_name LIKE '" . $term . "%' ORDER BY staff_name limit 30");
    }
    //////////////////////////////////////////////////////////////////
    function get_attendance_report($id) {
    $this->db->where('emp_id', $id);
    $this->db->where('date_month >=', $this->input->post('from_date'));
    $this->db->where('date_month <=', $this->input->post('to_date'));
    $this->db->order_by("date_month", "asc");
    $query = $this->db->get('tbl_attendance');
    $result = $query->result();
    return $result;
    }
    //////////////////////////////////////////////////////////////////
    function get_educational_details($student_id) {
    $this->db->where('student_from_id',$student_id);
    $query = $this->db->get('tbl_edu_qualification');
    return $query->result_array();
    }
        /*
    | --------------------------------------------------------------------------
    | Get Current Session
    | --------------------------------------------------------------------------
    */
    function get_current_session() {
    $current_date=date('Y-m-d');
    $this->db->where('from_date <=',$current_date);
    $this->db->where('to_date >=',$current_date);
    $query = $this->db->get('tbl_session');
    return $query->row_array();
    //return $query->row();
    //return $query->result_array();
    }
    function get_exam_data_list($exam_id) {
        $data = array();
        $sql = "Select * from " . 'tbl_exam' . " where id = '" . $exam_id . "'";
        $query = $this->db->query($sql);
        $data['exam'] = $query->result_array();

        $sql1 = "Select * from " . 'tbl_exam_scores' . " where exam_id = '" . $exam_id . "'";
        $query1 = $this->db->query($sql1);
        $data['score_data'] = $query1->result_array();

        return $data;
    }
        ////////////////////////////////////////////////////////////////////////////

    function insert_fees_breakup() {

        $breakup_labels = $this->input->post("breakup_label");
        $months = $this->input->post("month");
        $labels = $this->input->post("label");
        $amounts = $this->input->post("amount");
        $options = $this->input->post("option");

        $fees_id = $this->input->post("fees_id");

        if (!empty($breakup_labels)) {
            foreach ($breakup_labels as $key => $breakup_label) {

                $month = $months[$key];

                $all_fees_labels = $labels[$key];
                $all_fees_amounts = $amounts[$key];
                $all_fees_options = $options[$key];

                $fees = array();
                foreach ($all_fees_labels as $keyy => $single_fees_label) {
                    $fees[] = array(
                        "label" => $single_fees_label,
                        "amount" => $all_fees_amounts[$keyy],
                        "option" => $all_fees_options[$keyy]
                    );
                }
                $fees_json = json_encode($fees);

                $breakup_data = array(
                    "fees_id" => $fees_id,
                    "month" => $month,
                    "breakup_label" => $breakup_label,
                    "fees" => $fees_json
                );

                $this->db->insert('tbl_fees_structure_breakups', $breakup_data);
            }
        }

        return true;
    }
        ////////////////////////////////////////////////////////////////////////////

    function update_fees_breakup() {

        $breakup_labels = $this->input->post("breakup_label");
        $months = $this->input->post("month");
        $labels = $this->input->post("label");
        $amounts = $this->input->post("amount");
        $options = $this->input->post("option");

        $fees_id = $this->input->post("fees_id");

        if (!empty($breakup_labels)) {
            foreach ($breakup_labels as $key => $breakup_label) {

                $month = $months[$key];

                $all_fees_labels = $labels[$key];
                $all_fees_amounts = $amounts[$key];
                $all_fees_options = $options[$key];

                $fees = array();
                foreach ($all_fees_labels as $keyy => $single_fees_label) {
                    $fees[] = array(
                        "label" => $single_fees_label,
                        "amount" => $all_fees_amounts[$keyy],
                        "option" => $all_fees_options[$keyy]
                    );
                }
                $fees_json = json_encode($fees);

                $breakup_data = array(
                    "fees_id" => $fees_id,
                    "month" => $month,
                    "breakup_label" => $breakup_label,
                    "fees" => $fees_json
                );

                $this->db->where('id', $key);
                $this->db->update(TBL_FEES_STRUCTURE_BREAKUPS, $breakup_data);
            }
        }

        return true;
    }
        ////////////////////////////////////////////////////////////////////////////

    function delete_fees_structure($fees_id) {

        $sql = 'SELECT id FROM ' . TBL_FEES_STRUCTURE_BREAKUPS . ' WHERE fees_id="' . $fees_id . '"';
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                // Deleting manual fees records if any
                $this->db->where('breakup_id', $row['id']);
                $delete = $this->db->delete(TBL_FEES_STRUCTURE_BREAKUPS_MANUAL);

                // Deleting breakup fees records if any
                $this->db->where('id', $row['id']);
                $delete = $this->db->delete(TBL_FEES_STRUCTURE_BREAKUPS);
            }
        }
        // Deleting main fees record
        $this->db->where('id', $fees_id);
        $delete = $this->db->delete(TBL_FEES_STRUCTURE);

        return $delete;
    }
        ////////////////////////////////////////////////////////////////////////////

    function update_fees_structure() {

        $fees_structure_data = array(
            'general_comment' => $this->input->post('comment'),
            'late_payment_setting' => $this->input->post('late_payment_setting'),
            'late_payment_day_limit' => $this->input->post('late_payment_day_limit'),
            'late_payment_type' => $this->input->post('late_payment_type'),
            'late_payment_amount' => $this->input->post('late_payment_amount')
        );

        $this->db->where('id', $this->ablfunctions->ablDecrypt($this->input->post('fees_id')));
        $this->db->update(TBL_FEES_STRUCTURE, $fees_structure_data);

        return true;
    }

    ////////////////////////////////////////////////////////////////////////////

    function enroll_students() {

        $student_list = $this->input->post('student');
        $roll_numbers = $this->input->post('roll_no');

        if(!empty($student_list)) {
          //echo "<pre>";print_r($student_list);die;
            foreach ($student_list as $key => $student_id) {
                //echo $student_id;die;
                $semester_id = $this->input->post('semester_id_move');
                $class_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'course_id', ' and id="' . $semester_id . '" ');
                $section_id = $this->input->post('section_id_move');
                $roll_no = '';

                if(array_key_exists($key, $roll_numbers)) {
                    $roll_no = $roll_numbers[$key];
                }

                $sql = 'SELECT * FROM ' . TBL_STUDENT_ENROLLMENT . ' WHERE student_id="'.$student_id.'" AND semester_id="'.$semester_id.'"';
                $query = $this->db->query($sql);

                // If the student is already enrolled in the semester, update its section and roll number
                if($query->num_rows() > 0) {
                    $update_data = array(
                        //"section_id" => $section_id,
                        "roll_no" => $roll_no,
                        "enrollment_time" => time()
                    );

                    $this->db->where(array("student_id" => $student_id, "semester_id" => $semester_id));
                    $this->db->update(TBL_STUDENT_ENROLLMENT, $update_data);
                }
                // Insert new enrollment
                else {
                    $insert_data = array(
                        //"school_id" => $this->session->userdata('school_id'),
                        "student_id" => $student_id,
                        "class_id" => $class_id,
                        "semester_id" => $semester_id,
                        "section_id" => $section_id,
                        "roll_no" => $roll_no,
                        "enrollment_time" => time()
                    );

                    $this->db->insert(TBL_STUDENT_ENROLLMENT, $insert_data);
                }
            }
        }

        return true;
    }

    ////////////////////////////////////////////////////////////////////////////

    function get_fees_breakups_for_manual_fees($fees_id, $student_id) {

        $sql = "SELECT * FROM " . TBL_FEES_STRUCTURE_BREAKUPS . " WHERE fees_id='" . $fees_id . "'";
        $query = $this->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {

                // Check if any manual fees exists
                $sqlmanual = 'SELECT * FROM ' . TBL_FEES_STRUCTURE_BREAKUPS_MANUAL . ' WHERE student_id="' . $student_id . '" AND breakup_id="' . $row['id'] . '"';
                $querymanual = $this->db->query($sqlmanual);

                if ($querymanual->num_rows() > 0) {
                    $resultmanual = $querymanual->row_array();
                    $row['fees'] = $resultmanual['fees'];
                }

                $result[] = $row;
            }
        }

        return $result;
    }

    ////////////////////////////////////////////////////////////////////////////

    function update_manual_fees_setting() {

        if ($this->input->post('manual_fees_setting')) {
            $manual_fees_setting = 1;
        } else {
            $manual_fees_setting = 0;
        }
        $setting_data = array(
            'manual_fees_setting' => $manual_fees_setting
        );

        $this->db->where('id', $this->input->post('setting_student_id'));
        $this->db->update(TBL_STUDENT, $setting_data);
    }

    ////////////////////////////////////////////////////////////////////////////

    function manuallly_update_fees_breakup() {

        $breakup_labels = $this->input->post("breakup_label");
        $labels = $this->input->post("label");
        $amounts = $this->input->post("amount");
        $options = $this->input->post("option");

        if (!empty($breakup_labels)) {
            foreach ($breakup_labels as $key => $breakup_label) {

                $all_fees_labels = $labels[$key];
                $all_fees_amounts = $amounts[$key];
                $all_fees_options = $options[$key];

                $fees = array();
                foreach ($all_fees_labels as $keyy => $single_fees_label) {
                    $fees[] = array(
                        "label" => $single_fees_label,
                        "amount" => $all_fees_amounts[$keyy],
                        "option" => $all_fees_options[$keyy]
                    );
                }
                $fees_json = json_encode($fees);

                // Check if manual breakup is already present for this child
                $sql = 'SELECT * FROM ' . TBL_FEES_STRUCTURE_BREAKUPS_MANUAL . ' WHERE student_id="' . $this->input->post("student_id") . '" AND breakup_id="' . $key . '"';
                $query = $this->db->query($sql);

                if ($query->num_rows() > 0) {
                    $manual_details = $query->row_array();
                    $manual_data = array(
                        'fees' => $fees_json
                    );
                    $this->db->where('id', $manual_details['id']);
                    $this->db->update(TBL_FEES_STRUCTURE_BREAKUPS_MANUAL, $manual_data);
                } else {
                    $manual_data = array(
                        'student_id' => $this->input->post("student_id"),
                        'semester_id' => $this->input->post("semester_id"),
                        'breakup_id' => $key,
                        'fees' => $fees_json
                    );
                    $this->db->insert(TBL_FEES_STRUCTURE_BREAKUPS_MANUAL, $manual_data);
                }
            }
        }

        return true;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
   function get_attendence_year_list() {
    $sql = "SELECT DISTINCT YEAR(date_month) AS att_years FROM tbl_attendance ORDER BY att_years desc";
    $query = $this->db->query($sql);
    return $query->result();
   }
   ///////////////////////////////////////////////////////
    function manage_payslip($month, $year) {

    $this->db->where('month(payslip_date)', $month);
    $this->db->where('year(payslip_date)', $year);
    //  $this->db->order_by("doj", "asc");
    $query = $this->db->get('tbl_emp_payslip');
    $result = $query->result();
    return $result;
    }
    ///////////////////////////////////////////////////////
    function manage_attendance($month, $year) {

        $this->db->where('month(date_month)', $month);
        $this->db->where('year(date_month)', $year);
        $query = $this->db->get('tbl_attendance');
        $result = $query->result();
        return $result;
    }
        ///////////////////////////////////////////////////////
    function get_cl_for_selected_year($year, $e_id) {

    $emp_details = $this->my_custom_functions->get_details_from_id("", "tbl_staff", array("id" => $e_id));

    $this->db->where('emp_id', $e_id);
    $this->db->where('date_month >=', $year . "-01-01");
    $this->db->where('date_month <=', $year . "-12-31");
    $query = $this->db->get('tbl_attendance');
    return $query->result();
}
  /////////////////////////////////////////////////////
  function get_attendance($id) {
      $this->db->where('id', $id);
      $query = $this->db->get('tbl_attendance');
      return $query->row();
  }
  //////////////////////////////////////////////

function select_emp_attend_details() {
    $emp_id = $_POST['emp_id'];
    $month = $_POST['month'];
    $year = $_POST['year'];
    $this->db->where('emp_id', $emp_id);
    $this->db->where('date_month >=', $year-$month-"-01");
    $this->db->where('date_month <=', date('Y-m-d'));
    $query = $this->db->get('tbl_attendance');
    $data = array();
    if ($query->num_rows() == 1) {
        $userdata = $query->row();
        $data = array(
            'attend_id' => $userdata->id,
            'present' => $userdata->present,
            'absent' => $userdata->absent,
            'pl' => $userdata->pl,
            'cl' => $userdata->cl,
            'sl' => $userdata->sl,
            'opl' => $userdata->opl,
            'working_days' => $userdata->working_days,
        );
    } else {
        $data = array(
            'present' => "0",
            'absent' => "0",
            'pl' => "0",
            'cl' => "0",
            'sl' => "0",
            'opl' => "0",
        );
    }

    return $data;
}

    //////////////////////////////////////////////////////////////////

    function get_cl_details() {
    $emp_id = $_POST['emp_id'];
    $month = $_POST['month'];
    $year = $_POST['year'];
    $this->db->where('emp_id', $e_id);

    $this->db->where('date_month >=', $year . $month . "-01");
    $this->db->where('date_month <=', date('Y-m-d'));
    $query = $this->db->get('tbl_attendance');
    return $query->result();
    }

    ///////////////////////////////////////////////
    function select_emp_details() {
        $emp_id = $_POST['emp_id'];
        $this->db->where('id', $emp_id);
        $query = $this->db->get('tbl_staff');
        $data = array();
        //echo $this->db->last_query(); die;
        //echo $query->num_rows(); die;
        if ($query->num_rows() == 1) {
            $userdata = $query->row();
            $data = array(
                'designation' => $userdata->designation,
                'basic_pay' => $userdata->gross_salary,
                'grade_pay' => $userdata->grade_pay,
                'med_allow' => $userdata->med_allow
            );
        }
        //print_r($data); die;
        return $data;
    }
    /////////////////////////////////////////////////////
      function get_payslip_details($emp_id, $date_month) {
          $this->db->where('emp_id', $emp_id);
          $this->db->where('payslip_date', $date_month);
          $query = $this->db->get('tbl_emp_payslip');
          return $query->row();
      }
      function get_payslip_report($id) {
        $this->db->where('emp_id', $id);
        $this->db->where('payslip_date >=', $this->input->post('from_date'));
        $this->db->where('payslip_date <=', $this->input->post('to_date'));
        $query = $this->db->get('tbl_emp_payslip');
        $result = $query->result();
        return $result;
      }
      function get_payslip_report_excel($emp_id,$from_date,$to_date) {
        $this->db->where('emp_id', $emp_id);
        $this->db->where('payslip_date >=', $from_date);
        $this->db->where('payslip_date <=', $to_date);
        $query = $this->db->get('tbl_emp_payslip');
        $result = $query->result();
        return $result;
      }
      function payment_report($from_date,$to_date) {
        $this->db->where('join_date >=', $from_date);
        $this->db->where('join_date <=', $to_date);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get('tbl_student');
        $result = $query->result();
        return $result;
      }
      function payment_received_report($from_date,$to_date) {
        $this->db->where('date >=', $from_date);
        $this->db->where('date <=', $to_date);
        $query = $this->db->get('tbl_payment_received');
        $result = $query->result();
        return $result;
      }
      function payment_expense_report($from_date,$to_date) {
        $this->db->where('date >=', $from_date);
        $this->db->where('date <=', $to_date);
        $query = $this->db->get('tbl_college_expenses');
        $result = $query->result();
        return $result;
      }
    function get_payment_details($email,$phone) {
      $this->db->where('email', $email);
      $this->db->where('phone', $phone);
      $query = $this->db->get('tbl_student');
      return $query->row();
     }
     function get_time_table_data($semester_id, $section_id) {
         $create_data = array();
         $sql = 'SELECT * FROM ' . TBL_TIMETABLE . ' WHERE semester_id = "' . $semester_id . '" and section_id = "' . $section_id . '" order by period_start_time ASC';
         $query = $this->db->query($sql);
         $result = $query->result_array();

         $day_list = $this->config->item('days_list');
 //$period_list = $this->my_custom_functions->get_multiple_data(TBL_PERIODS, 'and school_id = "' . $this->session->userdata('school_id') . '" and status = 1');

         foreach ($day_list as $day => $val) {
 //foreach ($period_list as $ress) {
 //$create_data[$day][$ress['id']] = '';
             $counter = 0;
             foreach ($result as $routine) {
                 if ($day == $routine['day_id']) {
                     $create_data[$day][$counter] = $routine;
                 }

                 $counter++;
             }
 //}
         }
 //echo '<pre>';print_r($create_data);die;
         return $create_data;
     }
     ////////////////////////////////////////////////////////////////////////////

     function get_teacher_list_temp_assign($day_num) {

         $class_detail = array();
         $sql = "SELECT * from " . TBL_TIMETABLE . " where day_id = '" . $day_num . "' and teacher_id = '" . $this->input->post('teacher_id') . "' order by period_start_time ASC";
         $query = $this->db->query($sql);
         if ($query->num_rows() > 0) {
             $teacher_list = $this->my_custom_functions->get_multiple_data('tbl_staff', 'and designation = "2"');

             // echo "<pre>";print_r($teacher_list);
             foreach ($query->result_array() as $row) {//echo "<pre>";print_r($row);
                 $class_detail[$row['id']]['class_detail'] = $row;

                 foreach ($teacher_list as $teachers) {

                     if ($this->input->post('available_teacher') && $this->input->post('available_teacher') == 1) {

                         $check_availablity = $this->my_custom_functions->get_perticular_count(TBL_TIMETABLE, 'and day_id = "' . $day_num . '" and teacher_id = "' . $teachers['id'] . '" and period_start_time >="' . $row['period_start_time'] . '" and period_end_time <="' . $row['period_end_time'] . '"');
                         if ($check_availablity == 0) {
                             $class_detail[$row['id']]['teacher_list'][] = $teachers;
                         }
                     } else {

                         $class_detail[$row['id']]['teacher_list'][] = $teachers;
                     }
                 }
             }
         }
         //        echo "<pre>";
         //        print_r($class_detail);
         //        die;
         return $class_detail;
     }
     function get_exam_list($term_id) {

         $sql = "Select id,exam_name from " . TBL_EXAM . " where term_id  = '" . $term_id . "'";
         $query = $this->db->query($sql);
         $result = array();
         $terms = '';
         if ($query->num_rows() > 0) {
             $result = $query->result_array();
             return $result;
         }
     }
     function get_student_data($semester_id) {
         $result = array();
         //$semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and school_id = "' . $this->session->userdata('school_id') . '" and class_id = "' . $class_id . '" and session_id = "' . $session_id . '"');

         $sql = "select t_s.student_name,t_s.id  from " . 'tbl_student_from' . " t_s join " . TBL_STUDENT_ENROLLMENT . " t_s_e on t_s.id = t_s_e.student_id  where  t_s_e.semester_id = '" . $semester_id . "' order by t_s_e.roll_no ASC";

         $query = $this->db->query($sql);
         if ($query->num_rows() > 0) {
             $result = $query->result_array();
             //echo "<pre>";print_r($result);die;
             return $result;
         }
     }
     ////////////////////////////////////////////////////////////////////////////

     function student_detail($student_id) {
         $sql = "select t_s.*,t_s_e.* from " . 'tbl_student_from' . " t_s join " . TBL_STUDENT_ENROLLMENT . " t_s_e on t_s.id = t_s_e.student_id where t_s.id = '" . $student_id . "'";
         $query = $this->db->query($sql);
         $result = $query->result_array();
         return $result;
     }
     function score_card_detail($exam_id) {
         $sql = "Select * from " . TBL_EXAM_SCORES . " where exam_id = '" . $exam_id . "'";
         $query = $this->db->query($sql);
         $result = $query->result_array();
         //echo "<pre>";print_r($result);
         return $result;
     }





}

?>
