<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cms_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }    
    
    /* ************************************************************************ */
    /* ************************************************************************ */
    
    function get_all_cms_pages() {
        
        $this->db->order_by('id', 'DESC');        
        $query = $this->db->get("tbl_cms_pages");

        $data = array();
        if ($query->num_rows() > 0) {            
            $data = $query->result_array();            
        }

        return $data;
    }
            
    /* ************************************************************************ */
    /* ************************************************************************ */

    function insert_page_data($insert_data) {
                
        $this->db->insert("tbl_cms_pages", $insert_data);
        $page_id = $this->db->insert_id();

        return $page_id;
    }
    
    /* ************************************************************************ */
    /* ************************************************************************ */
    
    function update_page_data($page_id, $update_data) {
        
        $this->db->where('id', $page_id);
        $this->db->update('tbl_cms_pages', $update_data);

        return true;
    }
}

?>
