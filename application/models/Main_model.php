<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function login_check($ip, $username, $password) {

        $this->db->where(
            array(
                "username" => $username,
                "type" => LOGIN_TYPE_USER,
                "status" => '1'
            )
        );
        $query = $this->db->get("tbl_common_login");

        $login_data = array();
        if ($query->num_rows() > 0) {

            $login_data = $query->row_array();

            if(password_verify($password, $login_data['password'])) {

                $user_details = $this->ablfunctions->getDetailsFromId($login_data['id'], "tbl_student");

                if(!empty($user_details)) {

                    // Session variables for user
                    $session_data = array(
                        "user_id" => $login_data["id"],
                        "student_id" => $login_data["id"],
                        "user_name" => $user_details["name"],
                        "user_email" => $user_details["email"],
                        "user_type" => $login_data['type'],

                    );
                    $this->session->set_userdata($session_data);

                    return TRUE;
                } else {

                    return FALSE;
                }
            } else {

                return FALSE;
            }
        } else {

            return FALSE;
        }
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function insert_user_data($login_data, $user_data) {

        $this->db->insert("tbl_common_login", $login_data);
        $user_id = $this->db->insert_id();

        $user_data["id"] = $user_id;
        $this->db->insert("tbl_users", $user_data);

        return $user_id;
    }
    function insert_student_data($login_data, $user_data) {

        $this->db->insert("tbl_common_login", $login_data);
        $user_id = $this->db->insert_id();

        $user_data["id"] = $user_id;
        $this->db->insert("tbl_student", $user_data);

        $payment_data = array(
            "student_id" => $user_id,
            "admission_from_payment" => 0,
            "admission_payment" => 0,
        );
        $this->db->insert("tbl_student_payment_status", $payment_data);


        return $user_id;
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function get_user_settings() {

        $this->db->where('id', '1');
        $query = $this->db->get("tbl_user_settings");

        $data = array();
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
        }

        return $data;
    }
    /*
    | --------------------------------------------------------------------------
    | Get Current Session
    | --------------------------------------------------------------------------
    */
    function get_current_session() {
        $current_date=date('Y-m-d');
        $this->db->where('from_date <=',$current_date);
        $this->db->where('to_date >=',$current_date);
        $query = $this->db->get('tbl_session');
        return $query->row_array();
        //return $query->row();
        //return $query->result_array();
        }
}

?>
