<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /* ************************************************************************ */
    /* ************************************************************************ */

    function update_profile_data($user_id, $update_data) {

        $this->db->where('id', $user_id);
        $this->db->update('tbl_users', $update_data);

        return true;
    }
    ////////////////////////////////////////////////////////////////////////////

    function get_active_payment_gateways() {

        $sql = "SELECT * FROM " . TBL_PAYMENT_GATEWAY . " WHERE status=1";
        $query = $this->db->query($sql);

        $result = array();
        $result = $query->result_array();

        return $result;
    }
    ////////////////////////////////////////////////////////////////////////////

    function get_fees_detail() {

        $result = array();
        $student_old_id = $this->my_custom_functions->get_particular_field_value("tbl_student_from", "id", " and student_id ='".$this->session->userdata('student_id')."' LIMIT 1");
        if ($student_old_id=='') {
          $student_id = 0;
        }else{
          $student_id = $student_old_id;
        }
        $student_semesters = $this->my_custom_functions->get_semesters_of_a_student($student_id, $this->session->userdata('session_id'));

        if (!empty($student_semesters)) {
            foreach ($student_semesters as $semester) {

                $sql = "SELECT * FROM " . TBL_FEES_STRUCTURE . " WHERE semester_id='" . $semester['id'] . "'";
                $query = $this->db->query($sql);

                if ($query->num_rows() > 0) {

                    $result[$semester['id']]['semester_name'] = $semester['semester_name'];
                    $result[$semester['id']]['structure'] = $query->row_array();
                    $result[$semester['id']]['break_ups'] = array();

                    $sqlbrkup = "SELECT * FROM " . TBL_FEES_STRUCTURE_BREAKUPS . " WHERE fees_id='" . $result[$semester['id']]['structure']['id'] . "' ORDER BY month ASC";
                    $querybrkup = $this->db->query($sqlbrkup);

                    if ($querybrkup->num_rows() > 0) {
                        foreach ($querybrkup->result_array() as $row) {

                            // Check if the fees is paid already
                            $paid = 0;
                            $paid = $this->my_custom_functions->get_perticular_count(TBL_FEES_PAYMENT, 'and fees_id="' . $row['fees_id'] . '" and fees_breakup_id="' . $row['id'] . '"  and student_id="' . $this->session->userdata('student_id') . '" and payment_status=1');

                            if ($paid == 0) {
                                $result[$semester['id']]['break_ups'][] = $row;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }
    ////////////////////////////////////////////////////////////////////////////

    function get_fees_breakup_details($breakup_id) {

        $sql = "SELECT * FROM " . TBL_FEES_STRUCTURE_BREAKUPS . " WHERE id='" . $breakup_id . "'";
        $query = $this->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();

            // Check if manual fees is enabled for student
            $sqlsetting = 'SELECT manual_fees_setting FROM ' . TBL_STUDENT . ' WHERE id="' . $this->session->userdata('student_id') . '"';
            $querysetting = $this->db->query($sqlsetting);
            $setting = $querysetting->row_array();

            if ($setting['manual_fees_setting'] == 1) {

                $sqlmanual = 'SELECT * FROM ' . TBL_FEES_STRUCTURE_BREAKUPS_MANUAL . ' WHERE student_id="' . $this->session->userdata('student_id') . '" AND breakup_id="' . $breakup_id . '"';
                $querymanual = $this->db->query($sqlmanual);

                if ($querymanual->num_rows() > 0) {
                    $resultmanual = $querymanual->row_array();
                    $result['manual_breakup_id'] = $resultmanual['id'];
                    $result['fees'] = $resultmanual['fees'];
                }
            }
        }

        return $result;
    }
    ////////////////////////////////////////////////////////////////////////////

    function get_student_result($student_id,$class_id) {


        $session_id = $this->session->userdata('session_id');

        $semester_id = $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'id', 'and course_id="' . $class_id . '" and session_id="' . $session_id . '" ');

        $sql = "SELECT * FROM " . TBL_TERMS . " WHERE semester_id='" . $semester_id . "'";
        $query = $this->db->query($sql);

        $terms = array();
        $subjects = array();

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {

                $term_id = $row['id'];

                // Main term record
                $terms[$term_id] = $row;
                $terms[$term_id]['exams'] = array();

                $sql_exam = "SELECT t_e.id as exam_id, t_e.exam_name, t_e_s.subject_id, t_e_s.full_marks, t_e_s.pass_marks, t_s.score FROM "
                        . TBL_SCORE . " t_s INNER JOIN " . TBL_EXAM_SCORES . " t_e_s ON t_s.score_id=t_e_s.id "
                        . "INNER JOIN " . TBL_EXAM . " t_e ON t_e_s.exam_id=t_e.id "
                        . "WHERE t_s.student_id='" . $student_id . "' and t_e.term_id='" . $row['id'] . "' ";
                $query_exam = $this->db->query($sql_exam);

                if ($query_exam->num_rows() > 0) {
                    foreach($query_exam->result_array() as $e_s) {

                        $exam_id = $e_s['exam_id'];
                        $subject_id = $e_s['subject_id'];

                        $terms[$term_id]['exams'][$exam_id]['exam_name'] = $e_s['exam_name'];
                        $terms[$term_id]['exams'][$exam_id]['subjects'][$subject_id] = $e_s;

                        if(!array_key_exists($subject_id, $subjects)) {
                            $subjects[$subject_id] = $this->my_custom_functions->get_particular_field_value(TBL_SUBJECT, 'subject_name', 'and id="' . $subject_id . '"');
                        }
                    }
                }
            }
        }

        return array(
            'terms' => $terms,
            'subjects' => $subjects,
            'semester_detail' => $this->my_custom_functions->get_details_from_id($semester_id, TBL_SEMESTER)
        );
    }
    //////////////////////////////////////////////////////////////////
    function get_educational_details($student_id) {
    $this->db->where('student_from_id',$student_id);
    $query = $this->db->get('tbl_edu_qualification');
    return $query->result_array();
    }
}

?>
