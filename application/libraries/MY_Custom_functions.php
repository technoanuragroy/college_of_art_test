<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Custom_functions {

    public $CI;

    function __construct() {

        // Do something common to all functions
        $this->CI = & get_instance();
    }

    /*
    | -------------------------------------------------------------------
    | Create verification token
    | Parameters:
    | @$user_type => USER_TYPE_ADMIN, USER_TYPE_USER
    | @$user_id => admin/user table id
    | @$token_type => TOKEN_TYPE_EMAIL_VERIFICATION, TOKEN_TYPE_FORGOT_PASSWORD
    | -------------------------------------------------------------------
    */
    function createVerificationToken($user_type, $user_id, $token_type) {

        // Validate the token's expiry time with current time
        $time = date(DATETIME_FORMAT);

        $sql = "SELECT * FROM tbl_verification_tokens WHERE user_type='".$user_type."' AND user_id='".$user_id."' AND token_type='".$token_type."' AND expiry_time<'".$time."'";
        $query = $this->CI->db->query($sql);

        $token = "";

        // If there exists an expired token, delete it, create a new token
        if ($query->num_rows() > 0) {

            // Delete existing expired token
            $res = $query->row_array();
            $this->CI->db->where('id', $res['id']);
            $this->CI->db->delete('tbl_verification_tokens');

            // Create new verification token
            $token = $this->getVerificationTokenCode();
            $expiry_duration = '+'.TOKEN_EXPIRY_TIME.' minutes';

            $token_data = array(
                'user_type' => $user_type,
                'user_id' => $user_id,
                'token_type' => $token_type,
                'token' => $token,
                'expiry_time' => date(DATETIME_FORMAT, strtotime($expiry_duration))
            );
            $this->CI->db->insert("tbl_verification_tokens", $token_data);
        }
        // Create a new token if there is no token
        else {

            $sql = "SELECT * FROM tbl_verification_tokens WHERE user_type='".$user_type."' AND user_id='".$user_id."' AND token_type='".$token_type."'";
            $query = $this->CI->db->query($sql);

            if ($query->num_rows() == 0) {

                // Create new verification token
                $token = $this->getVerificationTokenCode();
                $expiry_duration = '+'.TOKEN_EXPIRY_TIME.' minutes';

                $token_data = array(
                    'user_type' => $user_type,
                    'user_id' => $user_id,
                    'token_type' => $token_type,
                    'token' => $token,
                    'expiry_time' => date(DATETIME_FORMAT, strtotime($expiry_duration))
                );
                $this->CI->db->insert("tbl_verification_tokens", $token_data);
            } else {

                $token_details = $query->row_array();
                $token = $token_details['token'];
            }
        }

        return $token;
    }

    /*
    | -------------------------------------------------------------------
    | Validate verification token
    | Parameters:
    | @$token_type => TOKEN_TYPE_EMAIL_VERIFICATION, TOKEN_TYPE_FORGOT_PASSWORD
    | @$token => Token string
    | -------------------------------------------------------------------
    */
    function validateVerificationToken($user_type, $token_type, $token) {

        // Validate the token's expiry time with current time
        $time = date(DATETIME_FORMAT);

        $sql = "SELECT * FROM tbl_verification_tokens WHERE user_type='".$user_type."' AND token_type='".$token_type."' AND token LIKE BINARY '".$token."' AND expiry_time>'".$time."'";
        $query = $this->CI->db->query($sql);

        $token_details = array();
        if ($query->num_rows() > 0) {

            $token_details = $query->row_array();
        }

        return $token_details;
    }

    /*
    | --------------------------------------------------------------------------
    | Get random and unique token code for verification
    | --------------------------------------------------------------------------
    */
    function getVerificationTokenCode() {

        //$token = bin2hex(random_bytes(20));
        $token = $this->random_bytes(4);

        // If the token already exists, call this function recursively to get a new token
        $sql = "SELECT * FROM tbl_verification_tokens WHERE token LIKE BINARY '".$token."'";
        $query = $this->CI->db->query($sql);

        if ($query->num_rows() > 0) {

            $token = $this->getVerificationTokenCode();
        }

        return $token;
    }
        // function get_verification_token_code() {
    //     return $token = bin2hex(random_bytes(20));
    // }
    // function get_verification_token_code() {
    //     return $token = $this->random_bytes(20);
    // }

function random_bytes($length = 6) { $characters = '0123456789'; $characters_length = strlen($characters); $output = ''; for ($i = 0; $i < $length; $i++) $output .= $characters[rand(0, $characters_length - 1)]; return $output; }

    /*
    | --------------------------------------------------------------------------
    | Get user public IP address
    | --------------------------------------------------------------------------
    */
    function getUserIP() {

        $ipaddress = '';

        if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else
            $ipaddress = 'Unknown IP Address';

        return $ipaddress;
    }

    /*
    | -------------------------------------------------------------------
    | Check admin security
    | -------------------------------------------------------------------
    */
    public function checkAdminSecurity() {

        if (!$this->CI->session->userdata('admin_id')) {
            redirect('admin');
        }
    }

    /*
    | -------------------------------------------------------------------
    | Check user security
    | -------------------------------------------------------------------
    */
    public function checkUserSecurity() {

        if (!$this->CI->session->userdata('user_id')) {
            redirect('main');
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Check user verification status
    | Parameters:
    | @$user_id => User id
    | --------------------------------------------------------------------------
    */
    public function checkUserVerificationStatus() {

        $user_id = $this->CI->session->userdata("user_id");

        $settings = array();
        $verification_status = array();

        // User settings set by admin
        $sql = "SELECT * FROM tbl_user_settings WHERE id='1'";
        $query = $this->CI->db->query($sql);

        if ($query->num_rows() > 0) {
            $settings = $query->row_array();
        }

        // User verification status
        $sql = "SELECT email_verification, otp_verification FROM tbl_common_login WHERE id='".$user_id."'";
        $query = $this->CI->db->query($sql);

        if ($query->num_rows() > 0) {
            $verification_status = $query->row_array();
        }

        $verified = TRUE;

        // Email verification status
        if($settings['email_verification'] == 1 AND $verification_status['email_verification'] == 0) {

            $verified = FALSE;
        }

        // OTP verification status
        if($settings['otp_verification'] == 1 AND $verification_status['otp_verification'] == 0) {

            $verified = FALSE;
        }

        if($verified === FALSE) {

            redirect('main/verifyAccount');
        }
    }

    /*
    | --------------------------------------------------------------------------
    | Generate OTP
    | Parameters:
    | @$length => Length of the OTP, default length = 4
    | @$chars => The numbers on which the OTP is created
    | --------------------------------------------------------------------------
    */
    function generateOTP($length = 4, $chars = '123456789') {

        $chars_length = (strlen($chars) - 1);
        $string = $chars{rand(0, $chars_length)};

        for ($i = 1; $i < $length; $i = strlen($string)) {
            $r = $chars{rand(0, $chars_length)};
            if ($r != $string{$i - 1})
                $string .= $r;
        }

        return $string;
    }

    /*
    | --------------------------------------------------------------------------
    | Get email config
    | --------------------------------------------------------------------------
    */
    function getEmailConfig() {

        $sql = "SELECT * FROM tbl_gateway_settings WHERE gateway_key='".SETTINGS_EMAIL_KEY."'";
        $query = $this->CI->db->query($sql);

        $settings = array();
        if ($query->num_rows() > 0) {
            $result = $query->row_array();

            // If SMS gateway is enabled
            if($result['gateway_settings'] == 1) {

                $settings = json_decode($result['gateway_config'], true);
            }
        }

        return $settings;
    }

    /*
    | --------------------------------------------------------------------------
    | Get SMS Url
    | --------------------------------------------------------------------------
    */
    function getSMSUrl() {

        $sql = "SELECT * FROM tbl_gateway_settings WHERE gateway_key='".SETTINGS_SMS_KEY."'";
        $query = $this->CI->db->query($sql);

        $sms_url = "";
        if ($query->num_rows() > 0) {
            $result = $query->row_array();

            // If SMS gateway is enabled
            if($result['gateway_settings'] == 1) {

                $settings = json_decode($result['gateway_config'], true);

                foreach($settings as $variable_name => $variable_value) {

                    if($variable_value != "") {
                        if($variable_name == "sms_url") {
                            $sms_url .= $variable_value;
                        } else {
                            $sms_url .= $variable_name."=".$variable_value."&";
                        }
                    }
                }
            }
        }

        return $sms_url;
    }

    /*
    | --------------------------------------------------------------------------
    | Get Gallery Image Url
    | --------------------------------------------------------------------------
    */
    function getGalleryImageUrl($img_url = "") {

        $img_urls['image_url'] = base_url().GALLERY_PLACEHOLDER_IMAGE;
        $img_urls['thumb_url'] = base_url().GALLERY_PLACEHOLDER_IMAGE;
        $img_urls['exists'] = "false";

        if($img_url != "") {

            $array_pursed_url = parse_url($img_url);

            if(!empty($array_pursed_url['scheme'])) {

                $img_urls['image_url'] = $img_url;
                $img_urls['thumb_url'] = $img_url;
                $img_urls['exists'] = "true";

            } else{

                $original_img_url = UPLOAD_DIR.GALLERY_DIR.$img_url;
                $thumnail_img_url = UPLOAD_DIR.GALLERY_THUMB_DIR.$img_url;

                if(file_exists($original_img_url) AND file_exists($thumnail_img_url)) {

                    $img_urls['image_url'] = base_url().$original_img_url;
                    $img_urls['thumb_url'] = base_url().$thumnail_img_url;
                    $img_urls['exists'] = "true";
                }
            }
        }

        return $img_urls;
    }
    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

      public function get_particular_field_value($tablename, $fieldname, $where = "") {

          $CI = & get_instance();
           $str = "select " . $fieldname . " from " . $tablename . " where 1=1 " . $where;

          $query = $CI->db->query($str);
          $record = "";
          if ($query->num_rows() > 0) {

              foreach ($query->result_array() as $row) {
                  $field_arr = explode(" as ", $fieldname);
                  if (count($field_arr) > 1) {
                      $fieldname = $field_arr[1];
                  } else {
                      $fieldname = $field_arr[0];
                  }
                  $record = $row[$fieldname];
              }
          }
          return $record;
      }

      /*     * *********************************************************************** */
      /*     * *********************************************************************** */

      function get_details_from_id($id = "", $table, $extra = array()) {

          $CI = & get_instance();
          if ($id != "") {
              $CI->db->where("id", $id);
          }
          if (count($extra) > 0) {
              $CI->db->where($extra);
          }
          $query = $CI->db->get($table);

          $record = array();
          if ($query->num_rows() > 0) {
              $record = $query->row_array();
          }
          return $record;
      }

      /*     * *********************************************************************** */
      /*     * *********************************************************************** */

      function get_multiple_data($table = "", $where = "") {

          $CI = & get_instance();
          $str = "select * from " . $table . " where 1=1 " . $where;

          $query = $CI->db->query($str);

          $record = array();
          $count = 0;
          foreach ($query->result_array() as $row) {
              $record[$count] = $row;
              $count++;
          }
          return $record;
      }

      /*     * *********************************************************************** */
      /*     * *********************************************************************** */
      /*     * *********************************************************************** */
      /*     * *********************************************************************** */

        function insert_data($data, $table) {

            $CI = & get_instance();
            $CI->db->insert($table, $data);
            return $CI->db->insert_id();
            //return 1;
        }

        function insert_data_last_id($data, $table) {

            $CI = & get_instance();
            $CI->db->insert($table, $data);
            return $CI->db->insert_id();
        }
        /*     * *********************************************************************** */
          /*     * *********************************************************************** */

          function update_data($data, $table, $where) {

              $CI = & get_instance();
              $CI->db->where($where);
              $CI->db->update($table, $data);
              return 1;
          }

          /*     * *********************************************************************** */
          /*     * *********************************************************************** */

    public function get_perticular_count($tablename, $where = "") {

        $CI = & get_instance();
        $str = "select * from " . $tablename . " where 1=1 " . $where;

        $query = $CI->db->query($str);
        $record = $query->num_rows();
        return $record;
    }
    function delete_data($table, $where = array()) {

        $CI = & get_instance();
        $CI->db->where($where);
        $CI->db->delete($table);
        return 1;
    }
        /////////////////////////////////////////////////////////////
    //  Function to convert view date to DATABASE format DATE
    /////////////////////////////////////////////////////////////
    function database_date($date) {
        $date_array = explode("/", $date);
        $new_date = @$date_array[2] . '-' . @$date_array[1] . '-' . @$date_array[0];
        //echo $new_date;die;
        return $new_date;
    }

    function database_date_dash($date) {
        $date_array = explode("-", $date);
        $new_date = @$date_array[2] . '-' . @$date_array[1] . '-' . @$date_array[0];
        //echo $new_date;die;
        return $new_date;
    }
        ////////////////////////////////////////////////////////////////////////////

    function get_general_breakup_of_months($fees_structure_id) {

        $fees_details = $this->get_details_from_id($fees_structure_id, TBL_FEES_STRUCTURE);
        $semester_details = $this->get_details_from_id($fees_details['semester_id'], TBL_SEMESTER);
        $session_details = $this->get_details_from_id($semester_details['session_id'], TBL_SESSION);

        $break_up_structure = array();
        $cycle_type = $fees_details['cycle_type'];
        $cycle_start = $session_details['from_date'];
        $cycle_end = $session_details['to_date'];

        // Monthly breakup
        if ($cycle_type == 1) {
            for ($i = $cycle_start; $i <= $cycle_end; $i = date("Y-m-d", strtotime("+ 1 months", strtotime($i)))) {

                if (strtotime($i) <= strtotime($cycle_end)) {

                    $label = date("F, Y", strtotime($i));
                } else {

                    $label = date("F, Y", strtotime($cycle_end));
                }
                $break_up_structure[$i] = array(
                    "start_month" => date("Y-m-d", strtotime($i)),
                    "label" => $label
                );
            }
        }
        // Quarterly breakup
        else if ($cycle_type == 2) {
            for ($i = $cycle_start; $i <= $cycle_end; $i = date("Y-m-d", strtotime("+ 3 months", strtotime($i)))) {

                if (strtotime("+ 2 months", strtotime($i)) <= strtotime($cycle_end)) {

                    $label = date("F, Y", strtotime($i)) . ' -> ' . date("F, Y", strtotime("+ 2 months", strtotime($i)));
                } else {

                    if (date("F, Y", strtotime($i)) != date("F, Y", strtotime($cycle_end))) {
                        $label = date("F, Y", strtotime($i)) . ' -> ' . date("F, Y", strtotime($cycle_end));
                    } else {
                        $label = date("F, Y", strtotime($i));
                    }
                }
                $break_up_structure[$i] = array(
                    "start_month" => date("Y-m-d", strtotime($i)),
                    "label" => $label
                );
            }
        }
        // Half yearly breakup
        else if ($cycle_type == 3) {
            for ($i = $cycle_start; $i <= $cycle_end; $i = date("Y-m-d", strtotime("+ 6 months", strtotime($i)))) {

                if (strtotime("+ 5 months", strtotime($i)) <= strtotime($cycle_end)) {

                    $label = date("F, Y", strtotime($i)) . ' -> ' . date("F, Y", strtotime("+ 5 months", strtotime($i)));
                } else {

                    if (date("F, Y", strtotime($i)) != date("F, Y", strtotime($cycle_end))) {
                        $label = date("F, Y", strtotime($i)) . ' -> ' . date("F, Y", strtotime($cycle_end));
                    } else {
                        $label = date("F, Y", strtotime($i));
                    }
                }
                $break_up_structure[$i] = array(
                    "start_month" => date("Y-m-d", strtotime($i)),
                    "label" => $label
                );
            }
        }
        // Yearly breakup
        else if ($cycle_type == 4) {

            $label = date("F, Y", strtotime($cycle_start)) . ' -> ' . date("F, Y", strtotime($cycle_end));
            $break_up_structure[$cycle_start] = array(
                "start_month" => date("Y-m-d", strtotime($cycle_start)),
                "label" => $label
            );
        }

        return $break_up_structure;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Get semesters of a student
    // If session id is provided, will show all the semesters(the student is enrolled in) of that session
    // Else will show all active semesters(the student is enrolled in) of all sessions
    ////////////////////////////////////////////////////////////////////////////
    function get_semesters_of_a_student($student_id, $session_id = "") {

        $CI = & get_instance();

        $where = '';
        if ($session_id != "") {
            $where = 'AND t_ses.id= ' . $session_id;
        } else {
            $where = 'AND t_ses.to_date>="' . date("Y-m-d") . '"';
        }

        $sql = 'SELECT t_sem.*, t_ses.from_date, t_ses.to_date,t_stu_en.section_id,t_stu_en.roll_no, t_stu_en.id AS enrollment_id '
                . 'FROM ' . TBL_STUDENT_ENROLLMENT . ' t_stu_en INNER JOIN ' . TBL_SEMESTER . ' t_sem '
                . 'ON t_stu_en.semester_id=t_sem.id '
                . 'INNER JOIN ' . TBL_SESSION . ' t_ses '
                . 'ON t_sem.session_id=t_ses.id '
                . 'WHERE t_stu_en.student_id=' . $student_id . ' ' . $where . ' '
                . 'ORDER BY t_ses.from_date DESC, t_sem.semester_name ASC';
        $query = $CI->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
        }

        return $result;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Get all semesters of a session
    ////////////////////////////////////////////////////////////////////////////
    function get_all_semesters_of_a_session($session_id) {

        $CI = & get_instance();

        $sql = 'SELECT t_sem.*, t_ses.from_date, t_ses.to_date '
                . 'FROM ' . TBL_SEMESTER . ' t_sem INNER JOIN ' . TBL_SESSION . ' t_ses '
                . 'ON t_sem.session_id=t_ses.id '
                . 'WHERE t_ses.id="' . $session_id . '" '
                . 'ORDER BY t_sem.semester_name ASC';
        $query = $CI->db->query($sql);

        $result = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
        }

        return $result;
    }
    function get_unenrolled_student_list() {
        $CI = & get_instance();
        $data = array();
        //$sql = "SELECT ".TBL_STUDENT.".id,".TBL_STUDENT.".name,".TBL_STUDENT.".father_name,".TBL_STUDENT_ENROLLMENT.".semester_id from ".TBL_STUDENT." LEFT JOIN ".TBL_STUDENT_ENROLLMENT." ON ".TBL_STUDENT.".id = ".TBL_STUDENT_ENROLLMENT.".student_id WHERE ".TBL_STUDENT.".school_id = '".$CI->session->userdata('school_id')."'";
        $sql = "SELECT ".TBL_STUDENT.".id,".TBL_STUDENT.".student_name,".TBL_STUDENT.".father_or_mother_name,".TBL_STUDENT_ENROLLMENT.".semester_id from ".TBL_STUDENT." LEFT JOIN ".TBL_STUDENT_ENROLLMENT." ON ".TBL_STUDENT.".id = ".TBL_STUDENT_ENROLLMENT.".student_id";
        $query = $CI->db->query($sql);
        $result = $query->result_array();
        foreach($result as $row){
            if($row['semester_id'] == ''){

                $data[] = $row;
            }
        }
        //echo "<pre>";print_r($data);die;
        return $data;

    }
    ////////////////////////////////////////////////////////////////////////////
    // Get student list of a session/semester/section
    ////////////////////////////////////////////////////////////////////////////
    function get_student_list($parameters = array()) {

        $CI = & get_instance();

        $student_list = array();
        $where = '';

        // If a particular semester is specified
        if (array_key_exists('semester_id', $parameters)) {
            if ($parameters['semester_id'] != "") {

                $where .= ' AND t_s_e.semester_id="' . $parameters['semester_id'] . '"';
            }
        }

        // If semester is not specified, get all the semesters of the session
        if ($where == '') {

            if (array_key_exists('session_id', $parameters)) {
                if ($parameters['session_id'] != "") {

                    $semesters = $this->get_all_semesters_of_a_session($parameters['session_id']);

                    $semesters_string = "";
                    if (!empty($semesters)) {
                        foreach ($semesters as $sem) {
                            $semesters_string .= $sem['id'] . ",";
                        }
                    }
                    $semesters_string = rtrim($semesters_string, ",");

                    if ($semesters_string != "") {
                        $where .= ' AND t_s_e.semester_id IN(' . $semesters_string . ')';
                    }
                }
            }
        }

        // If we get a session or a semester
        if ($where != '') {

            // If section is specified
            if (array_key_exists('section_id', $parameters)) {
                if ($parameters['section_id'] != "") {

                    $where .= ' AND t_s_e.section_id="' . $parameters['section_id'] . '"';
                }
            }

            // If student name is specified
            if (array_key_exists('name', $parameters)) {
                if ($parameters['name'] != "") {

                    $where .= ' AND t_s.name LIKE "%' . $parameters['name'] . '%"';
                }
            }

            // If student roll number is specified
            if (array_key_exists('roll_no', $parameters)) {
                if ($parameters['roll_no'] != "") {

                    $where .= ' AND t_s_e.roll_no LIKE "%' . $parameters['roll_no'] . '%"';
                }
            }

            $sql = 'SELECT t_s.*, t_s_e.class_id, t_s_e.semester_id,t_s_e.roll_no '
                    . 'FROM ' . TBL_STUDENT . ' t_s INNER JOIN ' . TBL_STUDENT_ENROLLMENT . ' t_s_e '
                    . 'ON t_s.id=t_s_e.student_id '
                    . 'WHERE 1=1 ' . $where . ' AND t_s.is_deleted=0 ORDER BY t_s_e.semester_id ASC ';
            $query = $CI->db->query($sql);

            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    $student_list[$row['id']] = $row;
                }
            }
        }

        return $student_list;
    }
    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To encrypt a string
    //// Example: ablEncrypt("280")

    function ablEncrypt($string) {

        $output = false;
        $key = hash("sha256", SECRET_KEY);
        $iv = substr(hash("sha256", SECRET_IV), 0, 16);

        $output = openssl_encrypt($string, ENCRYPT_METHOD, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To decrypt a string
    //// Example: ablDecrypt("280")

    function ablDecrypt($string) {

        $output = false;
        $key = hash("sha256", SECRET_KEY);
        $iv = substr(hash("sha256", SECRET_IV), 0, 16);

        $output = base64_decode($string);
        $output = openssl_decrypt($output, ENCRYPT_METHOD, $key, 0, $iv);

        return $output;
    }



}

?>
