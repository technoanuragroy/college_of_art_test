<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AblFunctions {

    public $CI;

    function __construct() {

        // Do something common to all functions
        $this->CI = & get_instance();

        /** String Encrypting Credentials * */
        define("ENCRYPT_METHOD", "AES-256-CBC");
        define("SECRET_KEY", "som3VerySampl3Key");
        define("SECRET_IV", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To get a particular column's value from a dataset
    //// Example: getParticularFieldValue("tbl_companies", "contact_person", " and id=2")

    public function getParticularFieldValue($tablename, $fieldname, $where = "") {

        $sql = "SELECT " . $fieldname . " FROM " . $tablename . " WHERE 1=1 " . $where;
        $query = $this->CI->db->query($sql);

        $record = "";
        if ($query->num_rows() > 0) {

            $row = $query->row_array();

            $record = $row[$fieldname];
        }

        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To get a full dataset
    //// Example: getDetailsFromId(2, "tbl_companies") or getDetailsFromId("", "tbl_companies", array("id" => 2))

    public function getDetailsFromId($id = "", $tablename, $extra = array()) {

        if ($id != "") {
            $this->CI->db->where("id", $id);
        }
        if (count($extra) > 0) {
            $this->CI->db->where($extra);
        }

        $query = $this->CI->db->get($tablename);

        $record = array();
        if ($query->num_rows() > 0) {

            $record = $query->row_array();
        }

        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To get multiple dataset
    //// Example: getMultipleData("tbl_companies", " and country=99")

    public function getMultipleData($tablename, $where = "") {

        $sql = "SELECT * FROM " . $tablename . " WHERE 1=1 " . $where;
        $query = $this->CI->db->query($sql);

        $record = array();
        if ($query->num_rows() > 0) {

            $record = $query->result_array();
        }

        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To get count of a list of datasets
    //// Example: getParticularCount("tbl_companies", " and country=99")

    public function getParticularCount($tablename, $where = "") {

        $sql = "SELECT * FROM " . $tablename . " WHERE 1=1 " . $where;
        $query = $this->CI->db->query($sql);

        $record = $query->num_rows();

        return $record;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To insert a dataset to database table
    //// Example: insertData(array("company_name" => "ABC Company", "contact_person" => "Surajit Das", "designation" => "Web Developer", "country" => rand(90, 100)), "tbl_companies")

    public function insertData($data, $tablename) {

        $this->CI->db->insert($tablename, $data);

        return $this->CI->db->insert_id();
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To delete a dataset from database table
    //// Example: deleteData("tbl_companies", array("country" => 89))

    public function deleteData($tablename, $where = array()) {

        $this->CI->db->where($where);
        $this->CI->db->delete($tablename);

        return $this->CI->db->affected_rows();
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To update a dataset in database table
    //// Example: updateData(array("contact_person" => "Biplob Mudi","email"=>"biplob@ablion.in"),"tbl_companies", array("id" => 89))

    public function updateData($data, $tablename, $where) {

        $this->CI->db->where($where);
        $this->CI->db->update($tablename, $data);

        return $this->CI->db->affected_rows();
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To sort an array against value of a particular key
    //    $data = array(
    //        1 => array(
    //            'company_name' => 'ABC',
    //            'sorter' => 200
    //        ), 
    //        2 => array(
    //            'company_name' => 'XYZ',
    //            'sorter' => 100 
    //        )
    //    );
    //// Example: sortArrayAgainstValueOfAParticularKey($data, "sorter")
    public function sortArrayAgainstValueOfAParticularKey(&$array, $key) {

        $sorter = array();
        $ret = array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
        return $array;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To encrypt a string to SHA512 encryption
    //// Example: encryptString("123456")

    public function encryptString($string) {

        return openssl_digest($string, 'sha512');
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To create alise from a string
    //// Example: CreateAlise("This is an Example")

    public function CreateAlise($name) {

        $name = strtolower(trim($name));

        $name = str_replace("_", "-", $name);
        $name = str_replace("+", "-Plus", $name);

        $name = str_replace("~", "", $name);
        $name = str_replace("!", "", $name);
        $name = str_replace("@", "", $name);
        $name = str_replace("#", "", $name);
        $name = str_replace("$", "", $name);
        $name = str_replace("%", "", $name);
        $name = str_replace("^", "", $name);
        $name = str_replace("&amp;", "", $name);
        $name = str_replace("&", "n", $name);
        $name = str_replace("*", "", $name);
        $name = str_replace("(", "", $name);
        $name = str_replace(")", "", $name);
        $name = str_replace("=", "", $name);
        $name = str_replace("{", "", $name);
        $name = str_replace("}", "", $name);
        $name = str_replace("[", "", $name);
        $name = str_replace("]", "", $name);
        $name = str_replace("'", "", $name);
        $name = str_replace("|", "", $name);
        $name = str_replace(":", "", $name);
        $name = str_replace(";", "", $name);
        $name = str_replace("<", "", $name);
        $name = str_replace(">", "", $name);
        $name = str_replace(",", "", $name);
        $name = str_replace(".", "", $name);
        $name = str_replace("?", "", $name);
        $name = str_replace("\"", "-", $name);
        $name = str_replace("/", "-", $name);
        $name = str_replace("\\", "-", $name);

        $name = str_replace(" ", "-", $name);  //preg_replace($pattern, $replacement, $string);
        $name = str_replace("--", "-", $name);
        $name = str_replace("--", "-", $name);
        $name = str_replace("--", "-", $name);
        $name = str_replace("--", "-", $name);

        $first = substr($name, 0, 1);

        if ($first == "-") {
            $name = substr($name, 1);
        }

        if (strlen($name) > 50) {
            $name = substr($name, 0, 50);
        }

        $alise = $name;

        return $alise;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To create Fixed size image
    //// Example: CreateFixedSizedImage("_upload/","_asset/",500,500)

    public function CreateFixedSizedImage($source, $dest, $width, $height) {

        $source_path = $source;

        list( $source_width, $source_height, $source_type ) = getimagesize($source_path);

        switch ($source_type) {
            case IMAGETYPE_GIF:
                $source_gdim = imagecreatefromgif($source_path);
                break;

            case IMAGETYPE_JPEG:
                $source_gdim = imagecreatefromjpeg($source_path);
                break;

            case IMAGETYPE_PNG:
                $source_gdim = imagecreatefrompng($source_path);
                break;
        }

        $source_aspect_ratio = $source_width / $source_height;
        $desired_aspect_ratio = $width / $height;

        if ($source_aspect_ratio > $desired_aspect_ratio) {
            //
            // Triggered when source image is wider
            //
            $temp_height = $height;
            $temp_width = (int) ( $height * $source_aspect_ratio );
        } else {
            //
            // Triggered otherwise (i.e. source image is similar or taller)
            //
            $temp_width = $width;
            $temp_height = (int) ( $width / $source_aspect_ratio );
        }

        //
        // Resize the image into a temporary GD image
        //

        $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
        imagecopyresampled(
                $temp_gdim, $source_gdim, 0, 0, 0, 0, $temp_width, $temp_height, $source_width, $source_height
        );

        //
        // Copy cropped region from temporary image into the desired GD image
        //

        $x0 = ( $temp_width - $width ) / 2;
        $y0 = ( $temp_height - $height ) / 2;
        //$y0 = 0; //if the cropping needs to be done form top

        $desired_gdim = imagecreatetruecolor($width, $height);
        imagecopy(
                $desired_gdim, $temp_gdim, 0, 0, $x0, $y0, $width, $height
        );

        //
        // Render the image
        // Alternatively, you can save the image in file-system or database
        //

        //header( 'Content-type: image/jpeg' );
        imagejpeg($desired_gdim, $dest);

        //
        // Add clean-up code here
        //
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To convert a date to database date format
    //// Example: databaseDate("21/10/2019")

    public function databaseDate($date) {

        $date_array = explode("/", $date);
        $new_date = $date_array[2] . '-' . $date_array[1] . '-' . $date_array[0];

        return $new_date;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To convert a date to database date format
    //// Example: databaseDate("21/10/2019")

    public function displayDate($date, $format = "d/m/Y") {

        
            $new_date = date($format, strtotime($date));
        

        return $new_date;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To generate random password
    //// Example: getRandomPassword()

    function getRandomPassword($chars_min = 8, $chars_max = 8, $use_upper_case = true, $include_numbers = true, $include_special_chars = true) {

        $length = rand($chars_min, $chars_max);
        $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
        if ($include_numbers) {
            $selection .= "1234567890";
        }
        if ($include_special_chars) {
            $selection .= "!@\"#$%&[]{}?|";
        }

        $password = "";
        for ($i = 0; $i < $length; $i++) {
            $current_letter = $use_upper_case ? (rand(0, 1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
            $password .= $current_letter;
        }

        return $password;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To generate OTP with user defined digits
    //// Example: generateOTP("6")

    function generateOTP($length = 4, $chars = '123456789') {
        
        $chars_length = (strlen($chars) - 1);
        $string = $chars{rand(0, $chars_length)};

        for ($i = 1; $i < $length; $i = strlen($string)) {
            $r = $chars{rand(0, $chars_length)};
            if ($r != $string{$i - 1})
                $string .= $r;
        }
        //echo $string;die;
        return $string;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To send emails
    //// Example: SendEmail("info@ablion.in","biplob@ablion.in","Test Email","This is a test email")

    function SendEmail($from, $to, $subject, $email_message, $attachment = "", $alternative_text = "") {
        
        $from_array = explode(',', $from);
        $from_email = $from_array[0];
        $from_name = $from_array[1];

        $this->CI->load->library("email");
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $this->CI->email->initialize($config);

        $this->CI->email->from($from_email, $from_name);
        $this->CI->email->to($to);
        $this->CI->email->subject($subject);
        $this->CI->email->message($email_message);

        if ($attachment != '') {
            $this->CI->email->attach($attachment);
        }

        if ($alternative_text != '') {
            $this->CI->email->set_alt_message($alternative_text);
        }

        $this->CI->email->send();
        return true;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To send SMS
    //// Example: sendSMS(http://mysmsshop.in/V2/http-api.php?apikey=ow3PwR2dtGdWheYi&senderid=BIDALY&format=json&, "9749552668","Hi, This is a test message")    
  
    function sendSMS($sms_url, $mobile_no, $message, $param = "") {
        
        if ($sms_url != "" AND $mobile_no != "" AND $message != "") {
            
            $msgen = urlencode($message);
            $send_url = $sms_url . "number=" . $mobile_no . "&message=" . $msgen . $param;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $send_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);
            curl_close($ch);

            return true;
        } else {
            
            return false;
        }
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To encrypt a string
    //// Example: ablEncrypt("280")

    function ablEncrypt($string) {

        $output = false;
        $key = hash("sha256", SECRET_KEY);
        $iv = substr(hash("sha256", SECRET_IV), 0, 16);

        $output = openssl_encrypt($string, ENCRYPT_METHOD, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To decrypt a string
    //// Example: ablDecrypt("280")

    function ablDecrypt($string) {

        $output = false;
        $key = hash("sha256", SECRET_KEY);
        $iv = substr(hash("sha256", SECRET_IV), 0, 16);

        $output = base64_decode($string);
        $output = openssl_decrypt($output, ENCRYPT_METHOD, $key, 0, $iv);

        return $output;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To resize a image
    //// Example: resizeImage("_upload/a.jpg","400","400","TRUE/FALSE","_upload/")

    function resizeImage($file, $w, $h, $crop = FALSE, $dest) {

        list($width, $height) = getimagesize($file);
        $r = $width / $height;
        if ($crop) {
            if ($width > $height) {
                $width = ceil($width - ($width * abs($r - $w / $h)));
            } else {
                $height = ceil($height - ($height * abs($r - $w / $h)));
            }
            $newwidth = $w;
            $newheight = $h;
        } else {
            if ($w / $h > $r) {
                $newwidth = $h * $r;
                $newheight = $h;
            } else {
                $newheight = $w / $r;
                $newwidth = $w;
            }
        }
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        imagejpeg($src, $dest);
        return $dst;
    }

    /*     * *********************************************************************** */
    /*     * *********************************************************************** */

    //// Purpose: To rotate a image
    //// Example: RotateJpg("_upload/a.jpg","0/90/180","true/false","0/1")

    function RotateJpg($filename = '', $angle = 0, $savename = false, $ignore_quality = 0) {

        // Your original file
        //$original = imagecreatefromjpeg($filename);

        list($width_orig, $height_orig, $type) = getimagesize($filename);

        // Temporarily increase the memory limit to allow for larger images
        //ini_set('memory_limit', '32M'); 

        switch ($type) {

            case IMAGETYPE_GIF:
                $original = imagecreatefromgif($filename);
                break;
            case IMAGETYPE_JPEG:
                $original = imagecreatefromjpeg($filename);
                break;
            case IMAGETYPE_PNG:
                $original = imagecreatefrompng($filename);
                break;
            default:
                throw new Exception('Unrecognized image type ' . $type);
        }

        // Rotate
        $rotated = imagerotate($original, $angle, 0);

        // If you have no destination, save to browser
        if ($savename == false) {
            header('Content-Type: image/jpeg');
            if ($ignore_quality == "1") {
                imagejpeg($rotated, NULL);
            } else {
                imagejpeg($rotated, NULL, 100);
            }
        }
        // Save to a directory with a new filename
        else {
            if ($ignore_quality == "1") {
                imagejpeg($rotated, $savename);
            } else {
                imagejpeg($rotated, $savename, 100);
            }
        }

        // Standard destroy command
        imagedestroy($rotated);
    }

}

?>