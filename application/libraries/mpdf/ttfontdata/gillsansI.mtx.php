<?php
$name='GillSansStd-Italic';
$type='TTF';
$desc=array (
  'CapHeight' => 682.0,
  'XHeight' => 449.0,
  'FontBBox' => '[-201 -250 1009 909]',
  'Flags' => 68,
  'Ascent' => 909.0,
  'Descent' => -250.0,
  'Leading' => 0.0,
  'ItalicAngle' => -8.0,
  'StemV' => 87.0,
  'MissingWidth' => 565.0,
);
$unitsPerEm=1000;
$up=-125;
$ut=50;
$strp=269;
$strs=50;
$ttffile='/mnt/E/webdev/ServiceChampion/site/mpdf/ttfonts/GillSansStd-Italic.ttf';
$TTCfontID='0';
$originalsize=30284;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='gillsansI';
$panose=' 0 0 2 b 5 2 2 1 4 9 2 3';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 682, -318, 200
// usWinAscent/usWinDescent = 909, -250
// hhea Ascent/Descent/LineGap = 682, -318, 200
$useOTL=0x0000;
$rtlPUAstr='';
?>