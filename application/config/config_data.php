<?php
/*
| Site variables
*/
define("SITE_NAME", "College of Art & Design");
//define("SITE_EMAIL", "demo@gmail.com");
define("SITE_EMAIL", "info@twib.app");
//define("SITE_EMAIL", "contactus@collegeofartanddesign.co.in");

/*
| Security variables
*/
define("CRON_KEY", "ABLIONCustomProject");

/** Travel Mode **/
$config['gender'] = array(
    '1' => 'Male',
    '2' => 'Female',
    '3' => 'Others'
);

/** Marital Status **/
$config['marital_status'] = array(
    '1' => 'Married',
    '2' => 'Single',
    '3' => 'Divorced'
);

/** Marital Status **/
$config['student_payment_type'] = array(
    '1' => 'Admission Form',
    '2' => 'Admission',
    '3' => 'semester Fees'
);

/** Caste **/
$config['caste'] = array(
    '1' => 'General',
    '2' => 'SC',
    '3' => 'ST',
    '4' => 'OBC',
    '5' => 'Physically Handicapped'
);

/** Caste **/
$config['form_status'] = array(
    '1' => 'Need Review',
    '2' => 'Review Pending',
    '3' => 'Admit (Test) Issued',
    '4' => 'Admit (Admission) Issued',
    '5' => 'Payment Link Activated',
    '6' => 'Admission Fees received',
    '7' => 'Enrolled'
);
/** Staff Designation **/
$config['designation'] = array(
    '1' => 'Principal',
    '2' => 'Teaching Staff',
    '3' => 'Non Teaching Staff'

);
/** Admit card **/
$config['admit_card'] = array(
    '1' => 'Admission test',
    '2' => 'Direct admission'

);
/** Hour **/
$config['hour_list'] = array(
    '1' => '1',
    '2' => '2',
    '3' => '3',
    '4' => '4',
    '5' => '5',
    '6' => '6',
    '7' => '7',
    '8' => '8',
    '9' => '9',
    '10' => '10',
    '11' => '11',
    '12' => '12',

);
$config['days_list'] = array(
    '1' => 'Monday',
    '2' => 'Tuesday',
    '3' => 'Wednesday',
    '4' => 'Thursday',
    '5' => 'Friday',
    '6' => 'Saturday',
    '7' => 'Sunday'
);

$config['cycle'] = array(
    '1' => 'monthly',
    '2' => 'quarterly',
    '3' => 'half yearly',
    '4' => 'yearly',
);

/*
| Date variables
*/
define("DATE_FORMAT", "Y-m-d");
define("DATETIME_FORMAT", "Y-m-d H:i:s");
define("DISPLAY_DATE_FORMAT","d-M-Y");
define("DISPLAY_DATETIME_FORMAT","d-M-Y h:i A");

/*
| User type variables
*/
define("ADMIN_TYPE_SUPER_ADMIN", '1');
define("ADMIN_TYPE_ADMIN", '2');
define("LOGIN_TYPE_USER", '1');
define("USER_TYPE_ADMIN", '1');
define("USER_TYPE_USER", '2');

/*
| Settings variables
*/
define("SETTINGS_SMS_KEY", "sms");
define("SETTINGS_EMAIL_KEY", "email");

/*
| Miscellaneous variables
*/
define("COOKIE_EXPIRY_TIME", '8650000'); // IN SECONDS
define("TOKEN_TYPE_EMAIL_VERIFICATION", '1');
define("TOKEN_TYPE_FORGOT_PASSWORD", '2');
define("TOKEN_TYPE_SMS_VERIFICATION", '3');
define("TOKEN_EXPIRY_TIME", '1440'); // IN MINUTES. 1 HOUR = 60 MINUTES / 12 HOURS = 720 MINUTES / 24 HOURS = 1440 MINUTES
define("USERNAME_TYPE_EMAIL", '1');
define("USERNAME_TYPE_MOBILE", '2');
define("USERNAME_TYPE_GENERAL", '3');

/*
| Upload variables
*/
define("UPLOAD_DIR", 'uploads/');

/*
| Gallery variables
*/
define("GALLERY_DIR", 'gallery/'); // WITH RESPECT TO UPLOAD_DIR
define("GALLERY_THUMB_DIR", 'gallery/thumb/'); // WITH RESPECT TO UPLOAD_DIR
/*
| Student From
*/
define("NOTICE_DOCUMENT", 'notice_document/');
define("SYLLABUS_DOCUMENT", 'syllabus_document/');
define("STUDENT_AGE_PROOF", 'student_from/age_proof/');
define("STUDENT_AGE_PROOF_THUMB", 'student_from/age_proof/thumb/');
define("PRINCIPAL_SIGN", 'principal_sign/');
define("PRINCIPAL_SIGN_THUMB", 'principal_sign/thumb/');
define("STAFF_SIGN", 'staff_details/staff_sign/');
define("STAFF_SIGN_THUMB", 'staff_details/staff_sign/thumb/');
define("STAFF_PHOTO", 'staff_details/staff_photo/');
define("STAFF_PHOTO_THUMB", 'staff_details/staff_photo/thumb/');
define("STUDENT_SIGN", 'student_from/candidate_signature/');
define("STUDENT_SIGN_THUMB", 'student_from/candidate_signature/thumb/');
define("STUDENT_SIGN_FULL", 'student_from/candidate_signature_full/');
define("STUDENT_SIGN_FULL_THUMB", 'student_from/candidate_signature_full/thumb/');
define("STUDENT_CASTE", 'student_from/caste_certificate/');
define("STUDENT_CASTE_THUMB", 'student_from/caste_certificate/thumb/');
define("STUDENT_GURDAIN_SIGN", 'student_from/guardian_signature/');
define("STUDENT_GURDAIN_SIGN_THUMB", 'student_from/guardian_signature/thumb/');
define("STUDENT_MARKSHEET", 'student_from/mark_sheet/');
define("STUDENT_MARKSHEET_THUMB", 'student_from/mark_sheet/thumb/');
define("STUDENT_PASSPORT_PHOTO", 'student_from/passport_photo/');
define("STUDENT_PASSPORT_PHOTO_THUMB", 'student_from/passport_photo/thumb/');

define("ALLOWED_IMAGE_TYPES", 'gif|jpg|png|jpeg|JPG|PNG|JPEG');
define("ALLOWED_PHOTO_IMAGE_TYPES", 'bmp|gif|jpg|png|jpeg|JPG|PNG|JPEG');
define("ALLOWED_SIGN_IMAGE_TYPES", 'bmp|gif|jpg|png|jpeg|JPG|PNG|JPEG');
define("ALLOWED_DOCUMENT_TYPES", 'pdf|PDF|bmp|gif|jpg|png|jpeg|JPG|PNG|JPEG');
define("ALLOWED_IMAGE_SIZE", '5000'); // IN KILOBYTES
define("ALLOWED_PHOTO_IMAGE_SIZE", '500'); // IN KILOBYTES
define("ALLOWED_SIGN_IMAGE_SIZE", '200'); // IN KILOBYTES
define("ALLOWED_DOCUMENT_IMAGE_SIZE", '1024'); // IN KILOBYTES
define("THUMB_WIDTH", '100'); // IN PIXELS
define("THUMB_HEIGHT", '100'); // IN PIXELS
define("PASSPORT_WIDTH", '437'); // IN PIXELS
define("PASSPORT_HEIGHT", '531'); // IN PIXELS
define("SIGN_WIDTH", '437'); // IN PIXELS
define("SIGN_HEIGHT", '177'); // IN PIXELS
define("ALLOWED_VIDEO_TYPES", 'mp4');
define("ALLOWED_VIDEO_SIZE", '10000'); // IN KILOBYTES
define("GALLERY_PLACEHOLDER_IMAGE", 'images/gallery_placeholder_image.png');



///////////////////////////////// TABLES  ///////////////////////////////
define('TBL_ADMIN','tbl_admins');
define('TBL_SCHOOL','tbl_schools');
define('TBL_CLASSES','tbl_classes');
define('TBL_SECTION','tbl_section');
define('TBL_SUBJECT','tbl_subjects');
define('TBL_TEACHER','tbl_teachers');
define('TBL_TEACHER_FILES','tbl_teacher_files');
define('TBL_PERIODS','tbl_periods');
define('TBL_TIMETABLE','tbl_timetable');
define('TBL_STUDENT','tbl_student_from');
define('TBL_STUDENT_DETAIL','tbl_student_details');
define('TBL_STUDENT_FILES','tbl_student_files');
define('TBL_PARENT','tbl_parents');
define('TBL_PARENT_KIDS_LINK','tbl_parents_kids_link');
define('TBL_COMMON_LOGIN','tbl_common_login');
define('TBL_NOTICE','tbl_notice');
define('TBL_ATTENDANCE','tbl_attendance');
define('TBL_HOLIDAY','tbl_holiday');
define('TBL_NOTICE_FILES','tbl_notice_files');
define('TBL_SYLLABUS','tbl_syllabus');
define('TBL_SYLLABUS_FILES','tbl_syllabus_files');
define('TBL_GENERAL_INFO','tbl_general_info');
define('TBL_NOTE','tbl_note');
define('TBL_NOTE_FILES','tbl_note_files');
define('TBL_HOME_NOTE','tbl_home_note');
define('TBL_HOME_NOTE_FILES','tbl_home_note_files');
define('TBL_ASSIGNMENT_NOTE','tbl_assignment_note');
define('TBL_ASSIGNMENT_NOTE_FILES','tbl_assignment_note_files');
define('TBL_DIARY','tbl_diary');
define('TBL_DELETE_FILES','tbl_delete_files');
define('TBL_SCHOOL_LOGO_FILES','tbl_school_logo_files');
define('TBL_REGISTER_DEVICE','tbl_register_device');
define('TBL_PERMISSION','tbl_permissions');
define('TBL_USER_PERMISSION','tbl_user_permissions');
define('TBL_TEMP_TIMETABLE','tbl_temp_timetable');
define('TBL_VERIFICATION_TOKEN','tbl_verification_token');
define('TBL_SYSTEM_EMAILS','tbl_system_emails');
define('TBL_API_LOG','tbl_api_log');
define('TBL_ADMIN_FILES','tbl_admin_files');
define('TBL_SMS_CREDITS','tbl_sms_credits');
define('TBL_FEES_PAYMENT','tbl_fees_payment');
define('TBL_PAYMENT_GATEWAY','tbl_paymentgateway');
define('TBL_COLLEGE_PAYMENT_SETTINGS','tbl_college_payment_settings');
define('TBL_PAYMENTS_TO_SCHOOL','tbl_payments_to_school');
define('TBL_FEES_STRUCTURE','tbl_fees_structure');
define('TBL_FEES_STRUCTURE_BREAKUPS','tbl_fees_structure_breakups');
define('TBL_FEES_STRUCTURE_BREAKUPS_MANUAL','tbl_fees_structure_breakups_manual');
define('TBL_NOTIFICATION_SETTINGS','tbl_notification_settings');
define('TBL_APP_MAINTAINANCE','tbl_app_maintenance');
define('TBL_SESSION','tbl_session');
define('TBL_SEMESTER','tbl_semester');
define('TBL_COUNTRY','tbl_country');
define('TBL_TERMS','tbl_terms');
define('TBL_EXAM','tbl_exam');
define('TBL_EXAM_SCORES','tbl_exam_scores');
define('TBL_STUDENT_ENROLLMENT', 'tbl_student_enrollment');
define('TBL_SCORE', 'tbl_score');
define('TBL_TEACHER_ATTENDANCE', 'tbl_teacher_attendance');
define('TBL_GATEWAY_SETTINGS','tbl_gateway_settings');
define('TBL_DUE_REMINDER_SCHEDULES', 'tbl_due_reminder_schedules');
define('TBL_SMS_LOG', 'tbl_sms_log');
?>
