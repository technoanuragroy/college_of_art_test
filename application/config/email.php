<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$CI = & get_instance();
$settings = $CI->my_custom_functions->getEmailConfig();

if(!empty($settings)) {
    $config['mailtype'] = 'html';
    $config['charset'] = 'utf-8';
    $config['protocol'] = 'smtp';
    $config['smtp_host'] = $settings['host'];
    $config['smtp_port'] = $settings['port'];
    $config['smtp_user'] = $settings['username'];
    $config['smtp_pass'] = $settings['password'];
    $config['newline'] = "\r\n";
}


