<?php 
$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//$route["registration"] = "main/registration";
$route["registration"] = "company/main/registration";
$route["userVerification/(:any).*"] = "company/main/email_verification";
$route["userResetPassword/(:any).*"] = "company/main/userResetPassword";
$route["forget_password"] = "main/forget_password";
$route["admin"] = "admin/main";
$route["admin/logout"] = "admin/main/logout";
//$route['company']="company/main";
$route['login']="company/main";
$route["company/logout"] = "company/main/logout";
$route["unsubscribe/(:any).*"] = "unsubscribe";

//$route["admin/main/(:any).*"] = "admin/main/$1";
//$route["admin/user/(:any).*"] = "admin/user/$1";
//$route['management']="management/main";
//$route['management/profile']="management/main/profile";
//$route['management/logout']="management/main/logout";
//
//$route['coach/profile']="coach/main/profile";
//$route['coach/forget_password']="coach/main/forget_password";
//$route['coach/change_password']="coach/main/change_password";
//$route['coach/logout']="coach/main/logout";
//$route['management/change_password']="management/main/change_password";
//$route['management/forget_password']="management/main/forget_password";
//$route['clients/main/forget_password']="clients/main/forget_password";

?>