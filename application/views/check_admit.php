<?php $this->load->view("include/header"); ?>
                
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Welcome Student!</h2>
                
                <div class="dashboardCard_wrap">
                    
                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>                     
                    
                    <div class="row">
                        <?php
                        $encrypted_id = $this->ablfunctions->getParticularFieldValue("tbl_student_from","id", " and student_id='" . $this->session->userdata('user_id') . "'");
                        $student_id = $this->ablfunctions->ablEncrypt($encrypted_id);
                        if ($admit_card_type!=0) {
                 ?>
                   <a href="<?php echo base_url().'user/downloadAdmitCard/'.$student_id; ?>" title="Issue Admit Card" target="_blank" class="c-link">Download Admit Card</a>
                  <?php 
                  } ?>
                        
                        
                    </div>
                </div>    
            </div>
        </div>    
    </div>	                                                 
                
<?php $this->load->view("include/footer"); ?> 
