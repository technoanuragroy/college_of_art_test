<?php $this->load->view("include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">


                <div class="dashboardCard_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>
                    <?php
                    $payment_status = $this->my_custom_functions->get_particular_field_value("tbl_student_payment_status","admission_from_payment", 'and student_id="'.$this->session->userdata('user_id').'"');
                    $from_applied = $this->my_custom_functions->get_particular_field_value("tbl_student_from","id", 'and student_id="'.$this->session->userdata('user_id').'"');
                    if (($payment_status==1) AND ($from_applied!='')) {
                    ?>
                    <h2 class="heading_02">Admission Form Download</h2>
                    <div class="row">

                        <a href="<?php echo base_url().'report/downloadAdmissionForm/'.$this->ablfunctions->ablEncrypt($this->session->userdata('user_id')); ?>" target="_blank" class="c-link" title='Click on the Pay button to pay.'>Download Admission Form</a>
                    </div>
                  <?php }
                  else {
                  ?>
                  <h2 class="heading_02"> Not Issued Admission Form </h2>
                  <?php } ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("include/footer"); ?>
