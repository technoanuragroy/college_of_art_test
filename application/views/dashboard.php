<?php $this->load->view("include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02"><?php
                if (isset($title)) {
                  echo $title;
                }
                ?></h2>

                <div class="dashboardCard_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">
                        

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("include/footer"); ?>
