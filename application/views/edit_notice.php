<?php $this->load->view("include/header"); ?>


    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">View Notice</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                    <div class="form-group">
                      <!-- <label for="notice_heading">Notice Heading</label> -->
                        <div class="form-material">


                            <label for="notice_heading"><h2><?php echo $notice_detail['notice_heading']; ?></h2></label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12" for="example-textarea-input">Notice</label>
                        <div class="col-12">
                          <?php
                          echo $notice_detail['notice_text']; ?>
                            <!-- <textarea class="form-control" id="editor1" name="notice_text" rows="6" placeholder="Content.."></textarea> -->
                        </div>
                        <script>

                            //CKEDITOR.replace('editor1');
                        </script>

                    </div>
                    <!-- <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="date_of_birth" name="date_of_publish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy">
                            <label for="date_of_birth">Date of Publish</label>
                        </div>
                    </div> -->

                    <div class="form-group row">
                       <label class="col-12" for="example-textarea-input">Document</label>
                        <?php
                        $document_url_array = explode(".", $notice_detail['document_url']);
                        $document_url_extension = $document_url_array[count($document_url_array) - 1];
                        $path=base_url().UPLOAD_DIR.NOTICE_DOCUMENT.$notice_detail['document_url'];
                        $display_path = base_url() . 'images/pdf.png';
                            ?>
                        <?php if (file_exists(UPLOAD_DIR.NOTICE_DOCUMENT.$notice_detail['document_url']) AND ($notice_detail['document_url']!='')) {
                             ?>
                            <div class="col-2" style="align:left;">



                                    <a href="<?php echo $path; ?>" target="_blank">
                                      <?php if ($document_url_extension=="pdf") {
                                      ?>
                                        <image src="<?php echo $display_path.'?t='.time();?>" style="height:50px;width: 50px;">
                                      <?php }else{
                                        ?>
                                          <image src="<?php echo $path.'?t='.time();?>" style="height:50px;width: 50px;">
                                      <?php }?>
                                    </a>
                                    <!-- <a href="javascript:;" title="Delete"
                                      onclick="confirm_delete('<?php echo base_url().'admin/college/deleteNoticeFile/'.$notice_detail['document_url'].'/'.$notice_detail['id'] ?>');" ><i class="fa fa-trash"></i></a> -->
                                </div>
                                <?php
                                }
                                ?>

                    </div>

<!--                    <td>Origal Img - <img id="original-Img"/></td>
                    <td>Compress Img - <img id="upload-Preview"/></td>-->


                    <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("include/footer"); ?>
