<?php $this->load->view("include/header"); ?>
        
    <script type="text/javascript">

        // Send OTP
        function sendVerificationOTP() {
                        
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>main/sendOTP",                
                success: function (msg) {                                    
                    if (msg != "") {
                        $('.otp_message').show();                        
                        onTimer();
                    }
                }
            });
        }                

        // Timer for showing "Resend OTP"
        var i = 60;
        function onTimer() {
        
            $('#mycounter').html(i + ' seconds left');

            i--;
            if (i < 0) {
                $('#otp_button').prop('disabled', false);
                $('#otp_button').text('Resend OTP');
                $('#mycounter').html('');
                $('.otp_message').hide();
                i = 60;
            } else {

                setTimeout(onTimer, 1000);
                $('#otp_button').prop('disabled', true);
            }
        }

        // Function to check if a string is a number
        function isNumber(evt) {
            
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Verify Account</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">
                                                   
                        <!-- Send OTP section -->
                        <?php if($user_settings['otp_verification'] == 1 AND $login_data['otp_verification'] == 0) { ?>
                                  
                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <h5 class="heading_05">OTP Verification for <?php echo $user_details['phone']; ?>
                                                <a href="javascript:" data-toggle="modal" data-target="#modal-mobile">
                                                    <span>
                                                        <i class="fa fa-edit"></i>
                                                    </span>
                                                </a>
                                            </h5>
                                        </span>                                            
                                    </li>     

                                    <li class="full_width_li">
                                        <span>
                                            <button id="otp_button" class="submit_btn" onclick="sendVerificationOTP();">Send OTP</button>
                                            <div id="mycounter"></div>
                                            <span class="otp_message" style="display:none;">An OTP has been sent to this mobile number</span>
                                        </span>                                            
                                    </li>   
                                    
                                    <?php echo form_open('', array('id' => 'formOTPVerification')); ?>
                                            
                                            <li class="full_width_li">
                                                <span>                   
                                                    <input type="text" name="otp" id="otp" placeholder="Enter OTP" required="">                    
                                                </span>
                                            </li> 

                                            <li class="full_width_li">	
                                                <input type="submit" name="submit" class="submit_btn" value="submit">
                                            </li>                                            

                                    <?php echo form_close(); ?>                                                                                                   
                                </ul>
                                                
                        <?php } ?>

                        <!-- Send email section -->
                        <!-- <?php if($user_settings['email_verification'] == 1 AND $login_data['email_verification'] == 0) { ?>
                                 
                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <h5 class="heading_05">Email Verification for <?php echo $user_details['email']; ?>
                                                <a href="javascript:" data-toggle="modal" data-target="#modal-email">
                                                    <span>
                                                        <i class="fa fa-edit"></i>
                                                    </span>
                                                </a>
                                            </h5>
                                        </span>                                            
                                    </li>     
                                            
                                    <li class="full_width_li">
                                        <span>
                                            <a href="<?php echo base_url(); ?>main/sendVerificationEmail" class="c-link">Send Verification Email</a>                        
                                        </span>                                            
                                    </li> 
                                                               
                        <?php } ?>  -->                     
                    
                    </div>
                </div>    
            </div>
        </div>    
    </div>	                                                 
                
<?php $this->load->view("include/footer"); ?> 
