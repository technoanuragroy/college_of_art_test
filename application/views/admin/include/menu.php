<header class="noprint_old">
    <?php /*
    <ul class="headerNav">
        <li><a href="index.html">home</a></li>
        <li><a href="#">pages</a></li>
        <li><a href="javascript:" class="target_popup">reports</a></li>
        <li><a href="#">apps</a></li>
    </ul>

    <ul class="adminInfoWidgate">
        <li><a href="javascript:"><i class="fa fa-search"></i></a>
            <span class="search_box_wrap">
                <input type="text" placeholder="Type and hit Enter....">
            </span>
        </li>
        <li><a href="javascript:" id="myProfile"><span>Hi, User <picture><img src="<?php echo base_url(); ?>images/user.jpg"></picture></span></a>
            <ul class="user_info_wrap">
                <li><p>User Name</p></li>
                <li><a href="#">my profile</a></li>
                <li><a href="#">my tasks</a></li>
                <li><a href="#">setting</a></li>
                <li><a href="login.html">logout</a></li>
            </ul>
        </li>
    </ul>
    */ ?>

    <ul class="headerNav">
        <!-- <li><a href="<?php echo base_url(); ?>admin/user/dashboard">Dashboard</a></li> -->
    </ul>

    <ul class="adminInfoWidgate">
        <?php /*
        <li><a href="javascript:"><i class="fa fa-search"></i></a>
            <span class="search_box_wrap">
                <input type="text" placeholder="Type and hit Enter....">
            </span>
        </li>
        */ ?>

        <li><a href="javascript:" id="myProfile"><span>Hi, <?php echo $this->my_custom_functions->get_particular_field_value("tbl_admins","name", 'and id="'.$this->session->userdata("admin_id").'"'); ?> <i class="fa fa-caret-down"></i> <picture><img src="<?php echo base_url(); ?>images/user-placeholder-circle.png"></picture></span></a>
            <ul class="user_info_wrap">
                <li><p><?php echo $this->session->userdata("admin_name"); ?></p></li>
                <li><a href="<?php echo base_url(); ?>admin/user/editProfile">Edit Profile</a></li>
                <li><a href="<?php echo base_url(); ?>admin/user/changePassword">Change Password</a></li>
                <li><a href="<?php echo base_url(); ?>admin/main/logout">Logout</a></li>
            </ul>
        </li>
    </ul>
</header>
