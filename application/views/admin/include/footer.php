        </section>

        <!-- Delete record confirmation modal -->
        <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete" aria-hidden="true">
            <div class="modal-dialog modal-dialog-popin modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Delete Record</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                        </button>
                    </div>

                    <div class="modal-body">
                        Do you want to delete this record?
                    </div>

                    <div class="modal-footer">
                        <a href="javascript:" class="c-link confirm_delete_link">Confirm</a>
                    </div>

                </div>
            </div>
        </div>
        <!-- END of modal -->
            <script type="text/javascript">
                // setTimeout(function() {
                //     $('.s_message').hide('slow');
                // }, 7000);
                // setTimeout(function() {
                //     $('.e_message').hide('slow');
                // }, 7000);
        </script>

    </body>
</html>
