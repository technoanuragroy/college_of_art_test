<aside class="side_nav noprint_old">
    <div class="sidenav_inner">
        <div class="sidenav_logo_btn">
            <a href="#"><?php echo SITE_NAME; ?></a>
            <span><i class="fa fa-bars"></i></span>
        </div>

        <ul class="sideNav_links">
            <li><a href="<?php echo base_url(); ?>admin/user/dashboard"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
            <li><a href="<?php echo base_url(); ?>admin/user/manageAdmins"><i class="fa fa-pencil-square-o"></i><span>Manage Admins</span></a></li>
            <li><a href="javascript:;"><i class="fa fa-cog"></i><span>Settings</span></a>
                <ul class="sub_links">
                    <li><a href="<?php echo base_url(); ?>admin/settings/emailSettings">Email Settings</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/settings/smsSettings">SMS Settings</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/settings/userSettings">User Settings</a></li>
                </ul>
            </li>
            <li><a href="javascript:;"><i class="fa fa-cog"></i><span>Information</span></a>
                <ul class="sub_links">
                    <li><a href="<?php echo base_url(); ?>admin/college/manageSession">Manage Session</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/college/manageClasses">Manage Courses</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/college/manageSections">Manage Group</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/college/manageSemester">Manage Semester</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/college/manageSubjects">Manage Subjects</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/college/manageTerms">Manage Terms</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/college/manageExam">Manage Exam</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/college/manageScore">Manage Score</a></li>


                </ul>
            </li>
            <li><a href="<?php echo base_url(); ?>admin/cms"><i class="fa fa-file-text-o"></i><span>Manage CMS</span></a></li>
            <li><a href="<?php echo base_url(); ?>admin/college/manageEnrollStudent"><i class="fa fa-file-text-o"></i><span>Manage Student</span></a></li>
            <li><a href="<?php echo base_url(); ?>admin/college/manageStudentRegister"><i class="fa fa-file-text-o"></i><span>Manage Admission</span></a></li>
            <li><a href="<?php echo base_url(); ?>admin/college/manageTimeTable"><i class="fa fa-file-text-o"></i><span>Manage Time Table</span></a></li>

            <li><a href="javascript:;"><i class="fa fa-cog"></i><span>Accounts</span></a>
                <ul class="sub_links">
                  <li><a href="<?php echo base_url(); ?>admin/college/paymentRecord">Add Payment Records</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/college/paymentReceived">Payment Received</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/college/expenseReport">Expenses</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/college/totalPaymentReport">Total Payment Report</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/college/paymentReport">Payment Report</a></li>
                </ul>
            </li>
            <li><a href="<?php echo base_url(); ?>admin/college/manageStudent"><i class="fa fa-file-text-o"></i><span>Manage User</span></a></li>
            <li><a href="<?php echo base_url(); ?>admin/college/manageNotice"><i class="fa fa-bell"></i><span>Manage Notice</span></a></li>
            <li><a href="javascript:;"><i class="fa fa-cog"></i><span>General info</span></a>
                <ul class="sub_links">
                  <li><a href="<?php echo base_url(); ?>admin/college/manageSyllabus">Manage Syllabus</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/college/manageFeesStructure">Manage FeesStructure</a></li>
                </ul>
            </li>
            <!-- <li><a href="<?php echo base_url(); ?>admin/college/managePrincipal"><i class="fa fa-file-text-o"></i><span>Manage Principal</span></a></li> -->
            <li><a href="<?php echo base_url(); ?>admin/college/manage_staff"><i class="fa fa-file-text-o"></i><span>Manage Staff</span></a></li>
            <li><a href="javascript:;"><i class="fa fa-cog"></i><span>Attendance</span></a>
                <ul class="sub_links">
                  <li><a href="<?php echo base_url(); ?>admin/college/manage_attendance">Manage Attendance</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/college/attendance_report">Attendance Report</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/college/pay_slip">Pay Silp</a></li>
                  <li><a href="<?php echo base_url(); ?>admin/college/pay_slip_report">Pay Silp Report</a></li>
                </ul>
            </li>

            <li><a href="javascript:;"><i class="fa fa-picture-o"></i><span>Gallery</span></a>
                <ul class="sub_links">
                    <li><a href="<?php echo base_url(); ?>admin/gallery/manageAlbums">Manage Albums</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/gallery/manageFiles">Manage Files</a></li>
                </ul>
            </li>
        </ul>
    </div>
</aside>
