<?php $this->load->view("admin/include/header"); ?>            

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Update Admin Details</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">    

                        <?php echo form_open('', array('id' => 'formEditAdmin')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>Admin Name</label>
                                            <input type="text" name="name" id="name" placeholder="Name" value="<?php echo $admin_details['name']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Email</label>
                                            <input type="email" name="email" id="email" placeholder="Email" value="<?php echo $admin_details['email']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Phone Number</label>
                                            <input type="number" name="phone" id="phone" placeholder="Phone Number" value="<?php echo $admin_details['phone']; ?>" required="">
                                        </span>
                                    </li>              

                                    <li class="full_width_li">
                                        <span>
                                            <label>Password</label>
                                            <input type="password" name="password" id="password" placeholder="Password">
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Confirm Password</label>
                                            <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password"> 
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Admin Type</label>
                                            <select name="type" id="type" required="">
                                                <option value="">Admin Type</option>
                                                <option value="<?php echo ADMIN_TYPE_SUPER_ADMIN; ?>" <?php if($admin_details['type'] == ADMIN_TYPE_SUPER_ADMIN) { echo 'selected="selected"'; } ?>>Super Admin</option>
                                                <option value="<?php echo ADMIN_TYPE_ADMIN; ?>" <?php if($admin_details['type'] == ADMIN_TYPE_ADMIN) { echo 'selected="selected"'; } ?>>Admin</option>                                              
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Status</label>
                                            <select name="status" id="status">
                                                <option value="1" <?php if($admin_details['status'] == 1) { echo 'selected="selected"'; } ?>>Active</option>
                                                <option value="0" <?php if($admin_details['status'] == 0) { echo 'selected="selected"'; } ?>>Inactive</option>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">	
                                        <input type="hidden" name="admin_id" value="<?php echo $admin_details['encrypted_id']; ?>">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Update">
                                    </li>
                                </ul> 

                        <?php echo form_close(); ?>
                        
                    </div>
                </div>    
            </div>
        </div>    
    </div>	     

<?php $this->load->view("admin/include/footer"); ?>
