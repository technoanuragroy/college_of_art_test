<?php $this->load->view("admin/include/header"); ?>
        
    <script type="text/javascript">     
        $(document).ready(function() {
            
            $("#submit").hide();
        });
        
        function check_username(username) {              
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>admin/user/validateAdmin",
                data: "username="+username,
                success: function(msg) { 
                    if(msg != "") {                                                    
                        $("#username_error").text(msg); 
                        $("#submit").hide();
                    } else {
                        $("#username_error").text("");  
                        $("#submit").show();
                    }
                }
            });         
        }
    </script>  

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Enter Admin Details</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">      
      
                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>Admin Name</label>
                                            <input type="text" name="name" id="name" placeholder="Admin Name" required="">
                                        </span>
                                    </li> 

                                    <li>
                                        <span>
                                            <label>Email</label>
                                            <input type="email" name="email" id="email" placeholder="Email" required="">
                                        </span>
                                    </li> 

                                    <li>
                                        <span>
                                            <label>Phone Number</label>
                                            <input type="number" name="phone" id="phone" placeholder="Phone Number" required="">
                                        </span>
                                    </li> 

                                    <li class="full_width_li">
                                        <span>
                                            <label>Username</label>
                                            <input type="text" name="username" id="username" placeholder="Username" onkeyup="return check_username(this.value);" required="">
                                            <div id="username_error"></div>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Password</label>
                                            <input type="password" name="password" id="password" placeholder="Password" required="">
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Confirm Password</label>
                                            <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password" required=""> 
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Admin Type</label>
                                            <select name="type" id="type" required="">
                                                <option value="">Admin Type</option>
                                                <option value="<?php echo ADMIN_TYPE_SUPER_ADMIN; ?>">Super Admin</option>
                                                <option value="<?php echo ADMIN_TYPE_ADMIN; ?>">Admin</option>                                                                                        
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Status</label>
                                            <select name="status" id="status">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </span>
                                    </li>                             

                                    <li class="full_width_li">	
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Register">
                                    </li>
                                </ul> 

                        <?php echo form_close(); ?>
                        
                    </div>
                </div>    
            </div>
        </div>    
    </div>	         
                
<?php $this->load->view("admin/include/footer"); ?>




























































































