<?php $this->load->view("admin/include/header"); ?>            
      
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">User Settings</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">      
      
                        <?php echo form_open('', array('id' => 'formUserSettings')); ?>
            
                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <h5 class="heading_05">Email Verification</h5>
                                            <label class="container_radio">Enabled
                                                <input type="radio" name="email_verification" value="1" <?php if($email_verification == 1) { echo 'checked="checked"'; } ?>>                                
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container_radio">Disabled
                                                <input type="radio" name="email_verification" value="0" <?php if($email_verification == 0) { echo 'checked="checked"'; } ?>>                                
                                                <span class="checkmark"></span>
                                            </label>
                                        </span>                                            
                                    </li>   
                                    
                                    <li class="full_width_li">
                                        <span>
                                            <h5 class="heading_05">OTP Verification</h5>
                                            <label class="container_radio">Enabled
                                                <input type="radio" name="otp_verification" value="1" <?php if($otp_verification == 1) { echo 'checked="checked"'; } ?>>                                
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container_radio">Disabled
                                                <input type="radio" name="otp_verification" value="0" <?php if($otp_verification == 0) { echo 'checked="checked"'; } ?>>
                                                <span class="checkmark"></span>
                                            </label>
                                        </span>                                            
                                    </li>      
                                    
                                    <li class="full_width_li">
                                        <span>
                                            <h5 class="heading_05">Username Content</h5>            
                                            <label class="container_radio">Email
                                                <input type="radio" name="username" value="1" <?php if($username == 1) { echo 'checked="checked"'; } ?>>      
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container_radio">Mobile
                                                <input type="radio" name="username" value="2" <?php if($username == 2) { echo 'checked="checked"'; } ?>>  
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="container_radio">General
                                                <input type="radio" name="username" value="3" <?php if($username == 3) { echo 'checked="checked"'; } ?>>                                
                                                <span class="checkmark"></span>
                                            </label>    
                                        </span>                                            
                                    </li>     
                                                                        
                                    <li class="full_width_li">	
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Save">
                                    </li>
                                </ul>                        
             
                        <?php echo form_close(); ?>
                
                    </div>
                </div>    
            </div>
        </div>    
    </div>	         
                
<?php $this->load->view("admin/include/footer"); ?>




























































































