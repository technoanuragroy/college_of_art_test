<?php $this->load->view("admin/include/header"); ?>            

    <script type="text/javascript">     
        $(document).ready(function() {
                                    
            $("#gateway_settings").on("click", function() {
                 
                // Make the inputs as "required" inputs if settings is enabled  
                if($(this).prop("checked")) {
                    $(".config").each(function() {
                        $(this).attr("required", "required");       
                        $(".settings_container").slideDown();
                    }); 
                } else {
                    $(".config").each(function() {
                        $(this).removeAttr("required");    
                        $(".settings_container").slideUp();
                    }); 
                }                               
            });
        });
    </script>      
    
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Email Gateway Settings</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">    
      
                        <?php echo form_open('', array('id' => 'formEmailSettings')); ?>
                                                                                           
                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <h5 class="heading_05">Enable Gateway Settings</h5>
                                            <label class="container_checkbox">Enable
                                                <input type="checkbox" name="gateway_settings" id="gateway_settings" value="1" <?php if($gateway_settings == 1) { echo 'checked="checked"'; } ?>>                                
                                                <span class="checkmark"></span>
                                            </label>
                                        </span>                                            
                                    </li>      

                                    <div class="settings_container" <?php echo $display; ?>>
                                        <li class="full_width_li">
                                            <span>
                                                <label>Gateway Label For UI</label>
                                                <input type="text" name="gateway_label" id="gateway_label" class="config" placeholder="Gateway Label For UI" value="<?php echo $gateway_label; ?>" required="">
                                            </span>
                                        </li>

                                        <li>
                                            <span>
                                                <label>Host</label>
                                                <input type="text" name="host" id="host" class="config" placeholder="Host" value="<?php echo $host; ?>" required="">
                                            </span>
                                        </li>  

                                        <li>
                                            <span>
                                                <label>Port</label>
                                                <input type="text" name="port" id="port" class="config" placeholder="Port" value="<?php echo $port; ?>" required="">
                                            </span>
                                        </li>

                                        <li>
                                            <span>
                                                <label>Username</label>
                                                <input type="text" name="username" id="username" class="config" placeholder="Username" value="<?php echo $username; ?>" required="">
                                            </span>
                                        </li>

                                        <li>
                                            <span>
                                                <label>Password</label>
                                                <input type="text" name="password" id="password" class="config" placeholder="Password" value="<?php echo $password; ?>" required="">
                                            </span>
                                        </li>                                                                            
                                    </div>
                                    
                                    <li class="full_width_li">	
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Save">
                                    </li>                                     
                                </ul>  
             
                        <?php echo form_close(); ?>
                
                    </div>
                </div>    
            </div>
        </div>    
    </div>	         
                
<?php $this->load->view("admin/include/footer"); ?>




























































































