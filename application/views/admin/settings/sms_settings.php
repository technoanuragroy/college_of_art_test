<?php $this->load->view("admin/include/header"); ?>            

    <script type="text/javascript">     
        $(document).ready(function() {
                                    
            $("#gateway_settings").on("click", function() {
                 
                // Make the inputs as "required" inputs if settings is enabled  
                if($(this).prop("checked")) {
                    $(".config").each(function() {
                        $(this).attr("required", "required");       
                        $(".settings_container").slideDown();
                    }); 
                } else {
                    $(".config").each(function() {
                        $(this).removeAttr("required");    
                        $(".settings_container").slideUp();
                    }); 
                }                               
            });
            
            $('.copier_link').click(function () {
                var variable_html = $(".sms_variables_copy").html();
                $(".sms_variables_append").append(variable_html);
            });
        });
        
        function remove_variable(e) {

            $(e).closest(".sms_variable").remove();
        }
    </script>      
    
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">SMS Gateway Settings</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">    
      
                        <?php echo form_open('', array('id' => 'formSMSSettings')); ?>
            
                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <h5 class="heading_05">Enable Gateway Settings</h5>
                                            <label class="container_checkbox">Enable
                                                <input type="checkbox" name="gateway_settings" id="gateway_settings" value="1" <?php if($gateway_settings == 1) { echo 'checked="checked"'; } ?>>                                
                                                <span class="checkmark"></span>
                                            </label>
                                        </span>                                            
                                    </li>                                                  
                                                
                                    <div class="settings_container" <?php echo $display; ?>>                    
                                        <li class="full_width_li">
                                            <span>
                                                <label>Gateway Label For UI</label>
                                                <input type="text" name="gateway_label" id="gateway_label" class="config" placeholder="Gateway Label For UI" value="<?php echo $gateway_label; ?>" required="">
                                            </span>
                                        </li>

                                        <li class="full_width_li">
                                            <span>
                                                <label>SMS url</label>
                                                <input type="text" name="sms_url" id="sms_url" class="config" placeholder="SMS url" value="<?php echo $sms_url; ?>" required="">
                                            </span>
                                        </li>

                                        <div class="sms_variables_append">                                                 
                                            <?php 
                                                if(!empty($variables)) {
                                                    foreach($variables as $variable_name => $variable_value) { 
                                            ?>  
                                                        <div class="sms_variable">
                                                            <li>
                                                                <span>
                                                                    <label>Variable Name</label>
                                                                    <input type="text" name="variable_name[]" class="config" placeholder="Variable Name" value="<?php echo $variable_name; ?>" required="">
                                                                </span>
                                                            </li>

                                                            <li>
                                                                <span>
                                                                    <label>Variable Value</label>
                                                                    <input type="text" name="variable_value[]" class="config" placeholder="Variable Value" value="<?php echo $variable_value; ?>" required="">
                                                                </span>
                                                            </li>

                                                            <li class="full_width_li">
                                                                <span>
                                                                    <a href="javascript:" onclick="remove_variable(this);"><i class="fa fa-minus-circle"></i> Remove Variable</a>
                                                                </span>
                                                            </li>  
                                                        </div>    
                                            <?php
                                                    }
                                                }    
                                            ?>
                                        </div>    
                                        
                                        <li class="full_width_li">
                                            <span>
                                                <a href="javascript:" class="copier_link"><i class="fa fa-plus"></i> Add More Variable</a>
                                            </span>
                                        </li>    
                                    </div>    
                     
                                    <li class="full_width_li">	
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Save">
                                    </li>                                     
                                </ul> 
             
                        <?php echo form_close(); ?>
                        
                        <div class="sms_variables_copy" style="display: none;">          
                            <div class="sms_variable">
                                <li>
                                    <span>
                                        <label>Variable Name</label>
                                        <input type="text" name="variable_name[]" class="config" placeholder="Variable Name" value="" required="">
                                    </span>
                                </li>

                                <li>
                                    <span>
                                        <label>Variable Value</label>
                                        <input type="text" name="variable_value[]" class="config" placeholder="Variable Value" value="" required="">
                                    </span>
                                </li>   

                                <li class="full_width_li">
                                    <span>
                                        <a href="javascript:" onclick="remove_variable(this);"><i class="fa fa-minus-circle"></i> Remove Variable</a>
                                    </span>
                                </li>  
                            </div>
                        </div>  
                
                    </div>
                </div>    
            </div>
        </div>    
    </div>	         
                
<?php $this->load->view("admin/include/footer"); ?>




























































































