<?php $this->load->view("admin/include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Session</h2>

                <a href="<?php echo base_url(); ?>admin/college/addSession" class="c-link">Add Session</a>

                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Session Name</th>
                                    <th>From Date</th>
                                    <th> To Date</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($all_session)) {
                                    foreach($all_session as $page) {
                                     $session_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>
                                            <td><?php echo $page['session_name']; ?></td>
                                            <td><?php echo $page['from_date']; ?></td>
                                            <td><?php echo $page['to_date']; ?></td>
                                            <td>
                                               <?php if (date('Y-m-d') > $page['to_date']) { ?>
                                                   <span class="badge badge-danger">Past</span>
                                               <?php } ?>
                                               <?php if (date('Y-m-d') > $page['from_date'] && date('Y-m-d') < $page['to_date']) { ?>
                                                   <span class="badge badge-success">Ongoing</span>
                                               <?php } ?>
                                               <?php if ($page['from_date']> date('Y-m-d')) { ?>
                                                   <span class="badge badge-primary">Future</span>
                                               <?php } ?>

                                          </td>
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/editSession/'.$session_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteSession/'.$session_id; ?>');" title="Delete Record">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="8">No Session found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
