<?php $this->load->view("admin/include/header"); ?>
      <script>
      $(document).ready(function() {
          $('a.back').click(function() {
              parent.history.back();
              return false;
          });
          $('a.printme').click(function(){
       window.print();
      });
      });
      </script>
      <style>
      .pay_slip{
        width: 56%;
        align: center;
      }
      @media print {

      .noprint_old { display: none; }
      }
      </style>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
              <?php
                function numberTowords($num)
                {

                $ones = array(
                0 =>"Zero",
                1 => "One",
                2 => "Two",
                3 => "Three",
                4 => "Four",
                5 => "Five",
                6 => "Six",
                7 => "Seven",
                8 => "Eight",
                9 => "Nine",
                10 => "Ten",
                11 => "Eleven",
                12 => "Twelve",
                13 => "Thirteen",
                14 => "Fourteen",
                15 => "Fifteen",
                16 => "Sixteen",
                17 => "Seventeen",
                18 => "Eighteen",
                19 => "Nineteen",
                "014" => "Fourteen"
                );
                $tens = array(
                0 => "Zero",
                1 => "Ten",
                2 => "Twenty",
                3 => "Thirty",
                4 => "Forty",
                5 => "Fifty",
                6 => "Sixty",
                7 => "Seventy",
                8 => "Eighty",
                9 => "Ninety"
                );
                $hundreds = array(
                "Hundred",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quardrillion"
                ); /*limit t quadrillion */
                $num = number_format($num,2,".",",");
                $num_arr = explode(".",$num);
                $wholenum = $num_arr[0];
                $decnum = $num_arr[1];
                $whole_arr = array_reverse(explode(",",$wholenum));
                krsort($whole_arr,1);
                $rettxt = "";
                foreach($whole_arr as $key => $i){

                while(substr($i,0,1)=="0")
                		$i=substr($i,1,5);
                if($i < 20){
                /* echo "getting:".$i; */
                $rettxt .= $ones[$i];
                }elseif($i < 100){
                if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)];
                if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)];
                }else{
                if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0];
                if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)];
                if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)];
                }
                if($key > 0){
                $rettxt .= " ".$hundreds[$key]." ";
                }
                }
                if($decnum > 0){
                $rettxt .= " and ";
                if($decnum < 20){
                $rettxt .= $ones[$decnum];
                }elseif($decnum < 100){
                $rettxt .= $tens[substr($decnum,0,1)];
                $rettxt .= " ".$ones[substr($decnum,1,1)];
                }
                }
                return $rettxt;
                }

              $designation = $this->my_custom_functions->get_particular_field_value("tbl_staff","designation", 'and id="'.$slip['emp_id'].'"');
              if ($designation == 1) {
                $desig_name = 'Principal';
              }
              else if ($designation == 2) {
                $desig_name = 'Teaching Staff';
              }
              else if ($designation == 3) {
                $desig_name = 'Non Teaching Staff';
              }
              ?>
            <div class="completeWrap_inner_main pay_slip">
                <h2 class="" style="text-align:center;">College of Art & Design</h2>
                <p class="" style="text-align:center;">(Affiliated to the University of Burdwan)</p>
                <p class="" style="text-align:left;">Name : &nbsp<?php echo $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$slip['emp_id'].'"'); ?></p>
                <p class="" style="text-align:left;">Designation : <?php echo $desig_name; ?></p>
                <div class="table_wrap">


                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th colspan="4">Debit</th>
                                    <th colspan="2">Amount</th>
                                </tr>
                                <tr>
                                    <th colspan="4"></th>
                                    <th colspan="2">Rs</th>
                                </tr>
                            </thead>
                            <tbody>

                                          <tr>
                                              <td colspan="4">Pay</td>
                                              <td colspan="2"><?php echo $slip['basic_pay']; ?></td>
                                          </tr>
                                          <tr>
                                              <td colspan="4">Grade Pay</td>
                                              <td colspan="2"><?php echo $slip['grade_pay']; ?></td>
                                          </tr>

                                          <tr>
                                              <td colspan="4">M.A.</td>
                                              <td colspan="2"><?php echo $slip['medical_allow']; ?></td>
                                          </tr>
                                          <tr>
                                              <td colspan="4">Other (if any)
                                              <?php if ($slip['other_allow_title']!='') {
                                                echo "(".$slip['other_allow_title'].")";
                                              }?></td>
                                              <td colspan="2"><?php echo $slip['other_allow']; ?></td>
                                          </tr>
                                          <tr>
                                              <td colspan="4">P.F. (13.61%)</td>
                                              <td colspan="2"><?php echo $slip['emp_contribute_pf']; ?></td>
                                          </tr>
                                          <tr>
                                              <th colspan="4" >Grand Total :</th>
                                              <td colspan="2"><?php echo $slip['salary_total']; ?></td>
                                          </tr>
                                          <tr>
                                              <th colspan="4">Deductions (Credit)</th>
                                              <td colspan="2"></td>
                                          </tr>
                                          <tr>
                                              <td colspan="4">P.F. (12% +13.61%)</td>
                                              <td colspan="2"><?php echo $slip['pf_deduction']; ?></td>
                                          </tr>
                                          <tr>
                                              <td colspan="4">I. T.</td>
                                              <td colspan="2"><?php echo $slip['income_tax']; ?></td>
                                          </tr>
                                          <tr>
                                              <td colspan="4">P. Tax</td>
                                              <td colspan="2"><?php echo $slip['ptax']; ?></td>
                                          </tr>
                                          <tr>
                                              <td colspan="4">Recov. Of Festival Adv.</td>
                                              <td colspan="2"><?php echo $slip['fest_advance']; ?></td>
                                          </tr>
                                          <tr>
                                              <td colspan="4">Other
                                                <?php if ($slip['other_deduction_title']!='') {
                                                  echo "(".$slip['other_deduction_title'].")";
                                                }?></td>
                                              <td colspan="2"><?php echo $slip['other_deduction']; ?></td>
                                          </tr>
                                          <tr>
                                              <th colspan="4">Grand Total of Deducations</th>
                                              <td colspan="2"><?php echo $slip['total_deduction']; ?></td>
                                          </tr>
                                          <tr>
                                              <th colspan="4">Net Amount Payable</th>
                                              <td colspan="2"><?php echo $slip['net_pay']; ?></td>
                                          </tr>
                                          <tr>
                                              <td colspan="4"></td>
                                              <td colspan="2"></td>
                                          </tr>
                                          <tr>
                                              <td colspan="4">Pay</td>
                                              <td colspan="2">Rs.<?php echo $slip['net_pay']; ?></td>
                                          </tr>
                                          <tr>
                                              <td colspan="6" ><?php echo "( Rupees".numberTowords($slip['net_pay']).")Only"; ?></td>
                                          </tr>
                                          <tr>
                                              <td colspan="2">Asstt</td>
                                              <td colspan="2">Accountant</td>
                                              <td colspan="2">Secretary</td>
                                          </tr>

                            </tbody>
                        </table>
                    </div>
                    <p  class="noprint_old">
                        <a href="#" class="btn btn-success min-width-125 back" >Back</a>

                       <a href="#" class="btn btn-success min-width-125 printme" >Print</a>
                    </p>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
