<?php $this->load->view("admin/include/header"); ?>
<style>
    #div_slideDown{
        display: none;
    }
</style>
<script type="text/javascript">

     jQuery(document).ready(function ($) {
// executes when HTML-Document is loaded and DOM is ready
        $("#clickbtn").click(function () {
            $("#div_slideDown").slideToggle('fast');
        });

    });

</script>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02"><?php echo $page_title; ?></h2>

                <a href="<?php echo base_url(); ?>admin/college/addSections" class="c-link">Add Group</a>
                <a href="javascript:" class="c-link" id="clickbtn">Search Group</a>

                 <div class="form_wrap" id="div_slideDown">

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">


                                    <li class="full_width_li">
                                        <span>
                                            <!-- <label>Status</label> -->
                                            <select id="class" name="class">
                                                <option value="">Select Course</option>
                                <?php
                                $selected = '';
                                foreach ($class_list as $class) {
                                    if ($post_data['class_id'] != '') {
                                        if ($post_data['class_id'] == $class['id']) {

                                            $selected = 'selected="selected"';
                                        } else {

                                            $selected = '';
                                        }
                                    }
                                    ?>
                                    <option value="<?php echo $class['id']; ?>" <?php echo $selected; ?>><?php echo $class['course_name']; ?></option>
                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Search">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>

                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Group Name</th>
                                    <th>Course</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($all_section)) {
                                    foreach($all_section as $page) {
                                     $section_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>

                                            <td><?php echo $page['section_name']; ?></td>
                                            <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_courses","course_name", 'and id="'.$page['class_id'].'"'); ?></td>
                                            <td><?php
                                                  if ($page['status'] == 1) {
                                                      echo "<span class='badge badge-success'>Active</span>";
                                                  } else {
                                                      echo "<span class='badge badge-danger'>Inactive</span>";
                                                  }
                                                  ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/editSections/'.$section_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteSections/'.$section_id; ?>');" title="Delete Record">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="8">No Group found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
