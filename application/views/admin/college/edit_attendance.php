<?php $this->load->view("admin/include/header"); ?>

<script>
    $(document).ready(function() {
        //$("#frm1").validate();
    });
    $(document).ready(function() {
        $('a.back').click(function() {
            parent.history.back();
            return false;
        });
         $('#cl').blur(function(){

           var sub = parseFloat($('#total_cl').val())+parseFloat($('#cl').val());

           if(sub<=10){
                $('#cl_msg').html('');
               document.getElementById("submit").disabled = false;
           }else{
               $('#cl_msg').html('Maximum number of CL is 10');
              document.getElementById("submit").disabled = true;
           }
        });
            $('#sl').blur(function(){

           var sub = parseFloat($('#total_sl').val())+parseFloat($('#sl').val());

           if(sub<=4){
                $('#sl_msg').html('');
               document.getElementById("submit").disabled = false;
           }else{
               $('#sl_msg').html('Maximum number of SL is 4');
              document.getElementById("submit").disabled = true;
           }
        });
    });

</script>
<h1>Edit Attendance</h1>
<div id="body">
    <div class="err"><?php echo validation_errors(); ?></div>
    <div class="err">
      <?php

      function monthDropdown($name = "month", $selected = null) {
          $dd = '<select name="' . $name . '" id="' . $name . '">';

          $months = array(
              1 => 'January',
              2 => 'February',
              3 => 'March',
              4 => 'April',
              5 => 'May',
              6 => 'June',
              7 => 'July',
              8 => 'August',
              9 => 'September',
              10 => 'October',
              11 => 'November',
              12 => 'December');
          /*         * * the current month ** */
          $selected = is_null($selected) ? date('n', time()) : $selected;
               for ($i = 1; $i <= 12; $i++) {

              if ($i == $selected) {
                  $dd .= '<option value="' . $i . '" selected';
                  $dd .= '>' . $months[$i] . '</option>';
              }
              /*             * * get the month ** */
          }
          $dd .= '</select>';
          return $dd;
      }

      function yearDropdown($startYear, $endYear, $id = "year") {
          //start the select tag
          echo "<select id=" . $id . " name=" . $id . ">n";
               //echo each year as an option
          for ($i = $startYear; $i <= $endYear; $i++) {
              echo "<option value=" . $i . ">" . $i . "</option>n";
          }

          //close the select tag
          echo "</select>";
      }

      echo form_open('', array('id' => 'frm1'));
    ?>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">
                    <ul class="input_listing">
                   <fieldset>
                   <legend>Personal Information</legend>
                   <input type="hidden" name="id" value="<?php echo $attend->id; ?>">
                   <input type="hidden" name="emp_id" value="<?php echo $attend->emp_id; ?>">
                   <input type="hidden" name="admin_name" value="<?php echo $this->session->userdata("admin_name"); ?>">

                    <!-- <input type="hidden" name="emp_id" id="hid_emp_id" value="<?php echo $employee->id; ?>"> -->
                    <p><label>Employee Name: </label>
                        <?php echo $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$attend->emp_id.'"'); ?>
                    </p>

        <p><label>Date: </label>
          <?php
          /*             * * example usage ** */
          $dtime = new DateTime($attend->date_month);
          $month = $dtime->format("m");
          $year=$dtime->format("Y");
          echo monthDropdown('month', $month);
          yearDropdown($year, $year);
          ?>
        </p>

        <p><label>Present: </label></label><?php echo form_input(array('name' => 'present', 'id' => 'present'), set_value('present', $attend->present)); ?></p>
        <p><label>Absent: </label><?php echo form_input(array('name' => 'absent', 'id' => 'absent', 'class' => 'required number'), set_value('absent', $attend->absent)); ?></p>
        <p><label>PL: </label><?php echo form_input(array('name' => 'pl', 'id' => 'pl','class'=>'number'), set_value('pl', $attend->pl)); ?></p>
        <p><label>CL: </label><?php echo form_input(array('name' => 'cl', 'id' => 'cl','class'=>'number'), set_value('cl', $attend->cl)); ?>

            <span id="cl_show">
            <?php
            $allowed_cl = 10;
            $total_cl = 0;
            $total_sl = 0;

            if(isset($employee->doj)){
                  $join_year = date('Y', strtotime($employee->doj));
                  if($join_year == date('Y')) {
                      $join_month = date('m', strtotime($employee->doj));
                      $month_remaining = 12-$join_month;
                      $allowed_cl = round((10/12)*$month_remaining);
                  }
            }

            if (isset($cl)) {
                foreach ($cl as $value) {
                    $total_cl += $value->cl;
                }
                echo "( " . $total_cl . " used , ". ($allowed_cl - $total_cl)." allowed )";
            } else {
                echo "empty";
            }
            ?>
            </span>
            <input type="hidden" value="<?php echo $total_cl;?>" id="total_cl">
            <input type="hidden" value="<?php echo $allowed_cl;?>" id="allowed_cl">
            <span id="cl_msg" style="color: red;"></span>

        </p>
        <p><label>SL: </label><?php echo form_input(array('name' => 'sl', 'id' => 'sl','class'=>'number'), set_value('sl',$attend->sl)); ?>

            <span id="sl_show">
            <?php
            if (isset($cl)) {
                foreach ($cl as $value) {
                    $total_sl+= $value->sl;
                }
                echo "( " . $total_sl . " used )";
            } else {
                echo "empty";
            }
            ?>
            </span>
            <input type="hidden" value="<?php echo $total_sl;?>" id="total_sl">
            <span id="sl_msg" style="color: red;"></span>
        </p>
        <p><label>OPL(Without Pay Leave): </label><?php echo form_input(array('name' => 'opl', 'id' => 'opl','class'=>'number'), set_value('opl', $attend->opl)); ?></p>
        <p><label>Working Days: </label><?php echo form_input(array('name' => 'working_days', 'id' => 'working_days'), set_value('working_days', $attend->working_days)); ?></p>
        <p><label>Note: </label><?php echo form_textarea(array('name' => 'note'), set_value('note', $attend->note)); ?></p>
    </fieldset>


    <li class="full_width_li">
    <p><?php
        //echo form_submit('submit','Submit');
        echo form_input(array('type' => 'submit', 'name' => 'submit', 'value' => 'Update','id' =>'submit','class'=>'submit_btn'));
        ?>
      </p>
    </li>
    <?php echo form_close(); ?>

    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>



<?php $this->load->view("admin/include/footer"); ?>
