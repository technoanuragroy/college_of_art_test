<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">
    $(document).ready(function() {
      $(".TabDisplayRecords").tablesorter({sortList: [[0,0]]} );
      $('a.printme').click(function(){
     window.print();
    });
    ////////////////////////////////////////////////////////////////
    $("#emp_name").autocomplete({

        source: "<?php echo base_url(); ?>admin/college/ajax_get_emps",
        minLength: 1
    });
    $('#from_date').datepicker(
            {changeMonth: true,
                changeYear: true,
                beforeShowDay: function(date) {
                    //getDate() returns the day (0-31)
                    if (date.getDate() == 1) {
                        return [true, ''];
                    }
                    return [false, ''];
                },dateFormat: 'yy-mm-dd'
            }
    );
    $('#to_date').datepicker(
            {
                changeMonth: true,
                changeYear: true,
                beforeShowDay: function(date) {
                    //getDate() returns the day (0-31)
                    if (date.getDate() == 1) {
                        return [true, ''];
                    }
                    return [false, ''];
                },dateFormat: 'yy-mm-dd'
            }
    );

});

</script>
<style>
    @media print {
   .noprint { display: none; }
   .noprint_old { display: none; }
}
</style>

<div id="body">

  <h1>Pay Slip Report <?php if (isset($emp_name)) { ?> of <?php echo $emp_name; ?> from <?php echo $from_date; ?> to <?php echo $to_date;
  } ?></h1>

    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

    <ul class="input_listing">
      <div class="noprint">
       <?php echo form_open('', array('id' => 'form1')); ?>
          <li class="full_width_li">
          <p>Emp Name &nbsp;<?php echo form_input(array('name' => 'emp_name', 'id' => 'emp_name','placeholder'=> 'Employee Name'), set_value('emp_name')); ?>
          </li>
          <li class="" colspan="3">
          &nbsp; From Date &nbsp;<?php echo form_input(array('name' => 'from_date', 'id' => 'from_date', 'class' => 'required','placeholder'=> 'From Date'),set_value('from_date')); ?>
          </li>
          <li class="" colspan="3">
          &nbsp; To Date &nbsp;<?php echo form_input(array('name' => 'to_date', 'id' => 'to_date', 'class' => 'required','placeholder'=> 'To Date'),set_value('to_date')); ?>
          </li>
          <li class="full_width_li">
          <?php echo form_input(array('type' => 'submit', 'name' => 'submit', 'value' => 'Search','class'=>'submit_btn')); ?></p>
          </li>
          <?php echo form_close(); ?>
          </ul>
        </div>

<?php if (!empty($slip)) { ?>

  <div class="table-responsive">
      <table class="table table-default table-bordered table-striped table-hover TabDisplayRecords">
            <thead>
              <tr>
                    <th align="left">Employee</th>
                    <th align="left">Pay</th>
                    <th align="left">Grade Pay</th>
                    <th align="left">Other & Med. Allow:</th>
                    <th align="left">Employer P.F. (13.61%):</th>
                    <th align="left">TOTAL:</th>
                    <th align="left">P.Tax:</th>
                    <th align="left">I. Tax:</th>
                    <th align="left">PF25.61%:</th>
                    <th align="left">Festival Adv:</th>
                    <th align="left">Total Deduction: </th>
                    <th align="left">Net Pay:</th>
                    <td align="center" class="noprint"><b>Action</b></td>
                </tr>
            </thead>
            <tbody>
           <?php
           $basic_pay_total=0;$grade_pay_total=0;$medical_allow_total=0;$emp_contribute_pf_total=0;$salary_total_total=0;
           $ptax_total=0;$income_tax_total=0;$pf_deduction_total=0;$fest_advance_total=0;$total_deduction_total=0;$net_pay_total=0;
    foreach ($slip as $row) :
        ?>
                    <tr>
                        <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$row->emp_id.'"'); ?></td>
                        <td align="right"><?php  $basic_pay_total+=$row->basic_pay; echo floatval($row->basic_pay);?></td>
                        <td align="right"><?php $grade_pay_total+=$row->grade_pay; echo floatval($row->grade_pay); ?></td>
                        <td align="right"><?php $medical_allow_total+=$row->medical_allow; echo floatval($row->medical_allow); ?></td>
                        <td align="right"><?php $emp_contribute_pf_total+=$row->emp_contribute_pf	; echo floatval($row->emp_contribute_pf)	; ?></td>
                        <td align="right"><?php $salary_total_total+=$row->salary_total; echo floatval($row->salary_total); ?></td>
                        <td align="right"><?php $ptax_total+=$row->ptax; echo floatval($row->ptax); ?></td>
                        <td align="right"><?php $income_tax_total+=$row->income_tax; echo floatval($row->income_tax); ?></td>
                        <td align="right"><?php $pf_deduction_total+=$row->pf_deduction; echo floatval($row->pf_deduction); ?></td>
                        <td align="right"><?php $fest_advance_total+=$row->fest_advance; echo floatval($row->fest_advance); ?></td>
                        <td align="right"><?php $total_deduction_total+=$row->total_deduction; echo floatval($row->total_deduction); ?></td>
                        <td align="right"><?php $net_pay_total+=$row->net_pay; echo floatval($row->net_pay); ?></td>




                        <td align="center"  class="noprint"> <?php echo anchor('admin/college/edit_payslip/' . $row->emp_id.'/'.$this->ablfunctions->ablEncrypt($row->id), "Edit"); ?>
                            | <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deletePaySlip/' .$this->ablfunctions->ablEncrypt($row->id);?>');"
                               title="Delete Record">
                                Delete
                            </a>
                            | <?php echo anchor('admin/college/viewPayslip/' .$this->ablfunctions->ablEncrypt($row->id), "View"); ?> </td>
                    </tr>

                 <?php endforeach; ?>
                     </tbody>
                    <tr>
                        <td>Total:</td>
                        <td align="right"><b><?php echo number_format((float)$basic_pay_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$grade_pay_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$medical_allow_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$emp_contribute_pf_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$salary_total_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$ptax_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$income_tax_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$pf_deduction_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$fest_advance_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$total_deduction_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$net_pay_total, 2, '.', '');?></b></td>
                        <td class="noprint"></td>
                    </tr>

        </table>

    <p  class="noprint">
      <a href="<?php echo base_url().'admin/college/downloadPayslipRecordEmployee/'.$row->emp_id.'/'.$from_date.'/'.$to_date; ?>" class="btn btn-success min-width-125" target="_blank">Export Table data into Excel</a>

       <a href="#" class="btn btn-success min-width-125 printme" >Print</a>
    </p>
   </div>
<?php
}
else {
    echo "No records to display";
}
?>

<?php  if (!empty($slip)) { ?>
    <div id="dvData" style="display: none;"  class="noprint">
        <table>
              <tr>
                <!-- <th>Employee</th>
                <th>Pay</th>
                <th>G Pay</th>
                <th>Med</th>
                <th>P.F.</th>
                <th>TOTAL</th>
                <th>P.Tax</th>
                <th>I. Tax</th>
                <th>PF25.61%</th>
                <th>Festival</th>
                <th>T Ded</th>
                <th>Net Pay</th> -->

                <td>Employee Name:</td>
                     <td>Pay:</td>
                    <td>Grade Pay:</td>
                    <td>Other & Med. Allow:</td>
                    <td>Employer P.F. (13.61%):</td>
                    <td>TOTAL:</td>
                    <td>P.Tax:</td>
                    <td>I. Tax:</td>
                    <td>PF25.61%:</td>
                    <td>Festival Adv:</td>
                    <td>Total Deduction:</td>
                    <td>Net Pay:</td>

                </tr>


           <?php
    foreach ($slip as $row) :
        ?>
                    <tr>
                      <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$row->emp_id.'"'); ?></td>
                      <td align="left"><?php  $basic_pay_total+=$row->basic_pay; echo floatval($row->basic_pay);?></td>
                      <td align="left"><?php $grade_pay_total+=$row->grade_pay; echo floatval($row->grade_pay); ?></td>
                      <td align="left"><?php $medical_allow_total+=$row->medical_allow; echo floatval($row->medical_allow); ?></td>
                      <td align="left"><?php $emp_contribute_pf_total+=$row->emp_contribute_pf	; echo floatval($row->emp_contribute_pf)	; ?></td>
                      <td align="left"><?php $salary_total_total+=$row->salary_total; echo floatval($row->salary_total); ?></td>
                      <td align="left"><?php $ptax_total+=$row->ptax; echo floatval($row->ptax); ?></td>
                      <td align="left"><?php $income_tax_total+=$row->income_tax; echo floatval($row->income_tax); ?></td>
                      <td align="left"><?php $pf_deduction_total+=$row->pf_deduction; echo floatval($row->pf_deduction); ?></td>
                      <td align="left"><?php $fest_advance_total+=$row->fest_advance; echo floatval($row->fest_advance); ?></td>
                      <td align="left"><?php $total_deduction_total+=$row->total_deduction; echo floatval($row->total_deduction); ?></td>
                      <td align="left"><?php $net_pay_total+=$row->net_pay; echo floatval($row->net_pay); ?></td>

                    </tr>

            <?php endforeach; ?>
             <tr>
               <td>Total:</td>
               <td align="right"><b><?php echo number_format((float)$basic_pay_total, 0, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$grade_pay_total, 0, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$medical_allow_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$emp_contribute_pf_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$salary_total_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$ptax_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$income_tax_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$pf_deduction_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$fest_advance_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$total_deduction_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$net_pay_total, 2, '.', '');?></b></td>
               <td class="noprint"></td>
              </tr>

              </table>
              </div>
    </div>


<?php
}
else {
    echo "No records to display";
}
?>
</div>

<?php $this->load->view("admin/include/footer"); ?>
