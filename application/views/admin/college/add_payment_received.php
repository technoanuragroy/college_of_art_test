<?php $this->load->view("admin/include/header"); ?>

      <script type="text/javascript">
      $(document).ready(function() {
      $('#date').datepicker(
              {changeMonth: true,
                  changeYear: true,
                  dateFormat: 'yy-mm-dd'
              }
      );
      });
      </script>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Add Payment Received</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                  <li class="full_width_li">
                                     <li colspan="3">
                                       <span>
                                           <label>Date</label>
                                           <input type="text" name="date" id="date" placeholder="Enter Date" required="" autocomplete="off">
                                       </span>
                                    </li>

                                    <li colspan="3">
                                      <span>
                                          <label>Reference Id</label>
                                          <input type="text" name="reference_id" placeholder="Enter Reference Id" required="" autocomplete="off">
                                      </span>
                                    </li>
                                  </li>
                                  <li class="full_width_li">
                                    <li colspan="3">
                                        <span>
                                            <label>Student Name</label>
                                            <input type="text" name="student_name"  placeholder="Student Name" required="">
                                        </span>
                                    </li>
                                    <li colspan="3">
                                      <span>
                                        <label>Select Payment Type</label>
                                         <select name="payment_type" required="">
                                         <option value="">Select Payment Type</option>
                                                 <?php
                                                 $p_type = $this->config->item('student_payment_type');
                                                 foreach($p_type as $key=>$mtype){?>
                                                 <option value="<?php echo $key;?>" <?php echo set_select('payment_type',$key, False); ?>><?php echo $mtype;?></option>
                                                 <?php } ?>
                                             </select>
                                     </span>
                                    </li>
                                  </li>
                                  <li class="full_width_li">
                                    <li colspan="3">
                                      <span>
                                          <label>Particular</label>
                                          <input type="text" name="particular"  placeholder="Particular" required="">
                                      </span>
                                    </li>
                                    <li colspan="3">
                                      <span>
                                          <label>Credit Amount</label>
                                          <input type="text" name="credit_ammount"  placeholder="Credit Amount" >
                                      </span>
                                    </li>
                                  </li>
                                    <li colspan="3">
                                      <span>
                                          <label>Debit Amount</label>
                                          <input type="text" name="debit_ammount"  placeholder="Debit Amount" >
                                      </span>
                                    </li>


                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Save">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
