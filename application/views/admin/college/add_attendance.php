<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">
    $(document).ready(function() {
        // $("#frm1").validate();
        //
        // $("#dob").datepicker()anurag;
        // $("#doa").datepicker();
        // $("#doj").datepicker();
        // $("#bond_start_dt").datepicker();
        // $("#bond_end_dt").datepicker();
        // $("#last_increment").datepicker();
        $("input[type=checkbox]").attr('checked', false);

        var status = (new Date).getFullYear();

        $('#year').val(status).attr("selected", "selected");


        // $('#emp_id').change(function() {
        //     var emp_id = $('#emp_id').val();
        //     var base_url = '<?php echo $this->config->item('base_url'); ?>' + 'admin/college/addAttendance/' + emp_id;
        //     $(location).attr('href', base_url);
        //
        // });
    });

  $(document).ready(function() {
        $('a.back').click(function() {
            parent.history.back();
            return false;
        });

        $('#cl').blur(function(){

           var cl_used = parseFloat($('#total_cl').val())+parseFloat($('#cl').val());
           var allowed_cl = $("#allowed_cl").val();

           if(cl_used <= allowed_cl) {
                $('#cl_msg').html('');
                document.getElementById("submit").disabled = false;
           } else {
                $('#cl_msg').html('Maximum number of CL is '+allowed_cl);
                document.getElementById("submit").disabled = true;
           }
        });

        $('#sl').blur(function(){

           var sub = parseFloat($('#total_sl').val())+parseFloat($('#sl').val());

           if(sub<=4){
                $('#sl_msg').html('');
                document.getElementById("submit").disabled = false;
           }else{
                $('#sl_msg').html('Maximum number of SL is 4');
                document.getElementById("submit").disabled = true;
           }
        });

        get_cl();
    });

    function get_cl() {

        if(!isNaN($("#hid_emp_id").val())) {
            var data = {
                "year": $("#year option:selected").val(),
                "employee": $("#hid_emp_id").val(),
                //csrf_test_name: "<?php //echo $_COOKIE['csrf_cookie_name']; ?>"
            };

            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>emp/get_cl",
                data: data,
                success: function(res) {
                    var msg = JSON.parse(res);

                    $("#cl_show").text("( " + msg.used_cl + " used , " + (msg.allowed_cl - msg.used_cl) + " allowed )");
                    $("#sl_show").text("( " + msg.used_sl + " used )");

                    $("#total_cl").val(msg.used_cl);
                    $("#allowed_cl").val(msg.allowed_cl);
                    $("#total_sl").val(msg.used_sl);
                }
            });
        }
    }
</script>
<h1>Add Attendance</h1>
<div id="body">
    <div class="err"><?php echo validation_errors(); ?></div>
    <div class="err">
        <?php
            if ($this->session->flashdata('op_msg') <> "") {
                echo "<p>" . $this->session->flashdata('op_msg') . "</p>";
            }
        ?>
    </div>
    <?php
    function monthDropdown($name = "month", $selected = null) {

        $months = array(
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'
        );

        $selected = is_null($selected) ? date('n', time()) : $selected;

        $mm = '<select name="' . $name . '" id="' . $name . '" onchange="get_cl();">';

            //$mm .= '<option>Month</option>';
            foreach($months as $m => $mon) {
                if($m == $selected) {
                    $mm .= '<option value="'.$m.'" selected="selected">'.$mon.'</option>';
                } else {
                    $mm .= '<option value="'.$m.'">'.$mon.'</option>';
                }
            }

        $mm .= '</select>';

        return $mm;
    }

    function yearDropdown($startYear, $endYear, $id = "year") {

        echo '<select id="' . $id . '" name="' . $id . '" onchange="get_cl();">';
            //echo "<option>Year</option>";
            for ($i = $endYear; $i >= $startYear; $i--) {
                echo '<option value="'.$i.'">'.$i.'</option>';
            }

        echo '</select>';
    }

    echo form_open('', array('id' => 'frm1'));
    ?>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">
                    <ul class="input_listing">
                   <fieldset>
                   <legend>Personal Information</legend>

          <?php if (isset($employee->staff_name)) { ?>
                    <input type="hidden" name="emp_id" id="hid_emp_id" value="<?php echo $employee->id; ?>">
                    <p><label>Employee Name: </label>
                        <?php echo form_input(array('name' => 'employee_name', 'id' => 'employee_name', 'readonly' => true), set_value('employee_name', $employee->staff_name)); ?>
                    </p>
            <?php } else if (isset($all_emp)) { ?>
                    <p><label>Employee Name: </label>
                        <select  name="emp_id" id="emp_id">
                            <option value="">Select Employee</option>
                            <?php foreach ($all_emp as $value) { ?>
                                    <option value="<?php echo $value->id; ?>"><?php echo $value->staff_name; ?></option>
                            <?php } ?>
                        </select>
                    </p>
            <?php } ?>

        <p><label>Date: </label>
            <?php
            /*             * * example usage ** */
            echo monthDropdown('month', date("m")) . "&nbsp;&nbsp;";
            yearDropdown(2004, date("Y"));
            ?>
        </p>

        <p><label>Present: </label><?php echo form_input(array('name' => 'present', 'id' => 'present', 'class' => 'required number'), set_value('present')); ?></p>
        <p><label>Absent: </label><?php echo form_input(array('name' => 'absent', 'id' => 'absent', 'class' => 'required number'), set_value('absent')); ?></p>
        <p><label>PL: </label><?php echo form_input(array('name' => 'pl', 'id' => 'pl','class'=>'number'), set_value('pl')); ?></p>
        <p><label>CL: </label><?php echo form_input(array('name' => 'cl', 'id' => 'cl','class'=>'number'), set_value('cl')); ?>

            <span id="cl_show">
            <?php
            $allowed_cl = 10;
            $total_cl = 0;
            $total_sl = 0;

            if(isset($employee->doj)){
                  $join_year = date('Y', strtotime($employee->doj));
                  if($join_year == date('Y')) {
                      $join_month = date('m', strtotime($employee->doj));
                      $month_remaining = 12-$join_month;
                      $allowed_cl = round((10/12)*$month_remaining);
                  }
            }

            if (isset($cl)) {
                foreach ($cl as $value) {
                    $total_cl += $value->cl;
                }
                echo "( " . $total_cl . " used , ". ($allowed_cl - $total_cl)." allowed )";
            } else {
                echo "empty";
            }
            ?>
            </span>
            <input type="hidden" value="<?php echo $total_cl;?>" id="total_cl">
            <input type="hidden" value="<?php echo $allowed_cl;?>" id="allowed_cl">
            <span id="cl_msg" style="color: red;"></span>

        </p>
        <p><label>SL: </label><?php echo form_input(array('name' => 'sl', 'id' => 'sl','class'=>'number'), set_value('sl')); ?>

            <span id="sl_show">
            <?php
            if (isset($cl)) {
                foreach ($cl as $value) {
                    $total_sl+= $value->sl;
                }
                echo "( " . $total_sl . " used )";
            } else {
                echo "empty";
            }
            ?>
            </span>
            <input type="hidden" value="<?php echo $total_sl;?>" id="total_sl">
            <span id="sl_msg" style="color: red;"></span>
        </p>
        <p><label>OPL(Without Pay Leave): </label><?php echo form_input(array('name' => 'opl', 'id' => 'opl','class'=>'number'), set_value('opl')); ?></p>
        <p><label>Working Days: </label><?php echo form_input(array('name' => 'working_days', 'id' => 'working_days'), set_value('working_days', 30)); ?></p>
        <p><label>Note: </label><?php echo form_textarea(array('name' => 'note')); ?></p>
    </fieldset>


    <li class="full_width_li">
    <p><?php
        //echo form_submit('submit','Submit');
        echo form_input(array('type' => 'submit', 'name' => 'submit', 'value' => 'Submit','id' =>'submit','class'=>'submit_btn'));
        ?>
      </p>
    </li>
    <?php echo form_close(); ?>

    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>



<?php $this->load->view("admin/include/footer"); ?>
