<?php $this->load->view("admin/include/header"); ?>


    <script type="text/javascript"> 
    function fileChange_staff(e) {

           $('#result_candidate_sign').html("");
           document.getElementById('input_candidate_sign').value = '';
           for (var i = 0; i < e.target.files.length; i++) {

               var file = e.target.files[i];

               if (file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/gif") {

                   var reader = new FileReader();
                   reader.onload = function (readerEvent) {
                       var image = new Image();
                       image.onload = function (imageEvent) {
                           //var max_size = 275;
                           var max_size = 900;
                           var allow_w = 158;
                           var allow_h = 350;
                           var w = image.width;
                           var h = image.height;
                           if (w > allow_w) {
                            alert('File Size Is to Big');
                                   $('#result_candidate_sign').html("please Enter Correct Image Format");
                                   $("#candidate_signature").val("");
                           }
                           else if (h > allow_h) {
                            alert('File Size Is to Big');
                                   $('#result_candidate_sign').html("please Enter Correct Image Format");
                                   $("#candidate_signature").val("");
                           }

                           else if (w > h) {
                               if (w > max_size) {
                                   h *= max_size / w;
                                   w = max_size;
                                   alert('File Size Is to Big');
                                   $('#result_candidate_sign').html("please Enter Correct Image Format");
                                   $("#candidate_signature").val("");
                               }
                           } else {
                               if (h > max_size) {
                                   w *= max_size / h;
                                   h = max_size;
                                   alert('File Size Is to Big');
                                   $('#result_candidate_sign').html("please Enter Correct Image Format");
                                   $("#candidate_signature").val("");
                               }
                           }
                           var canvas = document.createElement('canvas');
                           canvas.width = w;
                           canvas.height = h;
                           canvas.getContext('2d').drawImage(image, 0, 0, w, h);
                           if (file.type == "image/jpeg") {
                               var dataURL = canvas.toDataURL("image/jpeg", 1.0);
                           } else if (file.type == "image/png") {
                               var dataURL = canvas.toDataURL("image/png");
                           } else if (file.type == "image/gif") {
                               var dataURL = canvas.toDataURL("image/gif");                            
                           }

                           document.getElementById('input_candidate_sign').value = dataURL;

                       }
                       image.src = readerEvent.target.result;
                   }
                   reader.readAsDataURL(file);
               }
           }
       } 
    </script>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Edit Principal</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open_multipart('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>Principal Name</label>
                                            <input type="text" name="principal_name" id="principal_name" placeholder="Principal Name" value="<?php echo $details['principal_name']; ?>" required="">
                                        </span>
                                    </li>
                                    <li class="full_width_li">
                                    <span>
                                    <label>Signature of Principal</label>
                                    <?php $path=base_url().UPLOAD_DIR.PRINCIPAL_SIGN.$details['id'].".jpg";
                                                $path_thu=base_url().UPLOAD_DIR.PRINCIPAL_SIGN_THUMB.$details['id'].".jpg";
                                            ?>
                                            <?php if (file_exists(UPLOAD_DIR.PRINCIPAL_SIGN.$details['id'].".jpg") AND file_exists(UPLOAD_DIR.PRINCIPAL_SIGN_THUMB.$details['id'].".jpg")) {
                                              ?>
                                          
                                            
                                            <div class="album_cover">
                                            <a href="<?php echo $path;?>" target="_blank">
                                            <image src="<?php echo $path_thu.'?t='.time();?>" style="height:100px;width: 100px;">
                                            </a>
                                            <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deletePrincipalImage/'.$details['id']; ?>');" title="Delete Image">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            
                                            </div>
                                          <?php } else {echo "<br>No Image Found"; } ?> 

                                            <input type="file" name="candidate_signature" id="candidate_signature"  class="" accept="image/*" onchange="fileChange_candidate_sign(event);">
                                             <input type="hidden" name="input_candidate_sign" id="input_candidate_sign" class="input_candidate_sign" value="">
                                             <p id="result_candidate_sign"></p>
                                        </span>
                                        </li>
                                    <li class="full_width_li">
                                        <span>
                                            <label>Status</label>
                                            <select name="status" id="status">
                                                <option value="1" <?php if($details['status'] == '1') { echo 'selected="selected"'; } ?>>Active</option>
                                                <option value="0" <?php if($details['status'] == '0') { echo 'selected="selected"'; } ?>>Inactive</option>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <input type="hidden" name="page_id" value="<?php echo $this->uri->segment(4); ?>">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Update">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
