<?php $this->load->view("admin/include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Edit Payment Dttails</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>Student Name</label>
                                            <input type="text" name="name"  placeholder="Enter Student Name" value="<?php echo $details['name']; ?>" class="form-control" readonly>
                                        </span>
                                    </li>
                                    <li class="full_width_li">
                                        <span>
                                            <label>Student Email</label>
                                            <input type="text" name="email"  placeholder="Enter Student Email" value="<?php echo $details['email']; ?>" class="form-control" readonly>
                                        </span>
                                    </li>
                                    <li class="full_width_li">
                                        <span>
                                            <label>Student Phone</label>
                                            <input type="text" name="phone"  placeholder="Enter Student Email" value="<?php echo $details['phone']; ?>" class="form-control" readonly>
                                        </span>
                                    </li>
                                    <li class="full_width_li">
                                        <span>
                                            <label>Admission form Payment</label>
                                            <select name="admission_from_payment" class="form-control">
                                                <option value="0" <?php if($payment_details['admission_from_payment'] == '0') { echo 'selected="selected"'; } ?>>Pending</option>
                                                <option value="1" <?php if($payment_details['admission_from_payment'] == '1') { echo 'selected="selected"'; } ?>>Received</option>
                                            </select>
                                        </span>
                                    </li>
                                    <li class="full_width_li">
                                        <span>
                                            <label>Admission Payment</label>
                                            <select name="admission_payment" class="form-control">
                                                <option value="0" <?php if($payment_details['admission_payment'] == '0') { echo 'selected="selected"'; } ?>>Pending</option>
                                                <option value="1" <?php if($payment_details['admission_payment'] == '1') { echo 'selected="selected"'; } ?>>Received</option>
                                            </select>
                                        </span>
                                    </li>



                                    <li class="full_width_li">
                                        <input type="hidden" name="class_id" value="<?php echo $this->uri->segment(4); ?>">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Update">

                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
