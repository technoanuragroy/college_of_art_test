<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#from_date').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
        });
         $('#to_date').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
        });
    });
</script>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Edit Session</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>Session Name</label>
                                            <input type="text" name="session_name" id="session_name" placeholder="Enter a Staff Name" value="<?php echo $session_details['session_name']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Session Start Date</label>
                                            <input type="text" name="from_date" id="from_date" placeholder="Enter From Date" value="<?php echo $session_details['from_date']; ?>" required="">
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <label>Session End Date</label>
                                            <input type="text" name="to_date" id="to_date" placeholder="Enter To Date" value="<?php echo $session_details['to_date']; ?>" required="">
                                        </span>
                                    </li>


                                    <li class="full_width_li">
                                        <input type="hidden" name="session_id" value="<?php echo $this->uri->segment(4); ?>">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Update">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
