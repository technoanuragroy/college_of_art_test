<?php $this->load->view("admin/include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Principal</h2>

                <a href="<?php echo base_url(); ?>admin/college/addPrincipal" class="c-link">Add Principal</a>

                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Principal Name</th>
                                    <th>Principal Sign</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($details)) {
                                    foreach($details as $page) {
                                     $page_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>
                                            <td><?php echo $page['principal_name']; ?>
                                                
                                            </td>
                                            <td><?php $path=base_url().UPLOAD_DIR.PRINCIPAL_SIGN.$page['id'].".jpg";
                                                $path_thu=base_url().UPLOAD_DIR.PRINCIPAL_SIGN_THUMB.$page['id'].".jpg";
                                            ?>
                                            <?php if (file_exists(UPLOAD_DIR.PRINCIPAL_SIGN.$page['id'].".jpg") AND file_exists(UPLOAD_DIR.PRINCIPAL_SIGN_THUMB.$page['id'].".jpg")) {
                                              ?>
                                            <a href="<?php echo $path;?>" target="_blank">
                                            <image src="<?php echo $path_thu.'?t='.time();?>" style="height:100px;width: 100px;">
                                            </a>

                                            <?php } else {echo "<br>No Image Found"; } ?> 
                                                
                                            </td>
                                            <td><?php
                                                  if ($page['status'] == 1) {
                                                      echo "<span class='badge badge-success'>Active</span>";
                                                  } else {
                                                      echo "<span class='badge badge-danger'>Inactive</span>";
                                                  }
                                                  ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/editPrincipal/'.$page_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deletePrincipal/'.$page_id; ?>');" title="Delete Record">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="8">No Principal Record found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
