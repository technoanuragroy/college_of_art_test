<?php $this->load->view("admin/include/header"); ?>


    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Add Group</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                     <li class="full_width_li">
                                        <span>
                                            <label>Select Course</label>
                                            <select name="class_id" id="class_id">
                                                <option value="">Select</option>
                                                <?php foreach ($class_list as $row) { ?>
                                                    <option value="<?php echo $row['id']; ?>"><?php echo $row['course_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>
                                    <li class="full_width_li">
                                        <span>
                                            <label>Group Name</label>
                                            <input type="text" name="section_name" id="section_name" placeholder="Group Name" required="">
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Status</label>
                                            <select name="status" id="status">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Register">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
