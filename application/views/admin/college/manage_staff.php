<?php $this->load->view("admin/include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Staff</h2>

                <a href="<?php echo base_url(); ?>admin/college/addStaff" class="c-link">Add Staff</a>

                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>Staff Name</th>
                                    <th>Designation</th>
                                    <th>Phone Number</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($all_staff)) {
                                    foreach($all_staff as $page) {
                                     $staff_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>
                                            <td><?php echo $page['user_name']; ?></td>
                                            <td><?php echo $page['staff_name']; ?></td>
                                            <td><?php if ($page['designation']=='1') {
                                                echo "Principal";
                                            }else if ($page['designation']=='2') {
                                                echo "Teaching Staff";
                                            }else if ($page['designation']=='3') {
                                                echo "Non Teaching Staff";
                                            } ; ?></td>
                                            <td><?php echo $page['phone_number']; ?></td>
                                            <td><?php echo $page['email']; ?></td>
                                            <td><?php if ($page['status']==1) {
                                              echo "Active";
                                            } else {
                                              echo "InActive";
                                            } ?></td>
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/editStaff/'.$staff_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteStaff/'.$staff_id; ?>');" title="Delete Record">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="8">No staff found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
