<?php $this->load->view("admin/include/header"); ?>


    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Create Notice</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                    <div class="form-group">
                        <div class="form-material">
                            <input required="" type="text" class="form-control" id="notice_heading" name="notice_heading" placeholder="Heading">
                            <label for="notice_heading">Notice Heading</label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12" for="example-textarea-input">Notice</label>
                        <div class="col-12">
                            <textarea class="form-control" id="editor1" name="notice_text" rows="6" placeholder="Content.."></textarea>
                        </div>
                        <script>

                            CKEDITOR.replace('editor1');
                        </script>
                    </div>
                    <!-- <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="date_of_birth" name="date_of_publish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy">
                            <label for="date_of_birth">Date of Publish</label>
                        </div>
                    </div> -->

                    <div class="form-group row">
                        <label class="col-12" for="upload-Image">Upload Docs </label>
                        <div class="col-12">
<!--                            <input type="file" id="upload-Image" name="doc_upload[]" multiple="" onchange="loadImageFile();">-->
                            <input type="file" id="inp_files" name="doc_upload" >
                            <input id="inp_img" name="img" type="hidden" value="">
<!--                            <input type="hidden" id="img_compressed" name="img_compressed">-->
                        </div>
                    </div>
<!--                    <td>Origal Img - <img id="original-Img"/></td>
                    <td>Compress Img - <img id="upload-Preview"/></td>-->

                    <div class="form-group">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Submit">
                    </div>
                    <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
