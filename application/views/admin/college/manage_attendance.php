<?php $this->load->view("admin/include/header"); ?>
<script type="text/javascript">
    $(document).ready(function()
    {
        $(".TabDisplayRecords").tablesorter();
    }
    );

</script>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Staff</h2>

                <a href="<?php echo base_url(); ?>admin/college/addAttendance" class="c-link">Add Attendance</a>

                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                </div>
                <?php

            function monthDropdown($name = "month", $selected = null) {
                $dd = '<select name="' . $name . '" id="' . $name . '">';

                $months = array(
                  1 => 'January',
                  2 => 'February',
                  3 => 'March',
                  4 => 'April',
                  5 => 'May',
                  6 => 'June',
                  7 => 'July',
                  8 => 'August',
                  9 => 'September',
                  10 => 'October',
                  11 => 'November',
                  12 => 'December');
                /*         * * the current month ** */
                $selected = is_null($selected) ? date('n', time()) : $selected;
                $dd.='<option >Month</option>';
                for ($i = 1; $i <= 12; $i++) {


                    $dd .= '<option value="' . $i . '"';



                    if ($i == $selected) {
                        $dd .= 'selected';
                    }
                    /*             * * get the month ** */
                    $dd .= '>' . $months[$i] . '</option>';
                }
                $dd .= '</select>';
                return $dd;
            }

            $id = $this->uri->segment(4);
            echo form_open('admin/college/manage_attendance/'. $id, array('id' => 'form1'));
            ?>

            <div style="width:100%; ">
                <div style="float: left; width: 50%;margin-bottom: 10px;">
                    <?php //echo anchor('emp/add_attendance',"Add Employee"); ?>
                </div>
                <div style="float: right; width: 50%;margin-bottom: 5px;">
            <?php
            if (isset($year)) {
            ?>

                        <script>
                            $(document).ready(function() {
                                var year = '<?php echo $year; ?>';
                                $('#year').val(year).attr("selected", "selected");
                            });
                        </SCRIPT>
            <?php
            }
            if (isset($month)) {
            ?>
                        <script>
                            $(document).ready(function() {
            <?php if ($this->session->userdata('sess_month')) { ?>
                                    var month = '<?php echo $input = ltrim($this->session->userdata('sess_month'), '0'); ?>';
                        <?php } else { ?>
                                    var month = '<?php echo $month; ?>';
                        <?php } ?>

                                $('#month').val(month).attr("selected", "selected");

                            });
                        </SCRIPT>
                    <?php }
                    ?>
            <?php
            /* * * example usage ** */
            $name = 'month';
            $month = $this->session->userdata('sess_month');

            echo monthDropdown($name, $month);

            echo form_dropdown('year', $years, $year);
            ?>
            <!--            <select name="year" id="year">
                        <option>Year</option>
            <?php //echo "<option value='$year'>$year</option>"; ?>
                    </select>-->
                    <input type="submit" name="submit" value="Submit" />
            <?php echo form_close(); ?>
                </div>
            </div>
            </div>
        </div>
    </div>




<?php if (!empty($attend)) { ?>
  <div class="table-responsive">
      <table class="table table-default table-bordered table-striped table-hover">
          <thead>
            <tr>
                <th align="left">Employee Name</th>
                <th align="left">Present</th>
                <th align="left">Absent</th>
                <th align="left">PL</th>
                <th align="left">CL</th>
                <th align="left">SL</th>
                <th align="left">OPL</th>
                <td align="center"><b>Action</b></td>
            </tr>
        </thead>
        <tbody><?php
foreach ($attend as $row) :
    ?>
                <tr>
                    <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$row->emp_id.'"'); ?></td>
                    <td><?php echo $row->present; ?></td>
                    <td><?php echo $row->absent; ?></td>
                    <td><?php echo $row->pl; ?></td>
                    <td><?php echo $row->cl; ?></td>
                    <td><?php echo $row->sl; ?></td>
                    <td><?php echo $row->opl; ?></td>
                    <td align="center"><?php echo anchor("admin/college/edit_attendance/" . $row->emp_id . "/" . $row->id, "Edit"); ?> |
                      <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteAttendence/' .$row->id;?>');"
                         title="Delete Record">
                          Delete
                      </a></td>
                </tr>

<?php endforeach; ?>

        </tbody>
    </table>
    </div>
</div>
<?php
}
else {
echo "No records to display";
}
?>

<?php $this->load->view("admin/include/footer"); ?>
