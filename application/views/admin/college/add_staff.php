<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">


                function fileChange_staff(e,manage) {

                //document.getElementsByClassName("student_passport_photo").value = '';
                $('#result_staff_'+manage).html("");
                //document.getElementById('input_'+manage).value = '';

                for (var i = 0; i < e.target.files.length; i++) {

                    var file = e.target.files[i];

                    if (file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/gif") {

                        var reader = new FileReader();
                        reader.onload = function (readerEvent) {
                            var image = new Image();
                            image.onload = function (imageEvent) {
                                //var max_size = 275;

                          if (manage=='photo') {
                           var max_size = 900;
                           var allow_w = 437;
                           var allow_h = 531;

                           }else if (manage=='sign') {

                           var max_size = 900;
                           var allow_w = 437;
                           var allow_h = 177;

                           }

                            var file_size = document.getElementById('staff_'+manage).files[0].size; // In Bytes
                                var s = file_size / 1024; // In Kilobytes
                                // var w = image.width;
                                // var h = image.height;

                                //alert('size : '+s+' Kb width : '+w+' px height : '+h+' px');

                                if(s > max_size) {
                                    alert('File Size Is too Big');
                                    $('#result_staff_'+manage).html("Please Choose A File Of Given Size");
                                    $('#staff_'+manage).val("");
                                }
                                // else if (w > allow_w || h > allow_h) {
                                //     alert('File Dimensions Do Not Match');
                                //     $('#result_staff_'+manage).html("Please Choose A File Of Given Dimensions");
                                //     $('#staff_'+manage).val("");
                                // }


                            }
                            image.src = readerEvent.target.result;
                        }
                        reader.readAsDataURL(file);
                    }
                }
            }
    // function fileChange_staff(e,manage) {

    //        $('#result_staff_'+manage).html("");
    //        document.getElementById('input_staff_'+manage).value = '';
    //        for (var i = 0; i < e.target.files.length; i++) {

    //            var file = e.target.files[i];

    //            if (file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/gif") {

    //                var reader = new FileReader();
    //                reader.onload = function (readerEvent) {
    //                    var image = new Image();
    //                    image.onload = function (imageEvent) {

    //                        if (manage=='photo') {
    //                        var max_size = 900;
    //                        var allow_w = 300;
    //                        var allow_h = 400;

    //                        }else if (manage=='sign') {

    //                        var max_size = 900;
    //                        var allow_w = 300;
    //                        var allow_h = 350;

    //                        }
    //                        var w = image.width;
    //                        var h = image.height;

    //                        if (w > allow_w) {
    //                         alert('File Size Is to Big');
    //                                $('#result_staff_'+manage).html("Please Enter Correct Image Format");
    //                                $("#staff_"+manage).val("");
    //                        }
    //                        else if (h > allow_h) {
    //                         alert('File Size Is to Big');
    //                                $('#result_staff_'+manage).html("Please Enter Correct Image Format");
    //                                $("#staff_"+manage).val("");
    //                        }

    //                        else if (w > h) {
    //                            if (w > max_size) {
    //                                h *= max_size / w;
    //                                w = max_size;
    //                                alert('File Size Is to Big');
    //                                $('#result_staff_'+manage).html("Please Enter Correct Image Format");
    //                                $("#staff_"+manage).val("");
    //                            }
    //                        } else {
    //                            if (h > max_size) {
    //                                w *= max_size / h;
    //                                h = max_size;
    //                                alert('File Size Is to Big');
    //                                $('#result_staff_'+manage).html("Please Enter Correct Image Format");
    //                                $("#staff_"+manage).val("");
    //                            }
    //                        }
    //                        var canvas = document.createElement('canvas');
    //                        canvas.width = w;
    //                        canvas.height = h;
    //                        canvas.getContext('2d').drawImage(image, 0, 0, w, h);
    //                        if (file.type == "image/jpeg") {
    //                            var dataURL = canvas.toDataURL("image/jpeg", 1.0);
    //                        } else if (file.type == "image/png") {
    //                            var dataURL = canvas.toDataURL("image/png");
    //                        } else if (file.type == "image/gif") {
    //                            var dataURL = canvas.toDataURL("image/gif");
    //                        }

    //                        document.getElementById('input_staff_'+manage).value = dataURL;

    //                    }
    //                    image.src = readerEvent.target.result;
    //                }
    //                reader.readAsDataURL(file);
    //            }
    //        }
    //    }
    </script>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Enter Staff Details</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open_multipart('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>Staff Name</label>
                                            <input type="text" name="staff_name" id="staff_name" class="form-control" placeholder="Staff Name" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                        <label>Designation</label>
                                       <select name="designation" id="designation" class="form-control" required="">
                                        <option value="">Select
                                        Designation</option>
                                                <?php
                                                $gender = $this->config->item('designation');
                                                foreach($gender as $key=>$gtype){?>
                                                <option value="<?php echo $key;?>" <?php echo set_select('gender',$key, False); ?> ><?php echo $gtype;?></option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <label>Pay</label>
                                            <input type="text" name="scale" id="scale" class="form-control" placeholder="Pay" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Grade Pay</label>
                                            <input type="text" name="grade_pay" id="grade_pay" class="form-control" placeholder="Grade Pay" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Medical Allowance</label>
                                            <input type="text" name="med_allow" id="med_allow" class="form-control" placeholder="Medical Allowance" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>PF a/c No</label>
                                            <input type="text" name="pf_ac_no" id="pf_ac_no" class="form-control" placeholder="PF a/c No" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Phone Number</label>
                                            <input type="number" name="phone_number" id="phone_number" class="form-control" placeholder="Phone Number" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Email</label>
                                            <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>User Name</label>
                                            <input type="text" name="user_name" id="user_name" class="form-control" placeholder="User Name" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Password</label>
                                            <input type="password" name="password" id="password" class="form-control" placeholder="Password" required="">
                                        </span>
                                    </li>
                                    <li >
                                    <span>
                                    <label>Photos of Staff</label>
                                            <input type="file" name="staff_photo" id="staff_photo" class="form-control"  accept="image/*" onchange="fileChange_staff(event,'photo');">
                                             <!-- <input type="hidden" name="input_staff_photo" id="input_staff_photo" class="input_staff_photo" value=""> -->
                                             <p id="result_staff_photo"></p>
                                        </span>
                                        </li>

                                        <li >
                                    <span>
                                    <label>Signature of Staff</label>
                                            <input type="file" name="staff_sign" id="staff_sign" required="" class="form-control"   accept="image/*" onchange="fileChange_staff(event,'sign');">
                                             <!-- <input type="hidden" name="input_staff_sign" id="input_staff_sign" class="input_staff_sign" value=""> -->
                                             <p id="result_staff_sign"></p>
                                        </span>
                                        </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Status</label>
                                            <select name="status" class="form-control" id="status">
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Register">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
