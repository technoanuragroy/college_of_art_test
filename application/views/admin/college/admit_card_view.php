<?php $this->load->view("admin/include/header"); ?>

    <script type="text/javascript">
    $(document).ready(function () {
        $('#admit_card_issue_date').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
        });
    });
    </script>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Student Admit Card</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">


                            <?php
                            $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$card_details['id']));
                        $student_sign=base_url().UPLOAD_DIR.STUDENT_SIGN_FULL.$document_details['student_sign_full'];
                      $student_passport=base_url().UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$document_details['passport'];
              $prin_photo=base_url().UPLOAD_DIR.STAFF_SIGN.$principal[0]['id'].".jpg";
                $college_logo=base_url()."images/college/logo.jpg";
                            $encrypted_id = $this->uri->segment(4);
                            $student_id = $this->ablfunctions->ablDecrypt($encrypted_id);?>

                                <?php echo form_open_multipart('', array('id' => 'formEditAdmin')); ?>

                                <ul class="input_listing">
                                    <li >
                                        <span>
                                        <label>Form No:<?php echo $card_details['from_id'];?></label>
                                        </span>
                                    </li>
                                    <li >
                                        <span>
                                        <select name="course_id" id="course_id">
                                                <option value="">Select Course </option>
                                                <?php foreach ($all_course as $row) { ?>
                                                    <option value="<?php echo $row['id']; ?>" <?php if ($row['id']==$card_details['course_id']){ echo "selected";} ?>>
                                                        <?php echo $row['course_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>
                                    <li class="full_width_li">
                                        <span>
                                        <h4 style="text-align: center;">College of Art & Design</h4>
                                        <h6 style="text-align: center;">Admit Card</h6>

                                        </span>
                                    </li>

                                    <li >
                                        <span>
                                            <image src="<?php echo  $college_logo;?>" >
                                        </span>
                                    </li>

                                     <li>
                                        <span>


                                            <div class="album_cover">

                                            <image src="<?php echo $student_passport;?>" style="height:150px;width: 120px;">
                                            </div>
                                            <label>Passport Photo</label>

                                        </span>
                                    </li>



                                    <li class="full_width_li">
                                        <span>
                                            <label>Name of the Candidate :  <?php echo $card_details['student_name']; ?></label>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Address :  <?php echo $card_details['perm_address']; ?></label>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <select name="admit_card_type" id="admit_card_type" >
                                        <option value="">Select
                                        Admission Type</option>
                                                <?php
                                                $status = $this->config->item('admit_card');
                                                foreach($status as $key=>$stype){?>
                                                <option value="<?php echo $key;?>" <?php if ($key==$card_details['admit_card_type']){ echo "selected";} ?>><?php echo $stype;?></option>
                                                <?php } ?>
                                            </select>
                                          </span>
                                          </li>
                                        <li class="full_width_li">
                                          <li class="col-md-3">
                                             <span>
                                               <?php
                                               $publish_date_time = date('d/m/Y h:i A', $card_details['admit_card_issue_date']);
                                               $publish_date = explode(" ", $publish_date_time);

                                               $publish_time = explode(":", $publish_date[1]);
                                               $hour = (int) $publish_time[0];
                                               $minute = (int) $publish_time[1];
                                               ?>
                                        <label>Enter Issue date</label>

                                        <input type="text" name="admit_card_issue_date" id="admit_card_issue_date" placeholder="Enter Issue Date" data-date-format="dd/mm/yyyy" required="" <?php if ($card_details['admit_card_issue_date']!='') {
                                            ?>
                                            value="<?php echo $publish_date[0]; ?>"
                                            <?php
                                        } else{
                                             ?>
                                            value="Enter Issue date"
                                            <?php

                                        }?>>
                                      </span>
                                  </li>



                           <li class="col-md-3">
                               <span>
                                   <label>Select Hour</label>
                                   <?php $hour_list = $this->config->item('hour_list'); ?>
                                               <select required="" name="start_hour" >
                                                   <option value="">Select Hour</option>
                                                   <?php foreach($hour_list as $key=>$val){
                                                       if ($hour == $key) {
                                                           $selected = "selected='slected'";
                                                           } else {
                                                            $selected = "";
                                                           }
                                                       ?>
                                                   <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $val; ?></option>
                                                   <?php } ?>
                                               </select>
                                   </span>
                                   </li>

                                   <li class="col-md-3">
                                   <span>
                                   <label>Select Minute</label>
                                   <select required="" name="start_minute" >
                                       <option value="">Select Minute</option>
                                       <?php for($i = 0;$i<60;$i+=1){
                                           if($i==0){
                                             $selected = "selected='selected'";
                                           }else{
                                              $selected = '';
                                           }
                                           ?>
                                       <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                       <?php } ?>
                                   </select>
                                   </span>
                                   </li>

                                   <li class="col-md-3">
                                   <label>Select Start Meridian</label>
                                   <select required="" name="start_meridian" >
                                       <option value="">Select</option>
                                       <option value="AM" selected="selected">A.M</option>
                                       <option value="PM">P.M</option>

                                   </select>
                                   </li>
                                   </li>


                                   <li class="full_width_li">
                                    <li class="col-md-6">
                                        <span>

                                            <div class="album_cover">
                                            <image src="<?php echo $student_sign;?>" style="height:50px;width: 120px;">
                                            </div>
                                            <label>Signature of Candidate</label>

                                        </span>
                                    </li>

                                   <li class="col-md-6">
                                        <span>

                                            <div class="album_cover">
                                            <image src="<?php echo $prin_photo;?>" style="height:50px;width: 120px;">
                                            </div>
                                            <label>Principal / Teacher-in-charge
                                           College of Art & Design</label>

                                        </span>
                                    </li>
                                    </li>
                                    <li class="full_width_li">
                                        <input type="hidden" name="page_id" value="<?php echo $this->uri->segment(4); ?>">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Issue Admit" >
                                    </li>

                                </ul>
                                <?php echo form_close(); ?>


                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
