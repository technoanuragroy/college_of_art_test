<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('.copier_link').click(function () {
            var fees_container_html = $(".examSubjectcopy").html();
            $(".examSubjectAppend").append(fees_container_html);

            $(".examSubjectAppend").find(".subject").each(function (i, e) {
                $(this).attr("name", "subject[" + i + "]");
            });
            $(".examSubjectAppend").find(".total_marks").each(function (i, e) {
                $(this).attr("name", "total_marks[" + i + "]");
            });
            $(".examSubjectAppend").find(".pass_marks").each(function (i, e) {
                $(this).attr("name", "pass_marks[" + i + "]");
            });
        });

//        $('form#frmRegister').on('submit', function(event) {
//            alert();
//            $('.subject').each(function() {
//                $(this).rules("add", 
//                    {
//                        required: true,
//                        messages: {
//                            required: "Subject is required",
//                        }
//                    });
//            });
//            
//            $('.total_marks').each(function() {
//                $(this).rules("add", 
//                    {
//                        required: true,
//                        messages: {
//                            required: "Total marks is required",                            
//                        }
//                    });
//            });
//            
//            $('.pass_marks').each(function() {
//                $(this).rules("add", 
//                    {
//                        required: true,
//                        messages: {
//                            required: "Total marks is required",                            
//                        }
//                    });
//            });
//            
//            $("#frmRegister").validate();
//        });

    });
    function remove_fees(e) {

        $(e).closest(".row").remove();
    }

    $(document).ready(function () {
        $('#exam_start_date').datepicker({
            dateFormat: 'dd/mm/yy',
        });
        $('#exam_end_date').datepicker({
            dateFormat: 'dd/mm/yy',
        });
        $('#publish_date').datepicker({
            dateFormat: 'dd/mm/yy',
        });
    });
    

</script> 

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Add Exam</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                     <li class="full_width_li">
                                        <span>
                                            <label>Select Term</label>
                                            <select required class="form-control" id="term" name="term">
                                                <option value="">Select Term</option>
                                                <?php foreach ($term_list as $row) { ?>
                                                    <option value="<?php echo $row['id']; ?>"><?php echo $row['term_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Exam Name</label>
                                            <input required type="text" id="exam_name" class="form-control" name="exam_name" placeholder="Enter exam name">
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Exam Display Name</label>
                                            <input required type="text"class="form-control" id="exam_disp_name" name="exam_disp_name" placeholder="Enter exam display name">
                                        </span>
                                    </li>

                                     <li class="full_width_li">
                                        <span>
                                            <label>Exam Start Date</label>
                                            <input required type="text" class="form-control e_s_d" id="exam_start_date" name="exam_start_date" placeholder="Enter exam start date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Exam End Date</label>
                                            <input required type="text" class="form-control e_n_d" id="exam_end_date" name="exam_end_date" placeholder="Enter exam end date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                                        </span>
                                    </li>

                                     <li class="col-md-3">
                                        <span>
                                            <label>Exam Publish Date</label>
                                            <input required type="text" class="form-control e_p_d" id="publish_date" name="publish_date" placeholder="Enter exam publish date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                                        </span>
                                    </li>

                        <li class="col-md-3">
                            <span>
                                <label>Select Start Hour</label>
                                <?php $hour_list = $this->config->item('hour_list'); ?>
                                <select required="" name="start_hour" class="form-control">
                                    <option value="">Select Hour</option>
                                    <?php foreach($hour_list as $key=>$val){ 
                                        if($key == 12){
                                            $selected = "selected='selected'";
                                        }else{
                                            $selected = '';
                                        }
                                        ?>
                                    <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $val; ?></option>
                                    <?php } ?>
                                </select>
                                </span>
                                </li>

                                <li class="col-md-3">
                                <span>
                                <label>Select Minute</label>
                                <select required="" name="start_minute" class="form-control">
                                    <option value="">Select Minute</option>
                                    <?php for($i = 0;$i<60;$i+=1){ 
                                        if($i==0){
                                          $selected = "selected='selected'";  
                                        }else{
                                           $selected = ''; 
                                        }
                                        ?>
                                    <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                                </span>
                                </li>

                                <li class="col-md-3">
                                <label>Select Start Meridian</label>
                                <select required="" name="start_meridian" class="form-control">
                                    <option value="">Select</option>
                                    <option value="AM" selected="selected">A.M</option>
                                    <option value="PM">P.M</option>
                                    
                                </select>
                                </li>

                                <h3 class="content-heading">Marks Detail</h3>

                        <div class="examSubjectAppend">
                        <div class="row">

                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label for="period_start_time">Select Subject</label>
                                        <select required="" name="subject[]" class="form-control subject">
                                            <option value="">Select Subject</option>
                                            <?php foreach ($subject_list as $subject) { ?>
                                                <option value="<?php echo $subject['id']; ?>"><?php echo $subject['subject_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        
                                    </div> 
                                </div>
                            </li>

                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label for="total_marks">Total Marks</label>
                                        <input required type="text" class="form-control total_marks" name="total_marks[]" placeholder="Enter total marks">
                                        
                                    </div> 
                                </div>
                            </li>
                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label for="pass_marks">Pass Marks</label>
                                        <input required type="text" class="form-control pass_marks" name="pass_marks[]" placeholder="Enter pass marks">
                                        
                                    </div> 
                                </div>
                            </li>
                        </div>
                    </div>

                                <div class="form-group">
                                <div class="form-material">

                                <label for="copy_existing_exam"><a href="javascript:" class="copier_link"><i class="fa fa-plus"></i> Add More Subjects</a></label>
                                </div>
                                </div>

                                   

                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Register">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>
                        <div class="examSubjectcopy" style="display: none;">
                        <div class="row">

                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label >Select Subject</label>
                                        <select required="" name="subject[]" class="form-control subject">
                                            <option value="">Select Subject</option>
                                            <?php foreach ($subject_list as $subject) { ?>
                                                <option value="<?php echo $subject['id']; ?>"><?php echo $subject['subject_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        
                                    </div> 
                                </div>
                                <a href="javascript:" class="" onclick="remove_fees(this);"><i class="fa fa-minus-circle"></i>&nbsp;Remove</a>
                            </li>

                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Total Marks</label>
                                        <input required type="text" class="form-control total_marks" name="total_marks[]" placeholder="Enter total marks">
                                        
                                    </div> 
                                </div>
                            </li>
                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label for="pass_marks">Pass Marks</label>
                                        <input required type="text" class="form-control pass_marks" name="pass_marks[]" placeholder="Enter pass marks">
                                        
                                    </div> 
                                </div>

                            </li>

                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END Page Content -->
<script type="text/javascript">
    $(document).ready(function () {
        $(".e_s_d").keydown(function (event) {
            return false;
        });
        $(".e_n_d").keydown(function (event) {
            return false;
        });
        $(".e_p_d").keydown(function (event) {
            return false;
        });
    });

</script>

<?php $this->load->view("admin/include/footer"); ?>
