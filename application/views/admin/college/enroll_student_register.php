<?php $this->load->view("admin/include/header"); ?>

<style>
    #div_slideDown_search{
        display: none;
    }
</style>

 <?php if (isset($session_active)) {
    if ($session_active == '1') {
    ?>
    <script type="text/javascript">

        $("#clickbtn").show();

    </script>

    <?php
     }

     } else{
    ?>
    <style>
    #clickbtn{
        display: none;
    }
    </style>
    <?php

     } ?>
<script type="text/javascript">

     jQuery(document).ready(function ($) {
// executes when HTML-Document is loaded and DOM is ready
        $("#clickbtn").click(function () {
            $("#div_slideDown_search").slideToggle('fast');
        });

    });

</script>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Enroll Student</h2>


                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <!--<a href="javascript:" class="c-link" id="clickbtn" >Status Search </a>

                     <div class="form_wrap" id="div_slideDown_search">

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">


                                    <li class="full_width_li">
                                        <span>

                                            <select name="status" id="status" >
                                        <option value="">Select
                                        Status</option>
                                                <?php
                                                $status = $this->config->item('form_status');
                                                foreach($status as $key=>$stype){?>
                                                <option value="<?php echo $key;?>" ><?php echo $stype;?></option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <input type="submit" name="search" id="search" class="submit_btn" value="Status Search">
                                    </li>
                                    <input type="hidden" name="current_session_id" id="current_session_id" value="<?php echo $post_data['session_id']; ?>">
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div> -->


                 <div class="form_wrap" id="div_slideDown">

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">


                                    <li class="full_width_li">
                                        <span>
                                            <!-- <label>Status</label> -->
                                            <select id="session" name="session">
                                                <option value="">Select Session</option>
                                <?php
                                $selected = '';
                                foreach ($all_session as $session) {
                                    if ($post_data['session_id'] != '') {
                                        if ($post_data['session_id'] == $session['id']) {

                                            $selected = 'selected="selected"';
                                        } else {

                                            $selected = '';
                                        }
                                    }
                                    ?>
                                    <option value="<?php echo $session['id']; ?>" <?php echo $selected; ?>><?php echo $session['session_name']; ?></option>
                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Search">
                                        <!-- <a href="<?php echo base_url(); ?>admin/college/addStudentManual" class="btn btn-info min-width-125">Add Student</a>
                                          <a href="<?php echo base_url().'admin/enrollment/enrollStudent'; ?>" class="btn btn-success min-width-125">Enroll Student<a> -->
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>


                    <?php
                    if(!empty($all_student)) {
                    ?>
                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Student Name</th>
                                    <th>Course Name</th>
                                    <th>Status</th>
                                    <!-- <th>Issue Admit Card</th> -->
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($all_student)) {
                                    foreach($all_student as $page) {
                                     $student_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>
                                            <td><?php echo $page['student_name']; ?></td>
                                            <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_courses","course_name", 'and id="'.$page['course_id'].'"'); ?>

                                            </td>
                                            <td><?php
                                                  if ($page['status'] == 1) {
                                                      echo "<span class='badge badge-success'>Need Review</span>";
                                                  } else if ($page['status'] == 2) {
                                                      echo "<span class='badge badge-success'>Review Pending</span>";
                                                  }else if ($page['status'] == 3) {
                                                      echo "<span class='badge badge-success'>Admit (Test) Issued</span>";
                                                  }else if ($page['status'] == 4) {
                                                      echo "<span class='badge badge-success'>Admit (Admission) Issued</span>";
                                                  }else if ($page['status'] == 5) {
                                                      echo "<span class='badge badge-success'>Payment Link Activated</span>";
                                                  }else if ($page['status'] == 6) {
                                                      echo "<span class='badge badge-success'>Admission Fees received</span>";
                                                  }else if ($page['status'] == 7) {
                                                      echo "<span class='badge badge-success'>Enrolled</span>";
                                                  }
                                                  ?>
                                            </td>
                                          
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/editStudentEnroll/'.$student_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>


                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="8">No Register Student found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    }
                    ?>


                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
