<?php $this->load->view("admin/include/header"); ?>

  <script type="text/javascript">
  $(document).ready(function() {
  $('#from_date').datepicker(
          {changeMonth: true,
              changeYear: true,
              dateFormat: 'yy-mm-dd'
          }
  );
  $('#to_date').datepicker(
          {
              changeMonth: true,
              changeYear: true,
              dateFormat: 'yy-mm-dd'
          }
  );
  });
  </script>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Expense Report</h2>

                <a href="<?php echo base_url(); ?>admin/college/addExpenses" class="c-link">Add Expense</a>

                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <ul class="input_listing">
                      <div class="noprint">
                       <?php echo form_open('', array('id' => 'form1')); ?>

                          <li class="" colspan="3">
                          &nbsp; From Date &nbsp;<?php echo form_input(array('name' => 'from_date', 'id' => 'from_date', 'class' => 'required','placeholder'=> 'From Date','autocomplete'=> 'off'),set_value('from_date')); ?>
                          </li>
                          <li class="" colspan="3">
                          &nbsp; To Date &nbsp;<?php echo form_input(array('name' => 'to_date', 'id' => 'to_date', 'class' => 'required','placeholder'=> 'To Date','autocomplete'=> 'off'),set_value('to_date')); ?>
                          </li>
                          <li class="full_width_li">
                          <?php echo form_input(array('type' => 'submit', 'name' => 'submit', 'value' => 'Search','class'=>'submit_btn')); ?></p>
                          </li>
                          <?php echo form_close(); ?>
                          </ul>
                        </div>
                    <div class="table-responsive">
                      <?php
                      if(!empty($details)) {
                      ?>
                       <h4>Expense Report From <?=$from_date;?> To <?=$to_date;?></h4>
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Reference Id</th>
                                    <th>Account Name</th>
                                    <th>Particular</th>
                                    <th>Credit Amount</th>
                                    <th>Debit Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                    $credit_ammount_total=0;$debit_ammount_total=0;
                                    foreach($details as $page) {
                                     //$staff_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>
                                            <td><?php echo $page->date; ?></td>
                                            <td><?php echo $page->reference_id; ?></td>
                                            <td><?php echo $page->account_name; ?></td>
                                            <td><?php echo $page->particular; ?></td>
                                            <td><?php $credit_ammount_total+=$page->credit_ammount; echo $page->credit_ammount; ?></td>
                                            <td><?php $debit_ammount_total+=$page->debit_ammount; echo $page->debit_ammount; ?></td>
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/editExpenses/'.$this->ablfunctions->ablEncrypt($page->id); ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>|
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteExpenses/'.$this->ablfunctions->ablEncrypt($page->id); ?>');" title="Delete Record">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                            <?php
                                    }

                            ?>
                            <tr>
                                <td colspan="4" style="text-align:right;">Total:</td>
                                <td><?php echo $credit_ammount_total; ?></td>
                                <td><?php echo $debit_ammount_total; ?></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                        <a href="<?php echo base_url().'admin/college/downloadExpenses/'.$from_date.'/'.$to_date; ?>" class="btn btn-success min-width-125" target="_blank">Export Table data into Excel</a>
                        <?php
                          }else { ?>
                                <span>No Expense Record found</span>

                        <?php
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
