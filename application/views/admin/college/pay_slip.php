<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $(".TabDisplayRecords").tablesorter({sortList: [[0,0]]} );
      $('a.printme').click(function(){
     window.print();
});

    });
    $(document).ready(function () {

    function exportTableToCSV($table, filename) {

        var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace(/"/g, '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"',

            // Data URI
            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
                'href': csvData,
                'target': '_blank'
        });
    }

    // // This must be a hyperlink
    // $(".export").click(function ()  {
    //     // CSV
    //     exportTableToCSV.apply(this, [$('#dvData>table'), 'payslip_report_<?php echo  date('F', mktime(0, 0, 0, $month, 10))."_".$year?>.xls']);
    //
    //     // IF CSV, don't do event.preventDefault() or return false
    //     // We actually need this to be a typical hyperlink
    // });
    // This must be a hyperlink
    $(".export").click(downloadTeacherAttendances ()  {
        // CSV
        //exportTableToCSV.apply(this, [$('#dvData>table'), 'payslip_report_<?php echo  date('F', mktime(0, 0, 0, $month, 10))."_".$year?>.xls']);

        // IF CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
    });
});

</script>
<style>
    @media print {
   .noprint { display: none; }
   .noprint_old { display: none; }
}
</style>
<h1>Pay Slip for the month of <?php echo  date('F', mktime(0, 0, 0, $month, 10))." ".$year?></h1>

<div id="body">
    <div class="err"><?php if ($this->session->flashdata('add_data') == 'fail') {
    echo "<p>Record Add Failed</p>";
} ?></div>
    <div class="suc"><?php if ($this->session->flashdata('add_data') == 'success') {
    echo "<p>Record Added Successfully.</p>";
} ?></div>
    <div class="err"><?php if ($this->session->flashdata('op_msg') <> "") {
        echo "<p>" . $this->session->flashdata('op_msg') . "</p>";
    } ?></div>
    <?php

    function monthDropdown($name = "month", $selected = null) {
        $dd = '<select name="' . $name . '" id="' . $name . '">';

        $months = array(
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December');
        /*         * * the current month ** */
        //$selected = is_null($selected) ? $this->session->userdata('sess_month') : $selected;
        $dd.='<option >Month</option>';
        for ($i = 1; $i <= 12; $i++) {


            $dd .= '<option value="' . $i . '"';



            if ($i == $selected) {
                $dd .= 'selected';
            }
            /*             * * get the month ** */
            $dd .= '>' . $months[$i] . '</option>';
        }
        $dd .= '</select>';
        return $dd;
    }

    $id = $this->uri->segment(4);
    echo form_open('admin/college/pay_slip'. $id, array('id' => 'form1'));
    ?>

    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

    <div style="width:100%; "  class="noprint">

            <a href="<?php echo base_url(); ?>admin/college/add_payslip" class="c-link">Add Pay Slip</a>
            <?php
            if (isset($year)) {
                ?>

                <script>
                    $(document).ready(function() {
                        var year = '<?php echo $year; ?>';
                        $('#year').val(year).attr("selected", "selected");
                    });
                </SCRIPT>
            <?php }
            if (isset($month)) {
                ?>
                <script>
                    $(document).ready(function() {

                        <?php if($this->session->userdata('sess_month')){?>
                               var month = '<?php echo $input = ltrim($this->session->userdata('sess_month'), '0'); ?>';
                        <?php  } else{?>
                           var month = '<?php echo $month; ?>';
                         <?php   }?>

                        $('#month').val(month).attr("selected", "selected");
                    });
                </SCRIPT>
            <?php }
            ?>
            <?php
            /*             * * example usage ** */
            $name = 'month';
            $month = $this->session->userdata('sess_month');

            ?>
            <ul class="input_listing">

              <li class="" colspan="2">
              <p><?php echo monthDropdown($name, $month); ?>
              </li>
              <li class="" colspan="2">
              <p><?php echo form_dropdown('year', $years, $year); ?>
              </li>
              <li class="" colspan="2">
              <input type="submit" name="submit" value="Submit" />
              </li>
            <?php echo form_close(); ?>


            <?php echo form_close(); ?>
              </ul>
              </div>

<?php if (!empty($slip)) { ?>

  <div class="table-responsive">
      <table class="table table-default table-bordered table-striped table-hover TabDisplayRecords">
            <thead>
              <tr>
                    <th align="left">Employee</th>
                    <th align="left">Pay</th>
                    <th align="left">Grade Pay</th>
                    <th align="left">Other & Med. Allow:</th>
                    <th align="left">Employer P.F. (13.61%):</th>
                    <th align="left">TOTAL:</th>
                    <th align="left">P.Tax:</th>
                    <th align="left">I. Tax:</th>
                    <th align="left">PF25.61%:</th>
                    <th align="left">Festival Adv:</th>
                    <th align="left">Total Deduction: </th>
                    <th align="left">Net Pay:</th>
                    <td align="center" class="noprint"><b>Action</b></td>
                </tr>
            </thead>
            <tbody>
           <?php
           $basic_pay_total=0;$grade_pay_total=0;$medical_allow_total=0;$emp_contribute_pf_total=0;$salary_total_total=0;
           $ptax_total=0;$income_tax_total=0;$pf_deduction_total=0;$fest_advance_total=0;$total_deduction_total=0;$net_pay_total=0;
    foreach ($slip as $row) :
        ?>
                    <tr>
                        <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$row->emp_id.'"'); ?></td>
                        <td align="right"><?php  $basic_pay_total+=$row->basic_pay; echo floatval($row->basic_pay);?></td>
                        <td align="right"><?php $grade_pay_total+=$row->grade_pay; echo floatval($row->grade_pay); ?></td>
                        <td align="right"><?php $medical_allow_total+=$row->medical_allow; echo floatval($row->medical_allow); ?></td>
                        <td align="right"><?php $emp_contribute_pf_total+=$row->emp_contribute_pf	; echo floatval($row->emp_contribute_pf)	; ?></td>
                        <td align="right"><?php $salary_total_total+=$row->salary_total; echo floatval($row->salary_total); ?></td>
                        <td align="right"><?php $ptax_total+=$row->ptax; echo floatval($row->ptax); ?></td>
                        <td align="right"><?php $income_tax_total+=$row->income_tax; echo floatval($row->income_tax); ?></td>
                        <td align="right"><?php $pf_deduction_total+=$row->pf_deduction; echo floatval($row->pf_deduction); ?></td>
                        <td align="right"><?php $fest_advance_total+=$row->fest_advance; echo floatval($row->fest_advance); ?></td>
                        <td align="right"><?php $total_deduction_total+=$row->total_deduction; echo floatval($row->total_deduction); ?></td>
                        <td align="right"><?php $net_pay_total+=$row->net_pay; echo floatval($row->net_pay); ?></td>




                        <td align="center"  class="noprint"> <?php echo anchor('admin/college/edit_payslip/' . $row->emp_id.'/'.$this->ablfunctions->ablEncrypt($row->id), "Edit"); ?>
                            | <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deletePaySlip/' .$this->ablfunctions->ablEncrypt($row->id);?>');"
                               title="Delete Record">
                                Delete
                            </a>
                            | <?php echo anchor('admin/college/viewPayslip/' .$this->ablfunctions->ablEncrypt($row->id), "View"); ?> </td>
                    </tr>

    <?php endforeach; ?>
                     </tbody>
                    <tr>
                        <td>Total:</td>
                        <td align="right"><b><?php echo number_format((float)$basic_pay_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$grade_pay_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$medical_allow_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$emp_contribute_pf_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$salary_total_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$ptax_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$income_tax_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$pf_deduction_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$fest_advance_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$total_deduction_total, 2, '.', '');?></b></td>
                        <td align="right"><b><?php echo number_format((float)$net_pay_total, 2, '.', '');?></b></td>
                        <td class="noprint"></td>
                    </tr>

        </table>

    <p  class="noprint">
        <!-- <a href="<?php echo base_url().'admin/college/downloadTeacherAttendances/'.$month.'/'.$year; ?>" class="btn btn-success min-width-125 export" target="_blank">Export Table data into Excel</a> -->
        <a href="<?php echo base_url().'admin/college/downloadPayslipRecordMonth/'.$month.'/'.$year; ?>" class="btn btn-success min-width-125" target="_blank">Export Table data into Excel</a>
       <a href="#" class="btn btn-success min-width-125 printme" >Print</a>
    </p>
   </div>
<?php
}
else {
    echo "No records to display";
}
?>

<?php



if (!empty($slip)) { ?>
    <div id="dvData" style="display: none;"  class="noprint">
        <table>
              <tr>


                <td>Employee Name:</td>
                     <td>Pay:</td>
                    <td>Grade Pay:</td>
                    <td>Other & Med. Allow:</td>
                    <td>Employer P.F. (13.61%):</td>
                    <td>TOTAL:</td>
                    <td>P.Tax:</td>
                    <td>I. Tax:</td>
                    <td>PF25.61%:</td>
                    <td>Festival Adv:</td>
                    <td>Total Deduction:</td>
                    <td>Net Pay:</td>

                </tr>


           <?php
    foreach ($slip as $row) :
        ?>
                    <tr>
                      <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$row->emp_id.'"'); ?></td>
                      <td align="left"><?php  $basic_pay_total+=$row->basic_pay; echo floatval($row->basic_pay);?></td>
                      <td align="left"><?php $grade_pay_total+=$row->grade_pay; echo floatval($row->grade_pay); ?></td>
                      <td align="left"><?php $medical_allow_total+=$row->medical_allow; echo floatval($row->medical_allow); ?></td>
                      <td align="left"><?php $emp_contribute_pf_total+=$row->emp_contribute_pf	; echo floatval($row->emp_contribute_pf)	; ?></td>
                      <td align="left"><?php $salary_total_total+=$row->salary_total; echo floatval($row->salary_total); ?></td>
                      <td align="left"><?php $ptax_total+=$row->ptax; echo floatval($row->ptax); ?></td>
                      <td align="left"><?php $income_tax_total+=$row->income_tax; echo floatval($row->income_tax); ?></td>
                      <td align="left"><?php $pf_deduction_total+=$row->pf_deduction; echo floatval($row->pf_deduction); ?></td>
                      <td align="left"><?php $fest_advance_total+=$row->fest_advance; echo floatval($row->fest_advance); ?></td>
                      <td align="left"><?php $total_deduction_total+=$row->total_deduction; echo floatval($row->total_deduction); ?></td>
                      <td align="left"><?php $net_pay_total+=$row->net_pay; echo floatval($row->net_pay); ?></td>

                    </tr>

            <?php endforeach; ?>
             <tr>
               <td>Total:</td>
               <td align="right"><b><?php echo number_format((float)$basic_pay_total, 0, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$grade_pay_total, 0, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$medical_allow_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$emp_contribute_pf_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$salary_total_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$ptax_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$income_tax_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$pf_deduction_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$fest_advance_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$total_deduction_total, 2, '.', '');?></b></td>
               <td align="right"><b><?php echo number_format((float)$net_pay_total, 2, '.', '');?></b></td>
               <td class="noprint"></td>
              </tr>

              </table>
              </div>
    </div>

<?php
}
else {
    echo "No records to display";
}
?>

<?php $this->load->view("admin/include/footer"); ?>
