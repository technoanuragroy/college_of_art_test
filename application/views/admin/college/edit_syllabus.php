<?php $this->load->view("admin/include/header"); ?>


    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Edit Syllabus</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open_multipart('', array('id' => 'frmRegister', 'class' => 'js-validation-material')); ?>

                        <div class="form-group">
                            <label class="label-form"><span class="symbolcolor">*</span>Select Course</label>
                            <select required name="course_id" class="class" onchange="get_semester_list();">
                                <option value="">Select Class</option>
                                <?php
                                $selected = '';
                                foreach ($class_list as $class) {
                                    if ($details['course_id'] != '') {
                                        if ($details['course_id'] == $class['id']) {

                                            $selected = 'selected="selected"';
                                        } else {

                                            $selected = '';
                                        }
                                    }
                                    ?>
                                    <option value="<?php echo $class['id']; ?>"<?php echo $selected; ?>><?php echo $class['course_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    <div class="form-group row">
                        <label class="col-12" for="example-textarea-input">Notice</label>
                        <div class="col-12">
                            <textarea class="form-control" id="editor1" name="syllabus_text" rows="6" placeholder="Content.."><?php echo $details['syllabus_text']; ?></textarea>
                        </div>
                        <script>

                            CKEDITOR.replace('editor1');
                        </script>
                    </div>
                    <!-- <div class="form-group">
                        <div class="form-material">
                            <input type="text" class="form-control" id="date_of_birth" name="date_of_publish" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy">
                            <label for="date_of_birth">Date of Publish</label>
                        </div>
                    </div> -->

                    <div class="form-group row">
                        <?php
                        $document_url_array = explode(".", $details['document_url']);
                        $document_url_extension = $document_url_array[count($document_url_array) - 1];
                        $path=base_url().UPLOAD_DIR.SYLLABUS_DOCUMENT.$details['document_url'];
                        $display_path = base_url() . 'images/pdf.png';
                            ?>
                        <?php if (file_exists(UPLOAD_DIR.SYLLABUS_DOCUMENT.$details['document_url']) AND ($details['document_url']!='')) {
                             ?>
                            <div class="col-2" style="text-align:center;">

                                    <a href="<?php echo $path; ?>" target="_blank">
                                      <?php if ($document_url_extension=="pdf") {
                                      ?>
                                        <image src="<?php echo $display_path.'?t='.time();?>" style="height:50px;width: 50px;">
                                      <?php }else{
                                        ?>
                                          <image src="<?php echo $path.'?t='.time();?>" style="height:50px;width: 50px;">
                                      <?php }?>
                                    </a>
                                    <a href="javascript:;" title="Delete"
                                      onclick="confirm_delete('<?php echo base_url().'admin/college/deleteSyllabusFile/'.$details['document_url'].'/'.$details['id'] ?>');" ><i class="fa fa-trash"></i></a>
                                </div>
                                <?php
                                }
                                ?>
                        <label class="col-12" for="upload-Image">Upload Docs </label>
                        <div class="col-12 ">
                          <!--  <input type="file" id="upload-Image" name="doc_upload[]" multiple="" onchange="loadImageFile();">-->
                            <input type="file" id="inp_files" name="doc_upload" >
                            <!-- <input id="inp_img" name="img" type="hidden" value=""> -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                          <label>Status</label>
                          <select name="status" id="status">
                              <option value="1" <?php if($details['status'] == '1') { echo 'selected="selected"'; } ?>>Active</option>
                              <option value="0" <?php if($details['status'] == '0') { echo 'selected="selected"'; } ?>>Inactive</option>
                          </select>
                        </div>
                    </div>
<!--                    <td>Origal Img - <img id="original-Img"/></td>
                    <td>Compress Img - <img id="upload-Preview"/></td>-->

                    <div class="form-group">
                      <input type="hidden" name="notice_id" value="<?php echo $this->uri->segment(4); ?>">
                        <input type="submit" class="btn btn-alt-primary" name="submit" value="Update">
                    </div>
                    <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
