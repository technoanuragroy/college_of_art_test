<?php $this->load->view("admin/include/header"); ?>


    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                      <h1>Payment Report <?php if (isset($emp_name)) { ?> of <?php echo $emp_name; ?> from <?php echo $from_date; ?> to <?php echo $to_date;
                              }
                              ?></h1>
                              <div id="body">

                                <ul class="input_listing">
                                  <div class="noprint">
                              <?php echo form_open('', array('id' => 'form1')); ?>
                              <li class="" colspan="3">
                                  <span>
                                      <label>Payment type</label>
                                      <select name="payment_type"  required>
                                        <option value="">Select Payment type</option>
                                          <option value="admission_from_payment">Admission form payment</option>
                                          <option value="admission_payment">Admission Fees Payment</option>
                                      </select>
                                  </span>
                              </li>
                                      <li class="" colspan="3">
                                      &nbsp; Student Email: &nbsp;
                                      <input type="email" name="email" id="email"  placeholder="Student Email" required="" <?php echo set_value('email');?>>
                                      <!-- <?php echo form_input(array('name' => 'email', 'id' => 'email', 'value' => '', 'class' => 'validate[required]','placeholder' => 'Enter Email'), set_value('email')); ?> -->
                                      </li>
                                      <li class="" colspan="3">
                                      &nbsp; Student Phone: &nbsp;
                                      <input type="text" name="phone" id="phone"  placeholder="Student Phone" required="" <?php echo set_value('phone');?>>
                                      <!-- <?php echo form_input(array('name' => 'phone', 'id' => 'phone','value' => '', 'class' => 'validate[required]','placeholder' => 'Enter Phone'), set_value('phone')); ?> -->
                                      </li>
                                      <li class="full_width_li">
                                      <?php echo form_input(array('type' => 'submit', 'name' => 'submit', 'value' => 'Search','class'=>'submit_btn')); ?></p>
                                      </li>
                              <?php echo form_close(); ?>
                                  </div>
                                  <div>
                              <?php if (isset($details) AND count($details)!=0) { ?>

                                <div class="table-responsive">
                                    <?php echo form_open('', array('id' => 'form1')); ?>
                                    <table class="table table-default table-bordered table-striped table-hover">
                                        <thead>
                                                  <tr>

                                                      <th align="left">Student Name</th>
                                                       <th align="left">Student Email</th>
                                                      <th align="left">Mobile No</th>
                                                      <?php if ($payment_type=='admission_from_payment') {
                                                      ?>
                                                      <th align="left">Admission Form<br> Payment Status</th>
                                                      <?php  }?>
                                                      <?php if ($payment_type=='admission_from_payment') {
                                                      ?>
                                                      <th align="left">Admission Form<br> Payment Amount</th>
                                                      <?php  }?>
                                                      <?php if ($payment_type=='admission_payment') {
                                                      ?>
                                                      <th align="left">Admission <br> Payment Status</th>
                                                      <?php  }?>
                                                      <?php if ($payment_type=='admission_payment') {
                                                      ?>
                                                      <th align="left">Admission <br> Payment Amount</th>
                                                      <?php  }?>

                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <?php

                                                     $staff_id=$this->ablfunctions->ablEncrypt($details->id);
                                                      ?>
                                                      <tr>
                                                        <input type="hidden" name="student_id" value="<?php echo $details->id;?>">
                                                        <input type="hidden" name="payment_type" value="<?php echo $payment_type;?>">

                                                        <td><?php echo $details->name;?></td>
                                                        <td><?php echo $details->email;?></td>
                                                        <td><?php echo $details->phone;?></td>
                                                        <?php if ($payment_type=='admission_from_payment') {
                                                        ?>
                                                        <td ><?php
                                                        $admission_from_payment = $this->my_custom_functions->get_particular_field_value("tbl_student_payment_status","admission_from_payment", 'and student_id="'.$details->id.'"');
                                                        $admission_from_payment_ammount = $this->my_custom_functions->get_particular_field_value("tbl_student_payment_status","admission_from_payment_ammount", 'and student_id="'.$details->id.'"');
                                                        if ($admission_from_payment == 0) {
                                                            echo "<span class='badge badge-danger'>Pending</span>";
                                                        ?>
                                                        <!-- <input type="checkbox" name="admission_from_payment" title="Check Payment" class="admission_from_payment"> -->
                                                        <?php
                                                        } else if ($admission_from_payment == 1) {
                                                            echo "<span class='badge badge-success'>Received</span>";
                                                        }
                                                         ?></td>
                                                         <td><?php if ($admission_from_payment_ammount==0) {
                                                          ?>
                                                           <input type="text" name="admission_from_payment_ammount" required>
                                                         <?php }else if ($admission_from_payment_ammount > 0) {
                                                           echo $admission_from_payment_ammount;
                                                            }
                                                         ?></td>
                                                        <?php  }?>
                                                        <?php if ($payment_type=='admission_payment') {
                                                        ?>
                                                        <td><?php
                                                        $admission_payment = $this->my_custom_functions->get_particular_field_value("tbl_student_payment_status","admission_payment", 'and student_id="'.$details->id.'"');
                                                        $admission_payment_ammount = $this->my_custom_functions->get_particular_field_value("tbl_student_payment_status","admission_payment_ammount", 'and student_id="'.$details->id.'"');
                                                        if ($admission_payment == 0) {
                                                            echo "<span class='badge badge-danger'>Pending</span>";
                                                            ?>
                                                            <!-- <input type="checkbox" name="admission_payment" title="Check Payment"> -->
                                                            <?php
                                                        } else if ($admission_payment == 1) {
                                                            echo "<span class='badge badge-success'>Received</span>";
                                                        }?></td>
                                                        <td><?php if ($admission_payment_ammount==0) {
                                                         ?>
                                                          <input type="text" name="admission_from_payment_ammount" class="form-control" required>
                                                        <?php }else if ($admission_payment_ammount > 0) {
                                                          echo $admission_payment_ammount;
                                                           }
                                                        ?></td>
                                                        <?php  }?>

                                                        <!-- <td>
                                                            <a href="<?php echo base_url().'admin/college/editPaymentRegister/'.$staff_id; ?>" title="Edit Record">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                        </td> -->
                                                      </tr>
                                              </tbody>


                                            </table>
                                            <?php
                                              $check = $this->my_custom_functions->get_particular_field_value("tbl_student_payment_status",$payment_type, 'and student_id="'.$details->id.'"');
                                              if ($check==0) {
                                                echo form_input(array('type' => 'submit', 'name' => 'submit', 'value' => 'Confirm','class'=>'submit_btn'));
                                              }

                                             ?></p>
                                    <?php echo form_close(); ?>
                                            </div>


                                      </div>
                                      <?php
                                  }else {
                                  echo "No Records Found";
                                  }
                                  ?>

                              </ul>
                              </div>
                              </div>

                    </div>
                </div>
            </div>
      </div>
     </div>




<?php $this->load->view("admin/include/footer"); ?>
