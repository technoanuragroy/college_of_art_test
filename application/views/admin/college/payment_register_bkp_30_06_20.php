<?php $this->load->view("admin/include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Payment Details</h2>



                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover js-dataTable-full">
                            <thead>
                                <tr>
                                    <th>Student Name</th>
                                    <th>Student Email</th>
                                    <th>Mobile No</th>
                                    <th>Admission From<br> Payment</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($details)) {
                                    foreach($details as $page) {
                                     $staff_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>

                                            <td><?php echo $page['name']; ?></td>
                                            <td><?php echo $page['email']; ?></td>
                                            <td><?php echo $page['phone']; ?></td>
                                            <td><?php
                                                  if ($page['admission_from_payment'] == 0) {
                                                      echo "<span class='badge badge-danger'>Pending</span>";
                                                  } else if ($page['admission_from_payment'] == 1) {
                                                      echo "<span class='badge badge-success'>Received</span>";
                                                  }
                                                  ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/editPaymentRegister/'.$staff_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>

                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="6">No Student found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
