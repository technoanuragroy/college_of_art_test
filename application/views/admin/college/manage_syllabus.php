<?php $this->load->view("admin/include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Syllabus</h2>

                <a href="<?php echo base_url(); ?>admin/college/addSyllabus" class="c-link">Add Syllabus</a>

                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover js-dataTable-full">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Course</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($details)) {
                                    $no=1;
                                    foreach($details as $page) {
                                     $staff_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>

                                            <td><?php echo $no;?></td>
                                            <td><?php echo $this->my_custom_functions->get_particular_field_value('tbl_courses', 'course_name', 'and id = "' . $page['course_id'] . '" '); ?></td>
                                            <td><?php if ($page['status']==1) {
                                              echo "Active";
                                            } else {
                                              echo "InActive";
                                            } ?></td>
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/editSyllabus/'.$staff_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteSyllabus/'.$page['document_url'].'/'.$page['id']; ?>')"
                                                   title="Delete Record">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                            <?php
                          }$no++;
                                } else { ?>

                                    <tr>
                                        <td colspan="6">No Syllabus found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
