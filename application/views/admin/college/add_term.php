<?php $this->load->view("admin/include/header"); ?>

    <script type="text/javascript">
    $(document).ready(function() {
        $('#combine_rslt').click(function() {
            if($(this).prop("checked") == true) {
                $('#term_label').attr('required','true');
                $('.final_label').slideDown();
            } else if($(this).prop("checked") == false) {
                $('#term_label').removeAttr('required');
                $('.final_label').slideUp();
            }
        });
    });

    </script>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Create Term</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                     <li class="full_width_li">
                                        <span>
                                            <label>Select Semester</label>
                                            <select name="semester_id" id="semester_id">
                                                <option value="">Select Semester</option>
                                                <?php foreach ($all_semester as $row) { ?>
                                                    <option value="<?php echo $row['id']; ?>"><?php echo $row['semester_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Term Name</label>
                                            <input type="text" name="term_name" id="term_name" placeholder="Term Name" required="">
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Combine Exam Result</label>
                                            <input  type="checkbox" name="combine_rslt" id="combine_rslt" value="1">
                                        </span>
                                    </li>

                                    <li  class="form-group final_label full_width_li" style="display:none;">
                                        <span>
                                            <label>Combine result name</label>
                                            <input type="text" class="" id="term_label" name="term_label" placeholder="Enter final term label">
                                        </span>
                                    </li>

                                   

                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Register">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
