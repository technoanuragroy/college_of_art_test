<?php $this->load->view("admin/include/header"); ?>

<style>
    #div_slideDown_search{
        display: none;
    }
</style>

<script type="text/javascript">
$(document).ready(function() {
    $(".TabDisplayRecords").tablesorter({sortList: [[0,0]]} );
  $('a.printme').click(function(){
 window.print();
});

});
  $(document).ready(function () {
    function exportTableToCSV($table, filename) {

        var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace(/"/g, '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"',

            // Data URI
            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

        $(this)
            .attr({
            'download': filename,
                'href': csvData,
                'target': '_blank'
        });
    }

    // This must be a hyperlink
    $(".export").click(function ()  {
        // CSV
        exportTableToCSV.apply(this, [$('#dvData>table'), 'student_list_<?php echo date('F');?>.xls']);

        // IF CSV, don't do event.preventDefault() or return false
        // We actually need this to be a typical hyperlink
    });
});

</script>

 <?php if (isset($session_active)) {
    if ($session_active == '1') {
    ?>
    <script type="text/javascript">

        $("#clickbtn").show();

    </script>

    <?php
     }

     } else{
    ?>
    <style>
    #clickbtn{
        display: none;
    }
    </style>
    <?php

     } ?>
<script type="text/javascript">

     jQuery(document).ready(function ($) {
// executes when HTML-Document is loaded and DOM is ready
        $("#clickbtn").click(function () {
            $("#div_slideDown_search").slideToggle('fast');
        });

    });

</script>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Student Admissions</h2>


                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <a href="javascript:" class="c-link" id="clickbtn" >Status Search </a>

                    <div class="form_wrap" id="div_slideDown_search">

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">


                                    <li class="full_width_li">
                                        <span>

                                            <select name="status" id="status" >
                                        <option value="">Select
                                        Status</option>
                                                <?php
                                                $status = $this->config->item('form_status');
                                                foreach($status as $key=>$stype){?>
                                                <option value="<?php echo $key;?>" ><?php echo $stype;?></option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <input type="submit" name="search" id="search" class="submit_btn" value="Status Search">
                                    </li>
                                    <input type="hidden" name="current_session_id" id="current_session_id" value="<?php echo $post_data['session_id']; ?>">
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>


                 <div class="form_wrap" id="div_slideDown">

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">


                                    <li class="full_width_li">
                                        <span>
                                            <!-- <label>Status</label> -->
                                            <select id="session" name="session">
                                                <option value="">Select Session</option>
                                <?php
                                $selected = '';
                                foreach ($all_session as $session) {
                                    if ($post_data['session_id'] != '') {
                                        if ($post_data['session_id'] == $session['id']) {

                                            $selected = 'selected="selected"';
                                        } else {

                                            $selected = '';
                                        }
                                    }
                                    ?>
                                    <option value="<?php echo $session['id']; ?>" <?php echo $selected; ?>><?php echo $session['session_name']; ?></option>
                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Search">
                                        <a href="<?php echo base_url(); ?>admin/college/addStudentManual" class="btn btn-info min-width-125">Add Student</a>
                                          <a href="<?php echo base_url().'admin/enrollment/enrollStudent'; ?>" class="btn btn-success min-width-125">Enroll Student<a>
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>


                    <?php
                    if(!empty($all_student)) {
                    ?>
                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Student Name</th>
                                    <th>Course Name</th>
                                    <th>Status</th>
                                    <!-- <th>Issue Admit Card</th> -->
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($all_student)) {
                                    foreach($all_student as $page) {
                                     $student_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>
                                            <td><?php echo $page['student_name']; ?></td>
                                            <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_courses","course_name", 'and id="'.$page['course_id'].'"'); ?>

                                            </td>
                                            <td><?php
                                                  if ($page['status'] == 1) {
                                                      echo "<span class='badge badge-success'>Need Review</span>";
                                                  } else if ($page['status'] == 2) {
                                                      echo "<span class='badge badge-success'>Review Pending</span>";
                                                  }else if ($page['status'] == 3) {
                                                      echo "<span class='badge badge-success'>Admit (Test) Issued</span>";
                                                  }else if ($page['status'] == 4) {
                                                      echo "<span class='badge badge-success'>Admit (Admission) Issued</span>";
                                                  }else if ($page['status'] == 5) {
                                                      echo "<span class='badge badge-success'>Payment Link Activated</span>";
                                                  }else if ($page['status'] == 6) {
                                                      echo "<span class='badge badge-success'>Admission Fees received</span>";
                                                  }else if ($page['status'] == 7) {
                                                      echo "<span class='badge badge-success'>Enrolled</span>";
                                                  }
                                                  ?>
                                            </td>
                                            <!-- <td>
                                             <a href="<?php echo base_url().'admin/college/downloadAdmitCard/'.$student_id; ?>" title="Issue Admit Card" target="_blank">
                                            <span class='badge badge-primary'>Admit Card</span>
                                            </a>
                                            </td> -->
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/editStudentRegister/'.$student_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>


                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="8">No Register Student found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>

                        <p  class="noprint">
                            <a href="#" class="btn btn-success min-width-125 export" >Export Table data into Excel</a>

                    </p>
                    </div>
                    <?php
                    }
                    ?>


                </div>
            </div>
        </div>


    <?php  if (!empty($all_student)) { ?>
        <div id="dvData" style="display: none;"  class="noprint">
            <table>
                  <tr>
                        <td>Name of the Candidate:</td>
                        <td>Form No:</td>
                        <td>Course Name:</td>
                        <td>Session Name:</td>
                        <td>Father’s /Mother’s Name:</td>
                        <td>Name of the Guardian:</td>
                        <td>Permanent Address:</td>
                        <td>Email:</td>
                        <td>Phone Number:</td>
                        <td>PF25.61%:</td>
                        <td>Festival Adv:</td>
                        <td>Total Deduction:</td>
                        <td>Net Pay:</td>

                    </tr>


               <?php
        foreach ($all_student as $row) :
            ?>
                        <tr>
                          <td align="left"><?php echo $row['student_name']; ?></td>
                          <td><?php echo $row['from_id']; ?></td>
                          <!-- <td align="left"><?php  echo $this->my_custom_functions->get_particular_field_value("tbl_courses","course_name", 'and id="'.$row['course_id'].'"');?></td>
                          <td align="left"><?php  echo $this->my_custom_functions->get_particular_field_value("tbl_session","session_name", 'and id="'.$row['session_id'].'"');?></td> -->
                          <td align="left"><?php echo $row['father_or_mother_name']; ?></td>
                          <td align="left"><?php echo $row['father_or_mother_name']; ?></td>
                          <td align="left"><?php echo $row['father_or_mother_name']; ?></td>
                          <td align="left"><?php echo $row['father_or_mother_name']; ?></td>
                          <td align="left"><?php echo $row['father_or_mother_name']; ?></td>
                          <td align="left"><?php echo $row['father_or_mother_name']; ?></td>
                          <td align="left"><?php echo $row['father_or_mother_name']; ?></td>
                          <td align="left"><?php echo $row['father_or_mother_name']; ?></td>
                          <td align="left"><?php echo $row['father_or_mother_name']; ?></td>
                          <td align="left"><?php echo $row['father_or_mother_name']; ?></td>

                        </tr>

                <?php endforeach; ?>


                  </table>
                  </div>

    <?php
    }

    ?>
      </div>

<?php $this->load->view("admin/include/footer"); ?>
