<?php $this->load->view("admin/include/header"); ?>

      <script type="text/javascript">
      $(document).ready(function() {
      $('#date').datepicker(
              {changeMonth: true,
                  changeYear: true,
                  dateFormat: 'yy-mm-dd'
              }
      );
      });
      </script>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Edit Expenses</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                  <li class="full_width_li">
                                     <li colspan="3">
                                       <span>
                                           <label>Date</label>
                                           <input type="text" name="date" id="date" placeholder="Enter Date" required="" autocomplete="off" value="<?php echo $details['date']; ?>">
                                       </span>
                                    </li>

                                    <li colspan="3">
                                      <span>
                                          <label>Reference Id</label>
                                          <input type="text" name="reference_id" placeholder="Enter Reference Id" required="" autocomplete="off" value="<?php echo $details['reference_id']; ?>">
                                      </span>
                                    </li>
                                  </li>
                                  <li class="full_width_li">
                                    <li colspan="3">
                                        <span>
                                            <label>Account Name</label>
                                            <input type="text" name="account_name" value="<?php echo $details['account_name']; ?>" placeholder="Student Name" required="" autocomplete="off">
                                        </span>
                                    </li>
                                    <li colspan="3">
                                      <span>
                                          <label>Particular</label>
                                          <input type="text" name="particular" value="<?php echo $details['particular']; ?>" placeholder="Particular" required="" autocomplete="off">
                                      </span>
                                    </li>
                                  </li>
                                  <li class="full_width_li">

                                    <li colspan="3">
                                      <span>
                                          <label>Credit Amount</label>
                                          <input type="text" name="credit_ammount" value="<?php echo $details['credit_ammount']; ?>" placeholder="Credit Amount" autocomplete="off">
                                      </span>
                                    </li>
                                    <li colspan="3">
                                      <span>
                                          <label>Debit Amount</label>
                                          <input type="text" name="debit_ammount" value="<?php echo $details['debit_ammount']; ?>" placeholder="Debit Amount" autocomplete="off">
                                      </span>
                                      <input type="hidden" name="page_id" value="<?php echo $this->ablfunctions->ablEncrypt($details['id']); ?>">
                                    </li>
                                  </li>



                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Update">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
