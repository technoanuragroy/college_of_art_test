<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('.copier_link').click(function () {
            var fees_container_html = $(".examSubjectcopy").html();
            $(".examSubjectAppend").append(fees_container_html);

            $(".examSubjectAppend").find(".subject").each(function (i, e) {
                $(this).attr("name", "subject[" + i + "]");
            });
            $(".examSubjectAppend").find(".total_marks").each(function (i, e) {
                $(this).attr("name", "total_marks[" + i + "]");
            });
            $(".examSubjectAppend").find(".pass_marks").each(function (i, e) {
                $(this).attr("name", "pass_marks[" + i + "]");
            });
        });



    });
    function remove_fees(e) {

        $(e).closest(".row").remove();
    }

    $(document).ready(function () {
        $('#exam_start_date').datepicker({
            dateFormat: 'dd/mm/yy',
        });
        $('#exam_end_date').datepicker({
            dateFormat: 'dd/mm/yy',
        });
        $('#publish_date').datepicker({
            dateFormat: 'dd/mm/yy',
        });
    });


</script>

            <!-- Top Modal -->
        <div class="modal fade" id="modal-top" tabindex="-1" role="dialog" aria-labelledby="modal-top" aria-hidden="true">
            <div class="modal-dialog modal-dialog-top" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Confirmation</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content message_block">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                            <i class="fa fa-check"></i> Ok
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Top Modal -->

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Copy Exam</h2>


                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open_multipart('admin/college/addExam', array('id' => 'formAddAdmin')); ?>
                              <?php
                    //echo "<pre>";print_r($exam_data); 
                    foreach ($exam_data['exam'] as $row_exam) {
                        ?>

                                <ul class="input_listing">
                                     <li class="full_width_li">
                                        <span>
                                            <label>Select Term</label>
                                            <select required class="form-control" id="term" name="term">
                                                <option value="">Select Term</option>
                                                <?php foreach ($term_list as $term ) { if ($term['id'] == $row_exam['term_id']) {
                                            $selected = "selected='selected'";
                                        } else {
                                            $selected = "";
                                        } ?>
                                        <option value="<?php echo $term['id']; ?>" <?php echo $selected; ?> ><?php echo $term['term_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Exam Name</label>
                                            <input required type="text" id="exam_name" class="form-control" name="exam_name" placeholder="Enter exam name" value="<?php echo $row_exam['exam_name']; ?>">
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Exam Display Name</label>
                                            <input required type="text"class="form-control" id="exam_disp_name" name="exam_disp_name" placeholder="Enter exam display name" value="<?php echo $row_exam['exam_display_name']; ?>">
                                        </span>
                                    </li>

                                     <li class="full_width_li">
                                        <span>
                                            <label>Exam Start Date</label>
                                            <input required type="text" class="form-control e_s_d" id="exam_start_date" name="exam_start_date" placeholder="Enter exam start date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d/m/Y', strtotime($row_exam['exam_start_date'])) ?>">
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Exam End Date</label>
                                            <input required type="text" class="form-control e_n_d" id="exam_end_date" name="exam_end_date" placeholder="Enter exam end date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?php echo date('d/m/Y', strtotime($row_exam['exam_end_date'])) ?>">
                                        </span>
                                    </li>

                                    <?php
                        $publish_date_time = date('d/m/Y h:i A', $row_exam['publish_date']);
                        $publish_date = explode(" ", $publish_date_time);

                        $publish_time = explode(":", $publish_date[1]);
                        $hour = (int) $publish_time[0];
                        $minute = (int) $publish_time[1];
                        ?>

                    <li class="col-md-3">
                    <span>
                    <label>Exam Publish Date</label>
                    <input required type="text" class="form-control e_p_d" id="publish_date" name="publish_date" placeholder="Enter exam publish date" data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="<?php echo $publish_date[0]; ?>">
                    </span>
                    </li>

                    <li class="col-md-3">
                    <span>
                    <label>Select Start Hour</label>
                    <?php $hour_list = $this->config->item('hour_list'); ?>
                                <select required="" name="start_hour" class="form-control">
                                    <option value="">Select Hour</option>
                                    <?php foreach($hour_list as $key=>$val){ 
                                        if ($hour == $key) {
                                            $selected = "selected='slected'";
                                            } else {
                                             $selected = "";
                                            }
                                        ?>
                                    <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $val; ?></option>
                                    <?php } ?>
                                </select>
                                </span>
                                </li>

                                <li class="col-md-3">
                                <span>
                                <label>Select Minute</label>
                                <select required="" name="start_minute" class="form-control">
                                    <option value="">Select Minute</option>
                                    <?php for($i = 0;$i<60;$i+=1){ 
                                        if ($i == $minute) {
                                        $selected = "selected='slected'";
                                        } else {
                                         $selected = "";
                                        }
                                        ?>
                                    <option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                                </span>
                                </li>

                                <li class="col-md-3">
                                <label>Select Start Meridian</label>
                                <select required="" name="start_meridian" class="form-control">
                                    <option value="">Select</option>
                                    <option value="AM" <?php
                                    if ($publish_date[2] == 'AM') {
                                     echo "selected";
                                    }
                                    ?>>A.M</option>
                                    <option value="PM" <?php
                                    if ($publish_date[2] == 'PM') {
                                    echo "selected";
                                    }
                                    ?>>P.M</option>
                                    
                                </select>
                                </li>

                                <h3 class="content-heading">Marks Detail</h3>

                        <?php } ?>

                        <div class="examSubjectAppend">
                        <?php
                        foreach ($exam_data['score_data'] as $row_score) { //echo "<pre>";print_r($row_score); 
                            $encrypted = $this->ablfunctions->ablEncrypt($row_score['id']);
                            ?>
                        <div class="row tt_<?php echo $encrypted; ?>">

                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label for="total_marks">Select Subject</label>
                                        <select required="" name="subject[]" class="form-control subject">
                                            <option value="">Select Subject</option>
                                            <?php foreach ($subject_list as $subject) {
                                                if($subject['id'] == $row_score['subject_id']){
                                                    $selected = "selected='selected'";
                                                }else{
                                                    $selected = "";
                                                }
                                                ?>
                                                <option value="<?php echo $subject['id']; ?>" <?php echo $selected; ?>><?php echo $subject['subject_name']; ?></option>
                                            <?php } ?>
                                        </select> 
                                </div>
                            </li>

                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label for="total_marks">Total Marks</label>
                                        <input required type="text" class="form-control total_marks" name="total_marks[]" placeholder="Enter total marks" value="<?php echo $row_score['full_marks'] ?>">
                                        
                                    </div> 
                                </div>
                            </li>
                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label for="pass_marks">Pass Marks</label>
                                        <input required type="text" class="form-control pass_marks" name="pass_marks[]" placeholder="Enter pass marks" value="<?php echo $row_score['pass_marks'] ?>">
                                        
                                    </div> 
                                </div>
                            </li>

                        </div>
                        <?php } ?>
                    </div>

                                <div class="form-group">
                                <div class="form-material">

                                <label for="copy_existing_exam"><a href="javascript:" class="copier_link"><i class="fa fa-plus"></i> Copy from existing exam</a></label>
                                </div>
                                </div>

                                   

                                    <li class="full_width_li">
                                        <input type="hidden" name="exam_id" value="<?php echo $this->uri->segment(4); ?>">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Submit">
                                    </li>
                                </ul>



                        <?php echo form_close(); ?>
                        <div class="examSubjectcopy" style="display: none;">
                        <div class="row">

                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label >Select Subject</label>
                                        <select required="" name="subject[]" class="form-control subject">
                                            <option value="">Select Subject</option>
                                            <?php foreach ($subject_list as $subject) { ?>
                                                <option value="<?php echo $subject['id']; ?>"><?php echo $subject['subject_name']; ?></option>
                                            <?php } ?>
                                        </select>
                                        
                                    </div> 
                                </div>
                                <a href="javascript:" class="" onclick="remove_fees(this);"><i class="fa fa-minus-circle"></i>&nbsp;Remove</a>
                            </li>

                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label>Total Marks</label>
                                        <input required type="text" class="form-control total_marks" name="total_marks[]" placeholder="Enter total marks">
                                        
                                    </div> 
                                </div>
                            </li>
                            <li class="col-md-4">
                                <div class="form-group">
                                    <div class="form-material">
                                        <label for="pass_marks">Pass Marks</label>
                                        <input required type="text" class="form-control pass_marks" name="pass_marks[]" placeholder="Enter pass marks">
                                        
                                    </div> 
                                </div>

                            </li>

                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END Page Content -->
<script type="text/javascript">
    $(document).ready(function () {
        $(".e_s_d").keydown(function (event) {
            return false;
        });
        $(".e_n_d").keydown(function (event) {
            return false;
        });
        $(".e_p_d").keydown(function (event) {
            return false;
        });
    });

</script>

<?php $this->load->view("admin/include/footer"); ?>
