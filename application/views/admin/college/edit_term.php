<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">
    $(document).ready(function () {
        if ($('#combine_rslt').prop("checked") == true) {
            $('#term_label').attr('required', 'true');
            $('.final_label').slideDown();
        } else {
            $('#term_label').removeAttr('required');
            $('.final_label').slideUp();
        }

        $('#combine_rslt').click(function () {
            if ($(this).prop("checked") == true) {
                $('#term_label').attr('required', 'true');
                $('.final_label').slideDown();
            } else if ($(this).prop("checked") == false) {
                $('#term_label').removeAttr('required');
                $('.final_label').slideUp();
                $('#term_label').val('');
            }
        });
    });
    
    function open_move_block(){
        $(".move_block").slideToggle();
    }


    </script>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Edit Term</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                     <li class="full_width_li">
                                        <span>
                                            <label>Select Semester</label>
                                            <select name="semester_id" id="semester_id">
                                                <option value="">Select Semester</option>
                                                <?php foreach ($all_semester as $row) {
                                                    if ($row['id'] == $details['semester_id']) {
                                        $selected = 'selected="selected"';
                                    } else {
                                        $selected = '';
                                    } ?>
                                                    <option value="<?php echo $row['id']; ?>" <?php echo $selected; ?>><?php echo $row['semester_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                  
                                    <li class="full_width_li">
                                        <span>
                                            <label>Term Name</label>
                                            <input type="text" name="term_name" id="term_name" placeholder="Term Name" required="" value="<?php echo $details['term_name']; ?>">
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Combine Exam Result</label>
                                            <input  type="checkbox" name="combine_rslt" id="combine_rslt" <?php
                            if ($details['combined_exam_results'] == 1) {
                                echo "checked";
                            }
                            ?> value="1">
                                        </span>
                                    </li>

                                    <li  class="form-group final_label full_width_li" style="display:none;">
                                        <span>
                                            <label>Final Term Label</label>
                                            <input type="text" class="" id="term_label" name="term_label" placeholder="Enter final term label" value="<?php echo $details['combined_result_name']; ?>">
                                        </span>
                                    </li>

                                   <input type="hidden" name="page_id" value="<?php echo $this->uri->segment(4); ?>">

                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Update">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
