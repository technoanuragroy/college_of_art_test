<?php $this->load->view("admin/include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Edit Student</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>Student Name</label>
                                            <input type="text" name="name"  placeholder="Enter Student Name" value="<?php echo $details['name']; ?>" required="">
                                        </span>
                                    </li>
                                    <li class="full_width_li">
                                        <span>
                                            <label>Student Email</label>
                                            <input type="text" name="email"  placeholder="Enter Student Email" value="<?php echo $details['email']; ?>" required="">
                                        </span>
                                    </li>



                                    <li class="full_width_li">
                                        <input type="hidden" name="class_id" value="<?php echo $this->uri->segment(4); ?>">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Update">
                                        <!-- <a href="<?php echo base_url().'admin/college/manualFeesBreakups/'.$this->uri->segment(4); ?>" class="btn btn-success min-width-125">Set Manual Fees<a> -->
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
