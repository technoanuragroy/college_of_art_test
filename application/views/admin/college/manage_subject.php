<?php $this->load->view("admin/include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Subjects</h2>

                <a href="<?php echo base_url(); ?>admin/college/addSubjects" class="c-link">Add Subjects</a>

                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Subject Name</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($all_subject)) {
                                    foreach($all_subject as $page) {
                                     $subject_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>
                                            <td><?php echo $page['subject_name']; ?></td>
                                            <td><?php
                                                  if ($page['status'] == 1) {
                                                      echo "<span class='badge badge-success'>Active</span>";
                                                  } else {
                                                      echo "<span class='badge badge-danger'>Inactive</span>";
                                                  }
                                                  ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/editSubjects/'.$subject_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteSubjects/'.$subject_id; ?>');" title="Delete Record">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="8">No Subjects found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
