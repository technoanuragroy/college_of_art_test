<?php $this->load->view("admin/include/header"); ?>
<script type="text/javascript">
$(document).ready(function() {
$('#from_date').datepicker(
        {changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        }
);
$('#to_date').datepicker(
        {
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        }
);
});
</script>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Payment Report</h2>

                <!-- <a href="<?php echo base_url(); ?>admin/college/addStaff" class="c-link">Add Staff</a> -->

                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="noprint">
                    <ul class="input_listing">

                       <?php echo form_open('admin/college/paymentReport', array('id' => 'form1')); ?>

                          <li class="" colspan="3">
                          &nbsp; From Date &nbsp;<?php echo form_input(array('name' => 'from_date', 'id' => 'from_date', 'class' => 'required','placeholder'=> 'From Date','autocomplete'=> 'off'),set_value('from_date')); ?>
                          </li>
                          <li class="" colspan="3">
                          &nbsp; To Date &nbsp;<?php echo form_input(array('name' => 'to_date', 'id' => 'to_date', 'class' => 'required','placeholder'=> 'To Date','autocomplete'=> 'off'),set_value('to_date')); ?>
                          </li>
                          <li class="full_width_li">
                          <?php echo form_input(array('type' => 'submit', 'name' => 'submit', 'value' => 'Search','class'=>'submit_btn')); ?></p>
                          </li>
                          <?php echo form_close(); ?>
                          </ul>
                        </div>

                    <div class="table-responsive">
                      <?php
                      if(!empty($details)) {
                      ?>
                      <h4>Payment Report From <?=$from_date;?> To <?=$to_date;?></h4>
                        <table class="table table-default table-bordered table-striped table-hover">
                             <?php echo form_open('admin/college/bulk_record_payment_report', array('id' => 'form1')); ?>
                            <thead>
                                <tr>
                                    <th>Check</th>
                                    <th>Student Name</th>
                                    <th>Student Email</th>
                                    <th>Phone</th>
                                    <th>Course</th>
                                    <th>Date</th>
                                    <th>Admission Form<br> Payment Status</th>
                                    <th>Admission Form<br> Payment Amount</th>
                                    <th>Admission<br> Payment Status</th>
                                    <th>Admission<br> Payment Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($details)) {
                                    foreach($details as $page) {
                                     //$staff_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>
                                            <td><input type="checkbox" name="check[<?php echo $page->id;?>]" value="<?php echo $page->id;?>"></td>
                                            <td><?php echo $page->name; ?></td>
                                            <td><?php echo $page->email; ?></td>
                                            <td><?php echo $page->phone; ?></td>
                                            <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_courses","course_name", 'and id="'.$page->course_id.'"'); ?></td>
                                            <td><?php echo date("d-m-Y", strtotime($page->join_date)); ?></td>
                                            <td><?php
                                            $admission_from_payment = $this->my_custom_functions->get_particular_field_value("tbl_student_payment_status","admission_from_payment", 'and student_id="'.$page->id.'"');
                                            if ($admission_from_payment == 0) {
                                                echo "<span class='badge badge-danger'>Pending</span>";
                                            }else if ($admission_from_payment == 1) {
                                                echo "<span class='badge badge-success'>Received</span>";
                                            }
                                             ?></td>
                                            <td><?php
                                            if ($admission_from_payment == 0) { ?>
                                              <input type="text" name="admission_from_payment_ammount[<?php echo $page->id;?>]" >
                                            <?php  }else if ($admission_from_payment == 1) {
                                                echo $this->my_custom_functions->get_particular_field_value("tbl_student_payment_status","admission_from_payment_ammount", 'and student_id="'.$page->id.'"');
                                            }
                                             ?></td>
                                            <td><?php
                                            $admission_payment = $this->my_custom_functions->get_particular_field_value("tbl_student_payment_status","admission_payment", 'and student_id="'.$page->id.'"');
                                            if ($admission_payment == 0) {
                                                echo "<span class='badge badge-danger'>Pending</span>";
                                            }else if ($admission_payment == 1) {
                                                echo "<span class='badge badge-success'>Received</span>";
                                            }
                                             ?></td>
                                            <td><?php
                                            if ($admission_payment == 0) { ?>
                                              <input type="text" name="admission_payment_ammount[<?php echo $page->id;?>]" >
                                            <?php  }else if ($admission_payment == 1) {
                                                echo $this->my_custom_functions->get_particular_field_value("tbl_student_payment_status","admission_payment_ammount", 'and student_id="'.$page->id.'"');
                                            }
                                            ?></td>

                                        </tr>


                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="8">No Data found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                        <input type="hidden" name="from_date" value="<?php echo $from_date; ?>">
                        <input type="hidden" name="to_date" value="<?php echo $to_date; ?>">
                        <?php
                        echo form_input(array('type' => 'submit', 'name' => 'submit_one', 'value' => 'Update','class'=>'submit_btn'));
                         echo form_close();
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
