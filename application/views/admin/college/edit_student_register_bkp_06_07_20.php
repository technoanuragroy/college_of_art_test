<?php $this->load->view("admin/include/header"); ?>

        <script type="text/javascript">
        $(document).ready(function () {
        $('#dob').datepicker({
            changeMonth: true,
            changeYear: true,
            //yearRange: '1950:2013',
            yearRange: "-100:+0",
            dateFormat: 'yy-mm-dd',
        });

          });

         //////////////////////Image Type Checking/////////////////////////

        function fileChange_student(e,manage) {

                //document.getElementsByClassName("student_passport_photo").value = '';
                $('#result_'+manage).html("");
                //document.getElementById('input_'+manage).value = '';

                for (var i = 0; i < e.target.files.length; i++) {

                    var file = e.target.files[i];

                    if (file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/gif") {

                        var reader = new FileReader();
                        reader.onload = function (readerEvent) {
                            var image = new Image();
                            image.onload = function (imageEvent) {
                                //var max_size = 275;

                                if (manage=='passport') {

                                    var max_size = 500; // In Kilobytes
                                    var allow_w = 437;
                                    var allow_h = 531;

                                } else if ((manage=='document_one')||(manage=='document_two')||(manage=='document_three')||(manage=='document_four')) {

                                    var max_size = 2000;
                                    var allow_w = 1000;
                                    var allow_h = 1000;

                                } else if (manage=='age_verification') {

                                    var max_size = 2000;
                                    var allow_w = 1000;
                                    var allow_h = 1000;

                                } else if (manage=='caste_cf') {

                                    var max_size = 2000;
                                    var allow_w = 1000;
                                    var allow_h = 1000;

                                } else if (manage=='guardian_sign') {

                                    var max_size = 900;
                                    var allow_w = 437;
                                    var allow_h = 177;

                                } else if (manage=='candidate_sign') {

                                    var max_size = 900;
                                    var allow_w = 437;
                                    var allow_h = 177;

                                } else if (manage=='candidate_sign_full') {

                                    var max_size = 900;
                                    var allow_w = 437;
                                    var allow_h = 177;
                                }

                                var file_size = document.getElementById('student_'+manage).files[0].size; // In Bytes
                                var s = file_size / 1024; // In Kilobytes
                                // var w = image.width;
                                // var h = image.height;

                                //alert('size : '+s+' Kb width : '+w+' px height : '+h+' px');

                                if(s > max_size) {
                                    alert('File Size Is too Big');
                                    $('#result_'+manage).html("Please Choose A File Of Given Size");
                                    $('#student_'+manage).val("");
                                }
                                // else if (w > allow_w || h > allow_h) {
                                //     alert('File Dimensions Do Not Match');
                                //     $('#result_'+manage).html("Please Choose A File Of Given Dimensions");
                                //     $('#student_'+manage).val("");
                                // }

//                                else if (w > h) {
//                                    if (w > max_size) {
//                                        h *= max_size / w;
//                                        w = max_size;
//                                        alert('File Size Is too Big');
//                                        $('#result_'+manage).html("Please Enter Correct Image Format");
//                                        $('#student_'+manage).val("");
//                                    }
//                                } else {
//                                    if (h > max_size) {
//                                        w *= max_size / h;
//                                        h = max_size;
//                                        alert('File Size Is too Big');
//                                        $('#result_'+manage).html("Please Enter Correct Image Format");
//                                        $('#student_'+manage).val("");
//                                    }
//                                }
//                                var canvas = document.createElement('canvas');
//                                canvas.width = w;
//                                canvas.height = h;
//                                canvas.getContext('2d').drawImage(image, 0, 0, w, h);
//                                if (file.type == "image/jpeg") {
//                                    var dataURL = canvas.toDataURL("image/jpeg", 1.0);
//                                } else if (file.type == "image/png") {
//                                    var dataURL = canvas.toDataURL("image/png");
//                                } else if (file.type == "image/gif") {
//                                    var dataURL = canvas.toDataURL("image/gif");
//                                }
//
//                                document.getElementById('input_'+manage).value = dataURL;
                                //document.getElementsByClassName("student_passport_photo").value = dataURL;
                            }
                            image.src = readerEvent.target.result;
                        }
                        reader.readAsDataURL(file);
                    }
                }
            }
          function fileChange_document(e,manage) {

                //document.getElementsByClassName("student_passport_photo").value = '';
                $('#result_'+manage).html("");
                //document.getElementById('input_'+manage).value = '';

                for (var i = 0; i < e.target.files.length; i++) {

                    var file = e.target.files[i];

                    if (file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/gif") {

                        var reader = new FileReader();
                        reader.onload = function (readerEvent) {
                            var image = new Image();
                            image.onload = function (imageEvent) {
                                //var max_size = 275;



                                    var max_size = 2000;
                                    var allow_w = 1000;
                                    var allow_h = 1000;



                                var file_size = document.getElementById('student_'+manage).files[0].size; // In Bytes
                                var s = file_size / 1024; // In Kilobytes
                                var w = image.width;
                                var h = image.height;

                                //alert('size : '+s+' Kb width : '+w+' px height : '+h+' px');

                                if(s > max_size) {
                                    alert('File Size Is too Big');
                                    $('#result_'+manage).html("Please Choose A File Of Given Size");
                                    $('#student_'+manage).val("");
                                }
                                else if (w > allow_w || h > allow_h) {
                                    alert('File Dimensions Do Not Match');
                                    $('#result_'+manage).html("Please Choose A File Of Given Dimensions");
                                    $('#student_'+manage).val("");
                                }


                            }
                            image.src = readerEvent.target.result;
                        }
                        reader.readAsDataURL(file);
                    }
                }
            }

       //      function fileChange_passport(e) {
       //     //document.getElementsByClassName("student_passport_photo").value = '';
       //     $('.result_html').html("");
       //     document.getElementById('photo_passport').value = '';
       //     for (var i = 0; i < e.target.files.length; i++) {

       //         var file = e.target.files[i];

       //         if (file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/gif") {

       //             var reader = new FileReader();
       //             reader.onload = function (readerEvent) {
       //                 var image = new Image();
       //                 image.onload = function (imageEvent) {
       //                     //var max_size = 275;
       //                     var max_size = 900;
       //                     var allow_w = 300;
       //                     var allow_h = 400;
       //                     var w = image.width;
       //                     var h = image.height;
       //                     if (w > allow_w) {
       //                      alert('File Size Is too Big');
       //                             $('.result_html').html("please Enter Correct Image Format");
       //                             $(".student_passport_photo").val("");
       //                     }
       //                     else if (h > allow_h) {
       //                      alert('File Size Is too Big');
       //                             $('.result_html').html("please Enter Correct Image Format");
       //                             $(".student_passport_photo").val("");
       //                     }

       //                     else if (w > h) {
       //                         if (w > max_size) {
       //                             h *= max_size / w;
       //                             w = max_size;
       //                             alert('File Size Is too Big');
       //                             $('.result_html').html("please Enter Correct Image Format");
       //                             $(".student_passport_photo").val("");
       //                         }
       //                     } else {
       //                         if (h > max_size) {
       //                             w *= max_size / h;
       //                             h = max_size;
       //                             alert('File Size Is too Big');
       //                             $('.result_html').html("please Enter Correct Image Format");
       //                             $(".student_passport_photo").val("");
       //                         }
       //                     }
       //                     var canvas = document.createElement('canvas');
       //                     canvas.width = w;
       //                     canvas.height = h;
       //                     canvas.getContext('2d').drawImage(image, 0, 0, w, h);
       //                     if (file.type == "image/jpeg") {
       //                         var dataURL = canvas.toDataURL("image/jpeg", 1.0);
       //                     } else if (file.type == "image/png") {
       //                         var dataURL = canvas.toDataURL("image/png");
       //                     } else if (file.type == "image/gif") {
       //                         var dataURL = canvas.toDataURL("image/gif");
       //                     }

       //                     document.getElementById('photo_passport').value = dataURL;
       //                     //document.getElementsByClassName("student_passport_photo").value = dataURL;
       //                 }
       //                 image.src = readerEvent.target.result;
       //             }
       //             reader.readAsDataURL(file);
       //         }
       //     }
       // }
       // function fileChange_guardian_sign(e) {
       //     //document.getElementsByClassName("student_passport_photo").value = '';
       //     $('#result_guardian_sign').html("");
       //     document.getElementById('input_guardian_sign').value = '';
       //     for (var i = 0; i < e.target.files.length; i++) {

       //         var file = e.target.files[i];

       //         if (file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/gif") {

       //             var reader = new FileReader();
       //             reader.onload = function (readerEvent) {
       //                 var image = new Image();
       //                 image.onload = function (imageEvent) {
       //                     //var max_size = 275;
       //                     var max_size = 900;
       //                     var allow_w = 300;
       //                     var allow_h = 350;
       //                     var w = image.width;
       //                     var h = image.height;
       //                     if (w > allow_w) {
       //                      alert('File Size Is too Big');
       //                             $('#result_guardian_sign').html("please Enter Correct Image Format");
       //                             $("#guardian_signature").val("");
       //                     }
       //                     else if (h > allow_h) {
       //                      alert('File Size Is too Big');
       //                             $('#result_guardian_sign').html("please Enter Correct Image Format");
       //                             $("#guardian_signature").val("");
       //                     }

       //                     else if (w > h) {
       //                         if (w > max_size) {
       //                             h *= max_size / w;
       //                             w = max_size;
       //                             alert('File Size Is too Big');
       //                             $('#result_guardian_sign').html("please Enter Correct Image Format");
       //                             $("#guardian_signature").val("");
       //                         }
       //                     } else {
       //                         if (h > max_size) {
       //                             w *= max_size / h;
       //                             h = max_size;
       //                             alert('File Size Is too Big');
       //                             $('#result_guardian_sign').html("please Enter Correct Image Format");
       //                             $("#guardian_signature").val("");
       //                         }
       //                     }
       //                     var canvas = document.createElement('canvas');
       //                     canvas.width = w;
       //                     canvas.height = h;
       //                     canvas.getContext('2d').drawImage(image, 0, 0, w, h);
       //                     if (file.type == "image/jpeg") {
       //                         var dataURL = canvas.toDataURL("image/jpeg", 1.0);
       //                     } else if (file.type == "image/png") {
       //                         var dataURL = canvas.toDataURL("image/png");
       //                     } else if (file.type == "image/gif") {
       //                         var dataURL = canvas.toDataURL("image/gif");
       //                     }

       //                     document.getElementById('input_guardian_sign').value = dataURL;
       //                     //document.getElementsByClassName("student_passport_photo").value = dataURL;
       //                 }
       //                 image.src = readerEvent.target.result;
       //             }
       //             reader.readAsDataURL(file);
       //         }
       //     }
       // }




        </script>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Update Student Details</h2>
                 <a href="<?php echo base_url().'admin/college/issueAdmitCard/'.$this->uri->segment(4); ?>" title="Issue Admit Card"  class="c-link">Issue Admit Card</a>
                 <?php
                 $encrypted_id = $this->uri->segment(4);
                 $student_id = $this->ablfunctions->ablDecrypt($encrypted_id);
                 if ($details['admit_card_type']!='0') {
                 ?>
                   <a href="<?php echo base_url().'Admit_card/downloadAdmitCard/'.$this->uri->segment(4); ?>" title="Issue Admit Card" target="_blank" class="c-link">Download Admit Card</a>
                  <?php
                  } ?>

                <a href="<?php echo base_url().'admin/college/manualFeesBreakups/'.$this->uri->segment(4); ?>" class="btn btn-success min-width-125">Set Manual Fees<a>


                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open_multipart('', array('id' => 'formEditAdmin')); ?>



                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>Course Applied For</label>
                                            <select name="course_id" id="course_id">
                                                <option value="">Select Course </option>
                                                <?php foreach ($all_course as $row) { ?>
                                                    <option value="<?php echo $row['id']; ?>" <?php if ($row['id']==$details['course_id']){ echo "selected";} ?>>
                                                        <?php echo $row['course_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            <label>Form No: <?php echo $details['from_id']; ?></label>
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label><?php echo $this->my_custom_functions->get_particular_field_value("tbl_session","session_name", 'and id="'.$details['session_id'].'"'); ?></label>
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Name of the Candidate</label>
                                            <input type="text" name="student_name" id="student_name" value="<?php echo $details['student_name']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Father’s /Mother’s Name</label>
                                            <input type="text" name="father_or_mother_name" id="father_or_mother_name" value="<?php echo $details['father_or_mother_name']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Name of the Guardian</label>
                                            <input type="text" name="guardian_name" id="guardian_name" value="<?php echo $details['guardian_name']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Permanent Address</label>
                                            <input type="text" name="perm_address" id="perm_address" value="<?php echo $details['perm_address']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Email</label>
                                            <input type="email" name="email" id="email" value="<?php echo $details['email']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Phone Number</label>
                                            <input type="number" name="phone_no" id="phone_no" value="<?php echo $details['phone_no']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Address for correspondence</label>
                                            <input type="text" name="corres_address" id="corres_address" value="<?php echo $details['corres_address']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Aadhar No</label>
                                            <input type="number" name="aadhar_no" id="aadhar_no" value="<?php echo $details['aadhar_no']; ?>" required="">
                                        </span>
                                    </li>

                                     <li>
                                        <span>
                                            <label>Registered Mobile No</label>
                                            <input type="number" name="reg_mobile_no" id="reg_mobile_no" value="<?php echo $details['reg_mobile_no']; ?>" required="">
                                        </span>
                                    </li>



                                    <li>
                                        <span>
                                            <label>D.O.B</label>
                                            <input type="text" name="dob" id="dob" value="<?php echo $details['dob']; ?>" required="">
                                        </span>
                                    </li>


                                     <li>
                                        <span>
                                            <label>Passport Photo</label>
                                            <?php $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$student_id));
                                            $path=base_url().UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$document_details['passport'];
                                                $path_thu=base_url().UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$document_details['passport'];
                                            ?>
                                             <?php if (file_exists(UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$document_details['passport']) AND file_exists(UPLOAD_DIR.STUDENT_PASSPORT_PHOTO_THUMB.$document_details['passport']) AND ($document_details['passport']!='')) {
                                              ?>
                                            <div class="album_cover">
                                            <a href="<?php echo $path;?>" target="_blank">
                                            <!-- <a href="<?php echo $path;?>" data-fancybox="images" data-fancybox="group"> -->
                                            <image src="<?php echo $path_thu.'?t='.time();?>" style="height:100px;width: 100px;">
                                            </a>
                                            <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deletePassportImage/'.$student_id; ?>');" title="Delete Image">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </div>
                                            <?php } else {echo "<br>No Image Found"; } ?>
                                            <input type="file" name="student_passport_photo"  class="student_passport_photo"
                                            id="student_passport"  accept="image/*" onchange="fileChange_student(event,'passport');">
                                            <!--  <input type="hidden" name="photo_passport" id="photo_passport" class="photo_passport" value=""> -->
                                             <p id="result_passport"></p>
                                            <p>Height 4.5cm X Width 3.5cm Max file size - 500KB</p>


                                        </span>
                                    </li>

                                     <li>
                                        <span>
                                            <label>Age
                                    </label>
                                            <input type="number" name="age" id="age" value="<?php echo $details['age']; ?>" required="">
                                        </span>
                                    </li>
                                    <li>
                                       <span>
                                           <label>Annual income of the Guardian</label>
                                           <input type="number" name="gud_annual_income" value="<?php echo $details['gud_annual_income']; ?>" required="">
                                       </span>
                                   </li>

                                     <li>
                                        <span>
                                            <label>Select
                                        Sex</label>
                                            <select name="gender" id="gender" >
                                        <option value="">Select
                                        Sex</option>
                                                <?php
                                                $gender = $this->config->item('gender');
                                                foreach($gender as $key=>$gtype){?>
                                                <option value="<?php echo $key;?>" <?php if ($key==$details['gender']){ echo "selected";} ?>><?php echo $gtype;?></option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>


                                    <li>
                                        <span>
                                            <label>Marital Status</label>
                                            <select name="marital_status" id="marital_status" >
                                        <option value="">Select
                                        Marital Status</option>
                                                <?php
                                                $marital_status = $this->config->item('marital_status');
                                                foreach($marital_status as $key=>$mtype){?>
                                                <option value="<?php echo $key;?>" <?php if ($key==$details['marital_status']){ echo "selected";} ?>><?php echo $mtype;?></option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Nationality
                                    </label>
                                            <input type="text" name="nationality" id="nationality" value="<?php echo $details['nationality']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Select
                                        Caste</label>
                                            <select name="caste" id="caste" >
                                        <option value="">Select
                                        Sex</option>
                                                <?php
                                                $caste = $this->config->item('caste');
                                                foreach($caste as $key=>$gtype){?>
                                                <option value="<?php echo $key;?>" <?php if ($key==$details['caste']){ echo "selected";} ?>><?php echo $gtype;?></option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>



                    <li class="full_width_li">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Examination passed</th>
                                    <th>Subject</th>
                                    <th>Year of passing</th>
                                    <th>Board/Council/ University</th>
                                    <th>Division obtained</th>
                                    <th>Document</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($education_details as $row) { ?>
                                <tr>
                                    <td><input type="text"  name="exam_pass_<?php echo $row['id']; ?>" class="validate[required]" value="<?php echo $row['exam_passed']; ?>"></td>
                                    <td><input type="text" name="subject_<?php echo $row['id']; ?>" class="validate[required]" value="<?php echo $row['subject']; ?>"></td>
                                    <td><input type="text" name="year_pass_<?php echo $row['id']; ?>"  class="validate[required]" value="<?php echo $row['year_of_pass']; ?>"></td>
                                    <td><input type="text" name="board_<?php echo $row['id']; ?>"  class="validate[required]" value="<?php echo $row['board_coun_uni']; ?>"></td>
                                    <td><input type="text" name="div_obt_<?php echo $row['id']; ?>"  class="validate[required]" value="<?php echo $row['div_obt']; ?>"></td>
                                    <td>
                                    <?php
                                    $document_url_array = explode(".", $row['document_url']);
                                    $document_url_extension = $document_url_array[count($document_url_array) - 1];
                                    $path=base_url().UPLOAD_DIR.STUDENT_MARKSHEET.$row['document_url'];
                                            ?>
                                            <?php if (file_exists(UPLOAD_DIR.STUDENT_MARKSHEET.$row['document_url']) AND ($row['document_url']!='')) {
                                              $display_path = base_url() . 'images/pdf.png';
                                              ?>
                                    <div class="album_cover">
                                             <a href="<?php echo $path;?>" target="_blank">
                                               <?php if ($document_url_extension=="pdf") {
                                               ?>
                                                 <image src="<?php echo $display_path.'?t='.time();?>" style="height:50px;width: 50px;">
                                               <?php }else{
                                                 ?>
                                                   <image src="<?php echo $path.'?t='.time();?>" style="height:50px;width: 50px;">
                                               <?php }?>
                                            </a>
                                            <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteMarkSheetImage/'.$row['document_url'].'/'.$row['student_from_id'].'/'.$row['id']; ?>');" title="Delete Image">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                       </div>
                                       <?php } else {echo "<br>No Image Found"; } ?>
                                        <input type="file" name="document_<?php echo $row['id']; ?>"
                                        id="student_<?php echo $row['id']; ?>" class=""  accept="image/*"
                                        <?php $id_one='input_document_'.$row['id'].''; ?> onchange="fileChange_document(event,<?php echo $row['id']; ?>)">

                                             <!-- <input type="hidden" name="input_document_<?php echo $row['id']; ?>" id="input_document_<?php echo $row['id']; ?>" class="input_document_<?php echo $row['id']; ?>" value=""> -->
                                             <p id="result_<?php echo $row['id']; ?>"></p></td></td>
                                    <input type="hidden" name="table_id"  value="<?php echo $row['id']; ?>">
                                </tr>

                            <?php } ?>


                            </tbody>
                        </table>


                                 </li>

                                 <!-- <li>
                                        <span>
                                            <label>Original copies of mark sheets</label>
                                            <?php $path=base_url().UPLOAD_DIR.STUDENT_MARKSHEET.$student_id.".jpg";
                                            ?>
                                            <div class="album_cover">
                                             <a href="<?php echo $path;?>" target="_blank">
                                            <image src="<?php echo $path;?>" style="height:100px;width: 100px;">
                                            </a>
                                            </div>



                                        </span>
                                    </li> -->

                                    <li>
                                        <span>
                                            <label>Original copies of age verification</label>
                                            <?php
                                            $filename_array = explode(".", $document_details['age_photo']);
                                            $age_photo_extension = $filename_array[count($filename_array) - 1];

                                            $path=base_url().UPLOAD_DIR.STUDENT_AGE_PROOF.$document_details['age_photo'];
                                            $path_thu=base_url().UPLOAD_DIR.STUDENT_AGE_PROOF_THUMB.$document_details['age_photo'];
                                            ?>
                                            <?php if (file_exists(UPLOAD_DIR.STUDENT_AGE_PROOF.$document_details['age_photo']) AND ($document_details['age_photo']!='')) {
                                              ?>
                                            <div class="album_cover">
                                            <a href="<?php echo $path;?>" target="_blank">
                                            <?php if ($age_photo_extension=="pdf") {
                                            ?>
                                              <image src="<?php echo $display_path.'?t='.time();?>" style="height:50px;width: 50px;">
                                            <?php }else{
                                              ?>
                                                <image src="<?php echo $path.'?t='.time();?>" style="height:50px;width: 50px;">
                                            <?php }?>

                                            </a>
                                            <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteAgeproofImage/'.$student_id; ?>');" title="Delete Image">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </div>
                                            <?php } else {echo "<br>No Image Found"; } ?>
                                            <input type="file" name="age_verification_xerox" id="student_age_verification" class=""  accept="image/*" onchange="fileChange_student(event,'age_verification');">
                                             <!-- <input type="hidden" name="input_age_verification" id="input_age_verification" class="input_age_verification" value=""> -->
                                             <p id="result_age_verification"></p>
                                            <p>Max file size - 1MB</p>



                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Original copies of SC/ST/OBC/Handicapped Certificate</label>
                                            <?php
                                            $caste_photo_array = explode(".", $document_details['caste_photo']);
                                            $caste_photo_extension = $caste_photo_array[count($caste_photo_array) - 1];
                                            $path=base_url().UPLOAD_DIR.STUDENT_CASTE.$document_details['caste_photo'];
                                            $path_thu=base_url().UPLOAD_DIR.STUDENT_CASTE_THUMB.$document_details['caste_photo'];
                                            ?>
                                            <?php if (file_exists(UPLOAD_DIR.STUDENT_CASTE.$document_details['caste_photo']) AND ($document_details['caste_photo']!='')) {
                                              ?>
                                            <div class="album_cover">
                                            <a href="<?php echo $path;?>" target="_blank">
                                              <?php if ($caste_photo_extension=="pdf") {
                                              ?>
                                                <image src="<?php echo $display_path.'?t='.time();?>" style="height:50px;width: 50px;">
                                              <?php }else{
                                                ?>
                                                  <image src="<?php echo $path.'?t='.time();?>" style="height:50px;width: 50px;">
                                              <?php }?>
                                            </a>
                                            <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteCasteImage/'.$student_id; ?>');" title="Delete Image">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </div>
                                            <?php } else {echo "<br>No Image Found"; } ?>
                                            <input type="file" name="caste_certificate_xerox"
                                            id="student_caste_cf" class=""  accept="image/*" onchange="fileChange_student(event,'caste_cf');">
                                             <!-- <input type="hidden" name="result_caste_cf" id="result_caste_cf" class="input_caste_cf" value=""> -->
                                            <p id="result_caste_cf"></p>
                                            <p>Max file size - 1MB</p>


                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Countersignature of the Guardian</label>
                                            <?php $path=base_url().UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$document_details['guardian_sign'];
                                            $path_thu=base_url().UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$document_details['guardian_sign'];
                                            ?>
                                            <?php if (file_exists(UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$document_details['guardian_sign']) AND file_exists(UPLOAD_DIR.STUDENT_GURDAIN_SIGN_THUMB.$document_details['guardian_sign']) AND ($document_details['guardian_sign']!='')) {
                                              ?>
                                            <div class="album_cover">
                                            <a href="<?php echo $path;?>" target="_blank">
                                            <image src="<?php echo $path_thu.'?t='.time();?>" style="height:70px;width: 200px;">
                                            </a>
                                            <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteGdsignImage/'.$student_id; ?>');" title="Delete Image">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </div>
                                            <?php } else {echo "<br>No Image Found"; } ?>
                                            <input type="file" name="guardian_signature" id="student_guardian_sign" class=""accept="image/*"onchange="fileChange_student(event,'guardian_sign');">
                                             <!-- <input type="hidden" name="input_guardian_sign" id="input_guardian_sign" class="input_guardian_sign" value=""> -->
                                             <p id="result_guardian_sign"></p>
                                            <p>Height 1.5cm X Width 3.5cm Max file size - 200KB</p>


                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Signature of Candidate</label>
                                            <?php $path=base_url().UPLOAD_DIR.STUDENT_SIGN.$document_details['student_sign'];
                                            $path_thu=base_url().UPLOAD_DIR.STUDENT_SIGN_THUMB.$document_details['student_sign'];
                                            ?>
                                            <?php if (file_exists(UPLOAD_DIR.STUDENT_SIGN.$document_details['student_sign']) AND file_exists(UPLOAD_DIR.STUDENT_SIGN_THUMB.$document_details['student_sign']) AND ($document_details['student_sign']!='')) {
                                              ?>
                                            <div class="album_cover">
                                            <a href="<?php echo $path;?>" target="_blank">
                                            <image src="<?php echo $path_thu.'?t='.time();?>" style="height:70px;width: 200px;">
                                            </a>
                                            <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteStsignImage/'.$student_id; ?>');" title="Delete Image">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </div>
                                            <?php } else {echo "<br>No Image Found"; } ?>
                                            <input type="file" name="candidate_signature" id="student_candidate_sign" class="" accept="image/*" onchange="fileChange_student(event,'candidate_sign');">
                                             <!-- <input type="hidden" name="input_candidate_sign" id="input_candidate_sign" class="input_candidate_sign" value=""> -->
                                             <p id="result_candidate_sign"></p><p>Height 1.5cm X Width 3.5cm Max file size - 200KB</p>


                                        </span>
                                    </li>

                                     <li>
                                        <span>
                                            <label>Signature of Candidate in full</label>
                                            <?php $path=base_url().UPLOAD_DIR.STUDENT_SIGN_FULL.$document_details['student_sign_full'];
                                            $path_thu=base_url().UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$document_details['student_sign_full'];
                                            ?>
                                            <?php if (file_exists(UPLOAD_DIR.STUDENT_SIGN_FULL.$document_details['student_sign_full']) AND file_exists(UPLOAD_DIR.STUDENT_SIGN_FULL_THUMB.$document_details['student_sign_full']) AND ($document_details['student_sign_full']!='')) {
                                              ?>
                                            <div class="album_cover">
                                            <a href="<?php echo $path;?>" target="_blank">
                                            <image src="<?php echo $path_thu.'?t='.time();?>" style="height:70px;width: 200px;">
                                            </a>
                                            <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteStfsignImage/'.$student_id; ?>');" title="Delete Image">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </div>
                                            <?php } else {echo "<br>No Image Found"; } ?>
                                            <input type="file" name="candidate_signature_full" id="student_candidate_sign_full" class="" accept="image/*" onchange="fileChange_student(event,'candidate_sign_full');">
                                             <!-- <input type="hidden" name="input_candidate_sign_full" id="input_candidate_sign_full" class="input_candidate_sign_full" value=""> -->
                                             <p id="result_candidate_sign_full"></p>
                                            <p>Height 1.5cm X Width 3.5cm Max file size - 200KB</p>


                                        </span>
                                    </li>

                                 <li>
                                    <span>
                                        <label>Local address of the candidate
                                    </label>
                                            <input type="text" name="local_gu_name_adds" id="local_gu_name_adds" value="<?php echo $details['local_gu_name_adds']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                    <span>
                                        <label>Name & Address of the local guardian
                                    </label>
                                            <input type="text" name="local_address" id="local_address" value="<?php echo $details['local_address']; ?>" required="">
                                        </span>
                                    </li>





                                    <li class="full_width_li">
                                        <span>
                                            <label>Select
                                        Status</label>
                                            <select name="status" id="status" >
                                        <option value="">Select
                                        Status</option>
                                                <?php
                                                $status = $this->config->item('form_status');
                                                foreach($status as $key=>$stype){?>
                                                <option value="<?php echo $key;?>" <?php if ($key==$details['status']){ echo "selected";} ?>><?php echo $stype;?></option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <input type="hidden" name="student_id" value="<?php echo $this->uri->segment(4); ?>">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Update">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
