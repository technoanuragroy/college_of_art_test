<?php $this->load->view("admin/include/header"); ?>

<script>
    $(document).ready(function() {
        //$("#form1").validate();
        $(".TabDisplayRecords").tablesorter();
        $('a.printme').click(function() {
            window.print();
        });
        ////////////////////////////////////////////////////////////////
        $("#emp_name").autocomplete({
            source: "<?php echo base_url(); ?>admin/college/ajax_get_emps",
            minLength: 1
        });
        $('#from_date').datepicker(
                {changeMonth: true,
                    changeYear: true,
                    beforeShowDay: function(date) {
                        //getDate() returns the day (0-31)
                        if (date.getDate() == 1) {
                            return [true, ''];
                        }
                        return [false, ''];
                    },dateFormat: 'yy-mm-dd'
                }
        );
        $('#to_date').datepicker(
                {
                    changeMonth: true,
                    changeYear: true,
                    beforeShowDay: function(date) {
                        //getDate() returns the day (0-31)
                        if (date.getDate() == 1) {
                            return [true, ''];
                        }
                        return [false, ''];
                    },dateFormat: 'yy-mm-dd'
                }
        );
    });
    function getLastDayOfYearAndMonth(year, month)
    {
        return(new Date((new Date(year, month + 1, 1)) - 1)).getDate();
    }
    $(document).ready(function() {

        function exportTableToCSV($table, filename) {

            var $rows = $table.find('tr:has(td)'),
                    // Temporary delimiter characters unlikely to be typed by keyboard
                    // This is to avoid accidentally splitting the actual contents
                    tmpColDelim = String.fromCharCode(11), // vertical tab character
                    tmpRowDelim = String.fromCharCode(0), // null character

                    // actual delimiter characters for CSV format
                    colDelim = '","',
                    rowDelim = '"\r\n"',
                    // Grab text from table into CSV formatted string
                    csv = '"' + $rows.map(function(i, row) {
                        var $row = $(row),
                                $cols = $row.find('td');

                        return $cols.map(function(j, col) {
                            var $col = $(col),
                                    text = $col.text();

                            return text.replace(/"/g, '""'); // escape double quotes

                        }).get().join(tmpColDelim);

                    }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"',
                    // Data URI
                    csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
                    .attr({
                        'download': filename,
                        'href': csvData,
                        'target': '_blank'
                    });
        }

        // This must be a hyperlink
        $(".export").click(function() {
            // CSV
            exportTableToCSV.apply(this, [$('#dvData>table'), 'payslip_report.xls']);

            // IF CSV, don't do event.preventDefault() or return false
            // We actually need this to be a typical hyperlink
        });
    });

</script>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">
          <style>
          @media print {
              .noprint { display: none; }
          }
          </style>

                      <h1>Attendance Report <?php if (isset($emp_name)) { ?> of <?php echo $emp_name; ?> from <?php echo $from_date; ?> to <?php echo $to_date;
                              }
                              ?></h1>
                              <div id="body">

                                <ul class="input_listing">
                                  <div class="noprint">
                              <?php echo form_open('', array('id' => 'form1')); ?>
                                      <li class="full_width_li">
                                      <p>Emp Name &nbsp;<?php echo form_input(array('name' => 'emp_name', 'id' => 'emp_name'), set_value('emp_name')); ?>
                                      </li>
                                      <li class="" colspan="3">
                                      &nbsp; From Date &nbsp;<?php echo form_input(array('name' => 'from_date', 'id' => 'from_date', 'value' => '', 'class' => 'required')); ?>
                                      </li>
                                      <li class="" colspan="3">
                                      &nbsp; To Date &nbsp;<?php echo form_input(array('name' => 'to_date', 'id' => 'to_date', 'value' => '', 'class' => 'required')); ?>
                                      </li>
                                      <li class="full_width_li">
                                      <?php echo form_input(array('type' => 'submit', 'name' => 'submit', 'value' => 'Search','class'=>'submit_btn')); ?></p>
                                      </li>
                              <?php echo form_close(); ?>
                                  </div>
                                  <div>
                              <?php if (isset($attend) AND count($attend)!=0) { ?>

                                <div class="table-responsive">
                                    <table class="table table-default table-bordered table-striped table-hover">
                                        <thead>
                                                  <tr>

                                                      <th align="left">Employee</th>
                                                       <th align="left">Entered Date</th>
                                                      <th align="left">Present</th>
                                                      <th align="left">Absent</th>
                                                      <th align="left">PL</th>
                                                      <th align="left">CL</th>
                                                      <th align="left">SL</th>
                                                      <th align="left">OPL</th>
                                                      <td align="center" class="noprint"><b>Action</b></td>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <?php
                                                  foreach ($attend as $row) :
                                                      ?>
                                                      <tr>

                                                        <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$row->emp_id.'"'); ?></td>
                                                        <td><?php echo $row->date_month;?></td>
                                                        <td><?php echo $row->present;?></td>
                                                        <td><?php echo $row->absent;?></td>
                                                        <td ><?php echo $row->pl;?></td>
                                                        <td><?php echo $row->cl; ?></td>
                                                        <td><?php echo $row->sl; ?></td>
                                                        <td><?php echo $row->opl; ?></td>

                                                        <td align="center"><?php echo anchor("admin/college/edit_attendance/" . $row->emp_id . "/" . $row->id, "Edit"); ?> |
                                                          <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteAttendence/' .$row->id;?>');"
                                                             title="Delete Record">
                                                              Delete
                                                          </a></td>
                                                      </tr>

                                  <?php endforeach; ?>
                                              </tbody>


                                            </table>
                                            </div>


                                      </div>
                                      <?php
                                  }else {
                                  echo "No Records Found";
                                  }
                                  ?>

                              </ul>
                              </div>
                              </div>

                    </div>
                </div>
            </div>
      </div>
     </div>




<?php $this->load->view("admin/include/footer"); ?>
