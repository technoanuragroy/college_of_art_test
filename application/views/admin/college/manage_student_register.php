<?php $this->load->view("admin/include/header"); ?>

<style>
    #div_slideDown_search{
        display: none;
    }
</style>


<?php if (isset($session_active)) {
   if ($session_active == '1') {
   ?>
   <script type="text/javascript">

       $("#clickbtn").show();

   </script>

   <?php
    }

    } else{
   ?>
   <style>
   #clickbtn{
       display: none;
   }
   </style>
   <?php

    } ?>
<script type="text/javascript">

    jQuery(document).ready(function ($) {
// executes when HTML-Document is loaded and DOM is ready
       $("#clickbtn").click(function () {
           $("#div_slideDown_search").slideToggle('fast');
       });

   });

</script>

   <div class="completeWrap_inner">
       <div class="completeWrap_container">
           <div class="completeWrap_inner_main">
               <h2 class="heading_02">Manage Student Admissions</h2>


               <div class="table_wrap">

                   <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                   <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                   <a href="javascript:" class="c-link" id="clickbtn" >Status Search </a>

                   <div class="form_wrap" id="div_slideDown_search">

                   <div class="row">

                       <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                               <ul class="input_listing">


                                   <li class="full_width_li">
                                       <span>

                                           <select name="status" id="status" >
                                       <option value="">Select
                                       Status</option>
                                               <?php
                                               $status = $this->config->item('form_status');
                                               foreach($status as $key=>$stype){?>
                                               <option value="<?php echo $key;?>" ><?php echo $stype;?></option>
                                               <?php } ?>
                                           </select>
                                       </span>
                                   </li>

                                   <li class="full_width_li">
                                       <input type="submit" name="search" id="search" class="submit_btn" value="Status Search">
                                   </li>
                                   <input type="hidden" name="current_session_id" id="current_session_id" value="<?php echo $post_data['session_id']; ?>">
                               </ul>

                       <?php echo form_close(); ?>

                   </div>
               </div>


                <div class="form_wrap" id="div_slideDown">

                   <div class="row">

                       <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                               <ul class="input_listing">


                                   <li class="full_width_li">
                                       <span>
                                           <!-- <label>Status</label> -->
                                           <select id="session" name="session">
                                               <option value="">Select Session</option>
                               <?php
                               $selected = '';
                               foreach ($all_session as $session) {
                                   if ($post_data['session_id'] != '') {
                                       if ($post_data['session_id'] == $session['id']) {

                                           $selected = 'selected="selected"';
                                       } else {

                                           $selected = '';
                                       }
                                   }
                                   ?>
                                   <option value="<?php echo $session['id']; ?>" <?php echo $selected; ?>><?php echo $session['session_name']; ?></option>
                               <?php } ?>
                                           </select>
                                       </span>
                                   </li>

                                   <li class="full_width_li">
                                       <input type="submit" name="submit" id="submit" class="submit_btn" value="Search">
                                       <a href="<?php echo base_url(); ?>admin/college/addStudentManual" class="btn btn-info min-width-125">Add Student</a>
                                         <a href="<?php echo base_url().'admin/enrollment/enrollStudent'; ?>" class="btn btn-success min-width-125">Enroll Student<a>
                                   </li>
                               </ul>

                       <?php echo form_close(); ?>

                   </div>
               </div>


                   <?php
                   if(!empty($all_student)) {
                   ?>
                   <div class="table-responsive">
                       <table class="table table-default table-bordered table-striped table-hover">
                           <thead>
                               <tr>
                                   <th>Student Name</th>
                                   <th>Course Name</th>
                                   <th>Status</th>
                                   <!-- <th>Issue Admit Card</th> -->
                                   <th>Edit</th>
                               </tr>
                           </thead>
                           <tbody>
                           <?php
                               if(!empty($all_student)) {
                                   foreach($all_student as $page) {
                                    $student_id=$this->ablfunctions->ablEncrypt($page['id']);
                           ?>
                                       <tr>
                                           <td><?php echo $page['student_name']; ?></td>
                                           <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_courses","course_name", 'and id="'.$page['course_id'].'"'); ?>

                                           </td>
                                           <td><?php
                                                 if ($page['status'] == 1) {
                                                     echo "<span class='badge badge-success'>Need Review</span>";
                                                 } else if ($page['status'] == 2) {
                                                     echo "<span class='badge badge-success'>Review Pending</span>";
                                                 }else if ($page['status'] == 3) {
                                                     echo "<span class='badge badge-success'>Admit (Test) Issued</span>";
                                                 }else if ($page['status'] == 4) {
                                                     echo "<span class='badge badge-success'>Admit (Admission) Issued</span>";
                                                 }else if ($page['status'] == 5) {
                                                     echo "<span class='badge badge-success'>Payment Link Activated</span>";
                                                 }else if ($page['status'] == 6) {
                                                     echo "<span class='badge badge-success'>Admission Fees received</span>";
                                                 }else if ($page['status'] == 7) {
                                                     echo "<span class='badge badge-success'>Enrolled</span>";
                                                 }
                                                 ?>
                                           </td>
                                           <!-- <td>
                                            <a href="<?php echo base_url().'admin/college/downloadAdmitCard/'.$student_id; ?>" title="Issue Admit Card" target="_blank">
                                           <span class='badge badge-primary'>Admit Card</span>
                                           </a>
                                           </td> -->
                                           <td>
                                               <a href="<?php echo base_url().'admin/college/editStudentRegister/'.$student_id; ?>" title="Edit Record">
                                                   <i class="fa fa-edit"></i>
                                               </a>
                                           </td>


                                       </tr>
                           <?php
                                   }
                               }
                           ?>
                           </tbody>
                       </table>

                       <p  class="noprint">
                           <!-- <a href="#" class="btn btn-success min-width-125 export" >Export Table data into Excel</a> -->
                           <a href="<?php echo base_url().'admin/college/downloadStudentRecords/'.$secarch_name.'/'.$select_id; ?>" class="btn btn-success min-width-125" target="_blank">Export data into Excel</a>

                   </p>
                   <?php
                   }else {
                   echo "No Register Student found";

                   }
                   ?>
                   </div>



               </div>
           </div>
       </div>

<?php  if (!empty($all_student)) { ?>
    <div id="dvData" style="display: none;border:3px solid black;"  class="noprint">
        <table>
              <tr>
                <td>Sl No:</td>
                <td>Student Name:</td>
                     <td>Form No:</td>
                    <td>Course Name:</td>
                    <td>Session Name:</td>
                    <td>Guardian Name:</td>
                    <td>Permanent Address:</td>
                    <td>Email:</td>
                    <td>Phone Number:</td>
                    <td>Address:</td>
                    <td>Aadhar No:</td>
                    <td>Registered Mobile No:</td>
                    <td>D.O.B:</td>
                    <td>Passport Photo:</td>
                    <td>Age:</td>
                    <td>Sex:</td>
                    <td>Marital Status:</td>
                    <td>Nationality:</td>
                    <td>Caste:</td>
                    <td>Age Verification:</td>
                    <td>Caste Certificate:</td>
                    <td>Signature of the Guardian:</td>
                    <td>Signature of Candidate:</td>
                    <td>Signature of Candidate Full:</td>
                    <td>Local address of candidate:</td>
                    <td>Name & Address of local guardian:</td>
                </tr>

                <tr>

                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <<td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                  </tr>


           <?php
           $sl=1;
    foreach ($all_student as $row) :
      $document_details = $this->my_custom_functions->get_details_from_id("", "tbl_student_from_document", array("student_id" =>$row['id']));
      $passport_path=base_url().UPLOAD_DIR.STUDENT_PASSPORT_PHOTO.$document_details['passport'];
      $age_path=base_url().UPLOAD_DIR.STUDENT_AGE_PROOF.$document_details['age_photo'];
      $caste_path=base_url().UPLOAD_DIR.STUDENT_AGE_PROOF.$document_details['caste_photo'];
      $guardian_sign_path=base_url().UPLOAD_DIR.STUDENT_GURDAIN_SIGN.$document_details['guardian_sign'];
      $student_sign_path=base_url().UPLOAD_DIR.STUDENT_SIGN.$document_details['student_sign'];
      $student_sign_full_path=base_url().UPLOAD_DIR.STUDENT_SIGN_FULL.$document_details['student_sign_full'];
        ?>
        <tr>
          <td align="left"><?php echo $sl; ?></td>
          <td align="left"><?php echo $row['student_name']; ?></td>
          <td><?php echo $row['from_id']; ?></td>
          <td align="left"><?php  echo $this->my_custom_functions->get_particular_field_value("tbl_courses","course_name", 'and id="'.$row['course_id'].'"');?></td>
          <td align="left"><?php  echo $this->my_custom_functions->get_particular_field_value("tbl_session","session_name", 'and id="'.$row['session_id'].'"');?></td>

          <td align="left"><?php echo $row['guardian_name']; ?></td>
          <td align="left"><?php echo $row['perm_address']; ?></td>
          <td align="left"><?php echo $row['email']; ?></td>
          <td align="left"><?php echo $row['phone_no']; ?></td>
          <td align="left"><?php echo $row['corres_address']; ?></td>
          <td align="left"><?php echo $row['aadhar_no']; ?></td>
          <td align="left"><?php echo $row['reg_mobile_no']; ?></td>
          <td align="left"><?php echo $row['dob']; ?></td>
          <td align="left"><?php echo $passport_path; ?></td>
          <td align="left"><?php echo $row['age']; ?></td>
          <td align="left"><?php if ($row['gender']==1) {
            echo "Male";
          }else if ($row['gender']==2) {
            echo "Female";
          }else if ($row['gender']==3) {
            echo "Others";
          } ; ?></td>
          <td align="left"><?php if ($row['marital_status']==1) {
            echo "Married";
          }else if ($row['marital_status']==2) {
            echo "Single";
          }else if ($row['marital_status']==3) {
            echo "Divorced";
          } ; ?></td>
          <td align="left"><?php echo $row['nationality']; ?></td>
          <td align="left"><?php if ($row['caste']==1) {
            echo "General";
          }else if ($row['caste']==2) {
            echo "SC";
          }else if ($row['caste']==3) {
            echo "ST";
          }else if ($row['caste']==4) {
            echo "OBC";
          }else if ($row['caste']==5) {
            echo "Physically Handicapped";
          }; ?></td>
          <td align="left"><?php echo $age_path; ?></td>
          <td align="left"><?php echo $caste_path; ?></td>
          <td align="left"><?php echo $guardian_sign_path; ?></td>
          <td align="left"><?php echo $student_sign_path; ?></td>
          <td align="left"><?php echo $student_sign_full_path; ?></td>
          <td align="left"><?php echo $row['local_gu_name_adds']; ?></td>
          <td align="left"><?php echo $row['local_address']; ?></td>

        </tr>

            <?php $sl++; endforeach; ?>


              </table>
              </div>
    </div>

<?php

}


?>

<?php $this->load->view("admin/include/footer"); ?>
