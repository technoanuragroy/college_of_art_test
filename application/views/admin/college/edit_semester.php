<?php $this->load->view("admin/include/header"); ?>

<script type="text/javascript">
    $(document).ready(function () {
        if ($('#combine_rslt').prop("checked") == true) {
            $('#term_label').attr('required', 'true');
            $('.final_label').slideDown();
        } else {
            $('#term_label').removeAttr('required');
            $('.final_label').slideUp();
        }

        $('#combine_rslt').click(function () {
            if ($(this).prop("checked") == true) {
                $('#term_label').attr('required', 'true');
                $('.final_label').slideDown();
            } else if ($(this).prop("checked") == false) {
                $('#term_label').removeAttr('required');
                $('.final_label').slideUp();
                $('#term_label').val('');
            }
        });
    });
    
    function open_move_block(){
        $(".move_block").slideToggle();
    }


    </script>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Edit Semester</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">
                                     <li class="full_width_li">
                                        <span>
                                            <label>Select Course</label>
                                            <select name="course_id" id="course_id">
                                                <option value="">Select Course</option>
                                                <?php foreach ($all_course as $row) {
                                                    if ($row['id'] == $details['course_id']) {
                                        $selected = 'selected="selected"';
                                    } else {
                                        $selected = '';
                                    } ?>
                                                    <option value="<?php echo $row['id']; ?>" <?php echo $selected; ?>><?php echo $row['course_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label><?php echo $this->my_custom_functions->get_particular_field_value("tbl_session","session_name", 'and id="'.$details['id'].'"'); ?></label>
                                            <input type="hidden" name="session_id" id="session_id" value="<?php echo $details['id']; ?>" >
                                        </span>
                                    </li>
                                    <li class="full_width_li">
                                        <span>
                                            <label>Semester Name</label>
                                            <input type="text" name="semester_name" id="semester_name" placeholder="Semester Name" required="" value="<?php echo $details['semester_name']; ?>">
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <span>
                                            <label>Combine Result</label>
                                            <input  type="checkbox" name="combine_rslt" id="combine_rslt" <?php
                            if ($details['combined_term_results'] == 1) {
                                echo "checked";
                            }
                            ?> value="1">
                                        </span>
                                    </li>

                                    <li  class="form-group final_label full_width_li" style="display:none;">
                                        <span>
                                            <label>Final Term Label</label>
                                            <input type="text" class="" id="term_label" name="term_label" placeholder="Enter final term label" value="<?php echo $details['combined_result_name']; ?>">
                                        </span>
                                    </li>

                                   <input type="hidden" name="semester_id" value="<?php echo $this->ablfunctions->ablEncrypt($details['id']); ?>">

                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Submit">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
