<?php $this->load->view("admin/include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Exam</h2>

                <a href="<?php echo base_url(); ?>admin/college/addExam" class="c-link">Add Exam</a>

                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Exam Name</th>
                                    <th>Term Name</th>        
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($details)) {
                                    foreach($details as $page) {
                                     $page_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>
                                            <td><?php echo $page['exam_display_name']; ?></td>
                                            <td><?php echo $this->my_custom_functions->get_particular_field_value("tbl_terms","term_name", 'and id="'.$page['term_id'].'"'); ?>
                                                
                                            </td>

 
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/copyExam/'.$page_id; ?>" title="Copy another exam">
                                        <i class="fa fa-copy"></i>
                                                </a>
                                                <a href="<?php echo base_url().'admin/college/editExam/'.$page_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteExam/'.$page_id; ?>');" title="Delete Record">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>

                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="8">No Exam found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
