<?php $this->load->view("admin/include/header_feestructure"); ?>


    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Add Fees Structure</h2>

                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="row">

                        <?php echo form_open('', array('id' => 'formAddAdmin')); ?>

                                <ul class="input_listing">

                                    <li class="full_width_li">
                                        <span>
                                            <label>Select Semester</label>
                                            <select name="semester_id" class="form-control" id="semester_id" required>
                                                <option value="">Select</option>
                                            <?php foreach ($semesters as $row) { ?>
                                                <option value="<?php echo $row['id']; ?>"><?php echo $row['semester_name']; ?></option>
                                            <?php } ?>
                                            </select>
                                        </span>
                                    </li>
                                    <li class="full_width_li">
                                        <span>
                                            <label>Cycle Type</label>
                                            <?php $cycle = $this->config->item('cycle') ?>
                                        <select name="cycle_id" class="form-control" id="cycle_id" required>
                                            <option value="">Select</option>
                                            <?php foreach ($cycle as $key => $row) { ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $row; ?></option>
                                            <?php } ?>
                                        </select>
                                        </span>
                                    </li>
                                    <li class="full_width_li">
                                        <span>
                                            <label>Comment</label>
                                            <input type="text" class="form-control" name="comment" id="section_comment" placeholder="Enter Comment" required="">
                                        </span>
                                    </li>

                                    <li class="full_width_li">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="submit">
                                    </li>
                                </ul>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
