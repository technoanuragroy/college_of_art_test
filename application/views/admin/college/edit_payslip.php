<?php $this->load->view("admin/include/header_solcrm"); ?>
<style>
   #tooltip {
    white-space: pre-line;
}
    </style>
<script type="text/javascript">
    $(document).ready(function() {
    $(document).tooltip();


        //$("#frm1").validate();
        $('#emp_id').change(function() {
            var emp_id = $('#emp_id').val();
            var month = $('#month').val();
            var year = $('#year').val();
            //alert(emp_id)
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>admin/college/get_emp_attend_details',
                data: {"emp_id": emp_id, "month": month, "year": year},
                success: function(response)
                {
                    //console.log(response)
                    $.each(response, function(id, value)
                    {
                        if (id == 'attend_id') {
                            $('#attend_id').val(value);
                        }
                        if (id == 'present') {
                            $('#present').val(value);
                        }
                        if (id == 'absent') {
                            $('#absent').val(value);
                        }
                        if (id == 'pl') {
                            $('#pl').val(value);
                        }
                        if (id == 'cl') {
                            $('#cl').val(value);
                        }
                        if (id == 'sl') {
                            $('#sl').val(value);
                        }
                        if (id == 'opl') {
                            $('#opl').val(value);
                        }
                        if (id == 'working_days') {
                            $('#working_days').val(value);
                        }

                    });
                },
                error: function(rs)
                {
                    alert("Error");
                }
            });
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>admin/college/get_emp_details',
                data: {"emp_id": emp_id},
                success: function(response)
                {

                    $.each(response, function(id, value)
                    {
                        if (id == 'designation') {
                            if (value == 1) {
                              $('#designation').val("Principal");
                            }
                            else if (value == 2) {
                              $('#designation').val("Teaching Staff");
                            }
                            else if (value == 3) {
                              $('#designation').val("Non Teaching Staff");
                            }
                        }
                        if (id == 'basic_pay') {
                            $('#basic_pay').val(parseFloat(value));
                            calculate1();
                            //calculate2().delay(1000, null);
                            setTimeout(function(){ calculate2(); }, 1000);
                        }
                        if (id == 'grade_pay') {
                            $('#grade_pay').val(parseFloat(value));
                            calculate1();
                            //calculate2().delay(1000, null);
                            setTimeout(function(){ calculate2(); }, 1000);
                        }
                        if (id == 'med_allow') {
                            $('#medical_allow').val(parseFloat(value));
                            calculate1();
                            //calculate2().delay(1000, null);
                            setTimeout(function(){ calculate2(); }, 1000);
                        }

                    });


                },
                error: function(rs)
                {
                    alert("Error");
                }
            });
        });
        $('#year').change(function() {
            var emp_id = $('#emp_id').val();
            var month = $('#month').val();
            var year = $('#year').val();
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>admin/college/get_emp_attend_details',
                data: {"emp_id": emp_id, "month": month, "year": year},
                success: function(response)
                {

                    $.each(response, function(id, value)
                    {
                        if (id == 'attend_id') {
                            $('#attend_id').val(value);
                        }
                        if (id == 'present') {
                            $('#present').val(value);
                        }
                        if (id == 'absent') {
                            $('#absent').val(value);
                        }
                        if (id == 'pl') {
                            $('#pl').val(value);
                        }
                        if (id == 'cl') {
                            $('#cl').val(value);
                        }
                        if (id == 'sl') {
                            $('#sl').val(value);
                        }
                        if (id == 'opl') {
                            $('#opl').val(value);
                        }
                        if (id == 'working_days') {
                            $('#working_days').val(value);
                        }


                    });
                },
                error: function(rs)
                {
                    alert("Error");
                }
            });
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>admin/college/get_emp_details',
                data: {"emp_id": emp_id},
                success: function(response)
                {

                    $.each(response, function(id, value)
                    {
                      if (id == 'designation') {
                          if (value == 1) {
                            $('#designation').val("Principal");
                          }
                          else if (value == 2) {
                            $('#designation').val("Teaching Staff");
                          }
                          else if (value == 3) {
                            $('#designation').val("Non Teaching Staff");
                          }
                      }
                      if (id == 'basic_pay') {
                          $('#basic_pay').val(parseFloat(value));
                          calculate1();
                          //calculate2().delay(1000, null);
                          setTimeout(function(){ calculate2(); }, 1000);
                      }
                      if (id == 'grade_pay') {
                          $('#grade_pay').val(parseFloat(value));
                          calculate1();
                          //calculate2().delay(1000, null);
                          setTimeout(function(){ calculate2(); }, 1000);
                      }
                      if (id == 'med_allow') {
                          $('#medical_allow').val(parseFloat(value));
                          calculate1();
                          //calculate2().delay(1000, null);
                          setTimeout(function(){ calculate2(); }, 1000);
                      }

                    });
                },
                error: function(rs)
                {
                    alert("Error");
                }
            });
        });
        $('#month').change(function() {
            var emp_id = $('#emp_id').val();
            var month = $('#month').val();
            var year = $('#year').val();
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>admin/college/get_emp_attend_details',
                data: {"emp_id": emp_id, "month": month, "year": year },
                success: function(response)
                {

                    $.each(response, function(id, value)
                    {
                        if (id == 'attend_id') {
                            $('#attend_id').val(value);
                        }
                        if (id == 'present') {
                            if (value == '0') {
                                alert('Attendance')
                            }
                            $('#present').val(value);
                        }
                        if (id == 'absent') {
                            $('#absent').val(value);
                        }
                        if (id == 'pl') {
                            $('#pl').val(value);
                        }
                        if (id == 'cl') {
                            $('#cl').val(value);
                        }
                        if (id == 'sl') {
                            $('#sl').val(value);
                        }
                        if (id == 'opl') {
                            $('#opl').val(value);
                        }
                        if (id == 'working_days') {
                            $('#working_days').val(value);
                        }


                    });
                },
                error: function(rs)
                {
                    alert("Error");
                }
            });
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url(); ?>admin/college/get_emp_details',
                data: {"emp_id": emp_id },
                success: function(response)
                {

                    $.each(response, function(id, value)
                    {
                        if (id == 'designation') {
                            $('#designation').val(value);
                        }
                        if (id == 'basic_pay') {
                            $('#gross_earnings').val(value);
                            calculate1();
                            //calculate2().delay(1000, null);
                            setTimeout(function(){ calculate2(); }, 1000);
                        }

                    });
                },
                error: function(rs)
                {
                    alert("Error");
                }
            });
        });
    });

    $(document).ready(function() {
        $('a.back').click(function() {
            parent.history.back();
            return false;
        });
        $('#payment_date').datepicker(
                {dateFormat: 'yy-mm-dd',
                }).datepicker("setDate", "0");

        $('#other_allow').blur(function() {
            calculate1();
            calculate2();
        });
        $('#fest_advance').blur(function() {
          calculate1();
          calculate2();
        });
        $('#total_deduction').blur(function() {
          calculate1();
          calculate2();
        });

        $('#other_deduction').blur(function() {
          calculate1();
          calculate2();
        });
        $('#income_tax').blur(function() {
          calculate1();
          calculate2();
        });
    });

    function calculate2() {

        if ($('#fest_advance').val() == "") {
            $('#fest_advance').val('0');
        }
        if ($('#ptax').val() == "") {
            $('#ptax').val('0');
        }
        if ($('#other_deduction').val() == "") {
            $('#other_deduction').val('0');
        }
        if ($('#income_tax').val() == "") {
            $('#income_tax').val('0');
        }
        if ($('#leave_without_pay').val() == "") {
            $('#leave_without_pay').val('0');
        }

        if(($('#working_days').val() != "0") && ($('#working_days').val() != "")) {
            $('#leave_without_pay').val(Math.round(parseFloat($('#gross_earnings').val()) / parseFloat($('#working_days').val()) * parseFloat($('#opl').val())));
        } else {
            $('#leave_without_pay').val('0');
        }

        $('#total_deduction').val(parseFloat($('#ptax').val()) + parseFloat($('#pf_deduction').val()) + parseFloat($('#fest_advance').val())+ parseFloat($('#other_deduction').val())+ parseFloat($('#income_tax').val()));
        $('#net_pay').val(parseFloat($('#salary_total').val()) - parseFloat($('#total_deduction').val()));

        /// p-tax calculation
        if ($('#salary_total').val() <= 10000) {
            $('#ptax').val(0);
        }

        if ($('#salary_total').val() > 10000 && $('#salary_total').val() <= 15000) {
            $('#ptax').val(110);
        }
        if ($('#salary_total').val() > 15000 && $('#salary_total').val() <= 25000) {
            $('#ptax').val(130);
        }
        if ($('#salary_total').val() > 25000 && $('#salary_total').val() <= 40000) {
            $('#ptax').val(150);
        }
        if ($('#salary_total').val() > 40000) {
            $('#ptax').val(200);
        }

    }
    function calculate1() {
          if ($('#other_allow').val() == "") {
          $('#other_allow').val('0');
          }
        $('#emp_contribute_pf').val(Math.round((parseFloat($('#basic_pay').val()) * 13.61)/100));
        $('#salary_total').val((parseFloat($('#basic_pay').val()) + parseFloat($('#grade_pay').val()) + parseFloat($('#other_allow').val()) + parseFloat($('#medical_allow').val()) + parseFloat($('#emp_contribute_pf').val())));
        $('#pf_deduction').val(Math.round($('#basic_pay').val() * 25.61 / 100));
        // $('#ma').val(Math.round($('#basic_pay').val() * 5 / 100));
    }
</script>

<h1>Update Pay Slip</h1>
<div id="body">
    <div class="err"><?php echo validation_errors(); ?></div>
    <div class="err"><?php
        if ($this->session->flashdata('op_msg') <> "") {
            echo "<p>" . $this->session->flashdata('op_msg') . "</p>";
        }
        ?></div>
    <?php

    function monthDropdown($name = "month", $selected = null) {
        $dd = '<select name="' . $name . '" id="' . $name . '">';

        $months = array(
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December');
        /*         * * the current month ** */
        $selected = is_null($selected) ? date('n', time()) : $selected;
        $dd.='<option >Month</option>';
        for ($i = 1; $i <= 12; $i++) {
            $dd .= '<option value="' . $i . '"';



            if ($i == $selected - 1) {
                $dd .= 'selected';
            }
            /*             * * get the month ** */
            $dd .= '>' . $months[$i] . '</option>';
        }
        $dd .= '</select>';
        return $dd;
    }

    function yearDropdown($startYear, $endYear, $id = "year") {
        //start the select tag
        echo "<select id=" . $id . " name=" . $id . ">n";
        //echo each year as an option
        for ($i = $startYear; $i <= $endYear; $i++) {
            if ($i == $endYear) {
                echo "<option value=" . $i . " selected>" . $i . "</option>n";
            } else {
                echo "<option value=" . $i . ">" . $i . "</option>n";
            }
        }

        //close the select tag
        echo "</select>";
    }

    echo form_open('', array('id' => 'frm1'));
    ?>
    <fieldset>
        <legend>Employee Information</legend>
        <input type="hidden" name="admin_name" value="<?php echo $this->session->userdata("admin_name"); ?>">
        <input type="hidden" name="payslip_id" value="<?php echo $slip['id']; ?>" >

        <p><label><b>Employee Name:</b> </label><?php echo $this->my_custom_functions->get_particular_field_value("tbl_staff","staff_name", 'and id="'.$slip['emp_id'].'"'); ?></p>
    </fieldset>
    <br>
    <br>
    <fieldset>
        <legend>Attendance Details</legend>
        <p><label>Date: </label>
            <?php
            $payslip_date = $slip['payslip_date'];

            /*             * * example usage ** */
            // echo monthDropdown('month', date("m", strtotime($payslip_date))) . "  ";
            // yearDropdown(2004, date("Y",strtotime($payslip_date)));
            ?>
            <span>Month:<?php echo date("F", strtotime($payslip_date));?><br>Year:<?php echo date("Y", strtotime($payslip_date));?> </span>
        </p>
        <p><label>Present: </label><?php echo form_input(array('name' => 'present', 'id' => 'present', 'readonly' => true), set_value('present',$attend['present'])); ?></p>
        <p><label>Absent: </label><?php echo form_input(array('name' => 'absent', 'id' => 'absent', 'readonly' => true), set_value('absent',$attend['absent'])); ?></p>
        <p><label>PL: </label><?php echo form_input(array('name' => 'pl', 'id' => 'pl', 'readonly' => true), set_value('pl',$attend['pl'])); ?></p>
        <p><label>CL: </label><?php echo form_input(array('name' => 'cl', 'id' => 'cl', 'readonly' => true), set_value('cl',$attend['cl'])); ?></p>
        <p><label>SL: </label><?php echo form_input(array('name' => 'sl', 'id' => 'sl', 'readonly' => true), set_value('sl',$attend['sl'])); ?></p>
        <p><label>OPL(Without Pay Leave): </label><?php echo form_input(array('name' => 'opl', 'id' => 'opl', 'readonly' => true), set_value('opl',$attend['opl'])); ?></p>
        <p><label>Working Days: </label><?php echo form_input(array('name' => 'working_days', 'id' => 'working_days', 'readonly' => true), set_value('working_days',$attend['working_days'])); ?></p>
    </fieldset>
    <br />


    <fieldset>
      <ul class="input_listing">
        <legend>Pay Slip Details</legend>
        <legend>Salary:</legend>
        <li class="full_width_li">
        <span><label>Pay: </label><?php echo form_input(array('name' => 'basic_pay', 'id' => 'basic_pay', 'readonly' => true), set_value('basic_pay',$slip['basic_pay'])); ?>
        </span>
        </li>
        <li class="full_width_li">
        <span><label>Grade Pay: </label><?php echo form_input(array('name' => 'grade_pay', 'id' => 'grade_pay', 'readonly' => true), set_value('grade_pay',$slip['grade_pay'])); ?></span>
        </li>
        <li class="full_width_li">
        <span><label>Other if any: </label><?php echo form_input(array('name' => 'other_allow', 'id' => 'other_allow' ), set_value('other_allow',$slip['other_allow'])); ?>
        </span>
        <span><label>Other Allowence Title: </label><?php echo form_input(array('name' => 'other_allow_title', 'id' => 'other_allow_title' ), set_value('other_allow_title',$slip['other_allow_title'])); ?>
        </span>
        </li>
        <li class="full_width_li">
        <span><label>Other & Med. Allow: </label><?php echo form_input(array('name' => 'medical_allow', 'id' => 'medical_allow', 'readonly' => true), set_value('medical_allow',$slip['medical_allow'])); ?></span>
        </li>
        <li class="full_width_li">
        <span><label>Employer Contr. to P.F. (13.61%): </label><?php echo form_input(array('name' => 'emp_contribute_pf', 'id' => 'emp_contribute_pf', 'readonly' => true), set_value('emp_contribute_pf',$slip['emp_contribute_pf'])); ?></span>
        </li>
          <li class="full_width_li">
          <span><label>TOTAL: </label><?php echo form_input(array('name' => 'salary_total', 'id' => 'salary_total', 'readonly' => true), set_value('salary_total',$slip['salary_total'])); ?></span>
          </li>
        <legend>Deduction:</legend>
        <li class="full_width_li">
        <span><label >P.Tax: </label><?php echo form_input(array('name' => 'ptax', 'id' => 'ptax', 'class' => 'required', 'readonly' => true ), set_value('ptax',$slip['ptax'])); ?>
          <span id="tooltip"></span>
          <img src="<?php echo base_url(); ?>images/tooltip.png" title='Net Pay <= 10000 then PTax= 0 ,
              Net Pay > 10000 or <= 15000 then PTax= 110,
              Net Pay > 15000 or <= 25000 then PTax= 130 ,
              Net Pay > 25000 or <= 40000 then PTax= 150,
              Net Pay > 40000 then PTax= 200'>
            </span>
            </li>
        <li class="full_width_li">
        <span><label> I. Tax: </label><?php echo form_input(array('name' => 'income_tax', 'id' => 'income_tax'), set_value('income_tax',$slip['income_tax'])); ?></span>
        </li>
          <li class="full_width_li">
          <span><label>PF25.61% <br> (12%+13.61%): </label><?php echo form_input(array('name' => 'pf_deduction', 'id' => 'pf_deduction' , 'readonly' => true), set_value('pf_deduction',$slip['pf_deduction'])); ?></span>
          </li>
            <li class="full_width_li">
            <span><label>Festival Advance: </label><?php echo form_input(array('name' => 'fest_advance', 'id' => 'fest_advance' ), set_value('fest_advance',$slip['fest_advance'])); ?>
            </span>
            </li>
            <li class="full_width_li">
            <span><label>Other Deduction: </label><?php echo form_input(array('name' => 'other_deduction', 'id' => 'other_deduction' ), set_value('other_deduction',$slip['other_deduction'])); ?>
            </span>
            <span><label>Other Deduction Title: </label><?php echo form_input(array('name' => 'other_deduction_title', 'id' => 'other_deduction_title' ), set_value('other_deduction_title',$slip['other_deduction_title'])); ?>
            </span>
            </li>
        <li class="full_width_li">
        <span><label>Total Deduction: </label><?php echo form_input(array('name' => 'total_deduction', 'id' => 'total_deduction', 'readonly' => true), set_value('total_deduction',$slip['total_deduction'])); ?></span>
        </li>
          <li class="full_width_li">
          <span><label>Net Amount Payable: </label><?php echo form_input(array('name' => 'net_pay', 'id' => 'net_pay', 'readonly' => true), set_value('net_pay',$slip['net_pay'])); ?></span>
          </li>

    </fieldset>
    <br>
    <fieldset>
        <ul class="input_listing">
        <legend>Payment Details</legend>
        <li class="full_width_li">
            <span>
                <label>Payment Date: </label>
                <?php echo form_input(array('name' => 'payment_date', 'id' => 'payment_date' ,'value'=> $slip['payment_date'])); ?>
            </span>
        </li>
        <li class="full_width_li">
            <span>
                <label>Note: </label>
                <?php echo form_textarea(array('name' => 'note','value'=> $slip['note'])); ?>
            </span>
        </li>
        <li class="full_width_li">
            <span>
                <label>Payment Details : </label>
                <?php echo form_textarea(array('name' => 'payment_details','value'=> $slip['payment_details'])); ?>
            </span>
        </li>
        </ul>

    </fieldset>
    <p><?php
        //echo form_submit('submit','Submit');
        echo form_input(array('type' => 'submit', 'name' => 'submit', 'value' => 'Update','class'=>'submit_btn'));
        echo "&nbsp;&nbsp;&nbsp;&nbsp;";
        ?><a href="#" class="button back" >Cancel</a></p>
        <?php echo form_close(); ?>
</div>

<?php $this->load->view("admin/include/footer"); ?>
