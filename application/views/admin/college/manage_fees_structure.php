<?php $this->load->view("admin/include/header_feestructure"); ?>

        <script type="text/javascript">
        // function call_delete(id) {
        //     $('.message_block').html('<p>Are you sure that you want to delete this fees structure?</p>');
        //     $('.btn-alt-success').attr('onclick', "confirm_delete('" + id + "')");
        // }
        // function confirm_delete(id) {
        //     window.location.href = "<?php echo base_url(); ?>admin/college/deleteFeesStructure/" + id;
        // }
    </script>
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Fees Structure</h2>

                <a href="<?php echo base_url(); ?>admin/college/addFeesStructure" class="c-link">Add Fees Structure</a>

                            <div class="col-md-12">
                <?php if ($this->session->flashdata("s_message")) { ?>
                        <!-- Success Alert -->
                        <div class="alert alert-success alert-dismissable s_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("s_message"); ?></a>!</p>
                        </div>
                        <!-- END Success Alert -->
                <?php } ?>

                <?php if ($this->session->flashdata("e_message")) { ?>
                        <!-- Danger Alert -->
                        <div class="alert alert-danger alert-dismissable e_message" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                            <p class="mb-0"><?php echo $this->session->flashdata("e_message"); ?></a>!</p>
                        </div>
                        <!-- END Danger Alert -->
                <?php } ?>
            </div>

                <div class="table_wrap">
                    <?php
                    if (!empty($details)) {
                        $i = 1;
                ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover js-dataTable-full">
                            <thead>
                                <tr>
                                    <th>SEMESTER NAME</th>
                                    <th>FEES STRUCTURE BREAKUP</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($details as $row) {
                                        $encrypted = $this->ablfunctions->ablEncrypt($row['id']);
                                        $breakups_exist = $this->my_custom_functions->get_perticular_count(TBL_FEES_STRUCTURE_BREAKUPS, 'and fees_id="' . $row['id'] . '"');
                                ?>
                            <tr>
                            <td>
                                    <?php echo $this->my_custom_functions->get_particular_field_value(TBL_SEMESTER, 'semester_name', 'and id = "' . $row['semester_id'] . '"'); ?>
                            </td>

                            <td >
                            <?php if($breakups_exist > 0) { ?>
                            <a href="<?php echo base_url() . 'admin/college/editFeesStructureBreakup/' . $encrypted; ?>" class="btn btn-primary">Fees Structure Breakup</a>
                            <?php } else { ?>
                            <a href="<?php echo base_url() . 'admin/college/feesStructureBreakup/' . $encrypted; ?>" class="btn btn-primary">Fees Structure Breakup</a>
                                <?php } ?>
                            </td>

                        <td >
                            <a href="<?php echo base_url() . 'admin/college/editFeesStructure/' . $encrypted; ?>" title="Edit">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>

                        <td >
                            <!-- <a href="<?php //echo base_url() . 'admin/college/deleteFeesStructure/' . $encrypted; ?>" title="Delete" data-toggle="modal" data-target="#modal-top" onclick="return call_delete('<?php echo $encrypted; ?>');">
                                    <i class="fa fa-trash"></i>
                                </a> -->
                                <a href="javascript:;" title="Delete"  onclick="confirm_delete('<?php echo base_url().'admin/college/deleteFeesStructure/' . $encrypted; ?>');">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                                $i++;
                            }
                        ?>
                            </tbody>
                        </table>
                        <?php
                    } else {
                ?>
                        <tr>
                            <td colspan="4">No fees structure found</td>
                        </tr>
                <?php
                    }
                ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
