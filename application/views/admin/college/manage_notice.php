<?php $this->load->view("admin/include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Notice</h2>

                <a href="<?php echo base_url(); ?>admin/college/issueNotice" class="c-link">Issue Notice</a>

                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover js-dataTable-full">
                            <thead>
                                <tr>
                                    <th>Notice Heading</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($details)) {
                                    foreach($details as $page) {
                                     $staff_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>

                                            <td><?php echo $page['notice_heading']; ?></td>
                                            <td><?php if ($page['status']==1) {
                                              echo "Active";
                                            } else {
                                              echo "InActive";
                                            } ?></td>
                                            <td>
                                                <a href="<?php echo base_url().'admin/college/editNotice/'.$staff_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/college/deleteNotice/'.$page['document_url'].'/'.$page['id'] ?>');"
                                                   title="Delete Record">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="6">No Notice found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("admin/include/footer"); ?>
