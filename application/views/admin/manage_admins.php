<?php $this->load->view("admin/include/header"); ?>
            
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Admins</h2>
                
                <a href="<?php echo base_url(); ?>admin/user/addAdmin" class="c-link">Add Admin</a>   
					                
                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>                     
                                                                
                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Username</th>
                                    <th width="120px">Type</th>
                                    <th>Email</th>
                                    <th width="120px">Join On</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($admins)) {
                                    foreach($admins as $admin) {                                                 
                            ?>
                                        <tr>
                                            <td><?php echo $admin['name']; ?></td>
                                            <td><?php echo $admin['phone']; ?></td>
                                            <td><?php echo $admin['username']; ?></td>
                                            <td><?php echo $admin['type_text']; ?></td>
                                            <td><?php echo $admin['email']; ?></td>
                                            <td><?php echo date(DISPLAY_DATE_FORMAT, strtotime($admin['date_created'])); ?></td>
                                            <td><?php echo $admin['status_text']; ?></td>
                                            <td>                                    
                                                <a href="<?php echo base_url().'admin/user/editAdmin/'.$admin['encrypted_id']; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            <td>                                         
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/user/deleteAdmin/'.$admin['encrypted_id']; ?>');" title="Delete Record">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="9">No other admin found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>  
                                            
                </div>    
            </div>
        </div>    
    </div>	
                        
<?php $this->load->view("admin/include/footer"); ?>        

      


