<?php $this->load->view("admin/include/header"); ?>
            
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage CMS</h2>
                
                <a href="<?php echo base_url(); ?>admin/cms/addPage" class="c-link">Add Page</a>   
					                
                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>                     
                                                                
                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Page Name</th>
                                    <th>URL Alias</th>                                    
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($cms_pages)) {
                                    foreach($cms_pages as $page) {                                                 
                            ?>
                                        <tr>
                                            <td><?php echo $page['name']; ?></td>
                                            <td><?php echo $page['url_alias']; ?></td>                                            
                                            <td>                                    
                                                <a href="<?php echo base_url().'admin/cms/editPage/'.$page['encrypted_id']; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                            <td>                                         
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/cms/deletePage/'.$page['encrypted_id']; ?>');" title="Delete Record">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            </td>
                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="4">No cms page found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>  
                                            
                </div>    
            </div>
        </div>    
    </div>	
                        
<?php $this->load->view("admin/include/footer"); ?>        

      


