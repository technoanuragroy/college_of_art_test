<?php $this->load->view("admin/include/header"); ?>            

    <script src="https://cdn.tiny.cloud/1/h3a1jkd6rb4gwo2ffmtdo08eyy9k1qjn5niwkdeerpo8d6vu/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script type="text/javascript">          
        tinymce.init({
            selector: '.custom_editor',    
            height: 400,            
            plugins: [
                "advlist autolink lists link image code charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table paste imagetools wordcount"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar_drawer: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'ABLION',
        });
        
        function get_url_alias(name) {
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>admin/cms/getUrlAlias",
                data: "name="+name,
                success: function(msg) { 
                    if(msg != "") {                                                    
                        $("#url_alias").val(msg);      
                        check_url_alias(msg);
                    }                    
                }
            });  
        }
        
        function check_url_alias(url_alias) {              
            
            var alias_data = {
                'url_alias': url_alias,
                'page': $("#page_id").val()
            };
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>admin/cms/validateUrlAlias",
                data: alias_data,
                success: function(msg) { 
                    if(msg != "") {                                                    
                        $("#url_alias_error").text(msg); 
                        $("#submit").hide();
                    } else {
                        $("#url_alias_error").text("");  
                        $("#submit").show();
                    }
                }
            });         
        }
    </script>  

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Update Page Details</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">    

                        <?php echo form_open('', array('id' => 'formEditPage')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <h5 class="heading_05">Basic</h5>
                                            <label>Page Name</label>
                                            <input type="text" name="name" id="name" placeholder="Page Name" onkeyup="return get_url_alias(this.value);" value="<?php echo $page_details['name']; ?>" required="">
                                        </span>
                                    </li>
                                                                        
                                    <li class="full_width_li">
                                        <span>
                                            <label>URL Alias</label>
                                            <input type="text" name="url_alias" id="url_alias" placeholder="URL Alias" onkeyup="return check_url_alias(this.value);" value="<?php echo $page_details['url_alias']; ?>" required="">
                                            <div id="url_alias_error"></div>
                                        </span>
                                    </li> 
                                                                        
                                    <li class="full_width_li">
                                        <span>
                                            <label>Page Heading</label>
                                            <input type="text" name="page_heading" id="page_heading" placeholder="Page Heading" value="<?php echo $page_details['page_heading']; ?>" required="">                                           
                                        </span>
                                    </li>                                    
                                    
                                    <li class="full_width_li">
                                        <span>
                                            <label>Page Content</label>
                                            <textarea name="page_content" id="page_content" class="custom_editor"><?php echo $page_details['page_content']; ?></textarea>                                            
                                        </span>
                                    </li>  

                                    <li class="full_width_li">
                                        <span>
                                            <h5 class="heading_05">SEO</h5>
                                            <label>Page Title</label>
                                            <input type="text" name="page_title" id="page_title" placeholder="Page Title" value="<?php echo $page_details['page_title']; ?>">
                                        </span>
                                    </li> 
                                    
                                    <li class="full_width_li">
                                        <span>
                                            <label>Meta Keywords</label>
                                            <input type="text" name="meta_keywords" id="meta_keywords" placeholder="Meta Keywords" value="<?php echo $page_details['meta_keywords']; ?>">
                                        </span>
                                    </li> 
                                    
                                    <li class="full_width_li">
                                        <span>
                                            <label>Meta Description</label>
                                            <textarea name="meta_description" id="meta_description" class="custom_editor"><?php echo $page_details['meta_description']; ?></textarea>                                            
                                        </span>
                                    </li> 

                                    <li class="full_width_li">	
                                        <input type="hidden" name="page_id" id="page_id" value="<?php echo $page_details['encrypted_id']; ?>">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Update">
                                    </li>
                                </ul> 

                        <?php echo form_close(); ?>
                        
                    </div>
                </div>    
            </div>
        </div>    
    </div>	     

<?php $this->load->view("admin/include/footer"); ?>
