<?php $this->load->view("admin/include/header"); ?>            

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Update Profile</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">    

                        <?php echo form_open('', array('id' => 'formEditProfile')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>Name</label>
                                            <input type="text" name="name" id="name" placeholder="Name" value="<?php echo $admin_details['name']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Email</label>
                                            <input type="email" name="email" id="email" placeholder="Email" value="<?php echo $admin_details['email']; ?>" required="">
                                        </span>
                                    </li>

                                    <li>
                                        <span>
                                            <label>Phone Number</label>
                                            <input type="number" name="phone" id="phone" placeholder="Phone Number" value="<?php echo $admin_details['phone']; ?>" required="">
                                        </span>
                                    </li>     

                                    <!-- Super admin can edit its own type -->
                                    <?php if($admin_details['type'] == ADMIN_TYPE_SUPER_ADMIN) { ?>

                                            <li class="full_width_li">
                                                <span>
                                                    <label>Admin Type</label>
                                                    <select name="type" id="type" required="">
                                                        <option value="">Admin Type</option>
                                                        <option value="<?php echo ADMIN_TYPE_SUPER_ADMIN; ?>" <?php if($admin_details['type'] == ADMIN_TYPE_SUPER_ADMIN) { echo 'selected="selected"'; } ?>>Super Admin</option>
                                                        <option value="<?php echo ADMIN_TYPE_ADMIN; ?>" <?php if($admin_details['type'] == ADMIN_TYPE_ADMIN) { echo 'selected="selected"'; } ?>>Admin</option>                                              
                                                    </select>
                                                </span>
                                            </li>
                                            
                                    <?php } ?>

                                    <li class="full_width_li">	  
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Update">
                                    </li>
                                </ul>  

                        <?php echo form_close(); ?>
                        
                    </div>
                </div>    
            </div>
        </div>    
    </div>	     

<?php $this->load->view("admin/include/footer"); ?> 