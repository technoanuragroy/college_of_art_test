<?php $this->load->view("admin/include/header"); ?>                

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Enter Album Details</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">      
      
                        <?php echo form_open_multipart('', array('id' => 'formAddAlbum')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>Album Name</label>
                                            <input type="text" name="name" id="name" placeholder="Album Name" required="">
                                        </span>
                                    </li> 
                                    
                                    <li class="full_width_li">
                                        <span>
                                            <label>Album Details</label>
                                            <textarea name="details" id="details"></textarea>                                                     
                                        </span>
                                    </li> 

                                    <?php if(!empty($albums)) { ?>
                                    
                                            <li class="full_width_li">
                                                <span>
                                                    <label>Set This Album Under</label>
                                                    <select name="parent_album" id="parent_album">
                                                        <option value="">Select Parent Album</option>
                                                        
                                                        <?php foreach($albums as $album) { ?>
                                                                <option value="<?php echo $album['encrypted_id']; ?>"><?php echo $album['name']; ?></option>                                                                    
                                                        <?php } ?>
                                                    </select>
                                                </span>
                                            </li>
                                            
                                    <?php } ?>
                                            
                                    <li class="full_width_li">
                                        <span>
                                            <label>Cover Image</label>
                                            <input type="file" name="cover_image_url" id="cover_image_url">
                                        </span>
                                    </li>         

                                    <li class="full_width_li">	
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Save">
                                    </li>
                                </ul> 

                        <?php echo form_close(); ?>
                        
                    </div>
                </div>    
            </div>
        </div>    
    </div>	         
                
<?php $this->load->view("admin/include/footer"); ?>




























































































