<?php $this->load->view("admin/include/header"); ?>
            
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Manage Albums</h2>
                
                <a href="<?php echo base_url(); ?>admin/gallery/addAlbum" class="c-link">Add Album</a> 
                					                
                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>                     
                                                                                                                                    
                    <div class="row">
                        <?php
                            if(!empty($albums)) {
                                foreach($albums as $album) {
                        ?>                                    
                                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                                        <div class="album_cover">
                                            <?php $img_urls = $this->my_custom_functions->getGalleryImageUrl($album['cover_image_url']); ?>                                        
                                            <img src="<?php echo $img_urls['thumb_url']; ?>">
                                        </div>
                                        <div class="album_control">
                                            <span class="album_name"><?php echo $album['name']; ?></span> 
                                            <a href="<?php echo base_url().'admin/gallery/editAlbum/'.$album['encrypted_id']; ?>" title="Edit Record">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a> 
                                            <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/gallery/deleteAlbum/'.$album['encrypted_id']; ?>');" title="Delete Record">
                                                <i class="fa fa-trash-o"></i>
                                            </a>    
                                        </div>
                                    </div>   
                        <?php
                                }  
                            }
                        ?>
                    </div>                                                            
                                            
                </div>    
            </div>
        </div>    
    </div>	
                        
<?php $this->load->view("admin/include/footer"); ?>        

      


