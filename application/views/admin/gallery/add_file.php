<?php $this->load->view("admin/include/header"); ?>                        

    <script type="text/javascript">
        $(document).ready(function() {
           
           $(".file_uploader_container").hide();
           
           $(".type").on("click", function() {
               
                $(".file_uploader_container").hide();
                $(".file_uploader").each(function() {
                    $(this).removeAttr("required");
                });
               
                // Image
                if($(this).val() == 1) {

                    $(".image_url_container").show();
                    $("#image_url").attr("required", "true");
                } 
                // Video(Youtube Url)
                else if($(this).val() == 2) {
                   
                    $(".youtube_url_container").show();
                    $(".preview_image_url_container").show();
                    $("#youtube_url").attr("required", "true");                                     
                } 
                // Video File
                else if($(this).val() == 3) {
                   
                    $(".video_file_url_container").show();
                    $(".preview_image_url_container").show();
                    $("#video_file_url").attr("required", "true");
                    $("#preview_image_url").attr("required", "true");
                }
            });
        });        
    </script>      

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Upload File</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">      
      
                        <?php echo form_open_multipart('', array('id' => 'formUploadFile')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>File Title</label>
                                            <input type="text" name="title" id="title" placeholder="File Title">
                                        </span>
                                    </li> 
                                    
                                    <li class="full_width_li">
                                        <span>
                                            <label>File Details</label>
                                            <textarea name="details" id="details"></textarea>                                                     
                                        </span>
                                    </li> 
                                    
                                    <?php if(!empty($albums)) { ?>
                                    
                                            <li class="full_width_li">
                                                <span>
                                                    <label>Set This File Under</label>
                                                    <select name="album_id" id="album_id">
                                                        <option value="">Select Album</option>
                                                        
                                                        <?php foreach($albums as $album) { ?>
                                                                <option value="<?php echo $album['encrypted_id']; ?>"><?php echo $album['name']; ?></option>                                                                    
                                                        <?php } ?>
                                                    </select>
                                                </span>
                                            </li>
                                            
                                    <?php } ?>
                                            
                                    <li class="full_width_li">
                                        <span>
                                            <label class="block-label">File Type</label>
                                            <?php 
                                                if(!empty($file_types)) {
                                                    foreach($file_types as $key => $file_type) {                                                                                                     
                                            ?>
                                                        <label class="container_radio"><?php echo $file_type; ?>
                                                            <input type="radio" name="type" class="type" value="<?php echo $key; ?>" required="">                                
                                                            <span class="checkmark"></span>
                                                        </label>
                                            <?php   
                                                    }
                                                }
                                            ?>                                                
                                        </span>
                                    </li>                                    
                                            
                                    <li class="full_width_li image_url_container file_uploader_container">
                                        <span>
                                            <label>Select Image</label>
                                            <input type="file" name="image_url" id="image_url" class="file_uploader">
                                        </span>
                                    </li>    
                                    
                                    <li class="full_width_li youtube_url_container file_uploader_container">
                                        <span>
                                            <label>Video(Youtube Url)</label>
                                            <input type="text" name="youtube_url" id="youtube_url" class="file_uploader">
                                            <span>https://www.youtube.com/watch?v=3rAJzo91gdM</span>
                                        </span>
                                    </li>  
                                    
                                    <li class="full_width_li video_file_url_container file_uploader_container">
                                        <span>
                                            <label>Select Video File</label>
                                            <input type="file" name="video_file_url" id="video_file_url" class="file_uploader">                                           
                                        </span>
                                    </li>                                                                          
                                    
                                    <li class="full_width_li preview_image_url_container file_uploader_container">
                                        <span>
                                            <label>Select Preview Image</label>
                                            <input type="file" name="preview_image_url" id="preview_image_url" class="file_uploader">
                                        </span>
                                    </li> 
                                    
                                    <li class="full_width_li progress_bar">
                                        <div class="progress">
                                            <div class="bar"></div >
                                            <div class="percent">0%</div >
                                        </div>

                                        <div id="status"></div>
                                    </li>

                                    <li class="full_width_li">	
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Save">
                                    </li>
                                </ul> 

                        <?php echo form_close(); ?>
                        
                    </div>
                </div>    
            </div>
        </div>    
    </div>
                        
    <script src="http://malsup.github.com/jquery.form.js"></script>
    
    <script>
        (function() {

            var bar = $('.bar');
            var percent = $('.percent');
            var status = $('#status');

            $('form').ajaxForm({
                beforeSend: function() {
                    $(".progress_bar").show();
                    status.empty();                    
                    var percentVal = '0%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                success: function() {                    
                    var percentVal = '100%';
                    bar.width(percentVal)
                    percent.html(percentVal);       
                    window.location.href = "<?php echo base_url(); ?>admin/gallery";
                },
                complete: function(xhr) {                    
                    status.html(xhr.responseText);                   
                }
            }); 
        })();       
    </script>
    
<?php $this->load->view("admin/include/footer"); ?>




























































































