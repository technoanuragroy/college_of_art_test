<?php $this->load->view("admin/include/header"); ?>                

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Update Album Details</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">      
      
                        <?php echo form_open_multipart('', array('id' => 'formEditAlbum')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <label>Album Name</label>
                                            <input type="text" name="name" id="name" placeholder="Album Name" value="<?php echo $album_details['name']; ?>" required="">
                                        </span>
                                    </li> 
                                    
                                    <li class="full_width_li">
                                        <span>
                                            <label>Album Details</label>
                                            <textarea name="details" id="details"><?php echo $album_details['details']; ?></textarea>                                                     
                                        </span>
                                    </li> 

                                    <?php if(!empty($albums)) { ?>
                                    
                                            <li class="full_width_li">
                                                <span>
                                                    <label>Set This Album Under</label>
                                                    <select name="parent_album" id="parent_album">
                                                        <option value="">Select Parent Album</option>
                                                        
                                                        <?php 
                                                            foreach($albums as $album) { 
                                                                $selected = '';
                                                                if($album['id'] == $album_details['parent_album']) {
                                                                    $selected = 'selected="selected"';
                                                                }
                                                        ?>
                                                                <option value="<?php echo $album['encrypted_id']; ?>" <?php echo $selected; ?>><?php echo $album['name']; ?></option>                                                                    
                                                        <?php                                                         
                                                            } 
                                                        ?>
                                                    </select>
                                                </span>
                                            </li>
                                            
                                    <?php } ?>
                                            
                                    <?php                                     
                                        // Show cover image if any
                                        $img_urls = $this->my_custom_functions->getGalleryImageUrl($album_details['cover_image_url']);                                        
                                    ?>   
                                    <li class="full_width_li">
                                        <span>
                                            <img src="<?php echo $img_urls['thumb_url']; ?>">
                                        </span>
                                        <?php 
                                            if($img_urls['exists'] == "true") {
                                        ?>
                                                <a href="javascript:;" onclick="confirm_delete('<?php echo base_url().'admin/gallery/deleteAlbumCoverImage/'.$album_details['encrypted_id']; ?>');" title="Delete Image">
                                                    <i class="fa fa-trash-o"></i>
                                                </a> 
                                        <?php 
                                            }
                                        ?>
                                    </li>                                    
                                            
                                    <li class="full_width_li">
                                        <span>
                                            <label>Cover Image</label>
                                            <input type="file" name="cover_image_url" id="cover_image_url">
                                        </span>
                                    </li>         

                                    <li class="full_width_li">	
                                        <input type="hidden" name="album_id" value="<?php echo $album_details['encrypted_id']; ?>">
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Save">
                                    </li>
                                </ul> 

                        <?php echo form_close(); ?>
                        
                    </div>
                </div>    
            </div>
        </div>    
    </div>	         
                
<?php $this->load->view("admin/include/footer"); ?>




























































































