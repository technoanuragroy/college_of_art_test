<?php $this->load->view("include/header"); ?>

    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Notice</h2>


                <div class="table_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                    <div class="table-responsive">
                        <table class="table table-default table-bordered table-striped table-hover js-dataTable-full">
                            <thead>
                                <tr>
                                    <th>Notice Heading</th>
                                    <th>Status</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                if(!empty($details)) {
                                    foreach($details as $page) {
                                     $staff_id=$this->ablfunctions->ablEncrypt($page['id']);
                            ?>
                                        <tr>

                                            <td><?php echo $page['notice_heading']; ?></td>
                                            <td><?php if ($page['status']==1) {
                                              echo "Active";
                                            } else {
                                              echo "InActive";
                                            } ?></td>
                                            <td>
                                                <a href="<?php echo base_url().'user/viewNotice/'.$staff_id; ?>" title="Edit Record">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                        </tr>
                            <?php
                                    }
                                } else { ?>

                                    <tr>
                                        <td colspan="6">No Notice found</td>
                                    </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("include/footer"); ?>
