        </section>

        <!-- Change mobile number modal -->
        <div class="modal fade" id="modal-mobile" tabindex="-1" role="dialog" aria-labelledby="modal-mobile" aria-hidden="true">
            <div class="modal-dialog modal-dialog-popin modal-dialog-centered" role="document">
                <div class="modal-content">

                    <?php echo form_open('main/changeMobileNumber', array('id' => 'formChangeMobileNumber')); ?>

                            <div class="modal-header">
                                <h4 class="modal-title">Change Mobile Number</h4>
                                <button type="button" class="close" data-dismiss="modal">
                                    <i class="fa fa-close"></i>
                                </button>
                            </div>

                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="phone">Mobile Number</label>
                                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Mobile number" onkeypress="return isNumber(event)" maxlength="10" minlength="10" required="">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <input type="submit" class="c-link" value="Submit">
                            </div>

                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
        <!-- END of modal -->

        <!-- Change email address modal -->
        <div class="modal fade" id="modal-email" tabindex="-1" role="dialog" aria-labelledby="modal-email" aria-hidden="true">
            <div class="modal-dialog modal-dialog-popin modal-dialog-centered" role="document">
                <div class="modal-content">

                    <?php echo form_open('main/changeEmailAddress', array('id' => 'formChangeEmailAddress')); ?>

                            <div class="modal-header">
                                <h4 class="modal-title">Change Email Address</h4>
                                <button type="button" class="close" data-dismiss="modal">
                                    <i class="fa fa-close"></i>
                                </button>
                            </div>

                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    <input type="text" name="email" id="email" class="form-control" placeholder="Email address" required="">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <input type="submit" class="c-link" value="Submit">
                            </div>

                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
        <!-- END of modal -->
        <!-- Delete record confirmation modal -->
        <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete" aria-hidden="true">
            <div class="modal-dialog modal-dialog-popin modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Delete Record</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <i class="fa fa-close"></i>
                        </button>
                    </div>

                    <div class="modal-body">
                        Do you want to delete this record?
                    </div>

                    <div class="modal-footer">
                        <a href="javascript:" class="c-link confirm_delete_link">Confirm</a>
                    </div>

                </div>
            </div>
        </div>
        <!-- END of modal -->

    </body>
</html>
