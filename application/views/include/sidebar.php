<aside class="side_nav">
    <div class="sidenav_inner">
        <div class="sidenav_logo_btn">
            <a href="#">College Of Art & Design</a>
            <span><i class="fa fa-bars"></i></span>
        </div>

        <?php /*
        <ul class="sideNav_links">
            <li><a href="index.html"><i class="fa fa-home"></i><span>dashboard</span></a></li>
            <li><a href="javascript:"><i class="fa fa-th-large"></i><span>pages</span></a>
                <ul class="sub_links">
                    <li><a href="form.html">all inputs</a></li>
                    <li><a href="table.html">all tables</a></li>
                    <li><a href="list.html">all list style</a></li>
                    <li><a href="#">all blocks</a></li>
                </ul>
            </li>
            <li><a href="javascript:" class="target_popup"><i class="fa fa-th-list"></i><span>reports</span></a></li>
            <li><a href="#"><i class="fa fa-cog"></i><span>app</span></a></li>
            <li><a href="javascript:"><i class="fa fa-qrcode"></i><span>features</span></a>
                <ul class="sub_links">
                    <li><a href="#">pages link 01</a></li>
                    <li><a href="#">pages link 02</a></li>
                    <li><a href="#">pages link 03</a></li>
                    <li><a href="#">pages link 04</a></li>
                </ul>
            </li>
            <li><a href="#"><i class="fa fa-pencil-square-o"></i><span>builder</span></a></li>
            <li><a href="#"><i class="fa fa-crosshairs"></i><span>base</span></a></li>
        </ul>
        */ ?>

        <ul class="sideNav_links">
            <li><a href="<?php echo base_url(); ?>user/dashboard"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
            <li><a href="<?php echo base_url(); ?>user/manageNoticeView"><i class="fa fa-file-text-o"></i><span>Notice</span></a></li>
            <li><a href="javascript:;"><i class="fa fa-cog"></i><span>Admission</span></a>
            <ul class="sub_links">
            <li><a href="<?php echo base_url(); ?>user/payAdmissionFees" >Apply For Admission</a></li>
            <li><a href="<?php echo base_url(); ?>user/admissionForm_download">Download Admission Form</a></li>
            <li><a href="<?php echo base_url(); ?>user/checkAdmitCard">Download Admit Card</a></li>
            </ul>
            </li>
            <li><a href="javascript:"><i class="fa fa-file-text-o"></i><span>Time Table</span></a></li>
            <li><a href="javascript:"><i class="fa fa-file-text-o"></i><span>Attendance</span></a></li>
            <li><a href="javascript:"><i class="fa fa-file-text-o"></i><span>Syllabus</span></a></li>
            <li><a href="<?php echo base_url(); ?>user/feeStructure"><i class="fa fa-file-text-o"></i><span>Fees Structure</span></a></li>
            <li><a href="javascript:"><i class="fa fa-file-text-o"></i><span>Fees Payment History</span></a></li>
            <li><a href="<?php echo base_url(); ?>user/viewResult"><i class="fa fa-file-text-o"></i><span>Exam Results</span></a></li>
        </ul>
    </div>
</aside>
