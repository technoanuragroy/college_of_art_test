<html>
    <head>
        <title><?php echo SITE_NAME; ?>: User Panel</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <!-- CSS -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type='text/css'>
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet" type='text/css'>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/jquery-ui.min.css" rel="stylesheet" type='text/css'>

        <!-- JS -->
        <script src="<?php echo base_url(); ?>js/jquery-2.2.4.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/popper.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery-ui.min.js" type="text/javascript"></script>
        <script src="https://cdn.ckeditor.com/4.11.4/basic/ckeditor.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $(".sidenav_logo_btn span i").click(function(){
                    $(".sidenav_logo_btn a").fadeToggle(100);
                    $(".sideNav_links li a span").fadeToggle(200);
                    $(".side_nav").toggleClass("deactiveAside");
                    $(".completeWrap").toggleClass("activeSection");
                    $('.sub_links').slideUp(300);
                });

                setTimeout(function(){
                    $(".completeWrap_inner").addClass("activeCompleteWrap_inner");
                }, 500);

                $(".sideNav_links li:has('ul')").children("a").append("<span class='drop_down_icon fa fa-caret-down'></span>");

                $(".sideNav_links li").not(".sub_links li").click(function(){
                    $(this).find('.sub_links').slideToggle(300);
                    $(this).find('.drop_down_icon').toggleClass('rotateThis');
                });

                $(".adminInfoWidgate li a .fa-th-large").click(function(){
                    $('.completeWrap').addClass('completeWrapCover');
                    $('.contain_wrap').addClass('activeContain_wrap');
                });

                $(".contain_Inner span .fa-times").click(function(){
                    $('.contain_wrap').removeClass('activeContain_wrap');
                    $('.completeWrap').removeClass('completeWrapCover');
                });

                $(".adminInfoWidgate li a .fa-search").click(function(){
                    $('.search_box_wrap').toggleClass('activeSearch_box_wrap');
                    $(this).toggleClass('fa-search fa-times')
                });

                $("#myProfile").click(function(){
                    $('.user_info_wrap').toggleClass('active_user_info_wrap');
                });

                $(".target_popup").click(function(){
                    $('.popupWrap').fadeIn(500).addClass('activePopupWrap');
                });

                $(".closePop").click(function(){
                    $('.popupWrap').fadeOut(500).removeClass('activePopupWrap');
                });


            });
            function confirm_delete(delete_url) {
                $(".confirm_delete_link").attr("href", delete_url);
                $("#modal-delete").modal("show");
            }
        </script>
    </head>

    <body>

        <?php $this->load->view("include/sidebar"); ?>

        <section class="completeWrap">

            <?php $this->load->view("include/menu"); ?>
