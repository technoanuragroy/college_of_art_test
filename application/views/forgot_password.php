<html>
    <head>
        <title><?php echo SITE_NAME; ?>: Forgot Password</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        
        <!-- CSS -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type='text/css'>   
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet" type='text/css'>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">  
        <link href="<?php echo base_url(); ?>css/jquery-ui.min.css" rel="stylesheet" type='text/css'>          
                                                                         
        <!-- JS -->
        <script src="<?php echo base_url(); ?>js/jquery-2.2.4.min.js"></script>                  
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="<?php echo base_url(); ?>js/popper.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery-ui.min.js" type="text/javascript"></script>          
    </head>
    
    <body>
        
        <section class="loginComplete_wrap">
            <div class="loginContainer">
                <div class="loginInner">
                    <div class="loginMain">
                        <div class="side_info">
                            <div class="sideInfoInner">
                                <h1>Forgot Password?</h1>
                                <article>
                                    <p>The standard chunk of Lorem Ipsum used since the 1500s.</p>
                                </article>
                            </div>
                        </div>

                        <div class="loginForm_wrap">
                            <h3>Request New Password</h3>
                                                        
                            <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                            <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 

                            <?php echo form_open('', array('id' => 'formForgotPassword')); ?>

                                    <span>
                                        <input type="text" name="username" id="username" class="loginInput" placeholder="Enter Registered Username" required="">
                                    </span>
                            
                                    <span>
                                        <input type="submit" name="submit" id="submit" value="Submit"/>
                                    </span>                                               

                            <?php echo form_close(); ?>   
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </body>
</html>   
