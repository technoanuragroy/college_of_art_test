<html>
    <head>
        <title><?php echo SITE_NAME; ?>: User Registration</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <!-- CSS -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type='text/css'>
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet" type='text/css'>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/jquery-ui.min.css" rel="stylesheet" type='text/css'>
        <link href="<?php echo base_url(); ?>css/validationEngine.jquery.css" rel="stylesheet" type='text/css'>

        <!-- JS -->
        <script src="<?php echo base_url(); ?>js/jquery-2.2.4.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/popper.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery.validationEngine-en.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery.validationEngine.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery.datetimepicker" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#dob').datepicker({
                    onSelect: function (value, ui) {
                    var today = new Date();
                    age = today.getFullYear() - ui.selectedYear;
                    $('#age').val(age);
                        },
                    changeMonth: true,
                    changeYear: true,
                    //yearRange: '1950:2013',
                    yearRange: "-100:+0",
                    dateFormat: 'yy-mm-dd',
                });

            jQuery("#formRegistration").validationEngine({
                });

                $(".exam_passed").on("keyup change paste blur", function() {
                    var exam_passed = $(this).val();

                    if(exam_passed != "") {

                        $(this).parent().parent().find("input").each(function(i,e) {
                            if(!$(this).hasClass("validate[required]")) {
                                $(this).addClass("validate[required]");
                            }
                        });
                    } else {

                        $(this).parent().parent().find("input").each(function(i,e) {
                            if($(this).hasClass("validate[required]")) {
                                $(this).removeClass("validate[required]");
                                $(this).validationEngine('hide');
                                $(this).val("");
                            }
                        });
                    }
                });
            });

            function changeGeneral($i) {

                if ($i=='1') {
                  $('#caste_certificate_xerox').hide();
                }
                 if ($i=='2') {
                  $('#caste_certificate_xerox').show();
                }
                 if ($i=='3') {
                  $('#caste_certificate_xerox').show();
                }
                 if ($i=='4') {
                  $('#caste_certificate_xerox').show();
                }
                 if ($i=='5') {
                  $('#caste_certificate_xerox').show();
                }

            }



            //////////////////////Image Type Checking/////////////////////////

            function fileChange_student(e,manage) {

                //document.getElementsByClassName("student_passport_photo").value = '';
                $('#result_'+manage).html("");
                //document.getElementById('input_'+manage).value = '';

                for (var i = 0; i < e.target.files.length; i++) {

                    var file = e.target.files[i];

                    if (file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/gif") {

                        var reader = new FileReader();
                        reader.onload = function (readerEvent) {
                            var image = new Image();
                            image.onload = function (imageEvent) {
                                //var max_size = 275;

                                if (manage=='passport') {

                                    var max_size = 500; // In Kilobytes
                                    var allow_w = 413;
                                    var allow_h = 531;

                                } else if ((manage=='document_one')||(manage=='document_two')||(manage=='document_three')||(manage=='document_four')) {

                                    var max_size = 2000;
                                    var allow_w = 1000;
                                    var allow_h = 1000;

                                } else if (manage=='age_verification') {

                                    var max_size = 2000;
                                    var allow_w = 1000;
                                    var allow_h = 1000;

                                } else if (manage=='caste_cf') {

                                    var max_size = 2000;
                                    var allow_w = 1000;
                                    var allow_h = 1000;

                                } else if (manage=='guardian_sign') {

                                    var max_size = 900;
                                    var allow_w = 437;
                                    var allow_h = 177;

                                } else if (manage=='candidate_sign') {

                                    var max_size = 900;
                                    var allow_w = 437;
                                    var allow_h = 177;

                                } else if (manage=='candidate_sign_full') {

                                    var max_size = 900;
                                    var allow_w = 437;
                                    var allow_h = 177;
                                }

                                var file_size = document.getElementById('student_'+manage).files[0].size; // In Bytes
                                var s = file_size / 1024; // In Kilobytes
                                // var w = image.width;
                                // var h = image.height;

                                //alert('size : '+s+' Kb width : '+w+' px height : '+h+' px');

                                if(s > max_size) {
                                    alert('File Size Is too Big');
                                    $('#result_'+manage).html("Please Choose A File Of Given Size");
                                    $('#student_'+manage).val("");
                                }
                                // else if (w > allow_w || h > allow_h) {
                                //     alert('File Dimensions Do Not Match');
                                //     $('#result_'+manage).html("Please Choose A File Of Given Dimensions");
                                //     $('#student_'+manage).val("");
                                // }

//                                else if (w > h) {
//                                    if (w > max_size) {
//                                        h *= max_size / w;
//                                        w = max_size;
//                                        alert('File Size Is too Big');
//                                        $('#result_'+manage).html("Please Enter Correct Image Format");
//                                        $('#student_'+manage).val("");
//                                    }
//                                } else {
//                                    if (h > max_size) {
//                                        w *= max_size / h;
//                                        h = max_size;
//                                        alert('File Size Is too Big');
//                                        $('#result_'+manage).html("Please Enter Correct Image Format");
//                                        $('#student_'+manage).val("");
//                                    }
//                                }
//                                var canvas = document.createElement('canvas');
//                                canvas.width = w;
//                                canvas.height = h;
//                                canvas.getContext('2d').drawImage(image, 0, 0, w, h);
//                                if (file.type == "image/jpeg") {
//                                    var dataURL = canvas.toDataURL("image/jpeg", 1.0);
//                                } else if (file.type == "image/png") {
//                                    var dataURL = canvas.toDataURL("image/png");
//                                } else if (file.type == "image/gif") {
//                                    var dataURL = canvas.toDataURL("image/gif");
//                                }
//
//                                document.getElementById('input_'+manage).value = dataURL;
                                //document.getElementsByClassName("student_passport_photo").value = dataURL;
                            }
                            image.src = readerEvent.target.result;
                        }
                        reader.readAsDataURL(file);
                    }
                }
            }


        </script>

    </head>


    <body>

        <!-- <section class="loginComplete_wrap">
            <div class="loginContainer">
                <div class="loginInner">
                    <div class="loginMain"> -->
                        <!-- <div class="side_info">
                            <div class="sideInfoInner">
                                <h1>Student Admissiion From</h1>
                                <article>
                                    <p>The standard chunk of Lorem Ipsum used since the 1500s.</p>
                                </article>
                            </div>
                        </div> -->

                        <div class="loginForm_wrap">
                            <h3>Student Admission Form</h3>


                            <?php echo form_open_multipart('', array('id' => 'formRegistration')); ?>

                                        <span>
                                            <label><?php echo $this->my_custom_functions->get_particular_field_value("tbl_session","session_name", 'and id="'.$session_details['id'].'"'); ?></label>
                                            <input type="hidden" name="session_id" id="session_id" value="<?php echo $session_details['id']; ?>" >
                                        </span>


                                       <span>
                                        <label>Select Course </label>
                                            <select name="course_id" id="course_id" class="loginInput validate[required]">
                                        <option value="">Select Course </option>
                                                <?php foreach ($all_course as $row) { ?>
                                                    <option value="<?php echo $row['id']; ?>" <?php echo set_select('course_id', $row['id'], False); ?> ><?php echo $row['course_name']; ?> </option>
                                                <?php } ?>
                                            </select>
                                    </span>

                                    <span>
                                            <input type="checkbox" name="rules_condition_first" id="rules_condition_first" class="validate[required]" value="1"<?php echo set_checkbox('rules_condition_first','1',False); ?> />I beg to present myself for admission to the College of Art & Design , Burdwan. I undertake that I shall abide by all the rules and regulations of the College and shall be responsible for the good conduct, attendance, punctual payment of College fees etc.
                                            <br>
                                            further  vouch for the correctness of the following details regarding myself:


                                        </span>

                                        <span>

                                    <span>
                                        <input type="text" name="student_name" id="student_name" class="loginInput validate[required]" placeholder="Name of the Candidate (in capital letters)" value="<?php echo set_value('student_name'); ?>" onkeyup="this.value = this.value.toUpperCase();" required="">
                                    </span>

                                    <span>
                                        <!-- <input type="text" name="father_or_mother_name" id="father_or_mother_name" class="loginInput validate[required]" placeholder="Father’s /Mother’s Name" value="<?php echo set_value('father_or_mother_name'); ?>" required=""> -->
                                        <?php echo form_input(array('type' => 'text','placeholder'=>"Father’s /Mother’s Name",'name'=> 'father_or_mother_name','id'=> 'father_or_mother_name','value'=> set_value('father_or_mother_name'),'class'=>'loginInput validate[required]')); ?>
                                    </span>
                                    <span>
                                        <input type="text" name="guardian_name" id="guardian_name" class="loginInput validate[required]" placeholder="Name of the Guardian" value="<?php echo set_value('guardian_name'); ?>" required="">
                                    </span>
                                    <span>
                                        <input type="text" name="perm_address" id="perm_address" class="loginInput validate[required]" placeholder="Permanent Address" value="<?php echo set_value('perm_address'); ?>" required="">
                                    </span>

                                    <span>
                                        <input type="email" name="email" id="email" class="loginInput validate[required]" value="<?php echo set_value('email'); ?>" placeholder="Email" required="" autocomplete="off">
                                    </span>

                                    <span>
                                        <input type="number" name="phone_no" id="phone_no" class="loginInput validate[required]" placeholder="Phone Number" value="<?php echo set_value('phone_no'); ?>" required="" autocomplete="off">
                                    </span>

                                    <span>
                                        <input type="text" name="corres_address" id="corres_address" class="loginInput" placeholder=" Address for correspondence" value="<?php echo set_value('corres_address'); ?>" required="" autocomplete="off">
                                    </span>

                                    <span>
                                        <input type="number" name="aadhar_no" id="aadhar_no" class="loginInput validate[required]" placeholder="Aadhar No" value="<?php echo set_value('aadhar_no'); ?>" required="">
                                    </span>

                                    <span>
                                        <input type="number" name="reg_mobile_no" id="reg_mobile_no" class="loginInput validate[required]" placeholder="Registered Mobile No" value="<?php echo set_value('reg_mobile_no'); ?>" required="" autocomplete="off">
                                    </span>

                                    <span>
                                        <label>D.O.B</label>
                                        <input type="text" name="dob" id="dob" class="loginInput" placeholder="Enter D.O.B" value="<?php echo set_value('dob'); ?>" required="" autocomplete="off">
                                    </span>

                                     <span>
                                            <label>Passport Photo</label>
                                            <input type="file" name="student_passport"  id="student_passport" class="loginInput validate[required]"  required="" accept="image/*" onchange="fileChange_student(event,'passport');">
<!--                                             <input type="hidden" name="input_passport" id="input_passport" class="input_passport" value="">-->
                                             <p id="result_passport"></p>
                                            <p>Height 4.5cm X Width 3.5cm Max file size - 500KB</p>
                                        </span>


                                    <span>
                                        <input type="number" name="age" id="age" class="loginInput validate[required]" placeholder="Age" required="" value="<?php echo set_value('age'); ?>" required="">
                                    </span>

                                     <span>

                                        <select name="gender" id="gender" class="loginInput validate[required]" required="">
                                        <option value="">Select
                                        Sex</option>
                                                <?php
                                                $gender = $this->config->item('gender');
                                                foreach($gender as $key=>$gtype){?>
                                                <option value="<?php echo $key;?>" <?php echo set_select('gender',$key, False); ?> ><?php echo $gtype;?></option>
                                                <?php } ?>
                                            </select>
                                    </span>

                                     <span>

                                        <select name="marital_status" id="marital_status" class="loginInput validate[required]" required="">
                                        <option value="">Select
                                        Marital Status</option>
                                                <?php
                                                $marital_status = $this->config->item('marital_status');
                                                foreach($marital_status as $key=>$mtype){?>
                                                <option value="<?php echo $key;?>" <?php echo set_select('marital_status',$key, False); ?>><?php echo $mtype;?></option>
                                                <?php } ?>
                                            </select>
                                    </span>

                                    <span>
                                        <input type="text" name="nationality" id="nationality" class="loginInput" placeholder="Nationality" value="<?php echo set_value('nationality'); ?>" required="">
                                    </span>
                                    <span>
                                       <select name="caste" id="caste" class="loginInput" onchange="changeGeneral(value);" required="">
                                        <option value="">Select
                                        Caste</option>
                                                <?php
                                                $caste = $this->config->item('caste');
                                                foreach($caste as $key=>$cstype){?>
                                                <option value="<?php echo $key;?>" <?php echo set_select('caste',$key, False); ?>><?php echo $cstype;?></option>
                                                <?php } ?>
                                            </select>
                                    </span>


                    <div class="table_wrap">
                        <label>Educational qualification:</label>
                    <div class="table-responsive">
                        <!-- <table class="table table-default table-bordered table-striped table-hover"> -->
                        <table >
                            <thead>
                                <tr>
                                    <th>Examination passed</th>
                                    <th>Subject</th>
                                    <th>Year of passing</th>
                                    <th>Board/Council/ University</th>
                                    <th>Division obtained</th>
                                    <th>Original copies</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="text" name="exam_pass_one" id="exam_pass_one" class="validate[required]" value="<?php echo set_value('exam_pass_one'); ?>" required=""></td>
                                    <td><input type="text" name="subject_one" id="subject_one" class="validate[required]" value="<?php echo set_value('subject_one'); ?>" required=""></td>
                                    <td><input type="text" name="year_pass_one" id="year_pass_one" class="validate[required]" required="" value="<?php echo set_value('year_pass_one'); ?>"></td>
                                    <td><input type="text" name="board_one" id="board_one" class="validate[required]" value="<?php echo set_value('board_one'); ?>" required=""></td>
                                    <td><input type="text" name="div_obt_one" id="div_obt_one" class="validate[required]" value="<?php echo set_value('div_obt_one'); ?>" required=""></td>
                                    <td><input type="file" name="student_document_one" id="student_document_one" class="validate[required]"  required="" accept="image/*" onchange="fileChange_student(event,'document_one');">
<!--                                             <input type="hidden" name="input_document_one" id="input_document_one" class="input_document_one" value="">-->
                                             <p id="result_document_one"></p></td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="exam_pass_two" id="exam_pass_two" class="exam_passed" value="<?php echo set_value('exam_pass_two'); ?>"></td>
                                    <td><input type="text" name="subject_two" id="subject_two" value="<?php echo set_value('subject_two'); ?>"></td>
                                    <td><input type="text" name="year_pass_two" id="year_pass_two" value="<?php echo set_value('year_pass_two'); ?>"></td>
                                    <td><input type="text" name="board_two" id="board_two" value="<?php echo set_value('board_two'); ?>"></td>
                                    <td><input type="text" name="div_obt_two" id="div_obt_two" value="<?php echo set_value('div_obt_two'); ?>"></td>
                                    <td><input type="file" name="student_document_two" id="student_document_two" class=""  accept="image/*" onchange="fileChange_student(event,'document_two');">
<!--                                             <input type="hidden" name="input_document_two" id="input_document_two" class="input_document_two" value="">-->
                                             <p id="result_document_two"></p></td>

                                </tr>
                                <tr>
                                    <td><input type="text" name="exam_pass_three" id="exam_pass_three" class="exam_passed" value="<?php echo set_value('exam_pass_three'); ?>"></td>
                                    <td><input type="text" name="subject_three" id="subject_three" value="<?php echo set_value('subject_three'); ?>"></td>
                                    <td><input type="text" name="year_pass_three" id="year_pass_three" value="<?php echo set_value('year_pass_three'); ?>"></td>
                                    <td><input type="text" name="board_three" id="board_three" value="<?php echo set_value('board_three'); ?>"></td>
                                    <td><input type="text" name="div_obt_three" id="div_obt_three" value="<?php echo set_value('div_obt_three'); ?>"></td>
                                    <td><input type="file" name="student_document_three" id="student_document_three" class=""  accept="image/*" onchange="fileChange_student(event,'document_three');">
<!--                                             <input type="hidden" name="input_document_three" id="input_document_three" class="input_document_three" value="">-->
                                             <p id="result_document_three"></p></td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="exam_pass_four" id="exam_pass_four" class="exam_passed" value="<?php echo set_value('div_obt_three'); ?>"></td>
                                    <td><input type="text" name="subject_four" id="subject_four" value="<?php echo set_value('div_obt_three'); ?>"></td>
                                    <td><input type="text" name="year_pass_four" id="year_pass_four" value="<?php echo set_value('div_obt_three'); ?>"></td>
                                    <td><input type="text" name="board_four" id="board_four" value="<?php echo set_value('div_obt_three'); ?>"></td>
                                    <td><input type="text" name="div_obt_four" id="div_obt_four" value="<?php echo set_value('div_obt_three'); ?>"></td>
                                    <td><input type="file" name="student_document_four" id="student_document_four" class=""  accept="image/*" onchange="fileChange_student(event,'document_four');">
<!--                                             <input type="hidden" name="input_document_four" id="input_document_four" class="input_document_four" value="">-->
                                             <p id="result_document_four"></p></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                </div>

                                    <!-- <span>
                                    <label>Original copies of mark sheets</label>
                                            <input type="file" name="mark_sheets_xerox" id="mark_sheets_xerox" class="loginInput" value="<?php //echo set_value('mark_sheets_xerox'); ?>">
                                            <p>Max file size - 1MB</p>
                                    </span> -->

                                    <span>
                                    <label>Original copies of age verification</label>
                                            <input type="file" name="student_age_verification" id="student_age_verification" required="" class="loginInput" accept="image/*" onchange="fileChange_student(event,'age_verification');">
<!--                                             <input type="hidden" name="input_age_verification" id="input_age_verification" class="student_age_verification" value="">-->
                                             <p id="result_age_verification"></p>
                                            <p>Max file size - 1MB</p>
                                        </span>

                                    <span id="caste_certificate_xerox">
                                    <label>Original copies of SC/ST/OBC/Handicapped Certificate</label>
                                            <input type="file" name="student_caste_cf"
                                            id="student_caste_cf" class="loginInput"  accept="image/*" onchange="fileChange_student(event,'caste_cf');">
<!--                                             <input type="hidden" name="input_caste_cf" id="input_caste_cf" class="input_caste_cf" value="">-->
                                            <p id="result_caste_cf"></p>
                                            <p>Max file size - 1MB</p>
                                    </span>

                                  <span>
                                            <input type="text" name="local_address" id="local_address" class="loginInput" placeholder="Local address of the candidate" required="" value="<?php echo set_value('local_address'); ?>">
                                        </span>
                                    <span>
                                            <input type="text" name="local_gu_name_adds" id="local_gu_name_adds" class="loginInput" placeholder=" Name & Address of the local guardian:" required="" value="<?php echo set_value('local_gu_name_adds'); ?>">
                                        </span>
                                    <span>
                                            <input type="checkbox" name="rules_condition_one" id="rules_condition_one" class="validate[required]" value="1" <?php echo set_checkbox('rules_condition_one','1',False); ?> >I / We aware the money once deposited in any head or as fees in College account will not return back at any stage. I /We shall abide by all the rules and regulations of the College before submitting the Application Form.

                                        </span>

                                        <span>
                                    <label>Countersignature of the Guardian</label>
                                            <input type="file" name="student_guardian_sign" id="student_guardian_sign" class="loginInput" required="" value="<?php echo set_value('guardian_signature'); ?>"accept="image/*" onchange="fileChange_student(event,'guardian_sign');">
<!--                                             <input type="hidden" name="input_guardian_sign" id="input_guardian_sign" class="input_guardian_sign" value="">-->
                                             <p id="result_guardian_sign"></p>
                                            <p>Height 1.5cm X Width 3.5cm Max file size - 200KB</p>
                                        </span>

                                    <span>
                                    <label>Signature of Candidate</label>
                                            <input type="file" name="student_candidate_sign" id="student_candidate_sign" required="" class="loginInput" accept="image/*" onchange="fileChange_student(event,'candidate_sign');">
<!--                                             <input type="hidden" name="input_candidate_sign" id="input_candidate_sign" class="input_candidate_sign" value="">-->
                                             <p id="result_candidate_sign"></p><p>Height 1.5cm X Width 3.5cm Max file size - 200KB</p>
                                        </span>
                                    <label>
                                    <input type="checkbox" name="rules_condition_two" id="rules_condition_two" class="validate[required]" value="1" <?php echo set_checkbox('rules_condition_two','1',False); ?> >I do hereby agree (if admitted) to abide by the rules and regulations of the College as are in force and  undertake  that so long I shall remain a student of this College, I will do nothing either inside or outside the College that will interfere its orderliness and discipline. Any infringement of the aforesaid conditions by me will entail to accept any punishment prescribed by the authority.</label>
                                    <label>Date:<?php echo date("d-m-Y");?></label>
                                    <span>
                                    <label>Signature of Candidate in full
                                    </label>
                                            <input type="file" name="student_candidate_sign_full" id="student_candidate_sign_full" class="loginInput" required=""  accept="image/*" onchange="fileChange_student(event,'candidate_sign_full');">
<!--                                             <input type="hidden" name="input_candidate_sign_full" id="student_candidate_sign_full" class="input_candidate_sign_full" value="">-->
                                             <p id="result_candidate_sign_full"></p>
                                            <p>Height 1.5cm X Width 3.5cm Max file size - 200KB</p>
                                        </span>
                                    <span>

                                        <input type="submit" name="submit" id="submit" value="Submit">
                                    </span>

                            <?php echo form_close(); ?>


                        </div>
                           <span>

                                        <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                            <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>
                            <?php if (isset($img_upload_message)) {
                                echo '<div class="e_message">'.$img_upload_message.'</div>';
                            }
                            ?>
                            </span>
                    <!-- </div>
                </div>
            </div>
        </section> -->

    </body>


</html>
