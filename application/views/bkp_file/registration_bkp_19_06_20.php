<html>
    <head>
        <title><?php echo SITE_NAME; ?>: User Registration</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        
        <!-- CSS -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type='text/css'>   
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet" type='text/css'>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">  
        <link href="<?php echo base_url(); ?>css/jquery-ui.min.css" rel="stylesheet" type='text/css'>          
                                                                         
        <!-- JS -->
        <script src="<?php echo base_url(); ?>js/jquery-2.2.4.min.js"></script>                  
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="<?php echo base_url(); ?>js/popper.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery-ui.min.js" type="text/javascript"></script>    
        
        <script type="text/javascript">     
            $(document).ready(function() {

            //     $("#submit").hide();

            //     // Validate username according to user settings set by admin
            //     $("#username").on("keyup change paste keydown", function (event) {

            //         var key = event.keyCode || event.which;

            //         // Validate the input if space is given
            //         if(key == 32) {          
            //             return false;
            //         }

            //         // Remove space if a string with space is pasted 
            //         $("#username").val($("#username").val().replace(/ /g, ""));

            //         var username = $("#username").val();

            //         if (username != "") {

            //             var username_type = $("#username_type").val();
            //             var valid = '0';

            //             // If the username type is email
            //             if(username_type == '<?php echo USERNAME_TYPE_EMAIL; ?>') {
            //                 if (validateEmail(username)) {                         
            //                      var valid = '1';               
            //                 } 
            //             }

            //             // If the username type is mobile
            //             if(username_type == '<?php echo USERNAME_TYPE_MOBILE; ?>') {
            //                 if($.isNumeric(username) && username.length == 10) {
            //                     var valid = '1';
            //                 }
            //             }

            //             if(valid == '1') {       
            //                 $.ajax({
            //                     type: "POST",
            //                     url: "<?php echo base_url(); ?>main/validateUsername",
            //                     data: "username=" + username,
            //                     success: function (msg) { 
            //                         if(msg != "") {                                                    
            //                             $("#username_error").text(msg); 
            //                             $("#submit").hide();
            //                         } else {
            //                             $("#username_error").text("");  
            //                             $("#submit").show();
            //                         }
            //                     }
            //                 });
            //             } else {

            //                 $("#username_error").text('Please put a valid username.'); 
            //                 $("#submit").hide();
            //             }
            //         } else {                    

            //             $("#submit").hide();
            //         }
            //     });
            // });

            // Validate a string as a phone number
            function validateEmail(email) {

                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }                    
        </script>
    </head>
    
    <body>

        <section class="loginComplete_wrap">
            <div class="loginContainer">
                <div class="loginInner">
                    <div class="loginMain">
                        <div class="side_info">
                            <div class="sideInfoInner">
                                <h1>Student Registration</h1>
                                <article>
                                    <p>The standard chunk of Lorem Ipsum used since the 1500s.</p>
                                </article>
                            </div>
                        </div>

                        <div class="loginForm_wrap">
                            <h3>Register Yourself</h3>
                                                                                       
                            <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                            <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>  
                            
                            <?php echo form_open('/Main/student_registration', array('id' => 'formRegistration')); ?>

                                    <span>
                                        <input type="text" name="name" id="name" class="loginInput" placeholder="Name" required="">
                                    </span>

                                    <span>
                                        <input type="email" name="email" id="email" class="loginInput" placeholder="Email" required="">
                                    </span>

                                    <span>
                                        <input type="number" name="phone" id="phone" class="loginInput" placeholder="Phone Number" required="">
                                    </span> 

                                    <span>
                                        <input type="text" name="username" id="username" class="loginInput" placeholder="Username <?php echo $user_settings['username_label']; ?>" required="">
                                        <input type="hidden" name="username_type" id="username_type" value="<?php echo $user_settings['username']; ?>">
                                        <div id="username_error"></div>
                                    </span>

                                    <span>
                                        <input type="password" name="password" id="password" class="loginInput" placeholder="Password" required="">
                                    </span>

                                    <span>
                                        <input type="password" name="confirm_password" id="confirm_password" class="loginInput" placeholder="Confirm Password" required=""> 
                                    </span>                                                                                               

                                    <span>
                                        <input type="submit" name="submit" id="submit" value="Register">
                                    </span>                                    

                            <?php echo form_close(); ?>                            
                                    
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </body>
</html>                             
















































































