<?php $this->load->view("include/header"); ?>

    <div class="completeWrap_inner">
      <div class="form_wrap">

          <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
          <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>
        <div class="completeWrap_container">
          <?php $payment_status = $this->my_custom_functions->get_particular_field_value("tbl_student_payment_status","admission_from_payment", 'and student_id="'.$this->session->userdata("user_id").'"');
                $from_id = $this->my_custom_functions->get_particular_field_value("tbl_student_from","id", 'and student_id="'.$this->session->userdata("user_id").'"');
           if ($payment_status==0) { ?>


            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Collect Admission Form</h2>
                <p>Note: To apply for admission online, you need to pay <img src="http://i.stack.imgur.com/nGbfO.png" width="8" height="15"> 400/- to purchase the form. Once the amount is credited into our account the admission form will be available to you. This may take 1-2 working days.<br> Click on the pay button to pay online.</p>

                <a href="https://www.onlinesbi.com/sbicollect/icollecthome.htm" target="_blank" class="c-link" title='Click on the Pay button to pay.'>Pay</a>

            </div>
          <?php }else if (($payment_status==1) AND ($from_id=='')) {  ?>

            <div class="completeWrap_inner_main">
                <h2 class="heading_02"><?php echo $title;?></h2>
                <!-- <p>Note: Click on the apply online button to fill up the admission form and submit online.</p> -->
                <p><?php echo $note;?></p>
                <a href="<?php echo base_url(); ?>user/studentFromRegister" class="c-link" title='Fill up the admission from.'>Apply Online</a>

            </div>
          <?php }else if (($payment_status==1) AND ($from_id!='')) {  ?>

              <div class="completeWrap_inner_main">
                  <h2 class="heading_02"><?php echo $title;?></h2>
                  <!-- <p>Note: Click on the apply online button to fill up the admission form and submit online.</p> -->
                  <p><?php echo $note;?></p>
                  <!-- <a href="<?php echo base_url(); ?>user/studentFromRegister" class="c-link" title='Fill up the admission from.'>Apply Online</a> -->
                  <a href="<?php echo base_url(); ?>user/editStudentRegisterPortal" class="c-link" title='Fill up the admission from.'>Edit Application Form</a>
              </div>
              <?php } ?>
            </div>
            </div>
        </div>
    </div>

<?php $this->load->view("include/footer"); ?>
