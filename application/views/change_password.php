<?php $this->load->view("include/header"); ?>
                
    <div class="completeWrap_inner">
        <div class="completeWrap_container">
            <div class="completeWrap_inner_main">
                <h2 class="heading_02">Change Password</h2>
                
                <div class="form_wrap">

                    <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>                            
                    <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?> 
                    
                    <div class="row">

                        <?php echo form_open('', array('id' => 'formChangePassword')); ?>

                                <ul class="input_listing">
                                    <li class="full_width_li">
                                        <span>
                                            <input type="password" name="old_password" id="old_password" placeholder="Old Password" required="">                    
                                        </span>
                                    </li> 

                                    <li class="full_width_li">
                                        <span>
                                            <input type="password" name="new_password" id="new_password" placeholder="New Password" required="">
                                        </span>
                                    </li> 

                                    <li class="full_width_li">
                                        <span>
                                            <input type="password" name="confirm_new_password" id="confirm_new_password" placeholder="Confirm New Password" required="">
                                        </span>
                                    </li>

                                    <li class="full_width_li">	                                         
                                        <input type="submit" name="submit" id="submit" class="submit_btn" value="Change Password">                                                                                                  
                                    </li>
                                </ul> 

                        <?php echo form_close(); ?>        
                        
                    </div>
                </div>    
            </div>
        </div>    
    </div>	                                                 
                
<?php $this->load->view("include/footer"); ?>