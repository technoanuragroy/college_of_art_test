<html>
    <head>
        <title><?php echo SITE_NAME; ?>: User Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <!-- CSS -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet" type='text/css'>
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet" type='text/css'>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/jquery-ui.min.css" rel="stylesheet" type='text/css'>

        <!-- JS -->
        <script src="<?php echo base_url(); ?>js/jquery-2.2.4.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/popper.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>js/jquery-ui.min.js" type="text/javascript"></script>
    </head>

    <script type="text/javascript" >

           $(document).ready(function() {
           $("#otp").keyup(function(){
           //$("#otp").blur(function(){
          var user_id=$("#user_id").val();
          var otp=$("#otp").val();
          $.ajax({
          url: "<?php echo base_url();?>Main/checkOtpExists", // Url to which the request is send
          type: "POST",             // Type of request to be send, called as method
          data:{user_id:user_id,otp:otp}, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          //contentType: false,       // The content type used when sending data to the server.
          success: function(data)   // A function to be called if request succeeds
          {
          $("#otp_msg").html(data);
          }
          });
           });
           });
         //}
</script>

    <body>

        <section class="loginComplete_wrap">
            <div class="loginContainer">
                <div class="loginInner">
                    <div class="loginMain">
                        <div class="side_info">
                            <div class="sideInfoInner">
                              <image src="<?php echo base_url(); ?>images/college/Logo.png">
                                <h1>OTP Verification</h1>
                                
                            </div>
                        </div>

                        <div class="loginForm_wrap">
                            <h3>Enter the OTP</h3>

                            <!-- <h5>Type OTP:<?php echo $this->uri->segment(4);?></h5> -->

                            <?php if($this->session->flashdata("e_message")) { echo '<div class="e_message">'.$this->session->flashdata("e_message").'</div>'; } ?>
                            <?php if($this->session->flashdata("s_message")) { echo '<div class="s_message">'.$this->session->flashdata("s_message").'</div>'; } ?>

                            <?php echo form_open('', array('id' => 'formLogin')); ?>

                                    <span>
                                        <input type="text" name="otp" id="otp" class="loginInput" placeholder="Enter The OTP" required="" autocomplete="off">
                                        <input type="hidden" name="user_id" id="user_id" class="loginInput" value="<?php echo $user_id; ?> ">
                                    </span>

                                    <span id="otp_msg" ></span>

                                    <span>
                                        <input type="submit" name="submit" id="submit" value="submit"/>

                                    </span>


                            <?php echo form_close(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </body>
</html>
